unit SDLForm;

{*********************************************************************
             SDLForm -  30.04.2003.

Author:      Stojkovic Igor
Contact:     jimyiigor@ptt.yu  (please report any bugs)
             stojkovic@galeb.etf.bg.ac.yu
Description: TSDLForm - container for other controls.

You may freely use this source code for your own projects.
If you have any suggestions or you made some modifications
please inform me.

What's new:
 - Added Movable property to determine if form can be moved
 - Used AlwaysUp property of TSDLStdControl to draw form up
   even when it is pressed - except it has an image
**********************************************************************}

interface

uses SDL, SDLUtils, SDLGUI, SDLDraw;

type
  TSDLForm = class(TSDLStdControl)
  private
    FDraging : Boolean;
    FOldPos : TPoint;
    FCaption: string;
    FMovable: Boolean;
    FModal: Boolean;
    FTitleBarHeight: Word;
  protected
    procedure SetZ(const Value: Integer); override;
    procedure DoDraw; override;
    procedure MouseMove(Modifier: TSDLMod;AX,AY : Word); override;
    procedure MouseUp(Button: Integer;Modifier: TSDLMod;AX,AY: Integer); override;
    procedure MouseDown(Button: Integer;Modifier: TSDLMod;AX,AY: Integer); override;
    function LoseFocus: Boolean; override;
  public
    constructor Create(AParent: TSDLObject); override;
    procedure Close;
    function SetFocus: Boolean; override;
  published
    property Modal: Boolean read FModal write FModal;
    property TitleBarHeight: Word read FTitleBarHeight write FTitleBarHeight;
    property Movable: Boolean read FMovable write FMovable;
    property Caption: string read FCaption write FCaption;
  end;

implementation

var FormCount : Byte;

{ TSDLForm }

procedure TSDLForm.Close;
var i,t : Integer;
    tc : TSDLImageObject;
begin
  i := Gui.IndexOfObject(Self);
  t := Gui.Count-1;
  tc := nil;
  while (i<t)and not(tc is TSDLForm) do
  begin
    Inc(i);
    tc := Gui.Objects[i];
  end;
  if Assigned(tc)and(tc is TSDLForm) then
    tc.Z := tc.Z-1;
  Dec(FormCount);
  Kill;
end;

constructor TSDLForm.Create(AParent: TSDLObject);
begin
  Inc(FormCount);
  inherited;

  SetFocus;
  TitleBarHeight := 25;
  Movable := True;
  AlwaysUp := True;
end;

procedure TSDLForm.DoDraw;
var r : TSDL_Rect;
    c,ct : Cardinal;
begin
  inherited;
  if (Caption='')or(TitleBarHeight<5) then Exit;

  r := BoundsRect;
  r.h := TitleBarHeight;
  Font.TextRect(SDLScreen.Surface,r,FCaption);

  if Assigned(Image) then Exit;

  if HasFocus then c := FocusColor
  else if not Enabled then c := DisabledColor
  else c := Color;

  ct := LightColor(c,-40);
  c := LightColor(c,40);
  SDLScreen.DrawOrthoLine(SDLScreen.Surface,True,X+BorderWidth,Y+TitleBarHeight,
                Width-BorderWidth shl 1,c);
  SDLScreen.DrawOrthoLine(SDLScreen.Surface,True,X+BorderWidth,Y+TitleBarHeight+1,
                Width-BorderWidth shl 1,ct);
end;

function TSDLForm.LoseFocus: Boolean;
begin
  if Modal then
    Result := False
  else
    Result := inherited LoseFocus;
end;

procedure TSDLForm.MouseDown(Button: Integer;Modifier: TSDLMod;AX,AY: Integer);
var r : TSDL_Rect;
begin
  inherited;
  if (Button<>1)or not Movable then Exit;
  r := BoundsRect;
  r.h := TitleBarHeight;
  FOldPos := SDLPoint(AX,AY);
  FDraging := PointInRect(FOldPos,r);
end;

procedure TSDLForm.MouseMove(Modifier: TSDLMod; AX, AY: Word);
begin
  inherited;
  if Designing then Exit;
  if FDraging then
  begin
    X := X+(AX-FOldPos.X);
    Y := Y+(AY-FOldPos.Y);
    FOldPos.X := AX;
    FOldPos.Y := AY;
    if X+Width<5 then
      X := 5-Width;
    if Y+Height<5 then
      Y := 5-Height;
  end;
end;

procedure TSDLForm.MouseUp(Button: Integer;Modifier: TSDLMod; AX,AY: Integer);
begin
  inherited;
  FDraging := False;
end;

function TSDLForm.SetFocus: Boolean;
begin
  Result := inherited SetFocus;
  if Result then
    Z := FormCount;
end;

procedure TSDLForm.SetZ(const Value: Integer);
var i,t,v : Integer;
    tc : TSDLImageObject;
begin
  v := Value;
  if v>=FormCount then
    if HasFocus then
      v := FormCount
    else
      v := FormCount-1;

  i := Gui.IndexOfObject(Self);
  t := Gui.Count-1;
  tc := nil;
  while (i<t)and not(tc is TSDLForm) do
  begin
    Inc(i);
    tc := Gui.Objects[i];
  end;
  if Assigned(tc)and(tc is TSDLForm) then
    tc.Z := tc.Z-1;
  inherited SetZ(v);
end;

end.
