unit SDLFormB;

interface

uses SDLGUI, SDLForm, SDLButton, SDLFont, SDLLabel, SDLDraw, SDLCheck, Classes,
     SDLEdit, SDLGauge, SDLScroll, SDLListBox, SDLMemo, SDLComboBox, SDLMenus;

type
  TSDLFormB = class(TSDLForm)
  public
    constructor Create(AParent: TSDLObject); override;
    function AddButton(const ACaption: string;AX,AY,W,H: Integer;
                       AImage: TSDLImage;AOnClick: TSDLMouseClickEvent): TSDLButton;
    function AddEdit(const AText: string;AX,AY,W,H: Integer;
                     AImage: TSDLImage): TSDLEdit;
    function AddLabel(const ACaption: string;AX,AY,W,H: Integer;
                      AImage: TSDLImage): TSDLLabel;
    function AddCheckBox(const ACaption: string;AX,AY,W,H: Integer;
                         AImage: TSDLImage): TSDLCheckBox;
    function AddRadioButton(const ACaption: string;AX,AY,W,H: Integer;
                       AImage: TSDLImage): TSDLRadioButton;
    function AddGauge(AShowText: Boolean;AX,AY,W,H: Integer;
                      AImage: TSDLImage): TSDLGauge;
    function AddScrollBar(AJumping: Boolean;AX,AY,W,H: Integer;
                          AImage: TSDLImage): TSDLScrollBar;
    function AddListBox(AX,AY,W,H: Integer;AImage,SBImage: TSDLImage): TSDLListBox;
    function AddMemo(AX,AY,W,H: Integer;AImage,SBImage: TSDLImage): TSDLMemo;
    function AddCombo(AX,AY,W,H: Integer;AImage,SBImage: TSDLImage): TSDLComboBox;
    function AddRGroup(const ACaption: string;AX,AY,W,H: Integer;
                      AImage,RImage: TSDLImage): TSDLRadioGroup;
    function AddMenu(AItems: TStrings): TSDLMenu;
    procedure AddPopupMenu(AItems: TStrings; ToControl: TSDLComponent);
  end;

implementation

{ TSDLFormB }

function TSDLFormB.AddButton(const ACaption: string; AX, AY, W, H: Integer;
  AImage: TSDLImage;AOnClick: TSDLMouseClickEvent): TSDLButton;
begin
  Result := TSDLButton.Create(Self);
  with Result do
  begin
    Font := GlobalFont;
    Caption := ACaption;
    X := AX;
    Y := AY;
    if Assigned(AImage) then Image := AImage;
    if W>0 then Width := W;
    if H>0 then Height := H;
    OnClick := AOnClick;
  end;
end;

function TSDLFormB.AddCheckBox(const ACaption: string; AX, AY, W,
  H: Integer; AImage: TSDLImage): TSDLCheckBox;
begin
  Result := TSDLCheckBox.Create(Self);
  with Result do
  begin
    Caption := ACaption;
    Font := GlobalFont;
    X := AX;
    Y := AY;
    if Assigned(AImage) then Image := AImage;
    if W>0 then Width := W;
    if H>0 then Height := H;
  end;
end;

function TSDLFormB.AddCombo(AX, AY, W, H: Integer; AImage,
  SBImage: TSDLImage): TSDLComboBox;
begin
  Result := TSDLComboBox.Create(Self);
  with Result do
  begin
    Font := GlobalFont;
    if Assigned(SBImage) then
      DropList.ScrollBar.Image := SBImage;
    X := AX;
    Y := AY;
    if Assigned(AImage) then Image := AImage;
    if W>0 then Width := W;
    if H>0 then Height := H;
  end;
end;

function TSDLFormB.AddEdit(const AText: string; AX, AY, W,
  H: Integer; AImage: TSDLImage): TSDLEdit;
begin
  Result := TSDLEdit.Create(Self);
  with Result do
  begin
    Font := GlobalFont;
    Text := AText;
    X := AX;
    Y := AY;
    if Assigned(AImage) then Image := AImage;
    if W>0 then Width := W;
    if H>0 then Height := H;
  end;
end;

function TSDLFormB.AddGauge(AShowText: Boolean;AX, AY, W, H: Integer;
  AImage: TSDLImage): TSDLGauge;
begin
  Result := TSDLGauge.Create(Self);
  with Result do
  begin
    Font := GlobalFont;
    X := AX;
    Y := AY;
    if Assigned(AImage) then Image := AImage;
    if W>0 then Width := W;
    if H>0 then Height := H;
    ShowText := AShowText;
    Progress := 50;
  end;
end;

function TSDLFormB.AddLabel(const ACaption: string; AX, AY, W, H: Integer;
  AImage: TSDLImage): TSDLLabel;
begin
  Result := TSDLLabel.Create(Self);
  with Result do
  begin
    Font := GlobalFont;
    Caption := ACaption;
    X := AX;
    Y := AY;
    if Assigned(AImage) then Image := AImage;
    if W>0 then Width := W;
    if H>0 then Height := H;
  end;
end;

function TSDLFormB.AddListBox(AX, AY, W, H: Integer; AImage,
  SBImage: TSDLImage): TSDLListBox;
begin
  Result := TSDLListBox.Create(Self);
  with Result do
  begin
    Font := GlobalFont;
    if Assigned(SBImage) then ScrollBar.Image := SBImage;
    X := AX;
    Y := AY;
    if Assigned(AImage) then Image := AImage;
    if W>0 then Width := W;
    if H>0 then Height := H;
  end;
end;

function TSDLFormB.AddMemo(AX, AY, W, H: Integer; AImage,
  SBImage: TSDLImage): TSDLMemo;
begin
  Result := TSDLMemo.Create(Self);
  with Result do
  begin
    if Assigned(SBImage) then
      ScrollBar.Image := SBImage;
    X := AX;
    Y := AY;
    if Assigned(AImage) then Image := AImage;
    if W>0 then Width := W;
    if H>0 then Height := H;
    Font := GlobalFont;
  end;
end;

function TSDLFormB.AddMenu(AItems: TStrings): TSDLMenu;
begin
  Result := TSDLMenu.Create(Self);
  with Result do
  begin
    Font := GlobalFont;
    AddItems(AItems);
  end;
end;

procedure TSDLFormB.AddPopupMenu(AItems: TStrings; ToControl: TSDLComponent);
begin
  TSDLPopupMenu.Create(ToControl);
  if Assigned(AItems) then
    TSDLPopupMenu(ToControl.PopupMenu).AddItems(AItems);
end;

function TSDLFormB.AddRadioButton(const ACaption: string; AX, AY, W,
  H: Integer; AImage: TSDLImage): TSDLRadioButton;
begin
  Result := TSDLRadioButton.Create(Self);
  with Result do
  begin
    Caption := ACaption;
    Font := GlobalFont;
    X := AX;
    Y := AY;
    if Assigned(AImage) then Image := AImage;
    if W>0 then Width := W;
    if H>0 then Height := H;
  end;
end;

function TSDLFormB.AddRGroup(const ACaption: string;AX, AY, W, H: Integer;
  AImage, RImage: TSDLImage): TSDLRadioGroup;
begin
  Result := TSDLRadioGroup.Create(Self);
  with Result do
  begin
    Font := GlobalFont;
    Caption := ACaption;
    X := AX;
    Y := AY;
    Width := W;
    Height := H;
    if Assigned(AImage) then Image := AImage;
    if Assigned(RImage) then RadioImage := RImage;
  end;
end;

function TSDLFormB.AddScrollBar(AJumping: Boolean; AX, AY, W,
  H: Integer; AImage: TSDLImage): TSDLScrollBar;
begin
  Result := TSDLScrollBar.Create(Self);
  with Result do
  begin
    X := AX;
    Y := AY;
    if Assigned(AImage) then Image := AImage;
    if W>0 then Width := W;
    if H>0 then Height := H;
    Jumping := AJumping;
  end;
end;

constructor TSDLFormB.Create(AParent: TSDLObject);
begin
  inherited;
  Font := GlobalFont;
end;

end.
