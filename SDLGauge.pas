unit SDLGauge;

{*********************************************************************
             SDLGauge -  30.04.2003.

Author:      Stojkovic Igor
Contact:     jimyiigor@ptt.yu  (please report any bugs)
Description: TSDLGauge.

You may freely use this source code for your own projects.
If you have any suggestions or you made some modifications
please inform me.
**********************************************************************}

interface

uses sdl, SDLGUI, SDLDraw;

type
  TSDLGauge = class(TSDLComponent)
  private
    FProgress: Integer;
    FMaxValue: Integer;
    FMinValue: Integer;
    FKind: TSDLBarKind;
    FShowText: Boolean;
    FBFSpace: Byte;
    FBackColor: Cardinal;
    FForeColor: Cardinal;
    FBorderColor: Cardinal;
    function SolveForY(AX,AZ: Integer): Integer;
    procedure SetMax(Value: Integer);
    procedure SetMin(Value: Integer);
    procedure SetProgress(Value: Integer);
    procedure SetKind(const Value: TSDLBarKind);
  protected
    procedure SetHeight(const Value: Integer); override;
    procedure SetWidth(const Value: Integer); override;
    procedure SetImage(Value: TSDLImage); override;
    procedure DoDraw; override;
  public
    constructor Create(AParent: TSDLObject); override;
    property Progress: Integer read FProgress write SetProgress;
    property MinValue: Integer read FMinValue write SetMin;
    property MaxValue: Integer read FMaxValue write SetMax;
    property Kind: TSDLBarKind read FKind write SetKind;
    property ShowText: Boolean read FShowText write FShowText;
    property BorderColor: Cardinal read FBorderColor write FBorderColor;
    property BackColor: Cardinal read FBackColor write FBackColor;
    property ForeColor: Cardinal read FForeColor write FForeColor;
    property BorderForeSpace: Byte read FBFSpace write FBFSpace;
  end;

implementation

uses SysUtils;

{ TSDLGauge }

constructor TSDLGauge.Create(AParent: TSDLObject);
begin
  inherited;
  BorderWidth := 1;
  BorderForeSpace := 2;
  MaxValue := 100;
  ShowText := True;
  BorderColor := $FFFFFF;
  BackColor := $505050;
  ForeColor := $B4C3DC;
end;

procedure TSDLGauge.DoDraw;
var P : Integer;
  procedure Resize(var Rect: TSDL_Rect);
  var w : Integer;
  begin
    if FKind=sbHorizontal then
    begin
      w := P*Rect.w div 100;
      Rect.w := w;
    end
    else
    begin
      w := P*Rect.h div 100;
      Rect.y := Rect.y+Rect.h-w;
    end;
  end;

var dest,source: TSDL_Rect;
    S: string;
begin
  inherited;

  P := SolveForY(FProgress-FMinValue,FMaxValue-FMinValue);
  dest := BoundsRect;

  if Assigned(Image) then
  begin
    InflateRect(dest,-BorderWidth,-BorderWidth);
    source := Image.PatternRects[AnimCount+Trunc(AnimPos)];
    InflateRect(source,-BorderWidth,-BorderWidth);
    Resize(source);
    Resize(Dest);
    Image.DrawRect(SDLScreen.Surface,dest.x,dest.y,source);
  end
  else
    with SDLScreen do
    begin
      FillRect(Surface,dest,BorderColor);
      InflateRect(Dest,-BorderWidth,-BorderWidth);
      FillRect(Surface,dest,BackColor);
      InflateRect(dest,-BorderForeSpace,-BorderForeSpace);
      Resize(dest);
      FillRect(Surface,dest,ForeColor);
    end;

  if ShowText then
  begin
    dest := BoundsRect;
    S := Format('%d%%',[P]);
    Font.TextRect(SDLScreen.Surface,dest,S);
  end;
end;

procedure TSDLGauge.SetHeight(const Value: Integer);
begin
  if Assigned(Image)and(FKind=sbHorizontal) then Exit;
  inherited;
end;

procedure TSDLGauge.SetImage(Value: TSDLImage);
begin
  inherited;
  if Width>Height then FKind := sbHorizontal
  else FKind := sbVertical;
  AnimCount := AnimCount div 2;
end;

procedure TSDLGauge.SetKind(const Value: TSDLBarKind);
begin
  if (Value=FKind)or Assigned(Image) then Exit;
  FKind := Value;
end;

procedure TSDLGauge.SetMax(Value: Integer);
begin
  if Value=FMaxValue then Exit;
  if Value < FMinValue then
    Value := FMinValue+1;
  FMaxValue := Value;
  if FProgress > Value then FProgress := Value;
end;

procedure TSDLGauge.SetMin(Value: Integer);
begin
  if Value > FMaxValue then
    Value := FMaxValue-1;
  FMinValue := Value;
  if FProgress < Value then FProgress := Value;
end;

procedure TSDLGauge.SetProgress(Value: Integer);
begin
  if Value < FMinValue then
    Value := FMinValue
  else if Value > FMaxValue then
    Value := FMaxValue;
  FProgress := Value;
end;

procedure TSDLGauge.SetWidth(const Value: Integer);
begin
  if Assigned(Image)and(FKind=sbVertical) then Exit;
  inherited;
end;

function TSDLGauge.SolveForY(AX, AZ: Integer): Integer;
begin
  if AZ = 0 then Result := 0
  else Result := AX * 100 div AZ;
end;

end.
