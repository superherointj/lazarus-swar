unit SDLGUI;

interface

USES
   SDLUtils, Classes, SDL, SDL_TTF, SDLClasses;

CONST
   CHARSET = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ`1234567890-=~!@#$%^&*()_+[]\{}|;'':",./<>? ';
   LOWCASE = 'abcdefghijklmnopqrstuvwxyz`1234567890-=[]\;'',./ ';
   UPCASE = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ~!@#$%^&*()_+{}|:"<>? ';
   CHARCOUNT = LENGTH(CHARSET);

TYPE
   // Forward class definitions
   TSDLButton = CLASS;
   TSDLControl = CLASS;
   TSDLCustomButton = CLASS;
   TSDLEdit = CLASS;
   TSDLRasterFont = CLASS;
   TSDLForm = CLASS;
   TSDLLabel = CLASS;
   TSDLListbox = CLASS;
   TSDLScrollbutton = CLASS;
   TSDLGUI = CLASS;
   TSDLRadioButton = CLASS;
   TSDLTTFFont = CLASS;

   // "Class of" definitions
   TSDLControlClass = CLASS OF TSDLControl;
   TSDLFormClass = CLASS OF TSDLForm;

   // Typed-value definitions
   TScrollButtonPosition = (sbTop, sbBottom);
   TButtonState = (bsUp, bsDown);

   // Event definitions
   TSDLNotifyEvent = PROCEDURE( Sender : TSDLControl ) OF OBJECT;
   TSDLMouseEvent = PROCEDURE( Sender : TSDLControl; X, Y, Button : INTEGER ) OF OBJECT;

   // --------------------------------------------------------------------------
   // Class :
   //    TSDLSkin
   // Description :
   //    Contains the skin image and provides routines to draw each of the
   //    widgets to the supplied surface.  This is the "meat" of the display
   //    routines for the GUI.
   // Modification History :
   //    Author         Date        Modification
   //    Todd Lang      12-Nov-2001 Created
   // --------------------------------------------------------------------------
   TSDLSkin = CLASS( TCollectionItem )
   PRIVATE
      FImage : TSDLImage;
      FName : STRING;
      FFileName : STRING;
      PROCEDURE DrawRepeating( Target : PSDL_Surface; X, Y, W, H : INTEGER; LeftRect, CenterRect, RightRect : PSDL_RECT; OverrideHeight : INTEGER = -1 );
   PUBLIC
      CONSTRUCTOR Create( Collection : TCollection ); OVERRIDE;
      DESTRUCTOR Destroy; OVERRIDE;
      PROCEDURE Load( Name, Filename : STRING );
      PROCEDURE DrawMouse( Target : PSDL_Surface; X, Y : INTEGER );
      PROCEDURE DrawButton( Target : PSDL_Surface; X, Y, W, H : INTEGER; Up : BOOLEAN );
      PROCEDURE DrawArrow( Target : PSDL_Surface; X, Y : INTEGER; Up : BOOLEAN );
      PROCEDURE DrawScrollTrack( Target : PSDL_Surface; X, Y, H : INTEGER );
      PROCEDURE DrawScrollThumb( Target : PSDL_Surface; X, Y : INTEGER );
      PROCEDURE DrawSquareButton( Target : PSDL_Surface; X, Y, W, H : INTEGER; Up : BOOLEAN );
      PROCEDURE DrawEdit( Target : PSDL_Surface; X, Y, W, H : INTEGER );
      PROCEDURE DrawPanel( Target : PSDL_Surface; X, Y, W, H : INTEGER );
      PROCEDURE DrawBitButton( Target : PSDL_Surface; X, Y, W, H : INTEGER; Up : BOOLEAN );
      PROCEDURE DrawSelectionBar( Target : PSDL_Surface; X, Y, W, H : INTEGER );
      PROCEDURE DrawRadioButton( Target : PSDL_Surface; X, Y : INTEGER; Checked : BOOLEAN );
      PROPERTY Name : STRING READ FName WRITE FName;
   END;

   // --------------------------------------------------------------------------
   // Class :
   //    TSDLSkins
   // Description :
   //    Maintains a list of skins.  Theoretically, you could use many different
   //    skins your application.  This has, however, never been tested.
   // Modification History :
   //    Author         Date        Modification
   //    Todd Lang      12-Nov-2001 Created
   // --------------------------------------------------------------------------
   TSDLSkins = CLASS( TCollection )
   PRIVATE
      FUNCTION GetItem( Index : INTEGER ) : TSDLSkin;
      PROCEDURE SetItem( Index : INTEGER; CONST Value : TSDLSkin );
   PUBLIC
      FUNCTION Add : TSDLSkin;
      FUNCTION Insert( Index : INTEGER ) : TSDLSkin;
      FUNCTION ByName( Name : STRING ) : TSDLSkin;
      PROPERTY Items[ Index : INTEGER ] : TSDLSkin READ GetItem WRITE SetItem; DEFAULT;
   END;

   TSDLFont = CLASS( TCollectionItem )
   PRIVATE
      FName : STRING;
   PROTECTED
      FUNCTION GetSize : INTEGER; VIRTUAL; ABSTRACT;
   PUBLIC
      PROCEDURE TextOut( Target : PSDL_Surface; X, Y : INTEGER; Text : STRING ); VIRTUAL; ABSTRACT;
      FUNCTION TextWidth( Value : STRING ) : INTEGER; VIRTUAL; ABSTRACT;
      FUNCTION TextHeight( Value : STRING ) : INTEGER; VIRTUAL; ABSTRACT;
      PROPERTY Name : STRING READ FName WRITE FName;
      PROPERTY Size : INTEGER READ GetSize;
   END;

   // --------------------------------------------------------------------------
   // Class :
   //    TSDLFont
   // Description :
   //    Handles a variable-spaced bitmapped font.  I used this instead of
   //    the other font offerings (SDL_Monofont, for example) as it has some
   //    problems handling various extreme conditions.
   //    With the release of SDL_TTF, I will look in to switching this to be
   //    based on that instead.
   // Modification History :
   //    Author         Date        Modification
   //    Todd Lang      01-Dec-2001 Created
   // --------------------------------------------------------------------------
   TSDLRasterFont = CLASS( TSDLFont )
   PRIVATE
      FImage : TSDLImage;
      FCharWidth : ARRAY[ 0 .. CHARCOUNT-1 ] OF BYTE;
      FCharPos : ARRAY[ 0 .. CHARCOUNT-1 ] OF INTEGER;
      FHeight : BYTE;
   PROTECTED
      FUNCTION GetSize : INTEGER; OVERRIDE;
   PUBLIC
      CONSTRUCTOR Create( Collection : TCollection ); OVERRIDE;
      DESTRUCTOR Destroy; OVERRIDE;
      PROCEDURE Load( FontName, ImageFile, KerningFile : STRING );

      FUNCTION TextWidth( Text : STRING ) : INTEGER; OVERRIDE;
      FUNCTION TextHeight( Text : STRING ) : INTEGER; OVERRIDE;
      PROCEDURE TextOut( Target : PSDL_Surface; X, Y : INTEGER; Text : STRING ); OVERRIDE;
   END;

   TSDLTTFFont = CLASS( TSDLFont )
   PRIVATE
      FSize : INTEGER;
      FFont : PTTF_Font;
      FColour : TSDL_Color;
   PROTECTED
      FUNCTION GetSize : INTEGER; OVERRIDE;
   PUBLIC
      PROCEDURE Load( FontName, FileName : STRING; PointSize : INTEGER );
      PROPERTY Colour : TSDL_Color READ FColour WRITE FColour;

      FUNCTION TextWidth( Text : STRING ) : INTEGER; OVERRIDE;
      FUNCTION TextHeight( Text : STRING ) : INTEGER; OVERRIDE;
      PROCEDURE TextOut( Target : PSDL_Surface; X, Y : INTEGER; Text : STRING ); OVERRIDE;
   END;

   // --------------------------------------------------------------------------
   // Class :
   //    TSDLFonts
   // Description :
   //    Maintains a list of fonts, by supplied name.
   // Modification History :
   //    Author         Date        Modification
   //    Todd Lang      01-Dec-2001 Created
   // --------------------------------------------------------------------------
   TSDLFonts = CLASS( TCollection )
   PRIVATE
      FUNCTION GetItem( Index : INTEGER ) : TSDLFont;
      PROCEDURE SetItem( Index : INTEGER; CONST Value : TSDLFont );
   PUBLIC
      FUNCTION AddTTFFont : TSDLTTFFont;
      FUNCTION AddRasterFont : TSDLRasterFont;
      FUNCTION Insert( Index : INTEGER ) : TSDLFont;
      FUNCTION ByName( Name : STRING ) : TSDLFont;
      PROPERTY Items[ Index : INTEGER ] : TSDLFont READ GetItem WRITE SetItem; DEFAULT;
   END;

   // --------------------------------------------------------------------------
   // Class :
   //    TSDLControl
   // Description :
   //    The most base level class that defines a GUI "widget".  All other
   //    controls are based on this, and it implements most low-level
   //    behaviours.
   // Modification History :
   //    Author         Date        Modification
   //    Todd Lang      12-Nov-2001 Created
   // --------------------------------------------------------------------------
   TSDLControl = CLASS( TCollectionItem )
   PRIVATE
      FSkin : TSDLSkin;
      FOwner : TSDLForm;
      FGUI : TSDLGUI;
      FLeft, FTop : INTEGER;
      FName : STRING;
      FVisible : BOOLEAN;
      FHasFocus : BOOLEAN;
      FOnMouseDown : TSDLMouseEvent;
      FOnMouseUp : TSDLMouseEvent;
      FOnMouseMove : TSDLMouseEvent;
      FOnMouseEnter : TSDLMouseEvent;
      FOnMouseLeave : TSDLMouseEvent;
      FInControl : BOOLEAN;
      FEnabled : BOOLEAN;
      FHint : STRING;
      FFont : TSDLFont;
   PROTECTED
      PROCEDURE SetFont( CONST Value : TSDLFont ); VIRTUAL;
      PROCEDURE SetHint( CONST Value : STRING ); VIRTUAL;
      PROCEDURE SetFocus( CONST Value : BOOLEAN ); VIRTUAL;
      PROCEDURE SetOwner( CONST Value : TSDLForm ); VIRTUAL;
      PROCEDURE SetSkin( CONST Value : TSDLSkin ); VIRTUAL;
      PROCEDURE SetLeft( CONST Value : INTEGER ); VIRTUAL;
      PROCEDURE SetTop( CONST Value : INTEGER ); VIRTUAL;
      PROCEDURE SetWidth( CONST Value : INTEGER ); VIRTUAL; ABSTRACT;
      PROCEDURE SetHeight( CONST Value : INTEGER ); VIRTUAL; ABSTRACT;
      FUNCTION GetWidth : INTEGER; VIRTUAL; ABSTRACT;
      FUNCTION GetHeight : INTEGER; VIRTUAL; ABSTRACT;

      PROCEDURE Tick; VIRTUAL;
      PROCEDURE Draw( Target : PSDL_Surface); VIRTUAL;
      PROCEDURE Initialize( GUI : TSDLGUI ); VIRTUAL;
      PROCEDURE ComposeImage; VIRTUAL; ABSTRACT;

      PROCEDURE MouseDown( X, Y, Button : INTEGER; VAR Handled : BOOLEAN ); VIRTUAL;
      PROCEDURE MouseUp( X, Y, Button : INTEGER; VAR Handled : BOOLEAN ); VIRTUAL;
      PROCEDURE MouseMove( X, Y, DX, DY : INTEGER; VAR Handled : BOOLEAN ); VIRTUAL;
      PROCEDURE MouseWheelUp( VAR Handled : BOOLEAN ); VIRTUAL;
      PROCEDURE MouseWheelDown( VAR Handled : BOOLEAN ); VIRTUAL;
      PROCEDURE KeyDown( KeyCode : TSDLKey; Modifier : TSDLMod; VAR Handled : BOOLEAN ); VIRTUAL;
      PROCEDURE KeyUp( KeyCode : TSDLKey; Modifier : TSDLMod; VAR Handled : BOOLEAN ); VIRTUAL;

      PROPERTY GUI : TSDLGUI READ FGUI WRITE FGUI;
      PROPERTY Skin : TSDLSkin READ FSkin WRITE SetSkin;
      PROPERTY Owner : TSDLForm READ FOwner WRITE SetOwner;
      PROPERTY HasFocus : BOOLEAN READ FHasFocus WRITE SetFocus;
   PUBLIC
      CONSTRUCTOR Create( Collection : TCollection ); OVERRIDE;
      DESTRUCTOR Destroy; OVERRIDE;
      PROPERTY Font : TSDLFont READ FFont WRITE SetFont;
      PROPERTY Hint : STRING READ FHint WRITE SetHint;
      PROPERTY Left : INTEGER READ FLeft WRITE SetLeft;
      PROPERTY Top : INTEGER READ FTop WRITE SetTop;
      PROPERTY Width : INTEGER READ GetWidth WRITE SetWidth;
      PROPERTY Height : INTEGER READ GetHeight WRITE SetHeight;
      PROPERTY Name : STRING READ FName WRITE FName;
      PROPERTY Visible : BOOLEAN READ FVisible WRITE FVisible;
      PROPERTY Enabled : BOOLEAN READ FEnabled WRITE FEnabled;
      PROPERTY OnMouseDown : TSDLMouseEvent READ FOnMouseDown WRITE FOnMouseDown;
      PROPERTY OnMouseUp : TSDLMouseEvent READ FOnMouseUp WRITE FOnMouseUp;
      PROPERTY OnMouseMove : TSDLMouseEvent READ FOnMouseMove WRITE FOnMouseMove;
      PROPERTY OnMouseEnter : TSDLMouseEvent READ FOnMouseEnter WRITE FOnMouseEnter;
      PROPERTY OnMouseLeave : TSDLMouseEvent READ FOnMouseLeave WRITE FOnMouseLeave;
   END;

   // --------------------------------------------------------------------------
   // Class :
   //    TSDLControls
   // Description :
   //    Maintains a list of controls.  Fairly straight-forward stuff.
   // Modification History :
   //    Author         Date        Modification
   //    Todd Lang      12-Nov-2001 Created
   // --------------------------------------------------------------------------
   TSDLControls = CLASS( TCollection )
   PRIVATE
      FUNCTION GetItem( Index : INTEGER ) : TSDLControl;
      PROCEDURE SetItem( Index : INTEGER; CONST Value : TSDLControl );
   PROTECTED
      PROCEDURE Initialize( GUI : TSDLGUI );
      PROCEDURE Tick;
      PROCEDURE Draw( Target : PSDL_Surface );
      PROCEDURE MouseUp( X, Y, Button : INTEGER; VAR Handled : BOOLEAN );
      PROCEDURE MouseDown( X, Y, Button : INTEGER; VAR Handled : BOOLEAN );
      PROCEDURE MouseMove( X, Y, DX, DY : INTEGER; VAR Handled : BOOLEAN );
      PROCEDURE MouseWheelUp( VAR Handled : BOOLEAN );
      PROCEDURE MouseWheelDown( VAR Handled : BOOLEAN );
      PROCEDURE KeyDown( KeyCode : TSDLKey; Modifier : TSDLMod; VAR Handled : BOOLEAN );
      PROCEDURE KeyUp( KeyCode : TSDLKey; Modifier : TSDLMod; VAR Handled : BOOLEAN );
   PUBLIC
      PROPERTY Items[ Index : INTEGER ] : TSDLControl READ GetItem WRITE SetItem; DEFAULT;
   END;

   TSDLCheckControl = CLASS( TSDLControl )
   PRIVATE
      FChecked : BOOLEAN;
      FGroup : INTEGER;
   PROTECTED
      PROCEDURE SetWidth( CONST Value : INTEGER ); OVERRIDE;
      PROCEDURE SetHeight( CONST Value : INTEGER ); OVERRIDE;
      PROCEDURE MouseDown( X, Y, Button : INTEGER; VAR Handled : BOOLEAN ); OVERRIDE;
   PUBLIC
      PROPERTY Checked : BOOLEAN READ FChecked WRITE FChecked;
      PROPERTY Group : INTEGER READ FGroup WRITE FGroup;
   END;

   TSDLRadioButton = CLASS( TSDLCheckControl )
   PRIVATE
      FLabel : TSDLLabel;
      FCaption: STRING;
      PROCEDURE SetCaption( CONST Value : STRING );
   PROTECTED
      PROCEDURE SetOwner( CONST Value : TSDLForm ); OVERRIDE;  
      PROCEDURE SetLeft( CONST Value : INTEGER ); OVERRIDE;
      PROCEDURE SetTop( CONST Value : INTEGER ); OVERRIDE;
      PROCEDURE SetFont( CONST Value : TSDLFont ); OVERRIDE;
      PROCEDURE Initialize( GUI : TSDLGUI ); OVERRIDE;
      PROCEDURE ComposeImage; OVERRIDE;
      FUNCTION GetWidth : INTEGER; OVERRIDE;
      FUNCTION GetHeight : INTEGER; OVERRIDE;
   PUBLIC
      CONSTRUCTOR Create( Collection : TCollection ); OVERRIDE;
      DESTRUCTOR Destroy; OVERRIDE;
      PROCEDURE Draw( Target : PSDL_Surface ); OVERRIDE;
      PROPERTY Caption : STRING READ FCaption WRITE SetCaption;
   END;

   TSDLMeter = CLASS( TSDLControl )
   PRIVATE
      FImage : TSDLImage;
      FLabel : TSDLLabel;
      FPosition : INTEGER;
      FMin, FMax : INTEGER;
      FColour : CARDINAL;
      FWidth, FHeight : INTEGER;
      FCaption: STRING;
   PROTECTED
      PROCEDURE Initialize( GUI : TSDLGUI ); OVERRIDE;
      PROCEDURE SetFont( CONST Value : TSDLFont ); OVERRIDE;
      PROCEDURE ComposeImage; OVERRIDE;
      PROCEDURE SetWidth( CONST Value : INTEGER ); OVERRIDE;
      PROCEDURE SetHeight( CONST Value : INTEGER ); OVERRIDE;
      FUNCTION GetWidth : INTEGER; OVERRIDE;
      FUNCTION GetHeight : INTEGER; OVERRIDE;
      PROCEDURE SetPosition( CONST Value : INTEGER );
      PROCEDURE SetCaption( CONST Value : STRING );
      PROCEDURE MouseDown( X, Y, Button : INTEGER; VAR Handled : BOOLEAN ); OVERRIDE;
   PUBLIC
      CONSTRUCTOR Create( Collection : TCollection ); OVERRIDE;
      DESTRUCTOR Destroy; OVERRIDE;
      PROCEDURE Draw( Target : PSDL_Surface ); OVERRIDE;
      PROPERTY Minimum : INTEGER READ FMin WRITE FMin;
      PROPERTY Maximum : INTEGER READ FMax WRITE FMax;
      PROPERTY Position : INTEGER READ FPosition WRITE SetPosition;
      PROPERTY Colour : CARDINAL READ FColour WRITE FColour;
      PROPERTY Caption : STRING READ FCaption WRITE SetCaption;
   END;

   // --------------------------------------------------------------------------
   // Class :
   //    TSDLLabel
   // Description :
   //    A piece of static text.  The text to display is set through the Caption
   //    property.
   // Modification History :
   //    Author         Date        Modification
   //    Todd Lang      01-Dec-2001 Created
   // --------------------------------------------------------------------------
   TSDLLabel = CLASS( TSDLControl )
   PRIVATE
      FCaption : STRING;
      FImage : TSDLImage;
      PROCEDURE SetCaption( CONST Value : STRING );
   PROTECTED
      PROCEDURE ComposeImage; OVERRIDE;
      PROCEDURE SetWidth( CONST Value : INTEGER ); OVERRIDE;
      PROCEDURE SetHeight( CONST Value : INTEGER ); OVERRIDE;
      FUNCTION GetWidth : INTEGER; OVERRIDE;
      FUNCTION GetHeight : INTEGER; OVERRIDE;
   PUBLIC
      CONSTRUCTOR Create( Collection : TCollection ); OVERRIDE;
      DESTRUCTOR Destroy; OVERRIDE;
      PROCEDURE Draw( Target : PSDL_Surface ); OVERRIDE;
      PROPERTY Caption : STRING READ FCaption WRITE SetCaption;
   END;

   // --------------------------------------------------------------------------
   // Class :
   //    TSDLEdit
   // Description :
   //    A text-entry box.  Currently it doesn't do many of the advanced
   //    functions you'd like.  You cannot enter a value wider than the box
   //    (in pixels) and you cannot cut or paste.  It's stuff I'd like to add
   //    later.
   // Modification History :
   //    Author         Date        Modification
   //    Todd Lang      01-Dec-2001 Created
   // --------------------------------------------------------------------------
   TSDLEdit = CLASS( TSDLControl )
   PRIVATE
      FValue : STRING;
      FLabel : TSDLLabel;
      FWidth, FHeight : INTEGER;
      FTicks : CARDINAL;
      FShowCursor : BOOLEAN;
      FCursorIndex : INTEGER;
      FMouseDown : BOOLEAN;
      FImage : TSDLImage;
      procedure SetValue(const Value: STRING);
      function GetFont : TSDLFont;
      function KeyCodeToChar(KeyCode: TSDLKey; Modifier : TSDLMod): STRING;
   PROTECTED
      PROCEDURE ComposeImage; OVERRIDE;
      PROCEDURE Initialize( GUI : TSDLGUI ); OVERRIDE;
      PROCEDURE SetFont( CONST Value : TSDLFont ); OVERRIDE;
      FUNCTION GetWidth : INTEGER; OVERRIDE;
      FUNCTION GetHeight : INTEGER; OVERRIDE;
      PROCEDURE SetWidth( CONST Value : INTEGER ); OVERRIDE;
      PROCEDURE SetHeight( CONST Value : INTEGER ); OVERRIDE;
      PROCEDURE LostFocus;
      PROCEDURE SetOwner( CONST Value : TSDLForm ); OVERRIDE;
      PROCEDURE SetLeft( CONST Value : INTEGER ); OVERRIDE;
      PROCEDURE SetTop( CONST Value : INTEGER ); OVERRIDE;
      PROCEDURE MouseDown( X, Y, Button : INTEGER; VAR Handled : BOOLEAN ); OVERRIDE;
      PROCEDURE MouseUp( X, Y, Button : INTEGER; VAR Handled : BOOLEAN ); OVERRIDE;
      PROCEDURE KeyUp( KeyCode : TSDLKey; Modifier : TSDLMod; VAR Handled : BOOLEAN ); OVERRIDE;
   PUBLIC
      CONSTRUCTOR Create( Collection : TCollection ); OVERRIDE;
      DESTRUCTOR Destroy; OVERRIDE;
      PROCEDURE Draw( Target : PSDL_Surface ); OVERRIDE;
      PROCEDURE Tick; OVERRIDE;
      PROPERTY Value : STRING READ FValue WRITE SetValue;
      PROPERTY Width : INTEGER READ FWidth WRITE SetWidth;
      PROPERTY Height : INTEGER READ GetHeight WRITE SetHeight;
      PROPERTY Font : TSDLFont READ GetFont WRITE SetFont;
   END;

   // --------------------------------------------------------------------------
   // Class :
   //    TSDLCustomButton
   // Description :
   //    The ancestor of anything that behaves remotely like a button.  It
   //    is designed to behave like a Windows button in that if you push down
   //    on it and scroll off, it pops back up, but goes back down if you scroll
   //    back on to it.
   // Modification History :
   //    Author         Date        Modification
   //    Todd Lang      22-Dec-2001 Created
   // --------------------------------------------------------------------------
   TSDLCustomButton = CLASS( TSDLControl )
   PRIVATE
      FState : TButtonState;
      FDrawState : TButtonState;
      FGroup : INTEGER;
      FOnClick : TSDLNotifyEvent;
      FOnDown : TSDLNotifyEvent;
      FOnUp : TSDLNotifyEvent;
   PROTECTED
      PROCEDURE SetState( CONST Value : TButtonState ); VIRTUAL;
      PROCEDURE SetDrawState( CONST Value : TButtonState ); VIRTUAL;
      PROPERTY DrawState : TButtonState READ FDrawState WRITE SetDrawState;
      PROCEDURE MouseDown( X, Y, Button : INTEGER; VAR Handled : BOOLEAN ); OVERRIDE;
      PROCEDURE MouseUp( X, Y, Button : INTEGER; VAR Handled : BOOLEAN ); OVERRIDE;
      PROCEDURE MouseMove( X, Y, DX, DY : INTEGER; VAR Handled : BOOLEAN ); OVERRIDE;
   PUBLIC
      PROPERTY State : TButtonState READ FState WRITE SetState;
      PROPERTY Group : INTEGER READ FGroup WRITE FGroup;
      PROPERTY OnDown : TSDLNotifyEvent READ FOnDown WRITE FOnDown;
      PROPERTY OnUp : TSDLNotifyEvent READ FOnUp WRITE FOnUp;
      PROPERTY OnClick : TSDLNotifyEvent READ FOnClick WRITE FOnClick;
   END;

   // --------------------------------------------------------------------------
   // Class :
   //    TSDLBitButton
   // Description :
   //    A button class for a button with an image on it
   // Modification History :
   //    Author         Date        Modification
   //    Todd Lang      16-May-2002 Created
   // --------------------------------------------------------------------------
   TSDLBitButton = CLASS( TSDLCustomButton )
   PRIVATE
      FImage : TSDLImage;
      FWidth, FHeight : INTEGER;
      FUpImage, FDownImage : TSDLImage;
   PROTECTED
      PROCEDURE ComposeImage; OVERRIDE;
      PROCEDURE SetWidth( CONST Value : INTEGER ); OVERRIDE;
      PROCEDURE SetHeight( CONST Value : INTEGER ); OVERRIDE;
      FUNCTION GetWidth : INTEGER; OVERRIDE;
      FUNCTION GetHeight : INTEGER; OVERRIDE;
   PUBLIC
      CONSTRUCTOR Create( Collection : TCollection ); OVERRIDE;
      DESTRUCTOR Destroy; OVERRIDE;
      PROCEDURE Tick; OVERRIDE;
      PROCEDURE Draw( Target : PSDL_SURFACE ); OVERRIDE;
      PROPERTY Height : INTEGER READ GetHeight WRITE SetHeight;
      PROPERTY Width : INTEGER READ GetWidth WRITE SetWidth;
      PROPERTY Image : TSDLImage READ FImage WRITE FImage;
   END;

   // --------------------------------------------------------------------------
   // Class :
   //    TSDLScrollbutton
   // Description :
   //    A specific button class for scrollbars.
   // Modification History :
   //    Author         Date        Modification
   //    Todd Lang      10-Jan-2002 Created
   // --------------------------------------------------------------------------
   TSDLScrollButton = CLASS( TSDLCustomButton )
   PRIVATE
      FPosition : TScrollButtonPosition;
   PROTECTED
      PROCEDURE ComposeImage; OVERRIDE;
      FUNCTION GetWidth : INTEGER; OVERRIDE;
      FUNCTION GetHeight : INTEGER; OVERRIDE;
      PROCEDURE SetWidth( CONST Value : INTEGER ); OVERRIDE;
      PROCEDURE SetHeight( CONST Value : INTEGER ); OVERRIDE;
   PUBLIC
      PROCEDURE Draw( Target : PSDL_Surface ); OVERRIDE;
      PROPERTY Height : INTEGER READ GetHeight;
      PROPERTY Position : TScrollButtonPosition READ FPosition WRITE FPosition;
   END;

   // --------------------------------------------------------------------------
   // Class :
   //    TSDLScrollbar
   // Description :
   //    A scrollbar with a fixed-size grip.  Currently only supports vertical
   //    scrollbars.
   // Modification History :
   //    Author         Date        Modification
   //    Todd Lang      10-Jan-2001 Created
   // --------------------------------------------------------------------------
   TSDLScrollbar = CLASS( TSDLControl )
   PRIVATE
      FUpButton : TSDLScrollButton;
      FDownButton : TSDLScrollButton;
      FHeight : INTEGER;
      FMinimum, FMaximum, FPosition : INTEGER;
      FTickCount : INTEGER;
      FScrolling : BOOLEAN;
      FImage : TSDLImage;
      FChangeEvent : TSDLNotifyEvent;
      PROCEDURE ButtonUp( Sender : TSDLControl );
      PROCEDURE ButtonDown( Sender : TSDLControl );
      FUNCTION ThumbTop : INTEGER;
      PROCEDURE SetPosition( CONST Value : INTEGER );
   PROTECTED
      PROCEDURE ComposeImage; OVERRIDE;
      PROCEDURE Initialize( GUI : TSDLGUI ); OVERRIDE;
      PROCEDURE SetWidth( CONST Value : INTEGER ); OVERRIDE;
      PROCEDURE SetHeight( CONST Value : INTEGER ); OVERRIDE;
      PROCEDURE SetFocus( CONST Value : BOOLEAN ); OVERRIDE;
      FUNCTION GetWidth : INTEGER; OVERRIDE;
      FUNCTION GetHeight : INTEGER; OVERRIDE;
      PROCEDURE Tick; OVERRIDE;
      PROCEDURE SetOwner( CONST Value : TSDLForm ); OVERRIDE;
      PROCEDURE SetSkin( CONST Value : TSDLSkin ); OVERRIDE;
      PROCEDURE SetLeft( CONST Value : INTEGER ); OVERRIDE;
      PROCEDURE SetTop( CONST Value : INTEGER ); OVERRIDE;
      PROCEDURE MouseUp( X, Y, Button : INTEGER; VAR Handled : BOOLEAN ); OVERRIDE;
      PROCEDURE MouseDown( X, Y, Button : INTEGER; VAR Handled : BOOLEAN ); OVERRIDE;
      PROCEDURE MouseMove( X, Y, DX, DY : INTEGER; VAR Handled : BOOLEAN ); OVERRIDE;
      PROCEDURE MouseWheelUp( VAR Handled : BOOLEAN ); OVERRIDE;
      PROCEDURE MouseWheelDown( VAR Handled : BOOLEAN ); OVERRIDE;
   PUBLIC
      CONSTRUCTOR Create( Collection : TCollection ); OVERRIDE;
      DESTRUCTOR Destroy; OVERRIDE;
      PROCEDURE Draw( Target : PSDL_Surface ); OVERRIDE;
      PROPERTY Height : INTEGER READ FHeight WRITE SetHeight;
      PROPERTY Minimum : INTEGER READ FMinimum WRITE FMinimum;
      PROPERTY Maximum : INTEGER READ FMaximum WRITE FMaximum;
      PROPERTY Position : INTEGER READ FPosition WRITE SetPosition;
      PROPERTY Width : INTEGER READ GetWidth;
      PROPERTY OnChange : TSDLNotifyEvent READ FChangeEvent WRITE FChangeEvent;
   END;

   // --------------------------------------------------------------------------
   // Class :
   //    TSDLListBox
   // Description :
   //    Maintains a list of strings in a scrolling box.
   //    *** NOT COMPLETE BY ANY MEANS ***
   // Modification History :
   //    Author         Date        Modification
   //    Todd Lang      14-Apr-2001 Created
   // --------------------------------------------------------------------------
   TSDLListBox = CLASS( TSDLControl )
   PRIVATE
      FUpdating : BOOLEAN;
      FScrollbar : TSDLScrollbar;
      FWidth : INTEGER;
      FHeight : INTEGER;
      FItems : TStringList;
      FDisplayCount : INTEGER;
      FImage : TSDLImage;
      FItemIndex: INTEGER;
      FScrolling : INTEGER;
      FTickCount : INTEGER;
      FUNCTION GetItem( Index : INTEGER ) : STRING;
      PROCEDURE SetItem( Index : INTEGER; CONST Value : STRING );
      PROCEDURE ChangeEvent( Sender : TSDLControl );
      PROCEDURE SetItemIndex( CONST Value : INTEGER );
   PROTECTED
      PROCEDURE ComposeImage; OVERRIDE;
      PROCEDURE Initialize( GUI : TSDLGUI ); OVERRIDE;
      FUNCTION GetWidth : INTEGER; OVERRIDE;
      FUNCTION GetHeight : INTEGER; OVERRIDE;
      PROCEDURE SetFocus( CONST Value : BOOLEAN ); OVERRIDE;
      PROCEDURE SetWidth( CONST Value : INTEGER ); OVERRIDE;
      PROCEDURE SetHeight( CONST Value : INTEGER ); OVERRIDE;
      PROCEDURE Tick; OVERRIDE;
      PROCEDURE SetOwner( CONST Value : TSDLForm ); OVERRIDE;
      PROCEDURE SetSkin( CONST Value : TSDLSkin ); OVERRIDE;
      PROCEDURE SetLeft( CONST Value : INTEGER ); OVERRIDE;
      PROCEDURE SetTop( CONST Value : INTEGER ); OVERRIDE;
      PROCEDURE MouseUp( X, Y, Button : INTEGER; VAR Handled : BOOLEAN ); OVERRIDE;
      PROCEDURE MouseDown( X, Y, Button : INTEGER; VAR Handled : BOOLEAN ); OVERRIDE;
      PROCEDURE MouseMove( X, Y, DX, DY : INTEGER; VAR Handled : BOOLEAN ); OVERRIDE;
      PROCEDURE MouseWheelUp( VAR Handled : BOOLEAN ); OVERRIDE;
      PROCEDURE MouseWheelDown( VAR Handled : BOOLEAN ); OVERRIDE;
      PROCEDURE KeyDown( KeyCode : TSDLKey; Modifier : TSDLMod; VAR Handled : BOOLEAN ); OVERRIDE;
      PROCEDURE KeyUp( KeyCode : TSDLKey; Modifier : TSDLMod; VAR Handled : BOOLEAN ); OVERRIDE;
   PUBLIC
      CONSTRUCTOR Create( Collection : TCollection ); OVERRIDE;
      DESTRUCTOR Destroy; OVERRIDE;
      PROCEDURE BeginUpdate;
      PROCEDURE EndUpdate;
      PROCEDURE Draw( Target : PSDL_Surface ); OVERRIDE;
      PROCEDURE AddItem( CONST Value : STRING );
      PROPERTY Width : INTEGER READ FWidth WRITE SetWidth;
      PROPERTY Height : INTEGER READ FHeight WRITE SetHeight;
      PROPERTY ItemIndex : INTEGER READ FItemIndex WRITE SetItemIndex;
      PROPERTY Items[ Index : INTEGER ] : STRING READ GetItem WRITE SetItem; DEFAULT;
   END;


   // --------------------------------------------------------------------------
   // Class :
   //    TSDLButton
   // Description :
   //    A standard button with caption.
   // Modification History :
   //    Author         Date        Modification
   //    Todd Lang      22-Dec-2001 Created
   // --------------------------------------------------------------------------
   TSDLButton = CLASS( TSDLCustomButton )
   PRIVATE
      FWidth : INTEGER;
      FHeight : INTEGER;
      FCaption : STRING;
      FLabel : TSDLLabel;
      FUpImage, FDownImage : TSDLImage;
      PROCEDURE SetCaption( CONST Value : STRING );
   PROTECTED
      PROCEDURE ComposeImage; OVERRIDE;
      PROCEDURE Initialize( GUI : TSDLGUI ); OVERRIDE;
      PROCEDURE SetHeight( CONST Value : INTEGER ); OVERRIDE;
      PROCEDURE SetWidth( CONST Value : INTEGER ); OVERRIDE;
      PROCEDURE SetDrawState( CONST Value : TButtonState ); OVERRIDE;
      PROCEDURE SetOwner( CONST Value : TSDLForm ); OVERRIDE;
      PROCEDURE SetFont( CONST Value : TSDLFont ); OVERRIDE;
      FUNCTION GetWidth : INTEGER; OVERRIDE;
      FUNCTION GetHeight : INTEGER; OVERRIDE;
   PUBLIC
      CONSTRUCTOR Create( Collection : TCollection ); OVERRIDE;
      DESTRUCTOR Destroy; OVERRIDE;
      PROCEDURE Draw( Target : PSDL_Surface ); OVERRIDE;
      PROPERTY Width : INTEGER READ GetWidth WRITE SetWidth;
      PROPERTY Caption : STRING READ FCaption WRITE SetCaption;
   END;

   // --------------------------------------------------------------------------
   // Class :
   //    TSDLForm
   // Description :
   //    Maintains a set of controls for display.  Eats all messages passed to
   //    it.  Is not required to actually have a form image.  Currently it
   //    is assumed to be rectangular for purposes of image receipt, but it can
   //    be displayed in any shape desired.
   // Modification History :
   //    Author         Date        Modification
   //    Todd Lang      22-Dec-2001 Created
   // --------------------------------------------------------------------------
   TSDLForm = CLASS( TSDLControl )
   PRIVATE
      FImage : TSDLImage;
      FDragging : BOOLEAN;
      FControls : TSDLControls;
      FCanDrag : BOOLEAN;
      FZOrder : INTEGER;
      FCanFocus : BOOLEAN;
      FModal : BOOLEAN;
   PROTECTED
      FUNCTION GetWidth : INTEGER; OVERRIDE;
      FUNCTION GetHeight : INTEGER; OVERRIDE;
      PROCEDURE Tick; OVERRIDE;
      PROCEDURE Initialize( GUI : TSDLGUI ); OVERRIDE;
      PROPERTY Image : TSDLImage READ FImage WRITE FImage;
      PROCEDURE MouseDown( X, Y, Button : INTEGER; VAR Handled : BOOLEAN ); OVERRIDE;
      PROCEDURE MouseUp( X, Y, Button : INTEGER; VAR Handled : BOOLEAN ); OVERRIDE;
      PROCEDURE MouseMove( X, Y, DX, DY : INTEGER; VAR Handled : BOOLEAN ); OVERRIDE;
      PROCEDURE MouseWheelUp( VAR Handled : BOOLEAN ); OVERRIDE;
      PROCEDURE MouseWheelDown( VAR Handled : BOOLEAN ); OVERRIDE;
      PROCEDURE KeyDown( KeyCode : TSDLKey; Modifier : TSDLMod; VAR Handled : BOOLEAN ); OVERRIDE;
      PROCEDURE KeyUp( KeyCode : TSDLKey; Modifier : TSDLMod; VAR Handled : BOOLEAN ); OVERRIDE;
      PROCEDURE Draw( Target : PSDL_Surface ); OVERRIDE;
      PROPERTY ZOrder : INTEGER READ FZOrder WRITE FZOrder;
      PROPERTY Modal : BOOLEAN READ FModal WRITE FModal;
   PUBLIC
      CONSTRUCTOR Create( Collection : TCollection ); OVERRIDE;
      DESTRUCTOR Destroy; OVERRIDE;
      PROCEDURE CustomDraw( Target : PSDL_Surface ); VIRTUAL;
      FUNCTION AddBitButton( Name, Hint : STRING; Left, Top, Width, Height : INTEGER; Image : TSDLImage; OnClick : TSDLNotifyEvent ) : TSDLBitButton;
      FUNCTION AddButton( Name : STRING; Left, Top, Width, Height : INTEGER; Caption, FontName : STRING; OnClick : TSDLNotifyEvent ) : TSDLButton;
      FUNCTION AddLabel( Name : STRING; Left, Top : INTEGER; Caption, FontName : STRING ) : TSDLLabel;
      FUNCTION AddScrollbar( Name : STRING; Left, Top, Height, Min, Max, Pos : INTEGER; OnChange : TSDLNotifyEvent ) : TSDLScrollbar;
      FUNCTION AddEdit( Name : STRING; Left, Top, Width, Height : INTEGER; Value, FontName : STRING ) : TSDLEdit;
      FUNCTION AddListbox( Name : STRING; Left, Top, Width, Height : INTEGER; FontName : STRING ) : TSDLListBox;
      FUNCTION AddMeter( Name : STRING; Left, Top, Width, Height : INTEGER; Caption, FontName : STRING; Colour : CARDINAL; Min, Max, Position : INTEGER ) : TSDLMeter;
      FUNCTION AddRadioButton( Name : STRING; Left, Top : INTEGER; Checked : BOOLEAN; Caption, FontName : STRING ) : TSDLRadioButton;
      PROPERTY CanFocus : BOOLEAN READ FCanFocus WRITE FCanFocus;
      PROPERTY Width : INTEGER READ GetWidth;
      PROPERTY Height : INTEGER READ GetHeight;
      PROPERTY Controls : TSDLControls READ FControls;
      PROPERTY CanDrag : BOOLEAN READ FCanDrag WRITE FCanDrag;
   END;

   // --------------------------------------------------------------------------
   // Class :
   //    TSDLForms
   // Description :
   //    Maintains a set of forms.
   // Modification History :
   //    Author         Date        Modification
   //    Todd Lang      22-Dec-2001 Created
   // --------------------------------------------------------------------------
   TSDLForms = CLASS( TCollection )
   PRIVATE
      FUNCTION GetItem( Index : INTEGER ) : TSDLForm;
      PROCEDURE SetItem( Index : INTEGER; CONST Value : TSDLForm );
   PROTECTED
      PROCEDURE Initialize( GUI : TSDLGUI );
      PROCEDURE Draw( Target : PSDL_Surface );
      PROCEDURE Tick;
   PUBLIC
      FUNCTION ByName( CONST Name : STRING ) : TSDLForm;
      PROCEDURE Add( FormClass : TSDLFormClass; Name : STRING; VAR FormVar : TSDLForm ); OVERLOAD;
      FUNCTION Add : TSDLForm; OVERLOAD;
      FUNCTION Insert( Index : INTEGER ) : TSDLForm;
      PROPERTY Items[ Index : INTEGER ] : TSDLForm READ GetItem WRITE SetItem; DEFAULT;
   END;

   // --------------------------------------------------------------------------
   // Class :
   //    TSDLGUI
   // Description :
   //    Overall GUI management class.  This class is completely independent
   //    of the display management class being used.  It does not clear the
   //    surface, or flip it.  The only thing it will do is display a completely
   //    rendered GUI.
   // Modification History :
   //    Author         Date        Modification
   //    Todd Lang      14-Nov-2001 Created
   // --------------------------------------------------------------------------
   TSDLGUI = CLASS
   PRIVATE
      FSkins : TSDLSkins;
      FMouseX, FMouseY : INTEGER;
      FSkin : TSDLSkin;
      FForms : TSDLForms;
      FFonts : TSDLFonts;
      FImages : TSDLImages;
      FQuit : BOOLEAN;
      FZOrder : TList;
      FCurrentHint : STRING;
      FHintImage : TSDLImage;
      FCurrentHintOwner : TSDLControl;
      FShowToolTips : BOOLEAN;
   PROTECTED
      PROCEDURE SetHint( Sender : TSDLControl; CONST Value : STRING );
      FUNCTION ButtonGroupCount(AForm: TSDLControl; GroupIndex: INTEGER): INTEGER;
      FUNCTION CheckGroupCount( AFOrm : TSDLControl; GroupIndex : INTEGER) : INTEGER;
      PROCEDURE GroupButtonDown( AForm, AButton : TSDLControl; GroupIndex : INTEGER );
      PROCEDURE CheckGroupChange( AForm, ACheck : TSDLControl; GroupIndex : INTEGER );
      PROCEDURE EditFocus( AForm, AEdit : TSDLControl );
      PROCEDURE FormFocus( AForm : TSDLControl );
   PUBLIC
      CONSTRUCTOR Create;
      DESTRUCTOR Destroy; OVERRIDE;
      FUNCTION AddImage( Name, ImageFile : STRING ) : TSDLImage;
      PROCEDURE Tick;
      PROCEDURE TextOut( Target : PSDL_Surface; X, Y : INTEGER; Text : STRING );
      PROCEDURE Terminate;
      PROCEDURE Draw( Target : PSDL_Surface );
      PROCEDURE SetSkin( Name : STRING );
      PROCEDURE Initialize;
      PROCEDURE MouseDown( X, Y, Button : INTEGER );
      PROCEDURE MouseUp( X, Y, Button : INTEGER );
      PROCEDURE MouseMove( X, Y : INTEGER );
      PROCEDURE MouseWheelUp();
      PROCEDURE MouseWheelDown();
      PROCEDURE KeyDown( KeyCode : TSDLKey; Modifier : TSDLMod );
      PROCEDURE KeyUp( KeyCode : TSDLKey; Modifier : TSDLMod );
      PROCEDURE Show( FormName : STRING );
      PROCEDURE Hide( FormName : STRING ); OVERLOAD;
      PROCEDURE Hide( SDLForm : TSDLForm ); OVERLOAD;
      PROCEDURE ShowModal( FormName : STRING );
      PROPERTY Skins : TSDLSkins READ FSkins;
      PROPERTY MouseX : INTEGER READ FMouseX WRITE FMouseX;
      PROPERTY MouseY : INTEGER READ FMouseY WRITE FMouseY;
      PROPERTY Forms : TSDLForms READ FForms;
      PROPERTY Fonts : TSDLFonts READ FFonts;
      PROPERTY Images : TSDLImages READ FImages;
      PROPERTY Quit : BOOLEAN READ FQuit;
      PROPERTY ShowToolTips : BOOLEAN READ FShowToolTips WRITE FShowToolTips;
   END;

// Helper function for form sorting
function ZOrderSort( Item1, Item2 : POINTER ) : INTEGER;

implementation

USES
   SysUtils, SDL_Image;

CONST
   // These define where each of the screen elements appears on the
   // skin image.  Eventually, I'd like to make these configurable, but
   // hard-coded works for me for now.
   MOUSE_RECT : TSDL_RECT = (x:0; y:0; w:16; h:16);
   SELECTION_BAR : TSDL_RECT = (x:0; y:16; w:16; h:16);
   RADIO_BUTTON_UNSELECTED : TSDL_RECT = (x:16;y:0;w:16;h:16);
   RADIO_BUTTON_SELECTED : TSDL_RECT = (x:16;y:16;w:16;h:16);
   ARROW_UP : TSDL_RECT = (x:32; y:0; w:16; h:16);
   ARROW_DOWN : TSDL_RECT = (x:32; y:16; w:16; h:16);
   SCROLL_TRACK : TSDL_RECT = (x:48; y:0; w:16; h:16);
   SCROLL_THUMB : TSDL_RECT = (x:48; y:16; w:16; h:16);
   BUTTON_TOP_LEFT_UP : TSDL_RECT = (x:64; y:32; w:8; h:8 );
   BUTTON_TOP_CENTER_UP : TSDL_RECT = (x:72; y:32; w:16; h:8 );
   BUTTON_TOP_RIGHT_UP : TSDL_RECT = (x:88; y:32; w:8; h:8 );
   BUTTON_MIDDLE_LEFT_UP : TSDL_RECT = (x:64; y:40; w:8; h:16 );
   BUTTON_MIDDLE_CENTER_UP : TSDL_RECT = (x:72; y:40; w:16; h:16 );
   BUTTON_MIDDLE_RIGHT_UP : TSDL_RECT = (x:88; y:40; w:8; h:16 );
   BUTTON_BOTTOM_LEFT_UP : TSDL_RECT = (x:64; y:56; w:8; h:8 );
   BUTTON_BOTTOM_CENTER_UP : TSDL_RECT = (x:72; y:56; w:16; h:8 );
   BUTTON_BOTTOM_RIGHT_UP : TSDL_RECT = (x:88; y:56; w:8; h:8 );
   BUTTON_TOP_LEFT_DOWN : TSDL_RECT = (x:96; y:32; w:8; h:8 );
   BUTTON_TOP_CENTER_DOWN : TSDL_RECT = (x:104; y:32; w:16; h:8 );
   BUTTON_TOP_RIGHT_DOWN : TSDL_RECT = (x:120; y:32; w:8; h:8 );
   BUTTON_MIDDLE_LEFT_DOWN : TSDL_RECT = (x:96; y:40; w:8; h:16 );
   BUTTON_MIDDLE_CENTER_DOWN : TSDL_RECT = (x:104; y:40; w:16; h:16 );
   BUTTON_MIDDLE_RIGHT_DOWN : TSDL_RECT = (x:120; y:40; w:8; h:16 );
   BUTTON_BOTTOM_LEFT_DOWN : TSDL_RECT = (x:96; y:56; w:8; h:8 );
   BUTTON_BOTTOM_CENTER_DOWN : TSDL_RECT = (x:104; y:56; w:16; h:8 );
   BUTTON_BOTTOM_RIGHT_DOWN : TSDL_RECT = (x:120; y:56; w:8; h:8 );
   SQUAREBUTTON_TOP_LEFT_UP : TSDL_RECT = (x:0; y:32; w:8; h:8);
   SQUAREBUTTON_TOP_CENTER_UP : TSDL_RECT = (x:8; y:32; w:16; h:8);
   SQUAREBUTTON_TOP_RIGHT_UP : TSDL_RECT = (x:24; y:32; w:8; h:8);
   SQUAREBUTTON_MIDDLE_LEFT_UP : TSDL_RECT = (x:0; y:40; w:8; h:16);
   SQUAREBUTTON_MIDDLE_CENTER_UP : TSDL_RECT = (x:8; y:40; w:16; h:16);
   SQUAREBUTTON_MIDDLE_RIGHT_UP : TSDL_RECT = (x:24; y:40; w:8; h:16);
   SQUAREBUTTON_BOTTOM_LEFT_UP : TSDL_RECT = (x:0; y:56; w:8; h:8);
   SQUAREBUTTON_BOTTOM_CENTER_UP : TSDL_RECT = (x:8; y:56; w:16; h:8);
   SQUAREBUTTON_BOTTOM_RIGHT_UP : TSDL_RECT = (x:24; y:56; w:8; h:8);
   SQUAREBUTTON_TOP_LEFT_DOWN : TSDL_RECT = (x:32; y:32; w:8; h:8);
   SQUAREBUTTON_TOP_CENTER_DOWN : TSDL_RECT = (x:40; y:32; w:16; h:8);
   SQUAREBUTTON_TOP_RIGHT_DOWN : TSDL_RECT = (x:56; y:32; w:8; h:8);
   SQUAREBUTTON_MIDDLE_LEFT_DOWN : TSDL_RECT = (x:32; y:40; w:8; h:16);
   SQUAREBUTTON_MIDDLE_CENTER_DOWN : TSDL_RECT = (x:40; y:40; w:16; h:16);
   SQUAREBUTTON_MIDDLE_RIGHT_DOWN : TSDL_RECT = (x:56; y:40; w:8; h:16);
   SQUAREBUTTON_BOTTOM_LEFT_DOWN : TSDL_RECT = (x:32; y:56; w:8; h:8);
   SQUAREBUTTON_BOTTOM_CENTER_DOWN : TSDL_RECT = (x:40; y:56; w:16; h:8);
   SQUAREBUTTON_BOTTOM_RIGHT_DOWN : TSDL_RECT = (x:56; y:56; w:8; h:8);
   BITBUTTON_TOP_LEFT_UP : TSDL_RECT = (x:0; y:96; w:8; h:8);
   BITBUTTON_TOP_CENTER_UP : TSDL_RECT = (x:8; y:96; w:16; h:8);
   BITBUTTON_TOP_RIGHT_UP : TSDL_RECT = (x:24; y:96; w:8; h:8);
   BITBUTTON_MIDDLE_LEFT_UP : TSDL_RECT = (x:0; y:104; w:8; h:16);
   BITBUTTON_MIDDLE_CENTER_UP : TSDL_RECT = (x:8; y:104; w:16; h:16);
   BITBUTTON_MIDDLE_RIGHT_UP : TSDL_RECT = (x:24; y:104; w:8; h:16);
   BITBUTTON_BOTTOM_LEFT_UP : TSDL_RECT = (x:0; y:120; w:8; h:8);
   BITBUTTON_BOTTOM_CENTER_UP : TSDL_RECT = (x:8; y:120; w:16; h:8);
   BITBUTTON_BOTTOM_RIGHT_UP : TSDL_RECT = (x:24; y:120; w:8; h:8);
   BITBUTTON_TOP_LEFT_DOWN : TSDL_RECT = (x:32; y:96; w:8; h:8);
   BITBUTTON_TOP_CENTER_DOWN : TSDL_RECT = (x:40; y:96; w:16; h:8);
   BITBUTTON_TOP_RIGHT_DOWN : TSDL_RECT = (x:56; y:96; w:8; h:8);
   BITBUTTON_MIDDLE_LEFT_DOWN : TSDL_RECT = (x:32; y:104; w:8; h:16);
   BITBUTTON_MIDDLE_CENTER_DOWN : TSDL_RECT = (x:40; y:104; w:16; h:16);
   BITBUTTON_MIDDLE_RIGHT_DOWN : TSDL_RECT = (x:56; y:104; w:8; h:16);
   BITBUTTON_BOTTOM_LEFT_DOWN : TSDL_RECT = (x:32; y:120; w:8; h:8);
   BITBUTTON_BOTTOM_CENTER_DOWN : TSDL_RECT = (x:40; y:120; w:16; h:8);
   BITBUTTON_BOTTOM_RIGHT_DOWN : TSDL_RECT = (x:56; y:120; w:8; h:8);
   EDIT_TOP_LEFT : TSDL_RECT = (x:32; y:64; w:8; h:8);
   EDIT_TOP_CENTER : TSDL_RECT = (x:40; y:64; w:16; h:8);
   EDIT_TOP_RIGHT : TSDL_RECT = (x:56; y:64; w:8; h:8);
   EDIT_MIDDLE_LEFT : TSDL_RECT = (x:32; y:72; w:8; h:16);
   EDIT_MIDDLE_CENTER : TSDL_RECT = (x:40; y:72; w:16; h:16);
   EDIT_MIDDLE_RIGHT : TSDL_RECT = (x:56; y:72; w:8; h:16);
   EDIT_BOTTOM_LEFT : TSDL_RECT = (x:32; y:88; w:8; h:8);
   EDIT_BOTTOM_CENTER : TSDL_RECT = (x:40; y:88; w:16; h:8);
   EDIT_BOTTOM_RIGHT : TSDL_RECT = (x:56; y:88; w:8; h:8);
   PANEL_TOP_LEFT : TSDL_RECT = (x:0; y:64; w:8; h:8);
   PANEL_TOP_CENTER : TSDL_RECT = (x:8; y:64; w:16; h:8);
   PANEL_TOP_RIGHT : TSDL_RECT = (x:24; y:64; w:8; h:8);
   PANEL_MIDDLE_LEFT : TSDL_RECT = (x:0; y:72; w:8; h:16);
   PANEL_MIDDLE_CENTER : TSDL_RECT = (x:8; y:72; w:16; h:16);
   PANEL_MIDDLE_RIGHT : TSDL_RECT = (x:24; y:72; w:8; h:16);
   PANEL_BOTTOM_LEFT : TSDL_RECT = (x:0; y:88; w:8; h:8);
   PANEL_BOTTOM_CENTER : TSDL_RECT = (x:8; y:88; w:16; h:8);
   PANEL_BOTTOM_RIGHT : TSDL_RECT = (x:24; y:88; w:8; h:8);

// -----------------------------------------------------------------------------
// Method :
//    ZOrderSort
// Description :
//    Determines if one form is closer to the surface than another
// Parameters :
//    Item1    POINTER     First item to compare
//    Item2    POINTER     Second item to compare
// Returns :
//    -1 if Item1 < Item2
//    1 if Item1 > Item2
//    0 if Item1 = Item2 (should never happen)
// Modification History :
//    Author         Date        Modification
//    Todd Lang      22-Dec-2001 Created
//------------------------------------------------------------------------------
function ZOrderSort( Item1, Item2 : POINTER ) : INTEGER;
begin
   IF (TSDLForm(Item1).ZOrder < TSDLForm(Item2).ZOrder) THEN
      Result := -1
    ELSE
   IF (TSDLForm(Item1).ZOrder > TSDLForm(Item2).ZOrder) THEN
      Result := 1
    ELSE
      Result := 0;
end;

{ TSDLSkin }

// -----------------------------------------------------------------------------
// Class :
//    TSDLSkin
// Method :
//    DrawArrow
// Description :
//    Draws the scrollbutton arrow to the supplied surface
// Parameters :
//    Target   PSDL_Surface   SDL Surface to be drawn to
//    X        INTEGER        X-position on surface to draw to
//    Y        INTEGER        Y-position on surface to draw to
//    Up       BOOLEAN        Draw the up or down arrow
// Returns :
//    Nothing
// Modification History :
//    Author         Date        Modification
//    Todd Lang      10-Jan-2002 Created
//------------------------------------------------------------------------------
procedure TSDLSkin.DrawArrow(Target: PSDL_Surface; X, Y: INTEGER; Up: BOOLEAN);
begin
   IF (Up) THEN
      FImage.DrawSub( Target, @ARROW_UP, X, Y )
    ELSE
      FImage.DrawSub( Target, @ARROW_DOWN, X, Y );
end;

// -----------------------------------------------------------------------------
// Class :
//    TSDLSkin
// Method :
//    DrawButton
// Description :
//    Draw a button to the supplied display.  This is the standard captioned
//    button.  (As opposed to the square button used for scrollbars)
// Parameters :
//    Target   PSDL_Surface   SDL Surface to be drawn to
//    X        INTEGER        X-position on surface to draw to
//    Y        INTEGER        Y-position on surface to draw to
//    Up       BOOLEAN        Button state to draw
// Returns :
//    Nothing
// Modification History :
//    Author         Date        Modification
//    Todd Lang      14-Nov-2001 Created
//------------------------------------------------------------------------------
procedure TSDLSkin.DrawButton(Target: PSDL_Surface; X, Y, W, H: INTEGER; Up : BOOLEAN);
VAR
   Counter : INTEGER;
   NewH : INTEGER;
   TopLeftRect : TSDL_RECT;
   TopCenterRect : TSDL_RECT;
   TopRightRect : TSDL_RECT;
   MiddleLeftRect : TSDL_RECT;
   MiddleCenterRect : TSDL_RECT;
   MiddleRightRect : TSDL_RECT;
   BottomLeftRect : TSDL_RECT;
   BottomCenterRect : TSDL_RECT;
   BottomRightRect : TSDL_RECT;
begin
   IF (Target = NIL) THEN
      Exit;

   IF (Up) THEN
   BEGIN
      TopLeftRect := BUTTON_TOP_LEFT_UP;
      TopCenterRect := BUTTON_TOP_CENTER_UP;
      TopRightRect := BUTTON_TOP_RIGHT_UP;
      MiddleLeftRect := BUTTON_MIDDLE_LEFT_UP;
      MiddleCenterRect := BUTTON_MIDDLE_CENTER_UP;
      MiddleRightRect := BUTTON_MIDDLE_RIGHT_UP;
      BottomLeftRect := BUTTON_BOTTOM_LEFT_UP;
      BottomCenterRect := BUTTON_BOTTOM_CENTER_UP;
      BottomRightRect := BUTTON_BOTTOM_RIGHT_UP;
   END
    ELSE
   BEGIN
      TopLeftRect := BUTTON_TOP_LEFT_DOWN;
      TopCenterRect := BUTTON_TOP_CENTER_DOWN;
      TopRightRect := BUTTON_TOP_RIGHT_DOWN;
      MiddleLeftRect := BUTTON_MIDDLE_LEFT_DOWN;
      MiddleCenterRect := BUTTON_MIDDLE_CENTER_DOWN;
      MiddleRightRect := BUTTON_MIDDLE_RIGHT_DOWN;
      BottomLeftRect := BUTTON_BOTTOM_LEFT_DOWN;
      BottomCenterRect := BUTTON_BOTTOM_CENTER_DOWN;
      BottomRightRect := BUTTON_BOTTOM_RIGHT_DOWN;
   END;

   // *** TOP OF BITBUTTON ***
   DrawRepeating( Target, X, Y, W, H, @TopLeftRect, @TopCenterRect, @TopRightRect );

   // *** CENTER OF BITBUTTON ***
   Y := Y + TopCenterRect.h;
   FOR Counter := 1 TO ((H-(TopCenterRect.h + BottomCenterRect.h)) DIV MiddleCenterRect.h) DO
   BEGIN
      DrawRepeating( Target, X, Y, W, H, @MiddleLeftRect, @MiddleCenterRect, @MiddleRightRect );
      Y := Y + MiddleCenterRect.h;
   END;

   NewH := (H-(TopCenterRect.h + BottomCenterRect.h)) MOD MiddleCenterRect.h;
   IF (NewH <> 0) THEN
      DrawRepeating( Target, X, Y, W, H, @MiddleLeftRect, @MiddleCenterRect, @MiddleRightRect, NewH );
   Y := Y + NewH;

   // *** BOTTOM OF BITBUTTON ***
   DrawRepeating( Target, X, Y, W, H, @BottomLeftRect, @BottomCenterRect, @BottomRightRect );
end;

// -----------------------------------------------------------------------------
// Class :
//    TSDLSkin
// Method :
//    DrawMouse
// Description :
//    Draws the mouse cursor to the display
// Parameters :
//    Target   PSDL_Surface   SDL Surface to be drawn to
//    X        INTEGER        X-position on surface to draw to
//    Y        INTEGER        Y-position on surface to draw to
// Returns :
//    Nothing
// Modification History :
//    Author         Date        Modification
//    Todd Lang      14-Nov-2001 Created
//------------------------------------------------------------------------------
procedure TSDLSkin.DrawMouse(Target: PSDL_Surface; X, Y: INTEGER);
begin
   IF (FImage.DrawSub( Target, @MOUSE_RECT, X, Y ) = -2) THEN
      FImage.Load( FFileName )
end;

// -----------------------------------------------------------------------------
// Class :
//    TSDLSkin
// Method :
//    DrawScrollThumb
// Description :
//    Draws the scrollbar thumb button.  (The thing you pull on to scroll)
// Parameters :
//    Target   PSDL_Surface   SDL Surface to be drawn to
//    X        INTEGER        X-position on surface to draw to
//    Y        INTEGER        Y-position on surface to draw to
// Returns :
//    Nothing
// Modification History :
//    Author         Date        Modification
//    Todd Lang      10-Jan-2002 Created
//------------------------------------------------------------------------------
procedure TSDLSkin.DrawScrollThumb( Target : PSDL_Surface; X, Y : INTEGER );
begin
   IF (FImage.DrawSub( Target, @SCROLL_THUMB, X, Y ) = -2) THEN
      FImage.Load( FFileName );
end;

// -----------------------------------------------------------------------------
// Class :
//    TSDLSkin
// Method :
//    DrawScrollTrack
// Description :
//    Draws the scrollbar track for the thumb.
// Parameters :
//    Target   PSDL_Surface   SDL Surface to be drawn to
//    X        INTEGER        X-position on surface to draw to
//    Y        INTEGER        Y-position on surface to draw to
//    H        INTEGER        Height of the scrolltrack
// Returns :
//    Nothing
// Modification History :
//    Author         Date        Modification
//    Todd Lang      10-Jan-2002 Created
//------------------------------------------------------------------------------
procedure TSDLSkin.DrawScrollTrack(Target: PSDL_Surface; X, Y, H: INTEGER);
VAR
   Counter : INTEGER;
   PartialRect : TSDL_RECT;
begin
   // Draw all complete chunks of the track
   FOR Counter := 0 TO ((H DIV 16)-1) DO
   BEGIN
      IF (FImage.DrawSub(Target, @SCROLL_TRACK, X, Y + (16 * Counter) ) = -2) THEN
         FImage.Load( FFileName );
   END;

   // Draw any partial amounts leftover
   IF ((H MOD 16) <> 0) THEN
   BEGIN
      Counter := H MOD 16;
      PartialRect.x := SCROLL_TRACK.x;
      PartialRect.y := SCROLL_TRACK.y;
      PartialRect.w := SCROLL_TRACK.w;
      PartialRect.h := SCROLL_TRACK.h + Counter;
      IF (FImage.DrawSub(Target, @PartialRect, X, Y + ((H DIV 16)*16) ) = -2) THEN
         FImage.Load( FFileName );
   END;
end;

// -----------------------------------------------------------------------------
// Class :
//    TSDLSkin
// Method :
//    DrawSquareButton
// Description :
//    Draws the square button to the supplied surface.  This is supplied as a
//    "convenience" function.  Normally, you would use the regular button class,
//    but there may be instances where you want "square" buttons.
// Parameters :
//    Target   PSDL_Surface   SDL Surface to be drawn to
//    X        INTEGER        X-position on surface to draw to
//    Y        INTEGER        Y-position on surface to draw to
//    Up       BOOLEAN        State to draw the button in.
// Returns :
//    Nothing
// Modification History :
//    Author         Date        Modification
//    Todd Lang      10-Jan-2002 Created
//------------------------------------------------------------------------------
procedure TSDLSkin.DrawSquareButton(Target: PSDL_Surface; X, Y, W, H: INTEGER; Up: BOOLEAN);
VAR
   Counter : INTEGER;
   NewH : INTEGER;
   TopLeftRect : TSDL_RECT;
   TopCenterRect : TSDL_RECT;
   TopRightRect : TSDL_RECT;
   MiddleLeftRect : TSDL_RECT;
   MiddleCenterRect : TSDL_RECT;
   MiddleRightRect : TSDL_RECT;
   BottomLeftRect : TSDL_RECT;
   BottomCenterRect : TSDL_RECT;
   BottomRightRect : TSDL_RECT;
begin
   IF (Target = NIL) THEN
      Exit;

   IF (Up) THEN
   BEGIN
      TopLeftRect := SQUAREBUTTON_TOP_LEFT_UP;
      TopCenterRect := SQUAREBUTTON_TOP_CENTER_UP;
      TopRightRect := SQUAREBUTTON_TOP_RIGHT_UP;
      MiddleLeftRect := SQUAREBUTTON_MIDDLE_LEFT_UP;
      MiddleCenterRect := SQUAREBUTTON_MIDDLE_CENTER_UP;
      MiddleRightRect := SQUAREBUTTON_MIDDLE_RIGHT_UP;
      BottomLeftRect := SQUAREBUTTON_BOTTOM_LEFT_UP;
      BottomCenterRect := SQUAREBUTTON_BOTTOM_CENTER_UP;
      BottomRightRect := SQUAREBUTTON_BOTTOM_RIGHT_UP;
   END
    ELSE
   BEGIN
      TopLeftRect := SQUAREBUTTON_TOP_LEFT_DOWN;
      TopCenterRect := SQUAREBUTTON_TOP_CENTER_DOWN;
      TopRightRect := SQUAREBUTTON_TOP_RIGHT_DOWN;
      MiddleLeftRect := SQUAREBUTTON_MIDDLE_LEFT_DOWN;
      MiddleCenterRect := SQUAREBUTTON_MIDDLE_CENTER_DOWN;
      MiddleRightRect := SQUAREBUTTON_MIDDLE_RIGHT_DOWN;
      BottomLeftRect := SQUAREBUTTON_BOTTOM_LEFT_DOWN;
      BottomCenterRect := SQUAREBUTTON_BOTTOM_CENTER_DOWN;
      BottomRightRect := SQUAREBUTTON_BOTTOM_RIGHT_DOWN;
   END;

   // *** TOP OF BITBUTTON ***
   DrawRepeating( Target, X, Y, W, H, @TopLeftRect, @TopCenterRect, @TopRightRect );

   // *** CENTER OF BITBUTTON ***
   Y := Y + TopCenterRect.h;
   FOR Counter := 1 TO ((H-(TopCenterRect.h + BottomCenterRect.h)) DIV MiddleCenterRect.h) DO
   BEGIN
      DrawRepeating( Target, X, Y, W, H, @MiddleLeftRect, @MiddleCenterRect, @MiddleRightRect );
      Y := Y + MiddleCenterRect.h;
   END;

   NewH := (H-(TopCenterRect.h + BottomCenterRect.h)) MOD MiddleCenterRect.h;
   IF (NewH <> 0) THEN
      DrawRepeating( Target, X, Y, W, H, @MiddleLeftRect, @MiddleCenterRect, @MiddleRightRect, NewH );
   Y := Y + NewH;

   // *** BOTTOM OF BITBUTTON ***
   DrawRepeating( Target, X, Y, W, H, @BottomLeftRect, @BottomCenterRect, @BottomRightRect );
end;

// -----------------------------------------------------------------------------
// Class :
//    TSDLSkin
// Method :
//    Create
// Description :
//    Instantiates all necessary classes to support the skin
// Parameters :
//    Collection     TCollection    Collection this skin is part of
// Returns :
//    Nothing
// Modification History :
//    Author         Date        Modification
//    Todd Lang      14-Nov-2001 Created
//------------------------------------------------------------------------------
constructor TSDLSkin.Create(Collection: TCollection);
begin
   inherited;
   FImage := TSDLImage.Create( NIL );
end;

// -----------------------------------------------------------------------------
// Class :
//    TSDLSkin
// Method :
//    Destroy
// Description :
//    Release all resources used by the class
// Parameters :
//    None
// Returns :
//    Nothing
// Modification History :
//    Author         Date        Modification
//    Todd Lang      14-Nov-2001 Created
//------------------------------------------------------------------------------
destructor TSDLSkin.Destroy;
begin
   FImage.Free;
   inherited;
end;

// -----------------------------------------------------------------------------
// Class :
//    TSDLSkin
// Method :
//    Load
// Description :
//    Loads the skin image from a file.  Note that this can be whatever format
//    SDL supports.
// Parameters :
//    Name        STRING      User-name of the skin
//    FileName    STRING      File that contains the skin image
// Returns :
//    Nothing
// Modification History :
//    Author         Date        Modification
//    Todd Lang      14-Nov-2001 Created
//------------------------------------------------------------------------------
procedure TSDLSkin.Load(Name, Filename: STRING);
begin
   FName := Name;
   FFileName := FileName;
   FImage.Load( Filename );
end;

// -----------------------------------------------------------------------------
// Class :
//    TSDLSkin
// Method :
//    DrawEdit
// Description :
//    Displays the editbox on the supplied surface.
// Parameters :
//    Target   PSDL_Surface   SDL Surface to be drawn to
//    X        INTEGER        X-position on surface to draw to
//    Y        INTEGER        Y-position on surface to draw to
//    W        INTEGER        Width of the edit box
// Returns :
//    Nothing
// Modification History :
//    Author         Date        Modification
//    Todd Lang      01-Dec-2001 Created
//------------------------------------------------------------------------------
procedure TSDLSkin.DrawEdit(Target: PSDL_Surface; X, Y, W, H: INTEGER);
VAR
   Counter : INTEGER;
   NewH : INTEGER;
begin
   IF (Target = NIL) THEN
      Exit;

   // *** TOP OF PANEL ***
   DrawRepeating( Target, X, Y, W, H, @EDIT_TOP_LEFT, @EDIT_TOP_CENTER, @EDIT_TOP_RIGHT );

   // *** CENTER OF PANEL ***
   Y := Y + EDIT_TOP_CENTER.h;
   FOR Counter := 1 TO ((H-(EDIT_TOP_CENTER.h + EDIT_BOTTOM_CENTER.h)) DIV EDIT_MIDDLE_CENTER.h) DO
   BEGIN
      DrawRepeating( Target, X, Y, W, H, @EDIT_MIDDLE_LEFT, @EDIT_MIDDLE_CENTER, @EDIT_MIDDLE_RIGHT );
      Y := Y + EDIT_MIDDLE_CENTER.h;
   END;

   // Any left-over height for the center of the panel
   NewH := (H-(EDIT_TOP_CENTER.h + EDIT_BOTTOM_CENTER.h)) MOD EDIT_MIDDLE_CENTER.h;
   IF (NewH <> 0) THEN
      DrawRepeating( Target, X, Y, W, H, @EDIT_MIDDLE_LEFT, @EDIT_MIDDLE_CENTER, @EDIT_MIDDLE_RIGHT );

   Y := Y + NewH;

   // *** BOTTOM OF PANEL ***
   DrawRepeating( Target, X, Y, W, H, @EDIT_BOTTOM_LEFT, @EDIT_BOTTOM_CENTER, @EDIT_BOTTOM_RIGHT );
end;

// -----------------------------------------------------------------------------
// Class :
//    TSDLSkin
// Method :
//    DrawSelectionBar
// Description :
//    Displays the selection (highlight) bar on the supplied surface.
// Parameters :
//    Target   PSDL_Surface   SDL Surface to be drawn to
//    X        INTEGER        X-position on surface to draw to
//    Y        INTEGER        Y-position on surface to draw to
//    W        INTEGER        Width of the edit box
// Returns :
//    Nothing
// Modification History :
//    Author         Date        Modification
//    Todd Lang      01-Dec-2001 Created
//------------------------------------------------------------------------------
procedure TSDLSkin.DrawSelectionBar(Target: PSDL_Surface; X, Y, W, H: INTEGER);
VAR
   Counter : INTEGER;
begin
   // Offsets to bring the bar in so it doesn't overlap control edges
   Y := Y + 1;
   H := H - 2;

   // Draw all of the complete bars
   FOR Counter := 0 TO ((H DIV SELECTION_BAR.h)-1) DO
      DrawRepeating( Target, X, Y, W, H, NIL, @SELECTION_BAR, NIL );

   // Draw any partial bar if necessary
   IF ((H MOD SELECTION_BAR.h) > 0) THEN
      DrawRepeating( Target, X, Y, W, H, NIL, @SELECTION_BAR, NIL, H MOD SELECTION_BAR.h );
end;

procedure TSDLSkin.DrawRadioButton(Target: PSDL_Surface; X, Y : INTEGER; Checked: BOOLEAN);
begin
   IF (Checked) THEN
      FImage.DrawSub( Target, @RADIO_BUTTON_SELECTED, X, Y)
    ELSE
      FImage.DrawSub( Target, @RADIO_BUTTON_UNSELECTED, X, Y);
end;

// -----------------------------------------------------------------------------
// Class :
//    TSDLSkin
// Method :
//    DrawRepeating
// Description :
//    Draws a horizontally repeating strip of graphics from the skin.  This
//    can optionally include a left and right strip.  The height of the strip
//    will be clamped to the "OverrideHeight" parameter if it is supplied.
// Parameters :
//    Target         PSDL_Surface   SDL Surface to be drawn to
//    X              INTEGER        X-position on surface to draw to
//    Y              INTEGER        Y-position on surface to draw to
//    W              INTEGER        Width of the strip
//    H              INTEGER        Height of the strip
//    LeftRect       PSDL_RECT      Location of left-hand strip to blit
//    CenterRect     PSDL_RECT      Location of the center strip to blit
//    RightRect      PSDL_RECT      Location of the right-hand strip to blit
//    OverrideHeight INTEGER        Height to clamp the strip to. (Optional)
// Returns :
//    Nothing
// Modification History :
//    Author         Date        Modification
//    Todd Lang      21-May-2001 Created
//------------------------------------------------------------------------------
procedure TSDLSkin.DrawRepeating(Target : PSDL_Surface; X, Y, W, H: INTEGER; LeftRect, CenterRect, RightRect : PSDL_RECT; OverrideHeight : INTEGER);
VAR
   PartialWidth : INTEGER;
   PartialRect : TSDL_RECT;
   Counter : INTEGER;
   LeftRectWidth, RightRectWidth : INTEGER;
begin
   // Clamp the blit rects
   IF (OverrideHeight <> -1) THEN
   BEGIN
      IF (LeftRect <> NIL) THEN
         LeftRect.h := OverrideHeight;

      CenterRect.h := OverrideHeight;

      IF (RightRect <> NIL) THEN
         RightRect.h := OverrideHeight;
   END;

   // Handles NIL rectangles
   IF (LeftRect = NIL) THEN
      LeftRectWidth := 0
    ELSE
      LeftRectWidth := LeftRect.w;

   IF (RightRect = NIL) THEN
      RightRectWidth := 0
    ELSE
      RightRectWidth := RightRect.w;

   // Draw the left edge
   IF (LeftRect <> NIL) THEN
      FImage.DrawSub(Target, LeftRect, X, Y );

   // Draw all complete center blits
   FOR Counter := 1 TO ((W-(LeftRectWidth + RightRectWidth)) DIV CenterRect.w) DO
      FImage.DrawSub(Target, CenterRect, X + (CenterRect.w * (Counter-1)) + LeftRectWidth, Y );

   // Draw the partial center blit, if any
   PartialWidth := (W-(LeftRectWidth + RightRectWidth)) MOD CenterRect.w;
   IF (PartialWidth <> 0) THEN
   BEGIN
      PartialRect.x := CenterRect.x;
      PartialRect.y := CenterRect.y;
      PartialRect.w := PartialWidth;
      PartialRect.h := CenterRect.h;
      FImage.DrawSub(Target, @PartialRect, (X + W) - (RightRectWidth + PartialWidth), Y );
   END;

   // Draw the right edge
   IF (RightRect <> NIL) THEN
      FImage.DrawSub(Target, RightRect, X + W - RightRect.w, Y );
end;

// -----------------------------------------------------------------------------
// Class :
//    TSDLSkin
// Method :
//    DrawPanel
// Description :
//    Draw a blank panel to the surface.  This is generally used as the
//    background for a listbox.
// Parameters :
//    Target   PSDL_Surface   SDL Surface to be drawn to
//    X        INTEGER        X-position on surface to draw to
//    Y        INTEGER        Y-position on surface to draw to
//    W        INTEGER        Width of the panel
//    H        INTEGER        Heigh of the panel
// Returns :
//    Nothing
// Modification History :
//    Author         Date        Modification
//    Todd Lang      14-Apr-2002 Created
//------------------------------------------------------------------------------
procedure TSDLSkin.DrawPanel(Target: PSDL_Surface; X, Y, W, H: INTEGER);
VAR
   Counter : INTEGER;
   NewH : INTEGER;
begin
   IF (Target = NIL) THEN
      Exit;

   // *** TOP OF PANEL ***
   DrawRepeating( Target, X, Y, W, H, @PANEL_TOP_LEFT, @PANEL_TOP_CENTER, @PANEL_TOP_RIGHT );

   // *** CENTER OF PANEL ***
   Y := Y + PANEL_TOP_CENTER.h;
   FOR Counter := 1 TO ((H-(PANEL_TOP_CENTER.h + PANEL_BOTTOM_CENTER.h)) DIV PANEL_MIDDLE_CENTER.h) DO
   BEGIN
      DrawRepeating( Target, X, Y, W, H, @PANEL_MIDDLE_LEFT, @PANEL_MIDDLE_CENTER, @PANEL_MIDDLE_RIGHT );
      Y := Y + PANEL_MIDDLE_CENTER.h;
   END;

   // Any left-over height for the center of the panel
   NewH := (H-(PANEL_TOP_CENTER.h + PANEL_BOTTOM_CENTER.h)) MOD PANEL_MIDDLE_CENTER.h;
   IF (NewH <> 0) THEN
      DrawRepeating( Target, X, Y, W, H, @PANEL_MIDDLE_LEFT, @PANEL_MIDDLE_CENTER, @PANEL_MIDDLE_RIGHT );

   Y := Y + NewH;

   // *** BOTTOM OF PANEL ***
   DrawRepeating( Target, X, Y, W, H, @PANEL_BOTTOM_LEFT, @PANEL_BOTTOM_CENTER, @PANEL_BOTTOM_RIGHT );
end;

procedure TSDLSkin.DrawBitButton(Target: PSDL_Surface; X, Y, W, H: INTEGER;
  Up: BOOLEAN);
VAR
   Counter : INTEGER;
   NewH : INTEGER;
   TopLeftRect : TSDL_RECT;
   TopCenterRect : TSDL_RECT;
   TopRightRect : TSDL_RECT;
   MiddleLeftRect : TSDL_RECT;
   MiddleCenterRect : TSDL_RECT;
   MiddleRightRect : TSDL_RECT;
   BottomLeftRect : TSDL_RECT;
   BottomCenterRect : TSDL_RECT;
   BottomRightRect : TSDL_RECT;
begin
   IF (Target = NIL) THEN
      Exit;

   IF (Up) THEN
   BEGIN
      TopLeftRect := BITBUTTON_TOP_LEFT_UP;
      TopCenterRect := BITBUTTON_TOP_CENTER_UP;
      TopRightRect := BITBUTTON_TOP_RIGHT_UP;
      MiddleLeftRect := BITBUTTON_MIDDLE_LEFT_UP;
      MiddleCenterRect := BITBUTTON_MIDDLE_CENTER_UP;
      MiddleRightRect := BITBUTTON_MIDDLE_RIGHT_UP;
      BottomLeftRect := BITBUTTON_BOTTOM_LEFT_UP;
      BottomCenterRect := BITBUTTON_BOTTOM_CENTER_UP;
      BottomRightRect := BITBUTTON_BOTTOM_RIGHT_UP;
   END
    ELSE
   BEGIN
      TopLeftRect := BITBUTTON_TOP_LEFT_DOWN;
      TopCenterRect := BITBUTTON_TOP_CENTER_DOWN;
      TopRightRect := BITBUTTON_TOP_RIGHT_DOWN;
      MiddleLeftRect := BITBUTTON_MIDDLE_LEFT_DOWN;
      MiddleCenterRect := BITBUTTON_MIDDLE_CENTER_DOWN;
      MiddleRightRect := BITBUTTON_MIDDLE_RIGHT_DOWN;
      BottomLeftRect := BITBUTTON_BOTTOM_LEFT_DOWN;
      BottomCenterRect := BITBUTTON_BOTTOM_CENTER_DOWN;
      BottomRightRect := BITBUTTON_BOTTOM_RIGHT_DOWN;
   END;

   // *** TOP OF BITBUTTON ***
   DrawRepeating( Target, X, Y, W, H, @TopLeftRect, @TopCenterRect, @TopRightRect );

   // *** CENTER OF BITBUTTON ***
   Y := Y + TopCenterRect.h;
   FOR Counter := 1 TO ((H-(TopCenterRect.h + BottomCenterRect.h)) DIV MiddleCenterRect.h) DO
   BEGIN
      DrawRepeating( Target, X, Y, W, H, @MiddleLeftRect, @MiddleCenterRect, @MiddleRightRect );
      Y := Y + MiddleCenterRect.h;
   END;

   NewH := (H-(TopCenterRect.h + BottomCenterRect.h)) MOD MiddleCenterRect.h;
   IF (NewH <> 0) THEN
      DrawRepeating( Target, X, Y, W, H, @MiddleLeftRect, @MiddleCenterRect, @MiddleRightRect, NewH );
   Y := Y + NewH;

   // *** BOTTOM OF BITBUTTON ***
   DrawRepeating( Target, X, Y, W, H, @BottomLeftRect, @BottomCenterRect, @BottomRightRect );
end;

{ TSDLGUI }

// -----------------------------------------------------------------------------
// Class :
//    TSDLGUI
// Method :
//    AddImage
// Description :
//    Adds a supplied image to the collection.
//    To be perfectly honest, I don't know what this is used for anymore.
// Parameters :
//    Name        STRING      User-name of image
//    ImageFile   STRING      Filename of image to add
// Returns :
//    The image that was just added
// Modification History :
//    Author         Date        Modification
//    Todd Lang      01-Dec-2001 Created
//------------------------------------------------------------------------------
function TSDLGUI.AddImage(Name, ImageFile: STRING) : TSDLImage;
VAR
   NewImage : TSDLImage;
begin
   NewImage := FImages.Add;
   NewImage.Name := Name;
   NewImage.Load( ImageFile );

   Result := NewImage;
end;

// -----------------------------------------------------------------------------
// Class :
//    TSDLGUI
// Method :
//    Create
// Description :
//    Creates all of the necessary classes.  FSkins is the collection of skins,
//    FForms is all of the forms for the display.  FFonts is all of the fonts
//    to be used.  FImages - no idea.  FZOrder is a list of the Z-Order of the
//    forms.
// Parameters :
//    None
// Returns :
//    Nothing
// Modification History :
//    Author         Date        Modification
//    Todd Lang      14-Nov-2001 Created
//------------------------------------------------------------------------------
constructor TSDLGUI.Create;
begin
   FSkins := TSDLSkins.Create( TSDLSkin );
   FForms := TSDLForms.Create( TSDLForm );
   FFonts := TSDLFonts.Create( TSDLFont );
   FImages := TSDLImages.Create( TSDLImage );
   FZOrder := TList.Create;
   FHintImage := TSDLImage.Create( NIL );
   FShowToolTips := TRUE;
end;

// -----------------------------------------------------------------------------
// Class :
//    TSDLGUI
// Method :
//    Destroy
// Description :
//    Releases all resources in use.
// Parameters :
//    None
// Returns :
//    Nothing
// Modification History :
//    Author         Date        Modification
//    Todd Lang      14-Nov-2001 Created
//------------------------------------------------------------------------------
destructor TSDLGUI.Destroy;
begin
   FSkins.Free;
   FForms.Free;
   FFonts.Free;
   FImages.Free;
   FZOrder.Free;
   FHintImage.Free;
   inherited;
end;

// -----------------------------------------------------------------------------
// Class :
//    TSDLGUI
// Method :
//    Draw
// Description :
//    High-level function that puts all of the GUI elements to the display.
//    This is the only Draw function that should be called by the user.
// Parameters :
//    Target      PSDL_Surface      Surface to draw the GUI to
// Returns :
//    Nothing
// Modification History :
//    Author         Date        Modification
//    Todd Lang      14-Nov-2001 Created
//------------------------------------------------------------------------------
procedure TSDLGUI.Draw(Target: PSDL_Surface);
VAR
   Counter : INTEGER;
   XPos, YPos : INTEGER;
begin
   // Draw all of the forms in reverse Z-Order
   FOR Counter := (FZOrder.Count-1) DOWNTO 0 DO
      TSDLForm(FZOrder[Counter]).Draw( Target );

   // *** TODO *** Figure out a way to use multiple cursors

   // If there is a skin, draw the cursor.
   IF (FSkin <> NIL) THEN
      FSkin.DrawMouse( Target, FMouseX, FMouseY );

   IF (FCurrentHint <> '') AND (FShowToolTips) THEN
   BEGIN
      IF (FMouseX + FHintImage.Width > Target.w) THEN
         XPos := Target.w - FHintImage.Width
       ELSE
      IF (FMouseX < 0) THEN
         XPos := 0
       ELSE
         XPos := FMouseX;

      IF (FMouseY  > Target.h) THEN
         YPos := Target.h - FHintImage.Height
       ELSE
      IF ((FMouseY - FHintImage.Height) < 0) THEN
         YPos := 0
       ELSE
         YPos := FMouseY - FHintImage.Height;

      FHintImage.Draw( Target, XPos, YPos);
   END;
end;

// -----------------------------------------------------------------------------
// Class :
//    TSDLGUI
// Method :
//    EditFocus
// Description :
//    Called by an edit box when it wants focus.  Should not be called by the
//    user.
// Parameters :
//    AForm       TSDLControl    The form that contains the edit box
//    AEdit       TSDLControl    The editbox that wants focus
// Returns :
//    Nothing
// Modification History :
//    Author         Date        Modification
//    Todd Lang      01-Dec-2001 Created
//------------------------------------------------------------------------------
procedure TSDLGUI.EditFocus(AForm, AEdit: TSDLControl);
VAR
   Counter : INTEGER;
   FForm : TSDLForm;
   FEdit : TSDLEdit;
begin
   // Iterates through all of the form controls looking for all editboxes
   // that are NOT the one that wants focus and takes focus away from them.
   FForm := TSDLForm(AForm);
   FOR Counter := 0 TO (FForm.Controls.Count-1) DO
   BEGIN
      IF (FForm.Controls[Counter] IS TSDLEdit) THEN
      BEGIN
         FEdit := TSDLEdit(FForm.Controls[Counter]);
         IF (FEdit <> AEdit) THEN
            FEdit.LostFocus;
      END;
   END;
end;

// -----------------------------------------------------------------------------
// Class :
//    TSDLGUI
// Method :
//    FormFocus
// Description :
//    Called when a form wants focus.  Causes that form to be moved to the
//    top of the Z-Order list.
// Parameters :
//    AForm       TSDLControl       Form that wants focus
// Returns :
//    Nothing
// Modification History :
//    Author         Date        Modification
//    Todd Lang      22-Dec-2001 Created
//------------------------------------------------------------------------------
procedure TSDLGUI.FormFocus(AForm: TSDLControl);
VAR
   Counter : INTEGER;
begin
   // Set the form that wants focus to the highest possible Z-Order
   TSDLForm(AForm).ZOrder := 0;
   // Sort all of the forms now
   FZOrder.Sort( ZOrderSort );
   // Reset the Z-Order to be based on their current order.  The highest
   // Z-Order possible now will be 1, which means any form that wants focus
   // can always slip in front of it.
   FOR Counter := 0 TO (FZOrder.Count-1) DO
      TSDLForm(FZOrder[Counter]).ZOrder := Counter+1;
end;

// -----------------------------------------------------------------------------
// Class :
//    TSDLGUI
// Method :
//    GroupButtonDown
// Description :
//    Called by buttons to inform the manager that it has been pushed down
//    and any other buttons in its group must be raised.
// Parameters :
//    AForm       TSDLControl    Form that owns the button
//    AButton     TSDLControl    Button that was pushed down
// Returns :
//    Nothing
// Modification History :
//    Author         Date        Modification
//    Todd Lang      14-Nov-2001 Created
//------------------------------------------------------------------------------
procedure TSDLGUI.GroupButtonDown(AForm, AButton: TSDLControl; GroupIndex: INTEGER);
VAR
   Counter : INTEGER;
   FForm : TSDLForm;
   FButton : TSDLCustomButton;
begin
   // Iterate through all of the controls on the form looking for buttons
   // that are in the group of the button that was pushed down.  If it finds
   // any, it raises them.
   FForm := TSDLForm(AForm);
   FOR Counter := 0 TO (FForm.Controls.Count-1) DO
   BEGIN
      IF (FForm.Controls[Counter] IS TSDLCustomButton) THEN
      BEGIN
         FButton := TSDLCustomButton(FForm.Controls[Counter]);
         IF (FButton.Group = GroupIndex) AND (FButton <> AButton) THEN
         BEGIN
            FButton.State := bsUp;
         END;
      END;
   END;
end;

procedure TSDLGUI.CheckGroupChange(AForm, ACheck: TSDLControl;
  GroupIndex: INTEGER);
VAR
   Counter : INTEGER;
   FForm : TSDLForm;
   FCheck : TSDLCheckControl;
begin
   FForm := TSDLForm(AForm);
   FOR Counter := 0 TO (FForm.Controls.Count-1) DO
   BEGIN
      IF (FForm.Controls[Counter] IS TSDLCheckControl) THEN
      BEGIN
         FCheck := TSDLCheckControl(FForm.Controls[Counter]);
         IF (FCheck.Group = GroupIndex) AND (FCheck <> ACheck) THEN
         BEGIN
            FCheck.Checked := FALSE;
         END;
      END;
   END;
end;

function TSDLGUI.ButtonGroupCount( AForm : TSDLControl; GroupIndex : INTEGER ) : INTEGER;
VAR
   FForm : TSDLForm;
   Counter : INTEGER;
begin
   Result := 0;
   FForm := TSDLForm(AForm);
   FOR Counter := 0 TO (FForm.Controls.Count-1) DO
   BEGIN
      IF (FForm.Controls[Counter] IS TSDLCustomButton) THEN
      BEGIN
         IF (TSDLCustomButton(FForm.Controls[Counter]).Group = GroupIndex) THEN
            INC(Result);
      END;
   END;
end;

function TSDLGUI.CheckGroupCount(AForm: TSDLControl; GroupIndex: INTEGER): INTEGER;
VAR
   FForm : TSDLForm;
   Counter : INTEGER;
begin
   Result := 0;
   FForm := TSDLForm(AForm);
   FOR Counter := 0 TO (FForm.Controls.Count-1) DO
   BEGIN
      IF (FForm.Controls[Counter] IS TSDLCheckControl) THEN
      BEGIN
         IF (TSDLCheckControl(FForm.Controls[Counter]).Group = GroupIndex) THEN
            INC(Result);
      END;
   END;
end;

// -----------------------------------------------------------------------------
// Class :
//    TSDLGUI
// Method :
//    Initialize
// Description :
//    Propagates the initialize throughout the GUI subsystem.  This used to be
//    much more important when controls needed to be "compiled" for the surface.
//    This is largely superfluous now, but is still here for any future need.
// Parameters :
//    None
// Returns :
//    Nothing
// Modification History :
//    Author         Date        Modification
//    Todd Lang      14-Nov-2001 Created
//------------------------------------------------------------------------------
procedure TSDLGUI.Initialize;
VAR
   Counter : INTEGER;
begin
   FForms.Initialize( Self );
   FOR Counter := 0 TO (FForms.Count-1) DO
      FZOrder.Add( FForms[Counter] );
end;

// -----------------------------------------------------------------------------
// Class :
//    TSDLGUI
// Method :
//    KeyDown
// Description :
//    Propagates a key-down event to the GUI subsystem.
// Parameters :
//    KeyCode     TSDLKey     Key that was pushed
//    Modifier    TSDLMod     Modifier to the key that was pushed
// Returns :
//    Nothing
// Modification History :
//    Author         Date        Modification
//    Todd Lang      14-Nov-2001 Created
//------------------------------------------------------------------------------
procedure TSDLGUI.KeyDown(KeyCode: TSDLKey; Modifier : TSDLMod);
VAR
   Handled : BOOLEAN;
   Counter : INTEGER;
begin
   Handled := FALSE;
   // Propagate the event to the forms in Z-Order
   FOR Counter := 0 TO (FZOrder.Count-1) DO
      TSDLForm(FZOrder[Counter]).KeyDown( KeyCode, Modifier, Handled );
end;

// -----------------------------------------------------------------------------
// Class :
//    TSDLGUI
// Method :
//    KeyUp
// Description :
//    Propagates a key-up event to the GUI subsystem.
// Parameters :
//    KeyCode     TSDLKey     Key that was pushed
//    Modifier    TSDLMod     Modifier to the key that was pushed
// Returns :
//    Nothing
// Modification History :
//    Author         Date        Modification
//    Todd Lang      14-Nov-2001 Created
//------------------------------------------------------------------------------
procedure TSDLGUI.KeyUp(KeyCode: TSDLKey; Modifier : TSDLMod);
VAR
   Handled : BOOLEAN;
   Counter : INTEGER;
begin
   Handled := FALSE;
   // Propagate the event to the forms in Z-Order
   FOR Counter := 0 TO (FZOrder.Count-1) DO
      TSDLForm(FZOrder[Counter]).KeyUp( KeyCode, Modifier, Handled );
end;

// -----------------------------------------------------------------------------
// Class :
//    TSDLGUI
// Method :
//    MouseDown
// Description :
//    Propagates a mouse-down event to the GUI subsystem.
// Parameters :
//    X        INTEGER     X-location of the mouse-down event
//    Y        INTEGER     Y-location of the mouse-down event
//    Button   INTEGER     Button that was pushed.  (1 = Left, 2 = Right, etc)
// Returns :
//    Nothing
// Modification History :
//    Author         Date        Modification
//    Todd Lang      14-Nov-2001 Created
//------------------------------------------------------------------------------
procedure TSDLGUI.MouseDown(X, Y, Button: INTEGER);
VAR
   Counter : INTEGER;
   Handled : BOOLEAN;
begin
   IF (Button <= 3) THEN
   BEGIN
      FMouseX := X;
      FMouseY := Y;
      Handled := FALSE;
      // Propagate the event to the forms in Z-Order
      FOR Counter := 0 TO (FZOrder.Count-1) DO
         TSDLForm(FZOrder[Counter]).MouseDown( X, Y, Button, Handled );
   END;
end;

// -----------------------------------------------------------------------------
// Class :
//    TSDLGUI
// Method :
//    MouseMove
// Description :
//    Propagates a mouse-move event to the GUI subsystem.
// Parameters :
//    X        INTEGER     X-location of the mouse
//    Y        INTEGER     Y-location of the mouse
// Returns :
//    Nothing
// Modification History :
//    Author         Date        Modification
//    Todd Lang      14-Nov-2001 Created
//------------------------------------------------------------------------------
procedure TSDLGUI.MouseMove(X, Y : INTEGER);
VAR
   DX, DY : INTEGER;
   Handled : BOOLEAN;
   Counter : INTEGER;
begin
   DX := FMouseX - X;
   DY := FMouseY - Y;
   FMouseX := X;
   FMouseY := Y;
   Handled := FALSE;
   // Propagate the event to the forms in Z-Order
   FOR Counter := 0 TO (FZOrder.Count-1) DO
      TSDLForm(FZOrder[Counter]).MouseMove( X, Y, DX, DY, Handled );
end;

// -----------------------------------------------------------------------------
// Class :
//    TSDLGUI
// Method :
//    MouseUp
// Description :
//    Propagates a mouse-up event to the GUI subsystem.
// Parameters :
//    X        INTEGER     X-location of the mouse-up event
//    Y        INTEGER     Y-location of the mouse-up event
//    Button   INTEGER     Button that was released.  (1 = Left, 2 = Right, etc)
// Returns :
//    Nothing
// Modification History :
//    Author         Date        Modification
//    Todd Lang      14-Nov-2001 Created
//------------------------------------------------------------------------------
procedure TSDLGUI.MouseUp(X, Y, Button: INTEGER);
VAR
   Counter : INTEGER;
   Handled : BOOLEAN;
begin
   IF (Button <= 3) THEN
   BEGIN
      FMouseX := X;
      FMouseY := Y;
      Handled := FALSE;
      // Propagate the event to the forms in Z-Order
      FOR Counter := 0 TO (FZOrder.Count-1) DO
         TSDLForm(FZOrder[Counter]).MouseUp( X, Y, Button, Handled );
   END;
end;

procedure TSDLGUI.MouseWheelDown;
VAR
   Handled : BOOLEAN;
   Counter : INTEGER;
begin
   Handled := FALSE;
   FOR Counter := 0 TO (FZOrder.Count-1) DO
      TSDLForm(FZOrder[Counter]).MouseWheelDown( Handled );
end;

procedure TSDLGUI.MouseWheelUp;
VAR
   Handled : BOOLEAN;
   Counter : INTEGER;
begin
   Handled := FALSE;
   FOR Counter := 0 TO (FZOrder.Count-1) DO
      TSDLForm(FZOrder[Counter]).MouseWheelUp( Handled );
end;

// -----------------------------------------------------------------------------
// Class :
//    TSDLGUI
// Method :
//    SetSkin
// Description :
//    Sets the skin that determines the "Default" appearance of the controls.
//    *** NOTE ** This is mostly used to determine which skin defines the cursor
//    for the display.  Currently, that's ALL it does.
// Parameters :
//    Name     STRING      User-name of the skin to use
// Returns :
//    Nothing
// Modification History :
//    Author         Date        Modification
//    Todd Lang      14-Nov-2001 Created
//------------------------------------------------------------------------------
procedure TSDLGUI.SetHint(Sender : TSDLControl; const Value: STRING);
begin
   IF (Value = '') AND (Sender <> NIL) THEN
   BEGIN
      IF (Sender = FCurrentHintOwner) THEN
         FCurrentHint := '';
   END
    ELSE
   BEGIN
      IF (Value <> FCurrentHint) OR (FCurrentHintOwner = Sender) THEN
      BEGIN
         FCurrentHint := Value;
         FCurrentHintOwner := Sender;
         FHintImage.Alloc( FFonts[0].TextWidth( Value ), FFonts[0].TextHeight( Value ), 32 );
         FFonts[0].TextOut( FHintImage.Surface, 0, 0, Value );
      END;
   END;
end;

procedure TSDLGUI.SetSkin(Name: STRING);
begin
   FSkin := FSkins.ByName(Name);
end;

// -----------------------------------------------------------------------------
// Class :
//    TSDLGUI
// Method :
//    Show
// Description :
//    Makes the specified form visible
// Parameters :
//    Name     STRING      User-name of the form to make visible
// Returns :
//    Nothing
// Modification History :
//    Author         Date        Modification
//    Todd Lang      14-Nov-2001 Created
//------------------------------------------------------------------------------
procedure TSDLGUI.Show(FormName: STRING);
begin
   FForms.ByName(FormName).Visible := TRUE;
   FormFocus( FForms.ByName(FormName) );
end;

procedure TSDLGUI.ShowModal(FormName: STRING);
begin
   Show( FormName );
   FForms.ByName(FormName).Modal := TRUE;
   SetHint( NIL, '' );
end;

procedure TSDLGUI.Hide(FormName: STRING);
begin
   Hide( FForms.ByName(FormName) );
end;

procedure TSDLGUI.Hide(SDLForm: TSDLForm);
begin
   SDLForm.Visible := FALSE;
   SDLForm.Modal := FALSE;
end;

// -----------------------------------------------------------------------------
// Class :
//    TSDLGUI
// Method :
//    Terminate
// Description :
//    This is provided to all controls as a means to control program
//    termination.  The Quit flag should be polled to determine if the GUI
//    thinks the program should fold.
// Parameters :
//    None
// Returns :
//    Nothing
// Modification History :
//    Author         Date        Modification
//    Todd Lang      14-Nov-2001 Created
//------------------------------------------------------------------------------
procedure TSDLGUI.Terminate;
begin
   FQuit := TRUE;
end;

// -----------------------------------------------------------------------------
//    Debug-aid function.  Displays some text on the surface using the
//    first font in the list.
//------------------------------------------------------------------------------
procedure TSDLGUI.TextOut(Target : PSDL_Surface; X, Y: INTEGER; Text: STRING);
begin
   FFonts[0].TextOut( Target, X, Y, Text );
end;

// -----------------------------------------------------------------------------
// Class :
//    TSDLGUI
// Method :
//    Tick
// Description :
//    Propagates a tick throughout the GUI subsystem.  This is needed for
//    scrollbars to scroll while you hold a button down, also allows the
//    cursor to flash in edit boxes, etc.
// Parameters :
//    None
// Returns :
//    Nothing
// Modification History :
//    Author         Date        Modification
//    Todd Lang      14-Nov-2001 Created
//------------------------------------------------------------------------------
procedure TSDLGUI.Tick;
begin
   FForms.Tick;
end;

{ TSDLSkins }
// This is just a collection and involves nothing worth documenting

function TSDLSkins.Add: TSDLSkin;
begin
   Result := TSDLSkin(INHERITED Add);
end;

function TSDLSkins.ByName(Name: STRING): TSDLSkin;
VAR
   Counter : INTEGER;
begin
   FOR Counter := 0 TO (Count-1) DO
   BEGIN
      IF (CompareText(Items[Counter].Name, Name) = 0) THEN
      BEGIN
         Result := Items[Counter];
         Exit;
      END;
   END;
   Result := NIL;
end;

function TSDLSkins.GetItem(Index: INTEGER): TSDLSkin;
begin
   Result := TSDLSkin(INHERITED GetItem( Index ));
end;

function TSDLSkins.Insert(Index: INTEGER): TSDLSkin;
begin
   Result := TSDLSkin(INHERITED Insert( Index ));
end;

procedure TSDLSkins.SetItem(Index: INTEGER; const Value: TSDLSkin);
begin
   INHERITED SetItem( Index, Value );
end;

{ TSDLForms }

// -----------------------------------------------------------------------------
// Class :
//    TSDLForms
// Method :
//    Add
// Description :
//    Adds a form to the collection.  Returns the new form to the caller.
//    This is NOT the recommended method for adding a form since you would
//    be required call a few routines and set a number of properties manually.
// Parameters :
//    None
// Returns :
//    An instance of a new form.
// Modification History :
//    Author         Date        Modification
//    Todd Lang      22-Dec-2001 Created
//------------------------------------------------------------------------------
function TSDLForms.Add: TSDLForm;
begin
   Result := TSDLForm(INHERITED Add);
   Result.ZOrder := Count+1;
end;

// -----------------------------------------------------------------------------
// Class :
//    TSDLForms
// Method :
//    Add
// Description :
//    This is the general method you would use to add a form to the collection.
//    This instantiates a copy of the class in the FormVar and sets various
//    properties for you.  It also adds it to the collection directly.
// Parameters :
//    FormClass      TSDLFormClass     The class of the form to add
//    Name           STRING            User-name of the form
//    FormVar        TSDLForm          Instantiated form variable
// Returns :
//    An instance of a new form in FormVar.
// Modification History :
//    Author         Date        Modification
//    Todd Lang      22-Dec-2001 Created
//------------------------------------------------------------------------------
procedure TSDLForms.Add(FormClass: TSDLFormClass; Name : STRING; VAR FormVar : TSDLForm);
VAR
   FForm : TSDLForm;
begin
   FForm := FormClass.Create( Self );
   FForm.Name := Name;
   FForm.Visible := FALSE;
   FForm.ZOrder := Count+1;
   FormVar := FForm;
end;

function TSDLForms.ByName(const Name: STRING): TSDLForm;
VAR
   Counter : INTEGER;
begin
   FOR Counter := 0 TO (Count-1) DO
   BEGIN
      IF (CompareText(Items[Counter].Name, Name) = 0) THEN
      BEGIN
         Result := Items[Counter];
         Exit;
      END;
   END;
   Result := NIL;
end;

procedure TSDLForms.Draw(Target: PSDL_Surface);
VAR
   Counter : INTEGER;
begin
   FOR Counter := 0 TO (Count-1) DO
   BEGIN
      IF (Items[Counter].Visible) THEN
         Items[Counter].Draw( Target );
   END;
end;

function TSDLForms.GetItem(Index: INTEGER): TSDLForm;
begin
   Result := TSDLForm(INHERITED GetItem( Index ));
end;

procedure TSDLForms.Initialize(GUI: TSDLGUI);
VAR
   Counter : INTEGER;
begin
   FOR Counter := 0 TO (Count-1) DO
      Items[Counter].Initialize( GUI );
end;

function TSDLForms.Insert(Index: INTEGER): TSDLForm;
begin
   Result := TSDLForm(INHERITED Insert( Index ));
   Result.ZOrder := Count+1;
end;

procedure TSDLForms.SetItem(Index: INTEGER; const Value: TSDLForm);
begin
   INHERITED SetItem( Index, Value );
end;

procedure TSDLForms.Tick;
VAR
   Counter : INTEGER;
begin
   FOR Counter := 0 TO (Count-1) DO
      Items[Counter].Tick;
end;

{ TSDLForm }

// -----------------------------------------------------------------------------
// Class :
//    TSDLForm
// Method :
//    AddLabel
// Description :
//    Adds a label to the form.  All controls should be added to a form using
//    the helper functions that the form provides.  This ensures all necessary
//    properties are set and in the required order.
// Parameters :
//    Name        STRING      User-name of the control
//    Left        INTEGER     X-position of the label
//    Top         INTEGER     Y-position of the label
//    Caption     STRING      Caption of the label
//    FontName    STRING      User-name of the font to use
// Returns :
//    An instance of the new label.
// Modification History :
//    Author         Date        Modification
//    Todd Lang      22-Dec-2001 Created
//------------------------------------------------------------------------------
function TSDLForm.AddLabel( Name : STRING; Left, Top : INTEGER; Caption, FontName : STRING ) : TSDLLabel;
begin
   Result := TSDLLabel.Create( FControls );
   Result.Skin := Skin;
   Result.Font := GUI.Fonts.ByName(FontName);
   Result.Left := Left;
   Result.Top := Top;
   Result.Owner := Self;
   Result.Caption := Caption;
   Result.Name := Name;
   Result.Initialize( GUI );
end;

// -----------------------------------------------------------------------------
// Class :
//    TSDLForm
// Method :
//    AddButton
// Description :
//    Adds a button to the form.  All controls should be added to a form using
//    the helper functions that the form provides.  This ensures all necessary
//    properties are set and in the required order.
// Parameters :
//    Name        STRING      User-name of the control
//    Left        INTEGER     X-position of the label
//    Top         INTEGER     Y-position of the label
//    Width       INTEGER     Width of the button
//    Caption     STRING      Caption of the button
//    FontName    STRING      User-name of the font to use
// Returns :
//    An instance of the new bitbutton.
// Modification History :
//    Author         Date        Modification
//    Todd Lang      16-May-2002 Created
//------------------------------------------------------------------------------
function TSDLForm.AddButton(Name : STRING; Left, Top, Width, Height: INTEGER; Caption, FontName : STRING; OnClick : TSDLNotifyEvent) : TSDLButton;
begin
   Result := TSDLButton.Create( FControls );
   Result.Skin := Skin;
   Result.Owner := Self;
   Result.Left := Left;
   Result.Top := Top;
   Result.Width := Width;
   Result.Height := Height;
   Result.Font := GUI.Fonts.ByName(FontName);
   Result.Caption := Caption;
   Result.OnClick := OnClick;
   Result.Name := Name;
   Result.Initialize( GUI );
end;

// -----------------------------------------------------------------------------
// Class :
//    TSDLForm
// Method :
//    AddBitButton
// Description :
//    Adds a bitbutton to the form.  All controls should be added to a form using
//    the helper functions that the form provides.  This ensures all necessary
//    properties are set and in the required order.
// Parameters :
//    Name        STRING      User-name of the control
//    Left        INTEGER     X-position of the label
//    Top         INTEGER     Y-position of the label
//    Width       INTEGER     Width of the button
//    Caption     STRING      Caption of the button
//    FontName    STRING      User-name of the font to use
// Returns :
//    An instance of the new button.
// Modification History :
//    Author         Date        Modification
//    Todd Lang      22-Dec-2001 Created
//------------------------------------------------------------------------------
function TSDLForm.AddBitButton(Name, Hint: STRING; Left, Top, Width,
  Height: INTEGER; Image: TSDLImage; OnClick : TSDLNotifyEvent ): TSDLBitButton;
begin
   Result := TSDLBitButton.Create( FControls );
   Result.Skin := Skin;
   Result.Owner := Self;
   Result.Left := Left;
   Result.Top := Top;
   Result.Width := Width;
   Result.Height := Height;
   Result.Image := Image;
   Result.Name := Name;
   Result.Hint := Hint;
   Result.OnClick := OnClick;
   Result.Initialize( GUI );
end;

constructor TSDLForm.Create(Collection: TCollection);
begin
   inherited;
   FControls := TSDLControls.Create( TSDLControl );
   FCanFocus := TRUE;
   FCanDrag := FALSE;
end;

destructor TSDLForm.Destroy;
begin
   FControls.Free;
   inherited;
end;

procedure TSDLForm.Draw;
begin
   inherited;
   IF (FVisible) THEN
   BEGIN
      IF (FImage <> NIL) THEN
         FImage.Draw( Target, FLeft, FTop );
      FControls.Draw( Target );
      CustomDraw( Target );
   END;
end;

procedure TSDLForm.CustomDraw(Target: PSDL_Surface);
begin
   // Do nothing
end;

function TSDLForm.GetHeight: INTEGER;
begin
   IF (FImage <> NIL) THEN
      Result := FImage.Height
    ELSE
      Result := 0;
end;

function TSDLForm.GetWidth: INTEGER;
begin
   IF (FImage <> NIL) THEN
      Result := FImage.Width
    ELSE
      Result := 0;
end;

procedure TSDLForm.MouseDown(X, Y, Button: INTEGER; var Handled: BOOLEAN);
begin
   inherited;
   IF (Enabled) AND (Visible) THEN
   BEGIN
      X := X - FLeft;
      Y := Y - FTop;
      IF (X > 0) AND (X < Width) AND (Y > 0) AND (Y < Height) THEN
      BEGIN
         FControls.MouseDown( X, Y, Button, Handled );
         IF (Handled = FALSE) AND (Button = 1) THEN
         BEGIN
            IF (FCanFocus) THEN
               FGUI.FormFocus( Self );
            Handled := TRUE;
            FDragging := TRUE;
         END;
      END;
   END;
   IF (Modal) THEN
      Handled := TRUE;
end;

procedure TSDLForm.MouseMove(X, Y, DX, DY: INTEGER; var Handled: BOOLEAN);
begin
   inherited;
   IF (Enabled) AND (Visible) THEN
   BEGIN
      X := X - FLeft;
      Y := Y - FTop;
      FControls.MouseMove( X, Y, DX, DY, Handled );
      IF (FDragging) AND (FCanDrag) THEN
      BEGIN
         FLeft := FLeft - DX;
         FTop := FTop - DY;
      END;
   END;
   IF (Modal) THEN
      Handled := TRUE;
end;

procedure TSDLForm.MouseUp(X, Y, Button: INTEGER; var Handled: BOOLEAN);
begin
   inherited;
   IF (Enabled) AND (Visible) THEN
   BEGIN
      X := X - FLeft;
      Y := Y - FTop;
      FDragging := FALSE;
      FControls.MouseUp( X, Y, Button, Handled );
      // If the mouse-up event falls somewhere on the form, eat the event
      IF (X > 0) AND (X < Width) AND (Y > 0) AND (Y < Height) THEN
         Handled := TRUE;
   END;
   IF (Modal) THEN
      Handled := TRUE;
end;

procedure TSDLForm.MouseWheelDown(var Handled: BOOLEAN);
begin
   inherited;
   IF (Enabled) AND (Visible) THEN
      FControls.MouseWheelDown( Handled );
   IF (Modal) THEN
      Handled := TRUE;
end;

procedure TSDLForm.MouseWheelUp(var Handled: BOOLEAN);
begin
   inherited;
   IF (Enabled) AND (Visible) THEN
      FControls.MouseWheelUp( Handled );
   IF (Modal) THEN
      Handled := TRUE;
end;

// -----------------------------------------------------------------------------
// Class :
//    TSDLForm
// Method :
//    AddScrollbar
// Description :
//    Adds a scrollbar to the form.  All controls should be added to a form
//    using the helper functions that the form provides.  This ensures all
//    necessary properties are set and in the required order.
// Parameters :
//    Name        STRING            User-name of the control
//    Left        INTEGER           X-position of the label
//    Top         INTEGER           Y-position of the label
//    Height      INTEGER           Height of the scrollbar
//    Min         INTEGER           Minimum value of the scrollbar
//    Max         INTEGER           Maximum value of the scrollbar
//    Pos         INTEGER           Position the scrollbar should be started at
//    OnChange    TSDLNotifyEvent   Event to call if the value changes
// Returns :
//    An instance of the new scrollbar.
// Modification History :
//    Author         Date        Modification
//    Todd Lang      22-Dec-2001 Created
//------------------------------------------------------------------------------
function TSDLForm.AddScrollbar(Name : STRING; Left, Top, Height, Min, Max, Pos: INTEGER; OnChange : TSDLNotifyEvent): TSDLScrollbar;
begin
   Result := TSDLScrollbar.Create( FControls );
   Result.Skin := Skin;
   Result.Owner := Self;
   Result.Height := Height;
   Result.Left := Left;
   Result.Top := Top;
   Result.Minimum := Min;
   Result.Maximum := Max;
   Result.Position := Pos;
   Result.OnChange := OnChange;
   Result.Name := Name;
   Result.Initialize( GUI );
end;

procedure TSDLForm.Tick;
begin
   inherited;
   FControls.Tick;
end;

procedure TSDLForm.Initialize(GUI: TSDLGUI);
begin
   inherited;
   FControls.Initialize( GUI );
end;

// -----------------------------------------------------------------------------
// Class :
//    TSDLForm
// Method :
//    AddEdit
// Description :
//    Adds a editbox to the form.  All controls should be added to a form using
//    the helper functions that the form provides.  This ensures all necessary
//    properties are set and in the required order.
// Parameters :
//    Name        STRING      User-name of the control
//    Left        INTEGER     X-position of the label
//    Top         INTEGER     Y-position of the label
//    Width       INTEGER     Width of the editbox
//    Value       STRING      Text in the box
//    FontName    STRING      User-name of the font to use
// Returns :
//    An instance of the new editbox.
// Modification History :
//    Author         Date        Modification
//    Todd Lang      22-Dec-2001 Created
//------------------------------------------------------------------------------
function TSDLForm.AddEdit(Name : STRING; Left, Top, Width, Height: INTEGER; Value, FontName: STRING): TSDLEdit;
begin
   Result := TSDLEdit.Create( FControls );
   Result.Skin := Skin;
   Result.Owner := Self;
   Result.Font := GUI.Fonts.ByName(FontName);
   Result.Width := Width;
   Result.Height := Height;
   Result.Left := Left;
   Result.Top := Top;
   Result.Value := Value;
   Result.Name := Name;
   Result.Initialize( GUI );
end;

procedure TSDLForm.KeyDown(KeyCode: TSDLKey; Modifier : TSDLMod; var Handled: BOOLEAN);
begin
   inherited;
   IF (Enabled) THEN
      FControls.KeyDown( KeyCode, Modifier, Handled );
end;

procedure TSDLForm.KeyUp(KeyCode: TSDLKey; Modifier : TSDLMod; var Handled: BOOLEAN);
begin
   inherited;
   IF (Enabled) THEN
      FControls.KeyUp( KeyCode, Modifier, Handled );
end;

// -----------------------------------------------------------------------------
// Class :
//    TSDLForm
// Method :
//    AddListbox
// Description :
//    Adds a listbox to the form.  All controls should be added to a form using
//    the helper functions that the form provides.  This ensures all necessary
//    properties are set and in the required order.
// Parameters :
//    Name        STRING      User-name of the control
//    Left        INTEGER     X-position of the label
//    Top         INTEGER     Y-position of the label
//    Width       INTEGER     Width of the listbox
//    Height      INTEGER     Height of the listbox
//    FontName    STRING      User-name of the font to use
// Returns :
//    An instance of the new listbox.
// Modification History :
//    Author         Date        Modification
//    Todd Lang      22-Dec-2001 Created
//------------------------------------------------------------------------------
function TSDLForm.AddListbox(Name : STRING; Left, Top, Width, Height: INTEGER; FontName: STRING): TSDLListBox;
begin
   Result := TSDLListBox.Create( FControls );
   Result.Skin := Skin;
   Result.Owner := Self;
   Result.Font := GUI.Fonts.ByName(FontName);
   Result.Height := Height;
   Result.Top := Top;
   Result.Width := Width;
   Result.Left := Left;
   Result.Name := Name;
   Result.Initialize( GUI );
end;

function TSDLForm.AddMeter(Name: STRING; Left, Top, Width, Height: INTEGER;
  Caption, FontName : STRING; Colour: CARDINAL; Min, Max, Position: INTEGER): TSDLMeter;
begin
   Result := TSDLMeter.Create( FControls );
   Result.Skin := Skin;
   Result.Owner := Self;
   Result.Width := Width;
   Result.Height := Height;
   Result.Top := Top;
   Result.Left := Left;
   Result.Name := Name;
   Result.Colour := Colour;
   Result.Minimum := Min;
   Result.Maximum := Max;
   Result.Position := Position;
   Result.Font := GUI.Fonts.ByName(FontName);
   Result.Caption := Caption;
   Result.Initialize( GUI );
end;

function TSDLForm.AddRadioButton(Name: STRING; Left, Top: INTEGER; Checked: BOOLEAN; Caption, FontName : STRING): TSDLRadioButton;
begin
   Result := TSDLRadioButton.Create( FControls );
   Result.Skin := Skin;
   Result.Owner := Self;
   Result.Left := Left;
   Result.Top := Top;
   Result.Font := GUI.Fonts.ByName(FontName);
   Result.Caption := Caption;
   Result.Checked := Checked;
   Result.Initialize( GUI );
end;

{ TSDLCustomButton }

// -----------------------------------------------------------------------------
// Class :
//    TSDLCustomButton
// Method :
//    MouseDown
// Description :
//    Handles a mouse-down event for the button.  If it's part of a group, it
//    puts the button down and signals to raise the others.
// Parameters :
//    X        INTEGER     X-location of the event
//    Y        INTEGER     Y-location of the event
//    Button   INTEGER     Button that was pushed
//    Handled  BOOLEAN     Signals if the event has already been handled
// Returns :
//    The state of the message in Handled.  If it handled the event, it sets
//    Handled to TRUE, otherwise it leaves the state unchanged.
// Modification History :
//    Author         Date        Modification
//    Todd Lang      14-Nov-2001 Created
//------------------------------------------------------------------------------
procedure TSDLCustomButton.MouseDown(X, Y, Button: INTEGER; var Handled: BOOLEAN);
begin
   inherited;
   IF (Enabled) THEN
   BEGIN
      // Determine if it falls on the button and if it's been handled yet
      IF (X > FLeft) AND (X < (FLeft + GetWidth)) AND (Y > FTop) AND (Y < (FTop + GetHeight)) AND (Handled = FALSE) THEN
      BEGIN
         Handled := TRUE;
         // If it is part of a group and it's not down...
         IF ((GUI.ButtonGroupCount( Owner, Group ) = 1) AND (State = bsDown)) THEN
         BEGIN
            State := bsUp;
            DrawState := bsUp;
            IF ASSIGNED(FOnClick) THEN
               OnClick( Self );
         END
          ELSE
         BEGIN
            IF (Group <> 0) AND (State <> bsDown) THEN
            BEGIN
               // Signal that others in the group should be raised
               GUI.GroupButtonDown( Owner, Self, Group );
               IF ASSIGNED(FOnClick) THEN
                  OnClick( Self );
            END
             ELSE
            BEGIN
               IF ASSIGNED(FOnDown) THEN
                  OnDown( Self );
            END;
            // Show it down, and mark it down.  (DrawState and actual State are separate)
            State := bsDown;
            DrawState := bsDown;
         END;
      END;
   END;
end;

// -----------------------------------------------------------------------------
// Class :
//    TSDLCustomButton
// Method :
//    MouseMove
// Description :
//    Handles a mouse-event event for the button.
// Parameters :
//    X        INTEGER     X-location of the event
//    Y        INTEGER     Y-location of the event
//    DX       INTEGER     Amount the mouse moved since last event
//    DY       INTEGER     Amount the mouse moved since last event
//    Handled  BOOLEAN     Signals if the event has already been handled
// Returns :
//    The state of the message in Handled.  If it handled the event, it sets
//    Handled to TRUE, otherwise it leaves the state unchanged.
// Modification History :
//    Author         Date        Modification
//    Todd Lang      14-Nov-2001 Created
//------------------------------------------------------------------------------
procedure TSDLCustomButton.MouseMove(X, Y, DX, DY: INTEGER;
  var Handled: BOOLEAN);
begin
   inherited;
   IF (Enabled) THEN
   BEGIN
      // If the mouse moves away from the button, but the state is down, then
      // draw it as up
      IF ((X < FLeft) OR (X > (FLeft + GetWidth)) OR (Y < FTop) OR (Y > (FTop + GetHeight))) AND (FState = bsDown) AND (Group = 0) THEN
      BEGIN
         DrawState := bsUp;
      END
       ELSE
      // If the mouse moves on to the button, show it as down if it is actually down
      IF (X > FLeft) AND (X < (FLeft + GetWidth)) AND (Y > FTop) AND (Y < (FTop + GetHeight)) AND (FState = bsDown) AND (Group = 0) THEN
      BEGIN
         DrawState := bsDown;
      END;
   END;
end;

// -----------------------------------------------------------------------------
// Class :
//    TSDLCustomButton
// Method :
//    MouseUp
// Description :
//    Handles a mouse-up event for the button.
// Parameters :
//    X        INTEGER     X-location of the event
//    Y        INTEGER     Y-location of the event
//    Button   INTEGER     Button that was pushed
//    Handled  BOOLEAN     Signals if the event has already been handled
// Returns :
//    The state of the message in Handled.  If it handled the event, it sets
//    Handled to TRUE, otherwise it leaves the state unchanged.
// Modification History :
//    Author         Date        Modification
//    Todd Lang      14-Nov-2001 Created
//------------------------------------------------------------------------------
procedure TSDLCustomButton.MouseUp(X, Y, Button: INTEGER;
  var Handled: BOOLEAN);
begin
   inherited;
   IF (Enabled) THEN
   BEGIN
      // Determine if the event falls on the button and if it's been handled
      IF (X > FLeft) AND (X < (FLeft + GetWidth)) AND (Y > FTop) AND (Y < (FTop + GetHeight)) AND (Handled = FALSE) THEN
      BEGIN
         // If it was down previously, raise it and call the event
         Handled := TRUE;
         IF ASSIGNED(FOnClick) AND (State = bsDown) AND (Group = 0) THEN
            OnClick( Self )
          ELSE
         IF ASSIGNED(FOnUp) THEN
            OnUp( Self );

         IF (Group <> 0) THEN
         BEGIN
            Handled := TRUE;
            // Nothing else needs be done if it's in a group since the "down"
            // action of another button raised this one
         END
      END;
      // If it's not part of a group, raise the button
      IF (Group = 0) THEN
      BEGIN
         State := bsUp;
         DrawState := bsUp;
      END;
   END;
end;

procedure TSDLCustomButton.SetDrawState(const Value: TButtonState);
begin
   FDrawState := Value;
end;

procedure TSDLCustomButton.SetState(CONST Value : TButtonState);
begin
   FState := Value;
   DrawState := Value;
end;

{ TSDLButton }

constructor TSDLButton.Create(Collection: TCollection);
begin
   inherited;
   FLabel := TSDLLabel.Create( NIL );
   FUpImage := TSDLImage.Create( NIL );
   FDownImage := TSDLImage.Create( NIL );
end;

destructor TSDLButton.Destroy;
begin
   FLabel.Free;
   FUpImage.Free;
   FDownImage.Free;
   inherited;
end;

procedure TSDLButton.Draw(Target: PSDL_Surface);
begin
   IF (FDrawState = bsUp) THEN
   BEGIN
      IF (FUpImage.Draw( Target, FOwner.Left + FLeft, FOwner.Top + FTop ) = -2) THEN
         ComposeImage;
   END
    ELSE
   BEGIN
      IF (FDownImage.Draw( Target, FOwner.Left + FLeft, FOwner.Top + FTop ) = -2) THEN
         ComposeImage;
   END;
   FLabel.Draw( Target )
end;

function TSDLButton.GetHeight: INTEGER;
begin
   Result := FHeight;
end;

function TSDLButton.GetWidth: INTEGER;
begin
   Result := FWidth;
end;

// -----------------------------------------------------------------------------
// Class :
//    TSDLButton
// Method :
//    SetCaption
// Description :
//    Commits the Value to the caption of the button.  This sets the caption
//    of an internal TSDLLabel and repositions the label to be centered on the
//    button.
// Parameters :
//    Value    STRING      Caption to assign
// Returns :
//    Nothing
// Modification History :
//    Author         Date        Modification
//    Todd Lang      14-Nov-2001 Created
//------------------------------------------------------------------------------
procedure TSDLButton.SetCaption(const Value: STRING);
VAR
   X, Y : INTEGER;
begin
   // Set the new caption for the button and the internal label
   FCaption := Value;
   FLabel.Caption := Value;
   // Center the label with the new caption
   X := FOwner.Left + FLeft + ((GetWidth DIV 2) - (Font.TextWidth(FCaption) DIV 2));
   Y := FOwner.Top + FTop + ((GetHeight DIV 2) - (Font.TextHeight(FCaption) DIV 2));
   FLabel.Left := X-1;
   FLabel.Top := Y-1;
end;

procedure TSDLButton.SetFont(const Value: TSDLFont);
begin
   inherited;
   FLabel.Font := Value;
end;

procedure TSDLButton.SetOwner(const Value: TSDLForm);
begin
   inherited;
   FLabel.Owner := Value;
end;

// -----------------------------------------------------------------------------
// Class :
//    TSDLCustomButton
// Method :
//    SetDrawState
// Description :
//    Offsets the button by 1,1 if it is pushed, puts it back where it belongs
//    if it's raised.
// Parameters :
//    Value    TButtonState      bsUp or bsDown
// Returns :
//    Nothing
// Modification History :
//    Author         Date        Modification
//    Todd Lang      14-Nov-2001 Created
//------------------------------------------------------------------------------
procedure TSDLButton.SetDrawState(const Value: TButtonState);
begin
   IF (Value = bsDown) AND (FDrawState = bsUp) THEN
   BEGIN
      FLabel.Left := FLabel.Left + 1;
      FLabel.Top := FLabel.Top + 1;
   END
    ELSE
   IF (Value = bsUp) AND (FDrawState = bsDown) THEN
   BEGIN
      FLabel.Left := FLabel.Left - 1;
      FLabel.Top := FLabel.Top - 1;
   END;
   inherited;
end;

// -----------------------------------------------------------------------------
// Class :
//    TSDLCustomButton
// Method :
//    SetWidth
// Description :
//    Commits the width property of the button.  This causes it to dispose of
//    its previous internal "canvas" and builds a new one.
// Parameters :
//    Value       INTEGER     New width of the button
// Returns :
//    Nothing
// Modification History :
//    Author         Date        Modification
//    Todd Lang      14-Nov-2001 Created
//------------------------------------------------------------------------------
procedure TSDLButton.SetWidth(const Value: INTEGER);
begin
   // Store the width
   FWidth := Value;
   IF (Width > 0) AND (Height > 0) THEN
      ComposeImage;
end;

procedure TSDLButton.SetHeight(const Value: INTEGER);
begin
   FHeight := Value;
   IF (FWidth > 0) AND (FHeight > 0) THEN
      ComposeImage;
end;

procedure TSDLButton.Initialize(GUI: TSDLGUI);
begin
   inherited;
   FLabel.Initialize( GUI );
end;

procedure TSDLButton.ComposeImage;
begin
   // Get a new surface for the "up" state and draw to it
   FUpImage.Alloc( Width, Height, 32 );
   FSkin.DrawButton( FUpImage.Surface, 0, 0, Width, Height, TRUE );
   // Get a new surface for the "down" state and draw to it
   FDownImage.Alloc( Width, Height, 32 );
   FSkin.DrawButton( FDownImage.Surface, 0, 0, Width, Height, FALSE );
end;

{ TSDLControl }

constructor TSDLControl.Create(Collection: TCollection);
begin
   inherited;
   FEnabled := TRUE;
   FVisible := FALSE;
end;

destructor TSDLControl.Destroy;
begin
   inherited;
end;

procedure TSDLControl.Draw(Target: PSDL_Surface);
begin
   // Stub
end;

procedure TSDLControl.Initialize(GUI: TSDLGUI);
begin
   FGUI := GUI;
end;

procedure TSDLControl.KeyDown(KeyCode: TSDLKey; Modifier : TSDLMod; var Handled: BOOLEAN);
begin
   // Do nothing
end;

procedure TSDLControl.KeyUp(KeyCode: TSDLKey; Modifier : TSDLMod; var Handled: BOOLEAN);
begin
   // Do nothing
end;

procedure TSDLControl.MouseDown(X, Y, Button: INTEGER;
  var Handled: BOOLEAN);
begin
   IF ASSIGNED(OnMouseDown) AND (Enabled = TRUE) AND (Handled = FALSE) THEN
      OnMouseDown( Self, X, Y, Button );
end;

procedure TSDLControl.MouseMove(X, Y, DX, DY: INTEGER;
  var Handled: BOOLEAN);
VAR
   NewX, NewY : INTEGER;
begin
   IF (Enabled = TRUE) AND (Handled = FALSE) THEN
   BEGIN
      NewX := X - Left;
      NewY := Y - Top;

      IF (NewX >= 0) AND (NewX <= Width) AND (NewY >= 0) AND (NewY <= Height) THEN
      BEGIN
         IF (FInControl = FALSE) THEN
         BEGIN
            FGUI.SetHint( Self, FHint );
            IF ASSIGNED(OnMouseEnter) THEN
               OnMouseEnter( Self, X, Y, -1 );
         END;
         FInControl := TRUE;
      END
       ELSE
      BEGIN
         IF (FInControl = TRUE) THEN
         BEGIN
            FGUI.SetHint( Self, '' );
            IF ASSIGNED(OnMouseLeave) THEN
               OnMouseLeave( Self, X, Y, -1 );
         END;
         FInControl := FALSE;
      END;
      IF ASSIGNED(OnMouseMove) THEN
         OnMouseMove( Self, X, Y, -1 );
   END;
end;

procedure TSDLControl.MouseUp(X, Y, Button: INTEGER; var Handled: BOOLEAN);
begin
   IF ASSIGNED(OnMouseUp) AND (Enabled = TRUE) AND (Handled = FALSE) THEN
      OnMouseUp( Self, X, Y, Button );
end;

procedure TSDLControl.MouseWheelDown(var Handled: BOOLEAN);
begin
   // Do nothing
end;

procedure TSDLControl.MouseWheelUp(var Handled: BOOLEAN);
begin
   // Do nothing
end;

procedure TSDLControl.SetFocus(const Value: BOOLEAN);
begin
   FHasFocus := Value;
end;

procedure TSDLControl.SetFont(const Value: TSDLFont);
begin
   FFont := Value;
end;

procedure TSDLControl.SetHint(const Value: STRING);
begin
   FHint := Value;
end;

procedure TSDLControl.SetLeft(const Value: INTEGER);
begin
   FLeft := Value;
end;

procedure TSDLControl.SetOwner(const Value: TSDLForm);
begin
   FOwner := Value;
end;

procedure TSDLControl.SetSkin(const Value: TSDLSkin);
begin
   FSkin := Value;
end;

procedure TSDLControl.SetTop(const Value: INTEGER);
begin
   FTop := Value;
end;

procedure TSDLControl.Tick;
begin
   // Do Nothing
end;

{ TSDLControls }

procedure TSDLControls.Draw(Target : PSDL_Surface );
VAR
   Counter : INTEGER;
begin
   FOR Counter := 0 TO (Count-1) DO
      Items[Counter].Draw( Target );
end;

function TSDLControls.GetItem(Index: INTEGER): TSDLControl;
begin
   Result := TSDLControl(INHERITED GetItem( Index ));
end;

procedure TSDLControls.Initialize(GUI: TSDLGUI);
VAR
   Counter : INTEGER;
begin
   FOR Counter := 0 TO (Count-1) DO
      Items[Counter].Initialize( GUI );
end;

procedure TSDLControls.KeyDown(KeyCode: TSDLKey; Modifier : TSDLMod; var Handled: BOOLEAN);
VAR
   Counter : INTEGER;
begin
   FOR Counter := 0 TO (Count-1) DO
      Items[Counter].KeyDown( KeyCode, Modifier, Handled );
end;

procedure TSDLControls.KeyUp(KeyCode: TSDLKey; Modifier : TSDLMod; var Handled: BOOLEAN);
VAR
   Counter : INTEGER;
begin
   FOR Counter := 0 TO (Count-1) DO
      Items[Counter].KeyUp( KeyCode, Modifier, Handled );
end;

procedure TSDLControls.MouseDown(X, Y, Button: INTEGER; var Handled: BOOLEAN);
VAR
   Counter : INTEGER;
begin
   FOR Counter := 0 TO (Count-1) DO
      Items[Counter].MouseDown( X, Y, Button, Handled );
end;

procedure TSDLControls.MouseMove(X, Y, DX, DY: INTEGER; var Handled: BOOLEAN);
VAR
   Counter : INTEGER;
begin
   FOR Counter := 0 TO (Count-1) DO
      Items[Counter].MouseMove( X, Y, DX, DY, Handled );
end;

procedure TSDLControls.MouseUp(X, Y, Button: INTEGER; var Handled: BOOLEAN);
VAR
   Counter : INTEGER;
begin
   FOR Counter := 0 TO (Count-1) DO
      Items[Counter].MouseUp( X, Y, Button, Handled );
end;

procedure TSDLControls.MouseWheelDown(var Handled: BOOLEAN);
VAR
   Counter : INTEGER;
begin
   FOR Counter := 0 TO (Count-1) DO
      Items[Counter].MouseWheelDown( Handled );
end;

procedure TSDLControls.MouseWheelUp(var Handled: BOOLEAN);
VAR
   Counter : INTEGER;
begin
   FOR Counter := 0 TO (Count-1) DO
      Items[Counter].MouseWheelUp( Handled );
end;

procedure TSDLControls.SetItem(Index: INTEGER; const Value: TSDLControl);
begin
   INHERITED SetItem( Index, Value );
end;

procedure TSDLControls.Tick;
VAR
   Counter : INTEGER;
begin
   FOR Counter := 0 TO (Count-1) DO
      Items[Counter].Tick;
end;

{ TSDLFont }

constructor TSDLRasterFont.Create(Collection: TCollection);
begin
   inherited;
   FImage := TSDLImage.Create( NIL );
end;

destructor TSDLRasterFont.Destroy;
begin
   FImage.Free;
   inherited;
end;

// -----------------------------------------------------------------------------
// Class :
//    TSDLFont
// Method :
//    Load
// Description :
//    Reads in a file that contains the kerning information for each character
//    and an image file for the actual display.
// Parameters :
//    FontName    STRING      User-name of the font
//    ImageFile   STRING      Filename of the image of the font
//    KerningFile STRING      Filename of the kerning data
// Returns :
//    Nothing
// Modification History :
//    Author         Date        Modification
//    Todd Lang      14-Nov-2001 Created
//------------------------------------------------------------------------------
function TSDLRasterFont.GetSize: INTEGER;
begin
   Result := FHeight;
end;

procedure TSDLRasterFont.Load(FontName, ImageFile, KerningFile: STRING);
VAR
   F : FILE OF BYTE;
   Counter : INTEGER;
   DX : INTEGER;
begin
   // Loads the image from the disk
   Name := FontName;
   FImage.Load( ImageFile );
   // Loads the kerning from the disk, assigning it to an internal array
   // Also calculates the x position each char for speed
   ASSIGNFILE( F, KerningFile );
   RESET( F );
   Read( F, FHeight );
   DX := 0;
   FOR Counter := 0 TO (LENGTH(CHARSET)-1) DO
   BEGIN
      FCharPos[Counter] := DX;
      Read( F, FCharWidth[Counter] );
      DX := DX + FCharWidth[Counter];
   END;
   CLOSEFILE( F );
end;

function TSDLRasterFont.TextHeight(Text: STRING): INTEGER;
begin
   Result := FHeight;
end;

// -----------------------------------------------------------------------------
// Class :
//    TSDLFont
// Method :
//    TextOut
// Description :
//    Displays the supplied text to the supplied surface at the specified
//    location.
// Parameters :
//    Target   PSDL_Surface   SDL surface to draw to
//    X        INTEGER        X-location to draw to
//    Y        INTEGER        Y-location to draw to
//    Text     STRING         Text to display
// Returns :
//    Nothing
// Modification History :
//    Author         Date        Modification
//    Todd Lang      14-Nov-2001 Created
//------------------------------------------------------------------------------
procedure TSDLRasterFont.TextOut(Target : PSDL_Surface; X, Y: INTEGER; Text: STRING);
VAR
   Counter : INTEGER;
   CharIndex : INTEGER;
   FRect : TSDL_RECT;
   DX : INTEGER;
begin
   // If this font acutally has a surface...
   IF (FImage.Surface <> NIL) THEN
   BEGIN
      DX := X;
      // Iterate through each char in the Text string...
      FOR Counter := 1 TO LENGTH(Text) DO
      BEGIN
         // Determine where that char lies on the internal image and copy
         // that portion of the image to the display
         CharIndex := POS(COPY(Text, Counter, 1), CHARSET)-1;
         FRect.x := FCharPos[CharIndex];
         FRect.w := FCharWidth[CharIndex];
         FRect.y := 0;
         FRect.h := FImage.Height;
         FImage.DrawSub( Target, @FRect, DX, Y );
         DX := DX + FCharWidth[CharIndex];
      END;
   END;
end;

// -----------------------------------------------------------------------------
// Class :
//    TSDLFont
// Method :
//    TextWidth
// Description :
//    Determine the width of the supplied text in pixel.
// Parameters :
//     Text     STRING     Text to calculate the width of
// Returns :
//    The width of the text in pixels for this font
// Modification History :
//    Author         Date        Modification
//    Todd Lang      14-Nov-2001 Created
//------------------------------------------------------------------------------
function TSDLRasterFont.TextWidth( Text : STRING ): INTEGER;
VAR
   Counter : INTEGER;
   CharIndex : INTEGER;
begin
   Result := 0;
   IF (FImage.Surface <> NIL) THEN
   BEGIN
      // Iterate through each character and add up the width
      FOR Counter := 1 TO LENGTH(Text) DO
      BEGIN
         CharIndex := POS(COPY(Text, Counter, 1), CHARSET)-1;
         Result := Result + FCharWidth[CharIndex];
      END;
   END;
end;

{ TSDLFonts }

function TSDLFonts.AddRasterFont: TSDLRasterFont;
begin
   Result := TSDLRasterFont.Create( Self );
end;

function TSDLFonts.AddTTFFont: TSDLTTFFont;
begin
   Result := TSDLTTFFont.Create( Self );
end;

function TSDLFonts.ByName(Name: STRING): TSDLFont;
VAR
   Counter : INTEGER;
begin
   FOR Counter := 0 TO (Count-1) DO
   BEGIN
      IF (CompareText(Items[Counter].Name, Name) = 0) THEN
      BEGIN
         Result := Items[Counter];
         Exit;
      END;
   END;
   Result := NIL;
end;

function TSDLFonts.GetItem(Index: INTEGER): TSDLFont;
begin
   Result := TSDLFont(INHERITED GetItem( Index ));
end;

function TSDLFonts.Insert(Index: INTEGER): TSDLFont;
begin
   Result := TSDLFont(INHERITED Insert( Index ));
end;

procedure TSDLFonts.SetItem(Index: INTEGER; const Value: TSDLFont);
begin
   INHERITED SetItem( Index, Value );
end;

{ TSDLLabel }

constructor TSDLLabel.Create(Collection: TCollection);
begin
   inherited;
   FImage := TSDLImage.Create( NIL );
end;

destructor TSDLLabel.Destroy;
begin
   FImage.Free;
   inherited;
end;

procedure TSDLLabel.Draw(Target: PSDL_Surface);
begin
   inherited;
   IF (FImage.Draw( Target, FOwner.Left + FLeft, FOwner.Top + FTop ) = -2) THEN
      ComposeImage;
end;

function TSDLLabel.GetHeight: INTEGER;
begin
   Result := FFont.TextHeight(FCaption)
end;

function TSDLLabel.GetWidth: INTEGER;
begin
   Result := FFont.TextWidth(FCaption);
end;

procedure TSDLLabel.SetCaption( CONST Value : STRING );
begin
   FCaption := Value;
   ComposeImage;
end;

procedure TSDLLabel.ComposeImage;
begin
   FImage.Alloc( FFont.TextWidth(Caption), FFont.TextHeight(Caption), 32 );
   FFont.TextOut( FImage.Surface, 0, 0, Caption);
end;

procedure TSDLLabel.SetHeight(const Value: INTEGER);
begin
   // Do nothing
end;

procedure TSDLLabel.SetWidth(const Value: INTEGER);
begin
   // Do nothing
end;

{ TSDLScrollButton }

procedure TSDLScrollButton.ComposeImage;
begin
   // Do nothing
end;

procedure TSDLScrollButton.Draw(Target: PSDL_Surface);
begin
   inherited;
   FSkin.DrawSquareButton( Target, FOwner.Left + FLeft, FOwner.Top + FTop, Width, Height, FState = bsUp );
   FSkin.DrawArrow( Target, FOwner.Left + FLeft, FOwner.Top + FTop, FPosition = sbTop );
end;

function TSDLScrollButton.GetHeight: INTEGER;
begin
   Result := SCROLL_TRACK.w;
end;

function TSDLScrollButton.GetWidth: INTEGER;
begin
   Result := SCROLL_TRACK.h;
end;

{ TSDLScrollbar }

// -----------------------------------------------------------------------------
// Class :
//    TSDLScrollbar
// Method :
//    ButtonDown
// Description :
//    Handles the button-down event of the either the upper or lower buttons
//    for the scrollbar.  Causes the position to change by 1, clamped to the
//    min/max restrictions.
// Parameters :
//    Sender      TSDLControl    The button that caused the event
// Returns :
//    Nothing
// Modification History :
//    Author         Date        Modification
//    Todd Lang      14-Nov-2001 Created
//------------------------------------------------------------------------------
procedure TSDLScrollbar.ButtonDown(Sender: TSDLControl);
VAR
   LastPosition : INTEGER;
begin
   // Resets the ticks.  This is used to scrolling the thumb if the button
   // is held down.
   FTickCount := 0;
   LastPosition := FPosition;
   // Determines which button generated the event and moves the thumb
   IF (Sender = FUpButton) THEN
      FPosition := FPosition - 1
    ELSE
   IF (Sender = FDownButton) THEN
      FPosition := FPosition + 1;

   // Clamps the position
   IF (FPosition < FMinimum) THEN
      FPosition := FMinimum
    ELSE
   IF (FPosition > FMaximum) THEN
      FPosition := FMaximum;

   // If the value changed raise an event for it
   IF (LastPosition <> FPosition) THEN
   BEGIN
      IF ASSIGNED(FChangeEvent) THEN
         OnChange( Self );
   END;
end;

procedure TSDLScrollbar.ButtonUp(Sender: TSDLControl);
begin
   FTickCount := 0;
end;

procedure TSDLScrollbar.ComposeImage;
begin
   FImage.Alloc( GetWidth, Height - (FUpButton.Height + FDownButton.Height), 32 );
   FSkin.DrawScrollTrack( FImage.Surface, 0, 0, Height - FUpButton.Height);
end;

// -----------------------------------------------------------------------------
// Class :
//    TSDLScrollbar
// Method :
//    Create
// Description :
//    Allocates all necessary internal resources
// Parameters :
//    Collection     TCollection    The collection this control belongs to
// Returns :
//    Nothing
// Modification History :
//    Author         Date        Modification
//    Todd Lang      14-Nov-2001 Created
//------------------------------------------------------------------------------
constructor TSDLScrollbar.Create(Collection: TCollection);
begin
   inherited;
   // Create the "up" button and tie the events to internal methods
   FUpButton := TSDLScrollButton.Create( NIL );
   FUpButton.Position := sbTop;
   FUpButton.OnDown := ButtonDown;
   FUpButton.OnUp := ButtonUp;
   // Create the "down" button and tie the events to internal methods
   FDownButton := TSDLScrollButton.Create( NIL );
   FDownButton.Position := sbBottom;
   FDownButton.OnDown := ButtonDown;
   FDownButton.OnUp := ButtonUp;
   // Set some safe defaults
   FMinimum := 0;
   FMaximum := 99;
   FPosition := 0;
   FTickCount := 0;
   // Allocate the canvas the scrolltrack will be drawn to
   FImage := TSDLImage.Create( NIL );
end;

destructor TSDLScrollbar.Destroy;
begin
   FUpButton.Free;
   FDownButton.Free;
   FImage.Free;
   inherited;
end;

// -----------------------------------------------------------------------------
// Class :
//    TSDLScrollbar
// Method :
//    Draw
// Description :
//    Draws the scrollbar to the supplied surface
// Parameters :
//    Collection     TCollection    The collection this control belongs to
// Returns :
//    Nothing
// Modification History :
//    Author         Date        Modification
//    Todd Lang      14-Nov-2001 Created
//------------------------------------------------------------------------------
procedure TSDLScrollbar.Draw(Target: PSDL_Surface);
VAR
   X, Y : INTEGER;
   Pos : INTEGER;
   TrackSpace : INTEGER;
   Val : DOUBLE;
begin
   inherited;
   X := FOwner.Left + FLeft;
   Y := FOwner.Top + FTop;

   // Draw the scrolltrack
   IF (FImage.Draw( Target, X, Y + FUpButton.Height ) = -2) THEN
      ComposeImage;
   // Draw the up and down buttons
   FUpButton.Draw( Target );
   FDownButton.Draw( Target );

   // Determine where the thumb is on the scrolltrack
   Trackspace := Height - (FUpButton.Height + FDownButton.Height) - SCROLL_THUMB.h;
   IF ((FMaximum - FMinimum) > 0) THEN
      Val := (FPosition - FMinimum) / (FMaximum - FMinimum)
    ELSE
      Val := 0;
   Pos := Y + FUpButton.Height + ROUND(Trackspace * Val);

   // Draw the thumb
   FSkin.DrawScrollThumb( Target, X, Pos );
end;

function TSDLScrollbar.GetHeight: INTEGER;
begin
   Result := FHeight;
end;

function TSDLScrollbar.GetWidth: INTEGER;
begin
   Result := SCROLL_TRACK.w;
end;

// -----------------------------------------------------------------------------
// Class :
//    TSDLScrollbar
// Method :
//    MouseDown
// Description :
//    Handles the mouse-down event for the scrollbar.  This means determining
//    if it falls on one of the buttons or on the thumb.
// Parameters :
//    X        INTEGER     X-position of the event
//    Y        INTEGER     Y-position of the event
//    Handled  BOOLEAN     State of the event
// Returns :
//    The state of the message in Handled.  If it handled the event, it sets
//    Handled to TRUE, otherwise it leaves the state unchanged.
// Modification History :
//    Author         Date        Modification
//    Todd Lang      14-Nov-2001 Created
//------------------------------------------------------------------------------
procedure TSDLScrollbar.Initialize(GUI: TSDLGUI);
begin
   inherited;
   FUpButton.Initialize( GUI );
   FDownButton.Initialize( GUI );
end;

procedure TSDLScrollbar.MouseDown(X, Y, Button: INTEGER; var Handled: BOOLEAN);
VAR
   Trackspace : INTEGER;
   Val : DOUBLE;
   LastPosition : INTEGER;
begin
   inherited;
   IF (Enabled) THEN
   BEGIN
      // See if the buttons handle the event
      FUpButton.MouseDown( X, Y, Button, Handled );
      FDownButton.MouseDown( X, Y, Button, Handled );
      X := X - FLeft;
      Y := Y - FTop;
      // If it falls on the track and wasn't handled by the buttons...
      IF (X > 0) AND (X < Width) AND (Y > 0) AND (Y < Height) AND (Handled = FALSE) THEN
      BEGIN
         // Need to determine if it falls on the thumb
         Y := Y - FUpButton.Height;
         IF (Y >= ThumbTop) AND (Y <= (ThumbTop + SCROLL_THUMB.h)) THEN
         BEGIN
            // Fell on the thumb and now we can pull the thumb up and down
            FScrolling := TRUE;
            Handled := TRUE;
         END
          ELSE
         BEGIN
            // Fell on the track and needs to push the thumb to that position
            // Determine where the event falls on the track (normalized)
            Trackspace := Height - (FUpButton.Height + FDownButton.Height);
            Val := Y / Trackspace;
            LastPosition := FPosition;
            // Determine the new position on the track
            FPosition := ROUND(Val * (FMaximum - FMinimum)) + FMinimum;
            // Clamp the position
            IF (FPosition > FMaximum) THEN
               FPosition := FMaximum
             ELSE
            IF (FPosition < FMinimum) THEN
               FPosition := FMinimum;
            IF (FPosition <> LastPosition) THEN
            BEGIN
               IF ASSIGNED(FChangeEvent) THEN
                  OnChange( Self );
            END;
            Handled := TRUE;
         END;
      END;
   END;
end;

//------------------------------------------------------------------------------
// Class :
//    TSDLScrollbar
// Method :
//    MouseMove
// Description :
//    Drags the thumb if necessary.
// Parameters :
//    X        INTEGER     X-position of the event
//    Y        INTEGER     Y-position of the event
//    DX       INTEGER     Amount mouse has moved since last tick
//    DY       INTEGER     Amount mouse has moved since last tick
//    Handled  BOOLEAN     State of the event
// Returns :
//    The state of the message in Handled.  If it handled the event, it sets
//    Handled to TRUE, otherwise it leaves the state unchanged.
// Modification History :
//    Author         Date        Modification
//    Todd Lang      14-Nov-2001 Created
//------------------------------------------------------------------------------
procedure TSDLScrollbar.MouseMove(X, Y, DX, DY: INTEGER; var Handled: BOOLEAN);
VAR
   NewY : INTEGER;
   Val : DOUBLE;
   Trackspace : INTEGER;
   LastPosition : INTEGER;
begin
   inherited;
   IF (Enabled) THEN
   BEGIN
      // See if the buttons handle the event...
      FUpButton.MouseMove( X, Y, DX, DY, Handled );
      FDownButton.MouseMove( X, Y, DX, DY, Handled );
      X := X - FLeft;
      Y := Y - FTop;
      // If it falls on the track and the buttons didn't handle it (and we're scrolling)...
      IF (X > 0) AND (X < Width) AND (Y > 0) AND (Y < Height) AND (Handled = FALSE) THEN
      BEGIN
         HasFocus := TRUE;
         IF (FScrolling) THEN
         BEGIN
            // Determine where the event falls on the track (normalized)
            NewY := Y - FUpButton.Height - (SCROLL_THUMB.h DIV 2);
            Trackspace := Height - (FUpButton.Height + FDownButton.Height) - SCROLL_THUMB.h;
            Val := NewY / Trackspace;
            LastPosition := FPosition;
            // Determine the new position on the track
            FPosition := ROUND(Val * (FMaximum - FMinimum)) + FMinimum;
            // Clamp the position
            IF (FPosition > FMaximum) THEN
               FPosition := FMaximum
             ELSE
            IF (FPosition < FMinimum) THEN
               FPosition := FMinimum;
            IF (FPosition <> LastPosition) THEN
            BEGIN
               IF ASSIGNED(FChangeEvent) THEN
                  OnChange( Self );
            END;
            Handled := TRUE;
         END;
      END
       ELSE
      BEGIN
         HasFocus := FALSE;
      END;
   END;
end;

procedure TSDLScrollbar.MouseUp(X, Y, Button: INTEGER;
  var Handled: BOOLEAN);
begin
   inherited;
   IF (Enabled) THEN
   BEGIN
      FUpButton.MouseUp( X, Y, Button, Handled );
      FDownButton.MouseUp( X, Y, Button, Handled );
      FScrolling := FALSE;
   END;
end;

procedure TSDLScrollbar.MouseWheelDown(var Handled: BOOLEAN);
begin
   inherited;
   IF (HasFocus) AND (Enabled) THEN
   BEGIN
      Handled := TRUE;
      ButtonDown( FDownButton );
   END;
end;

procedure TSDLScrollbar.MouseWheelUp(var Handled: BOOLEAN);
begin
   inherited;
   IF (HasFocus) AND (Enabled) THEN
   BEGIN
      Handled := TRUE;
      ButtonDown( FUpButton );
   END;
end;

procedure TSDLScrollbar.SetFocus(const Value: BOOLEAN);
begin
   inherited;
   FUpButton.HasFocus := Value;
   FDownButton.HasFocus := Value;
end;

procedure TSDLScrollbar.SetHeight(const Value: INTEGER);
begin
   FHeight := Value;
   ComposeImage;
end;

procedure TSDLScrollbar.SetLeft(const Value: INTEGER);
begin
   inherited;
   FUpButton.Left := Value;
   FDownButton.Left := Value;
end;

procedure TSDLScrollbar.SetOwner(const Value: TSDLForm);
begin
   inherited;
   FUpButton.Owner := Value;
   FDownButton.Owner := Value;
end;

procedure TSDLScrollbar.SetPosition(const Value: INTEGER);
VAR
   LastPos : INTEGER;
begin
   LastPos := FPosition;
   FPosition := Value;
   IF (FPosition < FMinimum) THEN
      FPosition := FMinimum
    ELSE
   IF (FPosition > FMaximum) THEN
      FPosition := FMaximum;
   IF (LastPos <> FPosition) THEN
   BEGIN
      IF ASSIGNED(OnChange) THEN
         OnChange(Self);
   END;
end;

procedure TSDLScrollbar.SetSkin(const Value: TSDLSkin);
begin
   inherited;
   FUpButton.Skin := Value;
   FDownButton.Skin := Value;
end;

procedure TSDLScrollbar.SetTop(const Value: INTEGER);
begin
   inherited;
   FUpButton.Top := Value;
   FDownButton.Top := (Value + Height) - FDownButton.Height;
end;

//------------------------------------------------------------------------------
// Class :
//    TSDLScrollbar
// Method :
//    ThumbTop
// Description :
//    Figures out where the top of the thumb lies on the normalized tracklength
// Parameters :
//    None
// Returns :
//    The position of the thumb along the scrolltrack.
// Modification History :
//    Author         Date        Modification
//    Todd Lang      14-Nov-2001 Created
//------------------------------------------------------------------------------
procedure TSDLScrollbar.SetWidth(const Value: INTEGER);
begin
   // Do nothing
end;

function TSDLScrollbar.ThumbTop: INTEGER;
VAR
   TrackSpace : INTEGER;
   Val : DOUBLE;
begin
   // Calculate how much space there actually is on the scrolltrack.
   Trackspace := Height - (FUpButton.Height + FDownButton.Height) - SCROLL_THUMB.h;
   IF ((FMaximum - FMinimum) > 0) THEN
      Val := (FPosition - FMinimum) / (FMaximum - FMinimum)
    ELSE
      Val := 0;
   // Determine where the "position" falls in actual track co-ordinates
   Result := ROUND(Trackspace * Val);
end;

//------------------------------------------------------------------------------
// Class :
//    TSDLScrollbar
// Method :
//    Tick
// Description :
//    If a button is being held down, this causes the scrollbar to smoothly
//    scroll towards that extreme.
// Parameters :
//    None
// Returns :
//    Nothing
// Modification History :
//    Author         Date        Modification
//    Todd Lang      14-Nov-2001 Created
//------------------------------------------------------------------------------
procedure TSDLScrollbar.Tick;
VAR
   LastPosition : INTEGER;
begin
   inherited;
   // The tickcount delay is to prevent multiple firings if the user is just
   // clicking once.
   // *** TODO *** Figure out some scaling method so that you can still click
   // just once on "fast" machines.  (Note that this is tied to FPS so it's
   // not a huge priority)
   IF (FTickCount > 3) THEN
   BEGIN
      // Scroll towards the extreme
      LastPosition := FPosition;
      IF (FUpButton.State = bsDown) THEN
         FPosition := FPosition - 1
       ELSE
      IF (FDownButton.State = bsDown) THEN
         FPosition := FPosition + 1;
      // Clamp the position
      IF (FPosition < FMinimum) THEN
         FPosition := FMinimum
       ELSE
      IF (FPosition > FMaximum) THEN
         FPosition := FMaximum;

      IF (LastPosition <> FPosition) THEN
      BEGIN
         IF ASSIGNED(FChangeEvent) THEN
            OnChange( Self );
      END;
   END
    ELSE
   BEGIN
      IF (FUpButton.State = bsDown) OR (FDownButton.State = bsDown) THEN
         FTickCount := FTickCount + 1;
   END;
end;

procedure TSDLScrollButton.SetHeight(const Value: INTEGER);
begin
   // Do nothing
end;

procedure TSDLScrollButton.SetWidth(const Value: INTEGER);
begin
   // Do nothing
end;

{ TSDLEdit }

procedure TSDLEdit.ComposeImage;
begin
   IF (FImage.Surface = NIL) AND (Width > 0) AND (Height > 0) THEN
      FImage.Alloc( Width, Height, 32 );
   FSkin.DrawEdit( FImage.Surface, 0, 0, Width, Height );
end;

constructor TSDLEdit.Create(Collection: TCollection);
begin
   inherited;
   FLabel := TSDLLabel.Create( NIL );
   FImage := TSDLImage.Create( NIL );
   FShowCursor := FALSE;
end;

destructor TSDLEdit.Destroy;
begin
   FLabel.Free;
   FImage.Free;
   inherited;
end;

//------------------------------------------------------------------------------
// Class :
//    TSDLEdit
// Method :
//    Draw
// Description :
//    Displays the edit box.  Draws the background and the flashing cursor.
//    The internal label is what draws the text.
// Parameters :
//    Target         PSDL_Surface      Surface to draw to
// Returns :
//    Nothing
// Modification History :
//    Author         Date        Modification
//    Todd Lang      14-Nov-2001 Created
//------------------------------------------------------------------------------
procedure TSDLEdit.Draw(Target: PSDL_Surface);
VAR
   X1, Y1, Y2 : INTEGER;
begin
   inherited;
   // The background of the box
   IF (FImage.Draw( Target, FOwner.Left + FLeft, FOwner.Top + FTop ) = -2) THEN
      ComposeImage;
   // The text in the box
   FLabel.Draw( Target );
   // Draw the blinking cursor --- this routine is horrible, but works for now
   IF (FShowCursor) THEN
   BEGIN
      Y1 := FOwner.Top + FTop + 1;
      Y2 := FOwner.Top + FTop + Height - 2;
      X1 := FOwner.Left + FLeft;
      X1 := X1 + FLabel.Font.TextWidth( COPY(FValue, 1, FCursorIndex) );

      // *** TODO *** Convert this to draw a colour consistent with the skin
      // or even to draw a "text cursor" as defined in the skin
      // Locking the surface all the time seems sub-optimal
      SDL_LockSurface( Target );
      SDL_DrawLine( Target, X1, Y1, X1, Y2, $FFFFFF );
      SDL_UnlockSurface( Target );
   END;
end;

function TSDLEdit.GetFont: TSDLFont;
begin
   Result := FLabel.Font;
end;

function TSDLEdit.GetHeight: INTEGER;
begin
   Result := FHeight;
end;

function TSDLEdit.GetWidth: INTEGER;
begin
   Result := FWidth;
end;

//------------------------------------------------------------------------------
// Class :
//    TSDLEdit
// Method :
//    KeyCodeToChar
// Description :
//    Converts a TSDLKey and TSDLMod to its equivalent char as a string
// Parameters :
//    KeyCode        TSDLKey        Keycode to convert
//    Modifier       TSDLMod        Modifiers applied to key (shift, alt, etc)
// Returns :
//    A string containing the converted character
// Modification History :
//    Author         Date        Modification
//    Todd Lang      14-Nov-2001 Created
//------------------------------------------------------------------------------
procedure TSDLEdit.Initialize(GUI: TSDLGUI);
begin
   inherited;
   FLabel.Initialize( GUI );
end;

function TSDLEdit.KeyCodeToChar( KeyCode : TSDLKey; Modifier : TSDLMod ) : STRING;
begin
   IF ((Modifier AND KMOD_SHIFT) > 0) THEN
      Result := COPY( UPCASE, POS(CHAR(KeyCode), LOWCASE), 1 )
    ELSE
      Result := COPY( LOWCASE, POS(CHAR(KeyCode), LOWCASE), 1 );
end;

//------------------------------------------------------------------------------
// Class :
//    TSDLEdit
// Method :
//    KeyUp
// Description :
//    Handles the keystroke message generated by SDL.  Only processess the key
//    if it current has focus.
// Parameters :
//    KeyCode        TSDLKey        Key event generated
//    Modifier       TSDLMod        Modifier applied to key (shift, alt, etc)
//    Handled        Boolean        Whether or not the message has been handled
// Returns :
//    The state of the message in Handled.  If it handled the event, it sets
//    Handled to TRUE, otherwise it leaves the state unchanged.
// Modification History :
//    Author         Date        Modification
//    Todd Lang      14-Nov-2001 Created
//------------------------------------------------------------------------------
procedure TSDLEdit.KeyUp(KeyCode: TSDLKey; Modifier : TSDLMod; var Handled: BOOLEAN);
VAR
   TheKey : STRING;
begin
   inherited;
   IF (HasFocus) AND (Enabled) THEN
   BEGIN
      // If we have focus, we eat the message, even if we do nothing with it
      Handled := TRUE;
      // Backspace removes a character from BEFORE the cursor
      IF (KeyCode = SDLK_BACKSPACE) THEN
      BEGIN
         IF (FCursorIndex > 0) THEN
         BEGIN
            Value := COPY(FValue, 1, FCursorIndex-1) + COPY(FValue, FCursorIndex+1, LENGTH(FValue));
            DEC(FCursorIndex);
            FShowCursor := TRUE;
         END;
      END
       ELSE
      // Delete removes a character from AFTER the cursor
      IF (KeyCode = SDLK_DELETE) THEN
      BEGIN
         Value := COPY(FValue, 1, FCursorIndex) + COPY(FValue, FCursorIndex+2, LENGTH(FValue));
         FShowCursor := TRUE;
      END
       ELSE
      // Enter/Return gives up focus for the box.  ie - You're "done"
      IF (KeyCode = SDLK_RETURN) THEN
      BEGIN
         HasFocus := FALSE;
         FShowCursor := FALSE;
      END
       ELSE
      // General printable keystroke handler
      IF (KeyCode >= SDLK_SPACE) AND (KeyCode <= SDLK_Z) THEN
      BEGIN
         TheKey := KeyCodeToChar(KeyCode, Modifier);
         IF (FLabel.Font.TextWidth(FValue + TheKey) < Width) THEN
         BEGIN
            Value := COPY(FValue, 1, FCursorIndex) + TheKey + COPY(FValue, FCursorIndex+1, LENGTH(FValue));
            INC(FCursorIndex);
         END;
         FShowCursor := TRUE;
      END
       ELSE
      // Move back through the string
      IF (KeyCode = SDLK_LEFT) THEN
      BEGIN
         DEC(FCursorIndex);
         IF (FCursorIndex < 0) THEN
            FCursorIndex := 0;
         FShowCursor := TRUE;
      END
       ELSE
      // Move forward through the string
      IF (KeyCode = SDLK_RIGHT) THEN
      BEGIN
         INC(FCursorIndex);
         IF (FCursorIndex > LENGTH(FValue)) THEN
            FCursorIndex := LENGTH(FValue);
         FShowCursor := TRUE;
      END
       ELSE
      // Move to the end of the string
      IF (KeyCode = SDLK_END) THEN
      BEGIN
         FCursorIndex := LENGTH(FValue);
         FShowCursor := TRUE;
      END
       ELSE
      // Move to the start of the string
      IF (KeyCode = SDLK_HOME) THEN
      BEGIN
         FCursorIndex := 0;
         FShowCursor := TRUE;
      END;
   END;
end;

procedure TSDLEdit.LostFocus;
begin
   HasFocus := FALSE;
   FShowCursor := FALSE;
end;

procedure TSDLEdit.MouseDown(X, Y, Button: INTEGER; var Handled: BOOLEAN);
begin
   inherited;
   IF (Enabled) THEN
   BEGIN
      X := X - FLeft;
      Y := Y - FTop;
      IF (X >= 0) AND (X <= Width) AND (Y >= 0) AND (Y <= Height) THEN
      BEGIN
         Handled := TRUE;
         FMouseDown := TRUE;
      END;
   END;
   IF (HasFocus) AND ((X < 0) OR (X > Width) OR (Y < 0) OR (Y > Height)) THEN
      LostFocus;
end;

// -----------------------------------------------------------------------------
// Class :
//    TSDLEdit
// Method :
//    MouseUp
// Description :
//    Handles a mouse-up event for the button.
// Parameters :
//    X        INTEGER     X-location of the event
//    Y        INTEGER     Y-location of the event
//    Button   INTEGER     Button that was pushed
//    Handled  BOOLEAN     Signals if the event has already been handled
// Returns :
//    The state of the message in Handled.  If it handled the event, it sets
//    Handled to TRUE, otherwise it leaves the state unchanged.
// Modification History :
//    Author         Date        Modification
//    Todd Lang      14-Nov-2001 Created
//------------------------------------------------------------------------------
procedure TSDLEdit.MouseUp(X, Y, Button: INTEGER; var Handled: BOOLEAN);
VAR
   Counter : INTEGER;
begin
   inherited;
   IF (Enabled) THEN
   BEGIN
      X := X - FLeft;
      Y := Y - FTop;
      IF (X > 0) AND (X < Width) AND (Y > 0) AND (Y < Height) THEN
      BEGIN
         Handled := TRUE;
         // If we clicked down...
         IF (FMouseDown = TRUE) THEN
         BEGIN
            // and we don't already have focus...
            IF (HasFocus = FALSE) THEN
            BEGIN
               // Give the focus to this edit box and remove it from all others
               HasFocus := TRUE;
               GUI.EditFocus( FOwner, Self );
            END;
            // Find out where in the string we clicked by adding up the widths
            // of all of the characters and comparing it to where we clicked in
            // the box.  Once we find the char we're closest to, put the cursor
            // there.
            // *** TODO *** Find out which character we really are closer to by
            // doing a delta between the current character and the next.
            FOR Counter := 0 TO (LENGTH(FValue)) DO
            BEGIN
               IF (FLabel.Font.TextWidth( COPY( FValue, 1, Counter )) <= X) THEN
                  FCursorIndex := Counter;
               IF (FCursorIndex > LENGTH(FValue)) THEN
                  FCursorIndex := LENGTH(FValue);
            END;
         END;
      END;
      FMouseDown := FALSE;
   END;
end;

procedure TSDLEdit.SetFont(const Value: TSDLFont);
begin
   FLabel.Font := Value;
end;

procedure TSDLEdit.SetHeight(const Value: INTEGER);
begin
   FHeight := Value;
   ComposeImage;
end;

procedure TSDLEdit.SetLeft(const Value: INTEGER);
begin
   inherited;
   FLabel.Left := Value+1;
end;

procedure TSDLEdit.SetOwner(const Value: TSDLForm);
begin
   inherited;
   FLabel.Owner := Value;
end;

procedure TSDLEdit.SetTop(const Value: INTEGER);
begin
   inherited;
   FLabel.Top := Value + ((Height DIV 2) - (FLabel.Font.TextHeight(FValue) DIV 2));
end;

procedure TSDLEdit.SetValue(const Value: STRING);
begin
   FValue := Value;
   FLabel.Caption := Value;
end;

procedure TSDLEdit.SetWidth(const Value: INTEGER);
begin
   FWidth := Value;
   ComposeImage;
end;

procedure TSDLEdit.Tick;
begin
   inherited;
   INC(FTicks);
   IF (FTicks > 15) THEN
   BEGIN
      FTicks := 0;
      IF (HasFocus) THEN
         FShowCursor := NOT(FShowCursor);
   END;
end;

{ TSDLListbox }

// -----------------------------------------------------------------------------
// Class :
//    TSDLListBox
// Method :
//    AddItem
// Description :
//    Adds an item to the listbox string array.  This will handle all of the
//    necessary scrollbar adjustments.
// Parameters :
//    Value       STRING      Text to add
// Returns :
//    Nothing
// Modification History :
//    Author         Date        Modification
//    Todd Lang      14-Nov-2001 Created
//------------------------------------------------------------------------------
procedure TSDLListBox.AddItem(const Value: STRING);
VAR
   ItemCount : INTEGER;
begin
   // Add the string
   FItems.Add( Value );
   // Determine the number of items that would be offscreen
   ItemCount := FItems.Count - FDisplayCount;
   // Clamp the value
   IF (ItemCount < 0) THEN
      ItemCount := 0;
   // Adjust the scrollbar so you can scroll to see the items
   FScrollbar.Maximum := ItemCount;
   ChangeEvent( NIL );
end;

// -----------------------------------------------------------------------------
// Class :
//    TSDLListBox
// Method :
//    ChangeEvent
// Description :
//    Handles rebuilding the listbox image whenever anything changes in the
//    listbox.  Adding items, scrolling the scrollbar, etc.
// Parameters :
//    Sender         TSDLControl       Control that is being changed
// Returns :
//    Nothing
// Modification History :
//    Author         Date        Modification
//    Todd Lang      14-Nov-2001 Created
//------------------------------------------------------------------------------
procedure TSDLListBox.ChangeEvent(Sender: TSDLControl);
begin
   IF (FUpdating = FALSE) THEN
      ComposeImage;
end;

procedure TSDLListBox.BeginUpdate;
begin
   FUpdating := TRUE;
end;

procedure TSDLListBox.EndUpdate;
begin
   FUpdating := FALSE;
   ChangeEvent( NIL );
end;

// -----------------------------------------------------------------------------
// Class :
//    TSDLListBox
// Method :
//    Create
// Description :
//    Initializes the listbox to a set of defaults and creates all necessary
//    internal resources.
// Parameters :
//    Collection        TCollection       Collection the control is added to
// Returns :
//    Nothing
// Modification History :
//    Author         Date        Modification
//    Todd Lang      14-Nov-2001 Created
//------------------------------------------------------------------------------
constructor TSDLListbox.Create(Collection: TCollection);
begin
   inherited;
   FScrollbar := TSDLScrollbar.Create( NIL );
   FScrollbar.Minimum := 0;
   FScrollbar.Maximum := 0;
   FScrollbar.Position := 0;
   FScrollbar.OnChange := ChangeEvent;
   FItems := TStringList.Create;
   FImage := TSDLImage.Create( NIL );
end;

destructor TSDLListbox.Destroy;
begin
   FScrollbar.Free;
   FItems.Free;
   FImage.Free;
   inherited;
end;

procedure TSDLListbox.Draw(Target: PSDL_Surface);
begin
   inherited;
   IF (FImage.Draw( Target, FLeft + FOwner.Left, FTop + FOwner.Top ) = -2) THEN
      ComposeImage;
   FScrollbar.Draw( Target );
end;

function TSDLListBox.GetItem(Index: INTEGER): STRING;
begin
   Result := FItems[ Index ];
end;

// -----------------------------------------------------------------------------
// Class :
//    TSDLListBox
// Method :
//    MouseDown
// Description :
//    Handles clicking inside the listbox to select an item.
// Parameters :
//    X        INTEGER     X-location of the event
//    Y        INTEGER     Y-location of the event
//    Button   INTEGER     Button that was pushed
//    Handled  BOOLEAN     Signals if the event has already been handled
// Returns :
//    The state of the message in Handled.  If it handled the event, it sets
//    Handled to TRUE, otherwise it leaves the state unchanged.
// Modification History :
//    Author         Date        Modification
//    Todd Lang      17-May-2002 Created
//------------------------------------------------------------------------------
procedure TSDLListbox.MouseDown(X, Y, Button: INTEGER;
  var Handled: BOOLEAN);
VAR
   ItemPos : INTEGER;
begin
   inherited;
   IF (Enabled) THEN
   BEGIN
      // See if the scrollbar handles the message
      FScrollbar.MouseDown( X, Y, Button, Handled );
      X := X - FLeft;
      Y := Y - FTop;
      // If the scrollbar didn't handle it and it falls inside the panel...
      IF (X > 0) AND (X < (Width-FScrollbar.Width)) AND (Y > 0) AND (Y < Height) AND (Handled = FALSE) THEN
      BEGIN
         Handled := TRUE;
         // Find the position in the panel
         ItemPos := Y DIV FFont.Size;
         // Add the offset from the scrollbar
         ItemPos := ItemPos + FScrollbar.Position;
         // See if it's different than the currently selected item...
         IF (ItemPos <> FItemIndex) THEN
         BEGIN
            // If it is, set it and redraw the panel
            FItemIndex := ItemPos;
            ChangeEvent( NIL );
         END;
      END;
   END;
end;

procedure TSDLListbox.MouseMove(X, Y, DX, DY: INTEGER;
  var Handled: BOOLEAN);
begin
   inherited;
   IF (Enabled) THEN
   BEGIN
      FScrollbar.MouseMove( X, Y, DX, DY, Handled );
      X := X - Left;
      Y := Y - Top;
      IF (X >= 0) AND (X <= Width) AND (Y >= 0) AND (Y <= Height) AND (Handled = FALSE) THEN
         HasFocus := TRUE
       ELSE
         HasFocus := FALSE;
   END;
end;

procedure TSDLListbox.MouseUp(X, Y, Button: INTEGER; var Handled: BOOLEAN);
begin
   inherited;
   IF (Enabled) THEN
      FScrollbar.MouseUp( X, Y, Button, Handled );
end;

procedure TSDLListBox.MouseWheelDown(var Handled: BOOLEAN);
begin
   inherited;
   IF (Enabled) THEN
      FScrollbar.MouseWheelDown( Handled );
end;

procedure TSDLListBox.MouseWheelUp(var Handled: BOOLEAN);
begin
   inherited;
   IF (Enabled) THEN
      FScrollbar.MouseWheelUp( Handled );
end;

procedure TSDLListBox.KeyDown(KeyCode: TSDLKey; Modifier: TSDLMod; var Handled: BOOLEAN);
VAR
   LastIndex : INTEGER;
begin
   inherited;
   IF (HasFocus) AND (Enabled) THEN
   BEGIN
      Handled := TRUE;
      LastIndex := FItemIndex;
      FTickCount := 0;
      IF (KeyCode = SDLK_UP) THEN
      BEGIN
         DEC(FItemIndex);
         IF (FItemIndex < 0) THEN
            FItemIndex := 0;
         FScrolling := -1;
      END
       ELSE
      IF (KeyCode = SDLK_DOWN) THEN
      BEGIN
         INC(FItemIndex);
         IF (FItemIndex >= FItems.Count) THEN
            FItemIndex := FItems.Count-1;
         FScrolling := 1;
      END
       ELSE
      IF (KeyCode = SDLK_PAGEUP) THEN
      BEGIN
         FItemIndex := 0;
      END
       ELSE
      IF (KeyCode = SDLK_PAGEDOWN) THEN
      BEGIN
         FItemIndex := FItems.Count-1;
      END;
      IF (LastIndex <> FItemIndex) THEN
      BEGIN
         IF (FScrollbar.Position + FDisplayCount <= FItemIndex) THEN
            FScrollbar.Position := (FItemIndex - FDisplayCount)+1
          ELSE
         IF (FScrollbar.Position > FItemIndex) THEN
            FScrollbar.Position := FItemIndex;
         ComposeImage;
      END;
   END;
end;

procedure TSDLListBox.KeyUp(KeyCode: TSDLKey; Modifier: TSDLMod;
  var Handled: BOOLEAN);
begin
   inherited;
   IF (HasFocus) AND (Enabled) THEN
   BEGIN
      FScrolling := 0;
      Handled := TRUE;
   END;
end;

procedure TSDLListbox.SetHeight(const Value: INTEGER);
begin
   FHeight := Value;
   FScrollbar.Height := Value;
   FDisplayCount := FHeight DIV FFont.Size;
   IF (FHeight MOD FFont.Size <> 0) THEN
      FDisplayCount := FDisplayCount + 1;
end;

procedure TSDLListBox.SetItem(Index: INTEGER; const Value: STRING);
begin
   FItems[ Index ] := Value;
end;

procedure TSDLListbox.SetLeft(const Value: INTEGER);
begin
   inherited;
   FScrollbar.Left := FLeft + FWidth - FScrollbar.Width;
end;

procedure TSDLListbox.SetOwner(const Value: TSDLForm);
begin
   inherited;
   FScrollbar.Owner := Value;
end;

procedure TSDLListbox.SetSkin(const Value: TSDLSkin);
begin
   inherited;
   FScrollbar.Skin := Value;
end;

procedure TSDLListbox.SetTop(const Value: INTEGER);
begin
   inherited;
   FScrollbar.Top := Value;
end;

procedure TSDLListBox.SetFocus(const Value: BOOLEAN);
begin
   inherited;
   FScrollbar.HasFocus := Value;
end;

procedure TSDLListbox.SetWidth(const Value: INTEGER);
begin
   FWidth := Value;
end;

function TSDLListBox.GetHeight: INTEGER;
begin
   Result := FHeight;
end;

function TSDLListBox.GetWidth: INTEGER;
begin
   Result := FWidth;
end;

procedure TSDLListbox.Tick;
VAR
   LastIndex : INTEGER;
begin
   inherited;
   FScrollbar.Tick;
   IF (FScrolling <> 0) THEN
   BEGIN
      IF (FTickCount > 3) THEN
      BEGIN
         LastIndex := FItemIndex;
         FItemIndex := FItemIndex + FScrolling;
         IF (FItemIndex < 0) THEN
            FItemIndex := 0
          ELSE
         IF (FItemIndex >= FItems.Count) THEN
            FItemIndex := FItems.Count-1;

         IF (LastIndex <> FItemIndex) THEN
         BEGIN
            IF (FScrollbar.Position + FDisplayCount <= FItemIndex) THEN
               FScrollbar.Position := (FItemIndex - FDisplayCount)+1
             ELSE
            IF (FScrollbar.Position > FItemIndex) THEN
               FScrollbar.Position := FItemIndex;
            ComposeImage;
         END;
      END
       ELSE
         INC(FTickCount);
   END;
end;

procedure TSDLListBox.Initialize(GUI: TSDLGUI);
begin
   inherited;
   FScrollbar.Initialize( GUI );
end;

procedure TSDLListBox.ComposeImage;
VAR
   Y, Counter : INTEGER;
   DisplayCount : INTEGER;
begin
   // If we don't already have a surface, make one
   IF (FImage.Surface = NIL) THEN
      FImage.Alloc( FWidth - FScrollbar.Width, FHeight, 32 );
   // Draw the background panel
   FSkin.DrawPanel( FImage.Surface, 0, 0, FImage.Width, FImage.Height );
   Y := 0;
   // Calculate the number of items that will be displayed in the panel
   DisplayCount := FDisplayCount;
   IF (DisplayCount > FItems.Count) THEN
      DisplayCount := FItems.Count;
   // Draw the items, from the scrollbar position, to the maximum allowed
   FOR Counter := FScrollbar.Position TO ((FScrollbar.Position + DisplayCount)-1) DO
   BEGIN
      // If this is the currently selected item, draw the selection bar
      IF (Counter = FItemIndex) THEN
         FSkin.DrawSelectionBar( FImage.Surface, 1, Y, FImage.Width-2, FFont.Size );

      FFont.TextOut( FImage.Surface, 0, Y, FItems[Counter] );
      Y := Y + FFont.Size;
   END;
end;

procedure TSDLListBox.SetItemIndex(const Value: INTEGER);
begin
   IF (Value <> FItemIndex) THEN
   BEGIN
      FItemIndex := Value;
      ChangeEvent( NIL );
   END;
end;

{ TSDLBitButton }

procedure TSDLBitButton.ComposeImage;
begin
   // Get a new surface for the "up" state and draw to it
   FUpImage.Alloc( Width, Height, 32 );
   FSkin.DrawBitButton( FUpImage.Surface, 0, 0, Width, Height, TRUE );
   // Get a new surface for the "down" state and draw to it
   FDownImage.Alloc( Width, Height, 32 );
   FSkin.DrawBitButton( FDownImage.Surface, 0, 0, Width, Height, FALSE );
end;

constructor TSDLBitButton.Create(Collection: TCollection);
begin
   inherited;
   FUpImage := TSDLImage.Create( NIL );
   FDownImage := TSDLImage.Create( NIL );
end;

destructor TSDLBitButton.Destroy;
begin
   FUpImage.Free;
   FDownImage.Free;
   inherited;
end;

// -----------------------------------------------------------------------------
// Class :
//    TSDLBitButton
// Method :
//    Draw
// Description :
//    Blits the button image and the overlay image to the supplied surface.
//    The overlay image is centered on the button, offset by -1,-1 for the "up"
//    position and 1,1 for the "down" position.
// Parameters :
//    Target         PSDL_Surface         Surface to draw to
// Returns :
//    Nothing
// Modification History :
//    Author         Date        Modification
//    Todd Lang      16-May-2002 Created
//------------------------------------------------------------------------------
procedure TSDLBitButton.Draw(Target: PSDL_SURFACE);
VAR
   ImageLeft, ImageTop : INTEGER;
begin
   inherited;
   // Determine the state and draw the correct image
   IF (FDrawState = bsUp) THEN
   BEGIN
      IF (FUpImage.Draw( Target, FOwner.Left + FLeft, FOwner.Top + FTop ) = -2) THEN
         ComposeImage;
   END
    ELSE
   BEGIN
      IF (FDownImage.Draw( Target, FOwner.Left + FLeft, FOwner.Top + FTop ) = -2) THEN
         ComposeImage;
   END;

   // If there is an overlay image...
   IF (FImage <> NIL) THEN
   BEGIN
      // Center the image
      ImageLeft := FOwner.Left + FLeft;
      ImageTop := FOwner.Top + FTop;
      ImageLeft := ImageLeft + (FWidth DIV 2) - (FImage.Width DIV 2);
      ImageTop := ImageTop + (FHeight DIV 2) - (FImage.Height DIV 2);
      // And offset the image based on the current state
      IF (FDrawState = bsUp) THEN
         FImage.Draw( Target, ImageLeft, ImageTop )
       ELSE
         FImage.Draw( Target, ImageLeft+1, ImageTop+1 );
   END;
end;

function TSDLBitButton.GetHeight: INTEGER;
begin
   Result := FHeight;
end;

function TSDLBitButton.GetWidth: INTEGER;
begin
   Result := FWidth;
end;

procedure TSDLBitButton.SetHeight(const Value: INTEGER);
begin
   // Store the height
   FHeight := Value;
   IF (Width > 0) AND (Height > 0) THEN
      ComposeImage;
end;

procedure TSDLBitButton.SetWidth(const Value: INTEGER);
begin
   // Store the width
   FWidth := Value;
   IF (Width > 0) AND (Height > 0) THEN
      ComposeImage;
end;

procedure TSDLBitButton.Tick;
begin
   inherited;
   IF (FImage <> NIL) THEN
      FImage.Tick;
end;

{ TSDLMeter }

procedure TSDLMeter.ComposeImage;
VAR
   FillAmount : INTEGER;
   FillArea : TSDL_RECT;
begin
   IF (FImage.Surface = NIL) THEN
      FImage.Alloc( Width, Height, 32 );
   FSkin.DrawPanel( FImage.Surface, 0, 0, Width, Height );
   IF ((Maximum - Minimum) > 0) THEN
   BEGIN
      FillAmount := ROUND(((Position - Minimum) / (Maximum - Minimum)) * (Width-2));
      FillArea.x := 1;
      FillArea.y := 1;
      FillArea.w := FillAmount;
      FillArea.h := Height-2;
      FImage.FillRect( Colour, @FillArea );
   END;
end;

constructor TSDLMeter.Create(Collection: TCollection);
begin
   inherited;
   FImage := TSDLImage.Create( NIL );
   FLabel := TSDLLabel.Create( NIL );
end;

destructor TSDLMeter.Destroy;
begin
   FImage.Free;
   FLabel.Free;
   inherited;
end;

procedure TSDLMeter.Draw(Target: PSDL_Surface);
begin
   IF (FImage.Draw( Target, Left + FOwner.Left, Top + FOwner.Top ) = -2) THEN
      ComposeImage;
   IF (FLabel.Caption <> '') THEN
      FLabel.Draw( Target );
end;

function TSDLMeter.GetHeight: INTEGER;
begin
   Result := FHeight;
end;

function TSDLMeter.GetWidth: INTEGER;
begin
   Result := FWidth;
end;

procedure TSDLMeter.Initialize(GUI: TSDLGUI);
begin
   inherited;
   FLabel.GUI := GUI;
   FLabel.Owner := Owner;
end;

procedure TSDLMeter.MouseDown(X, Y, Button: INTEGER; var Handled: BOOLEAN);
begin
   inherited;
   X := X - Left;
   Y := Y - Top;
   IF (X >= 0) AND (X <= Width) AND (Y >= 0) AND (Y <= Height) THEN
   BEGIN
      Handled := TRUE;
   END;
end;

procedure TSDLMeter.SetCaption(const Value: STRING);
VAR
   X, Y : INTEGER;
begin
   FLabel.Caption := Value;
   X := (Width DIV 2) - (FLabel.Width DIV 2);
   Y := (Height DIV 2) - (FLabel.Height DIV 2);
   FLabel.Left := X + Left;
   FLabel.Top := Y + Top;
end;

procedure TSDLMeter.SetFont(const Value: TSDLFont);
begin
   inherited;
   FLabel.Font := Value;
end;

procedure TSDLMeter.SetHeight(const Value: INTEGER);
begin
   FHeight := Value;
   IF (FWidth > 0) AND (FHeight > 0) THEN
      ComposeImage;
end;

procedure TSDLMeter.SetPosition(const Value: INTEGER);
begin
   FPosition := Value;
   ComposeImage;
end;

procedure TSDLMeter.SetWidth(const Value: INTEGER);
begin
   FWidth := Value;
   IF (FWidth > 0) AND (FHeight > 0) THEN
      ComposeImage;
end;

{ TSDLCheckControl }

procedure TSDLCheckControl.MouseDown(X, Y, Button: INTEGER;
  var Handled: BOOLEAN);
begin
  inherited;
   IF (Enabled) THEN
   BEGIN
      // Determine if it falls on the button and if it's been handled yet
      IF (X > Left) AND (X < (FLeft + Width)) AND (Y > Top) AND (Y < (Top + Height)) AND (Handled = FALSE) THEN
      BEGIN
         Handled := TRUE;
         IF (Group <> 0) AND (GUI.CheckGroupCount( Owner, Group ) = 1) THEN
         BEGIN
            Checked := NOT(Checked);
         END
          ELSE
         IF (Group <> 0) THEN
         BEGIN
            GUI.CheckGroupChange( Owner, Self, Group );
            Checked := TRUE;
         END;
      END;
   END;
end;

procedure TSDLCheckControl.SetHeight(const Value: INTEGER);
begin
   // Do nothing
end;

procedure TSDLCheckControl.SetWidth(const Value: INTEGER);
begin
   // Do nothing
end;

{ TSDLRadioButton }

procedure TSDLRadioButton.ComposeImage;
begin
   // Do nothing
end;

constructor TSDLRadioButton.Create(Collection: TCollection);
begin
   inherited;
   FLabel := TSDLLabel.Create( NIL );
end;

destructor TSDLRadioButton.Destroy;
begin
   FLabel.Free;
   inherited;
end;

procedure TSDLRadioButton.Draw(Target: PSDL_Surface);
begin
   FSkin.DrawRadioButton( Target, Left + Owner.Left, Top + Owner.Top, Checked );
   FLabel.Draw( Target );
end;

function TSDLRadioButton.GetHeight: INTEGER;
begin
   Result := RADIO_BUTTON_SELECTED.h;
end;

function TSDLRadioButton.GetWidth: INTEGER;
begin
   Result := RADIO_BUTTON_SELECTED.w + FLabel.Width;
end;

procedure TSDLRadioButton.Initialize(GUI: TSDLGUI);
begin
   inherited;
   FLabel.GUI := GUI;
end;

procedure TSDLRadioButton.SetCaption(const Value: STRING);
begin
   FCaption := Value;
   FLabel.Caption := Value;
end;

procedure TSDLRadioButton.SetFont(const Value: TSDLFont);
begin
   inherited;
   FLabel.Font := Value;
end;

procedure TSDLRadioButton.SetLeft(const Value: INTEGER);
begin
   inherited;
   FLabel.Left := RADIO_BUTTON_SELECTED.w + Value;
end;

procedure TSDLRadioButton.SetOwner(const Value: TSDLForm);
begin
   inherited;
   FLabel.Owner := Value;
end;

procedure TSDLRadioButton.SetTop(const Value: INTEGER);
begin
   inherited;
   FLabel.Top := Value;
end;

{ TSDLTTFFont }

function TSDLTTFFont.GetSize: INTEGER;
begin
   Result := FSize;
end;

procedure TSDLTTFFont.Load( FontName, FileName: STRING; PointSize: INTEGER);
begin
   Name := FontName;
   FSize := PointSize;
   FFont := TTF_OpenFont( PChar(FileName), PointSize );
   FColour.r := 255;
   FColour.g := 255;
   FColour.b := 255;
end;

function TSDLTTFFont.TextHeight(Text: STRING): INTEGER;
VAR
   W, H : INTEGER;
begin
   IF (FFont <> NIL) THEN
   BEGIN
      TTF_SizeText( FFont, PChar(Text), W, H );
      Result := H;
   END
    ELSE
      Result := 0;
end;

procedure TSDLTTFFont.TextOut(Target: PSDL_Surface; X, Y: INTEGER;
  Text: STRING);
VAR
   FImage : TSDLImage;
begin
   IF (FFont <> NIL) THEN
   BEGIN
      FImage := TSDLImage.Create( NIL );
      FImage.Surface := TTF_RenderText_Solid( FFont, PChar(Text), FColour );
      FImage.Draw( Target, X, Y );
      FImage.Free;
   END;
end;

function TSDLTTFFont.TextWidth(Text: STRING): INTEGER;
VAR
   W, H : INTEGER;
begin
   IF (FFont <> NIL) THEN
   BEGIN
      TTF_SizeText( FFont, PChar(Text), W, H );
      Result := W;
   END
    ELSE
      Result := 0;
end;

end.
