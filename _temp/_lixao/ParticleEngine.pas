unit ParticleEngine;
{****************************************************************************

 <!!!> ParticleEngine is one of the models of the OpenGL engine.
 <!!!> This is a *development* version of the ParticleEngine, NOT FINAL.
 <!!!> NOT for distribution.

     The contents of this file are subject to the Mozilla Public License
     Version 1.1 (the "License"); you may not use this file except in
     compliance with the License. You may obtain a copy of the License at
     http://www.mozilla.org/MPL/

     Software distributed under the License is distributed on an "AS IS"
     basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
     License for the specific language governing rights and limitations
     under the License.

     The Initial Developer of the Original Code is Ariel Jacob.
     Portions created by Ariel Jacob are Copyright (C) 2002 Ariel Jacob.

     Contributor(s):
       Ariel Jacob;               email: ariel@global-rd.com
       Kisn�meth R�bert.
       (you name might be here)
******************************************************************************}

{                  ***** Development dump area *****

  [General] add a parameter to the move function so 1.0 will be normal speed
    0.7 is 70% of the speed.
    how much time consuming is to multiply a real num for all the objects ?
    ( I want something faster then catapults.. :)
  [Particle] add more events. (im afraid that it will have effect the speed)
  [Particle] add Vector and Position to TParticleGroup ? (is it a privet Group?)
    auto regenerate particles until ParticleGroup is in place (the just wait to
    clear all Particles)
  [Particle] play with the size not just alpha. (life/fade, life/size) 
}

interface
uses
  Classes, OpenGL12, Math3D;

type
  PParticle = ^TParticle;
  TParticle = record
    Position  : T3DVector; // my Position in 3D space
    Vector    : T3DVector; // my speed in 3D space
    life      : Single;    // life = 0 => his dead
    fadeSpeed : Single;    // Dec(life, fadeSpeed)
    r, g, b   : Single;    // Particle color
  end;

  TOnGroupDone = procedure;//(var dead: boolean);

  TParticleGroup = class
    NumberOfParticles    : cardinal;  // Current particles number
    MaxNumberOfParticles : cardinal;  // lenght of dynamic array (MAX)
    Particles   : array of TParticle; // Stores the particles
    Dead        : boolean;            // Do we need to clear the group?
    ID          : byte;               // Group ID
    TextureID : TGLUInt;              // Stores the ID of the texture
    OnGroupDone : TOnGroupDone;       // Done/dead event
    procedure Free;
    procedure Move; virtual;
    procedure Draw; virtual;
    procedure AddParticle( const _r, _g, _b: Single;
      _Position, _Vector: T3DVector;
      _life, _fadeSpeed: Single);
    constructor Create( _MaxNumberOfParticles: Cardinal; _TextureID: TGLUInt;
      _ID: byte = 0 );
  end;

  TParticleGroupList = class(TList)
  protected
    function Get(Index: Integer): TParticleGroup;
    procedure Put(Index: Integer; Item: TParticleGroup);
  public
    property Items[Index: Integer]: TParticleGroup read Get write Put; default;
  end;

  TParticleSystem = class
    NumberOfParticleGroups: cardinal;
    ParticleGroupList: TParticleGroupList;
    function AddParticleGroup( ParticleGroup: TParticleGroup ): TParticleGroup;
    procedure RemoveParticleGroup( ParticleGroup: TParticleGroup ); overload;
    procedure RemoveParticleGroup( ParticleGroupID: byte ); overload;
    function ParticlesCount: cardinal;
    procedure Move;
    procedure Draw;
    procedure Free;
    constructor Create;
  end;

implementation

{ -- TParticleGroupList ------------------------------------------------------- }

function TParticleGroupList.Get(Index: Integer): TParticleGroup;
begin
 Result:=inherited Get(Index);
end;

procedure TParticleGroupList.Put(Index: Integer; Item: TParticleGroup);
begin
 inherited Put(Index, Item);
end;

{ -- TParticleGroupList ------------------------------------------------------- }
constructor TParticleGroup.Create( _MaxNumberOfParticles: Cardinal;
  _TextureID: TGLUInt; _ID: byte = 0);
begin
  inherited Create;
  Dead:= False;
  MaxNumberOfParticles:= _MaxNumberOfParticles;
  NumberOfParticles:= 0;
  ID:= _ID;
  OnGroupDone:= nil;
  TextureID:= _TextureID;
  SetLength( Particles, MaxNumberOfParticles );
  // texture, etc... !@#
end;

procedure TParticleGroup.Free;
begin
  // texture, list, etc... !@#
  SetLength( Particles, 0 );
  inherited free;
end;

procedure TParticleGroup.AddParticle( const _r, _g, _b: Single;
  _Position, _Vector: T3DVector; _life, _fadeSpeed: Single);
begin
  if NumberOfParticles = MaxNumberOfParticles then
    Exit;
  with Particles[NumberOfParticles] do
  begin
    r:= _r;
    g:= _g;
    b:= _b;
    Position := _Position;
    Vector   := _Vector;
    life     := _life;
    fadeSpeed:= _fadeSpeed;
  end;
  Inc(NumberOfParticles);
end;

procedure TParticleGroup.Move;
var
  I: Cardinal;
begin
  I:= 0;
  Dead:= True;
  if NumberOfParticles = 0 then Exit;
  while I < NumberOfParticles-1 do
  begin
    with Particles[I] do
    begin
      if life > 0 then
      begin
        Dead:= False;
        Position:= SumVectors( Position, Vector );
        life:= life - fadeSpeed;
      end;
    end;
    Inc(I);
  end;
  if Dead and Assigned(OnGroupDone) then
    OnGroupDone;
end;

// not sure how wrong am I. !@#
procedure TParticleGroup.Draw;
var
  I       : Cardinal;
  x, y, z : single;
begin
  if NumberOfParticles = 0 then Exit;
  I:= 0;
  // Select Our Texture */
  glBindTexture(GL_TEXTURE_2D, TextureID); 
  while I < NumberOfParticles-1 do
  begin
    with Particles[I] do
    begin
      if Life > 0 then
      begin
        x:= Position.X;
        y:= Position.Y;
        z:= Position.Z;

        glColor4f( r, g, b, life ); // Fade Based On It's Life

        // Build Quad From A Triangle Strip
        glBegin( GL_TRIANGLE_STRIP );
          // Top Right */
        glTexCoord2d( 1, 0 );
        glVertex3f( x + 0.5, y + 0.5, z );
          // Top Left */
        glTexCoord2d( 0, 0 );
        glVertex3f( x - 0.5, y + 0.5, z );
          // Bottom Right */
        glTexCoord2d( 1, 1 );
        glVertex3f( x + 0.5, y - 0.5, z );
          // Bottom Left */
        glTexCoord2d( 0, 1 );
        glVertex3f( x - 0.5, y - 0.5, z );
        glEnd();
      end;
    end;
    Inc(I);
  end;
end;

{ -- TParticleSystem ---------------------------------------------------------- }
constructor TParticleSystem.Create;
begin
  inherited;
  ParticleGroupList:= TParticleGroupList.Create;
  NumberOfParticleGroups:= 0;
end;

function TParticleSystem.AddParticleGroup(
  ParticleGroup: TParticleGroup ): TParticleGroup;
begin
  Inc( NumberOfParticleGroups );
  ParticleGroupList.Add( ParticleGroup );
  result:= ParticleGroup;
end;

procedure TParticleSystem.RemoveParticleGroup( ParticleGroup: TParticleGroup );
begin
  ParticleGroup.Free;
  Dec( NumberOfParticleGroups );
  ParticleGroupList.Remove( ParticleGroup );
end;

procedure TParticleSystem.RemoveParticleGroup( ParticleGroupID: byte );
var
  I       : Cardinal;
begin
  if NumberOfParticleGroups = 0 then Exit;
  I:= 0;
  while I < NumberOfParticleGroups do
  begin
    if ParticleGroupID = ParticleGroupList[I].ID then
      ParticleGroupList[I].Dead:= True;
    Inc(I);
  end;
end;

function TParticleSystem.ParticlesCount: cardinal;
var
  I       : Cardinal;
begin
  result:= 0;
  if NumberOfParticleGroups = 0 then Exit;
  I:= 0;
  while I < NumberOfParticleGroups do
  begin
    result:= result + ParticleGroupList[I].NumberOfParticles;
    Inc(I);
  end;
end;

procedure TParticleSystem.Move;
var
  I       : Cardinal;
begin
  if NumberOfParticleGroups = 0 then Exit;
  I:= 0;
  while I < NumberOfParticleGroups do
  begin
    ParticleGroupList[I].Move;
    if ParticleGroupList[I].Dead then
      RemoveParticleGroup( ParticleGroupList[I] );
    Inc(I);
  end;
end;

procedure TParticleSystem.Draw;
var
  I       : Cardinal;
begin
  if NumberOfParticleGroups = 0 then Exit;
  I:= 0;
  while I < NumberOfParticleGroups do
  begin
    ParticleGroupList[I].Draw;
    Inc(I);
  end;
end;

procedure TParticleSystem.Free;
var
  I       : Cardinal;
begin
  if NumberOfParticleGroups > 0 then
  begin
    I:= 0;
    while I < NumberOfParticleGroups do
    begin
      RemoveParticleGroup( ParticleGroupList[I] );
    end;
  end;
  inherited Free;
end;

end.
 