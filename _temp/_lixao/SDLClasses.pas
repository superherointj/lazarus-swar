unit SDLClasses;

interface

USES
   Classes, SDL, SDL_Image, SDL_TTF;

CONST
   COLOURKEY_R = 255;
   COLOURKEY_G = 0;
   COLOURKEY_B = 255;
   COLOURKEY_RGB = (COLOURKEY_R SHL 16) OR (COLOURKEY_G SHL 8) OR COLOURKEY_B;

TYPE
   TSDLImage = CLASS( TCollectionItem )
   PRIVATE
      FSurface : PSDL_Surface;
      FName : STRING;
      FFrames : INTEGER;
      FFrame : INTEGER;
      FFrameWidth : INTEGER;
      FTicksPerFrame : INTEGER;
      FTicks : INTEGER;
      FUNCTION GetWidth : INTEGER;
      FUNCTION GetHeight : INTEGER;
      PROCEDURE SetFrames( CONST Value : INTEGER );
   PUBLIC
      DESTRUCTOR Destroy; OVERRIDE;
      FUNCTION Alloc( W, H, BPP : INTEGER; HWSurface : BOOLEAN = TRUE; RGBAMask : CARDINAL = $FF000000) : BOOLEAN;
      FUNCTION Load( FileName : STRING; Prepare : BOOLEAN = TRUE ) : BOOLEAN;
      FUNCTION Draw( Target : PSDL_Surface; X, Y : INTEGER ) : INTEGER;
      FUNCTION DrawSub( Target : PSDL_Surface; SX, SY, SW, SH, TX, TY : INTEGER ) : INTEGER; OVERLOAD;
      FUNCTION DrawSub( Target : PSDL_Surface; Src : PSDL_RECT; X, Y : INTEGER ) : INTEGER; OVERLOAD;
      PROCEDURE Fill( Colour : CARDINAL );
      PROCEDURE FillRect( Colour : CARDINAL; Src : PSDL_RECT );
      PROCEDURE NextFrame;
      PROCEDURE Tick;
      PROPERTY Width : INTEGER READ GetWidth;
      PROPERTY Height : INTEGER READ GetHeight;
      PROPERTY Surface : PSDL_Surface READ FSurface WRITE FSurface;
      PROPERTY Name : STRING READ FName WRITE FName;
      PROPERTY Frame : INTEGER READ FFrame WRITE FFrame;
      PROPERTY Frames : INTEGER READ FFrames WRITE SetFrames;
      PROPERTY TicksPerFrame : INTEGER READ FTicksPerFrame WRITE FTicksPerFrame;
   END;

   TSDLImages = CLASS( TCollection )
   PRIVATE
      FUNCTION GetItem( Index : INTEGER ) : TSDLImage;
      PROCEDURE SetItem( Index : INTEGER; CONST Value : TSDLImage );
   PUBLIC
      FUNCTION ByName( Name : STRING ) : TSDLImage;
      FUNCTION Add : TSDLImage;
      FUNCTION Insert( Index : INTEGER ) : TSDLImage;
      PROPERTY Items[ Index : INTEGER ] : TSDLImage READ GetItem WRITE SetItem; DEFAULT;
   END;

   TSDLDisplay = CLASS
   PRIVATE
      FSurface : PSDL_Surface;
      FImages : TSDLImages;
      FCursor : BOOLEAN;
      PROCEDURE SetCursor( CONST Value : BOOLEAN );
   PUBLIC
      CONSTRUCTOR Create;
      DESTRUCTOR Destroy; OVERRIDE;
      FUNCTION Initialize( Caption : STRING; W, H, BPP : INTEGER; Fullscreen : BOOLEAN = FALSE ) : BOOLEAN;
      PROCEDURE Clear( Colour : CARDINAL );
      PROCEDURE Flip;
      PROCEDURE Draw( Image : TSDLImage; X, Y : INTEGER );
      procedure DrawSub(Image : TSDLImage; SX, SY, SW, SH, TX, TY : INTEGER );
      PROPERTY Images : TSDLImages READ FImages;
      PROPERTY Cursor : BOOLEAN READ FCursor WRITE SetCursor;
      PROPERTY Surface : PSDL_Surface READ FSurface;
   END;

implementation

USES
   SysUtils;

{ TSDLImage }

function TSDLImage.Alloc(W, H, BPP: INTEGER; HWSurface : BOOLEAN; RGBAMask : CARDINAL): BOOLEAN;
VAR
   Flags : CARDINAL;
   R, G, B, A : BYTE;
begin
   IF (FSurface <> NIL) THEN
   BEGIN
      SDL_FreeSurface( FSurface );
      FSurface := NIL;
   END;
   Flags := SDL_HWPALETTE OR SDL_ASYNCBLIT OR SDL_SRCCOLORKEY;
   IF (HWSurface) THEN
      Flags := Flags OR SDL_HWSURFACE;
   A := (RGBAMask AND $FF000000) SHR 24;
   R := (RGBAMask AND $00FF0000) SHR 16;
   G := (RGBAMask AND $0000FF00) SHR 8;
   B := (RGBAMask AND $000000FF);
   IF (W <= 0) THEN
      W := 1;
   FSurface := SDL_AllocSurface( Flags, W, H, BPP, R, G, B, A );
   SDL_SetColorKey( FSurface, SDL_SRCCOLORKEY, SDL_MapRGB( FSurface.Format, COLOURKEY_R, COLOURKEY_G, COLOURKEY_B ));
   SDL_FillRect( FSurface, NIL, SDL_MapRGB( FSurface.Format, COLOURKEY_R, COLOURKEY_G, COLOURKEY_B ) );
   FSurface := SDL_DisplayFormat( FSurface );
   Result := FSurface <> NIL;
end;

destructor TSDLImage.Destroy;
begin
   IF (FSurface <> NIL) THEN
      SDL_FreeSurface( FSurface );
   inherited;
end;

function TSDLImage.Draw(Target: PSDL_Surface; X, Y: INTEGER) : INTEGER;
VAR
   Dst : TSDL_RECT;
   Src : TSDL_RECT;
begin
   Result := -1;
   IF (FSurface <> NIL) THEN
   BEGIN
      IF (FFrames <> 0) THEN
      BEGIN
         Src.x := FFrameWidth * FFrame;
         Src.y := 0;
         Src.w := FFrameWidth;
         Src.h := FSurface.h;
      END
       ELSE
      BEGIN
         Src.x := 0;
         Src.y := 0;
         Src.w := FSurface.w;
         Src.h := FSurface.h;
      END;

      Dst.x := X;
      Dst.y := Y;
      Dst.w := FSurface.w;
      Dst.h := FSurface.h;
      Result := SDL_BlitSurface( FSurface, @Src, Target, @Dst );
   END;
end;

function TSDLImage.DrawSub(Target: PSDL_Surface; SX, SY, SW, SH, TX, TY: INTEGER) : INTEGER;
VAR
   Dst : TSDL_RECT;
   Src : TSDL_RECT;
begin
   Src.x := SX;
   Src.y := SY;
   Src.w := SW;
   Src.h := SH;

   Dst.x := TX;
   Dst.y := TY;
   Dst.w := SW;
   Dst.h := SH;

   Result := SDL_BlitSurface( FSurface, @Src, Target, @Dst );
end;

function TSDLImage.DrawSub(Target: PSDL_Surface; Src: PSDL_RECT; X, Y: INTEGER) : INTEGER;
VAR
   Dst : TSDL_RECT;
begin
   Dst.x := X;
   Dst.y := Y;
   Dst.w := Src.w;
   Dst.h := Src.h;

   Result := SDL_BlitSurface( FSurface, Src, Target, @Dst );
end;

procedure TSDLImage.Fill(Colour: CARDINAL);
VAR
   R, G, B : BYTE;
begin
   R := (Colour AND $00FF0000) SHR 16;
   G := (Colour AND $0000FF00) SHR 8;
   B := (Colour AND $000000FF);
   Colour := SDL_MapRGB( FSurface.Format, R, G, B );
   SDL_FillRect( FSurface, NIL, Colour );
end;

procedure TSDLImage.FillRect(Colour: CARDINAL; Src: PSDL_RECT);
VAR
   R, G, B : BYTE;
begin
   R := (Colour AND $00FF0000) SHR 16;
   G := (Colour AND $0000FF00) SHR 8;
   B := (Colour AND $000000FF);
   Colour := SDL_MapRGB( FSurface.Format, R, G, B );
   SDL_FillRect( FSurface, Src, Colour );
end;

function TSDLImage.GetHeight: INTEGER;
begin
   IF (FSurface <> NIL) THEN
      Result := FSurface.h
    ELSE
      Result := 0;
end;

function TSDLImage.GetWidth: INTEGER;
begin
   IF (FSurface <> NIL) THEN
   BEGIN
      IF (FFrames > 0) THEN
         Result := FSurface.w DIV FFrames
       ELSE
         Result := FSurface.w
   END
    ELSE
      Result := 0;
end;

function TSDLImage.Load(FileName: STRING; Prepare : BOOLEAN): BOOLEAN;
begin
   FSurface := IMG_Load( PChar(FileName) );
   IF (FSurface <> NIL) AND (Prepare) THEN
   BEGIN
      SDL_SetColorKey( FSurface, SDL_SRCCOLORKEY OR SDL_RLEACCEL, SDL_MapRGB( FSurface.Format, COLOURKEY_R, COLOURKEY_G, COLOURKEY_B ));
      FSurface := SDL_DisplayFormat(FSurface);
   END;
   Result := FSurface <> NIL;
end;

procedure TSDLImage.NextFrame;
begin
   INC(FFrame);
   IF (FFrame >= FFrames) THEN
      FFrame := 0;
end;

procedure TSDLImage.SetFrames(const Value: INTEGER);
begin
   FFrames := Value;
   FFrameWidth := FSurface.w DIV FFrames;
end;

procedure TSDLImage.Tick;
begin
   INC(FTicks);
   IF (FTicks >= FTicksPerFrame) THEN
   BEGIN
      NextFrame;
      FTicks := 0;
   END;
end;

{ TSDLImages }

function TSDLImages.Add: TSDLImage;
begin
   Result := TSDLImage( INHERITED Add );
end;

function TSDLImages.ByName(Name: STRING): TSDLImage;
VAR
   Counter : INTEGER;
begin
   FOR Counter := 0 TO (Count-1) DO
   BEGIN
      IF (CompareText(Items[Counter].Name, Name) = 0) THEN
      BEGIN
         Result := Items[Counter];
         Exit;
      END;
   END;
   Result := NIL;
end;

function TSDLImages.GetItem(Index: INTEGER): TSDLImage;
begin
   Result := TSDLImage( INHERITED GetItem( Index ));
end;

function TSDLImages.Insert(Index: INTEGER): TSDLImage;
begin
   Result := TSDLImage( INHERITED Insert( Index ));
end;

procedure TSDLImages.SetItem(Index: INTEGER; const Value: TSDLImage);
begin
   INHERITED SetItem( Index, Value );
end;

{ TSDLDisplay }

procedure TSDLDisplay.Clear(Colour: CARDINAL);
VAR
   NewRGB : CARDINAL;
begin
   NewRGB := SDL_MapRGB( FSurface.format, (Colour AND $00FF0000) SHR 16, (Colour AND $0000FF00) SHR 8, (Colour AND $000000FF) );
   SDL_FillRect( FSurface, NIL, NewRGB );
end;

constructor TSDLDisplay.Create;
begin
   FImages := TSDLImages.Create( TSDLImage );
end;

destructor TSDLDisplay.Destroy;
begin
   FImages.Free;
   IF (FSurface <> NIL) THEN
      SDL_FreeSurface( FSurface );
   SDL_QUIT;
   inherited;
end;

procedure TSDLDisplay.Draw(Image: TSDLImage; X, Y: INTEGER);
begin
   Image.Draw( FSurface, X, Y );
end;

procedure TSDLDisplay.DrawSub(Image : TSDLImage; SX, SY, SW, SH, TX, TY : INTEGER );
begin
   Image.DrawSub( FSurface, SX, SY, SW, SH, TX, TY );
end;

procedure TSDLDisplay.Flip;
begin
   SDL_Flip( FSurface );
end;

function TSDLDisplay.Initialize( Caption : STRING; W, H, BPP: INTEGER;
  Fullscreen: BOOLEAN): BOOLEAN;
VAR
   Flags : CARDINAL;
begin
   Result := FALSE;
   // Only attempt to alloc the display if the video inits correctly
   IF (SDL_Init( SDL_INIT_VIDEO ) >= 0) THEN
   BEGIN
      IF (TTF_INIT >= 0) THEN
      BEGIN
         // Create the display and set the caption
         Flags := SDL_ASYNCBLIT OR SDL_DOUBLEBUF OR SDL_HWSURFACE;
         IF (Fullscreen) THEN
            Flags := Flags OR SDL_FULLSCREEN;
         FSurface := SDL_SetVideoMode( W, H, BPP, Flags );
         SDL_WM_SetCaption( PChar(Caption), NIL );

         Result := TRUE;
      END;
   END;
end;

procedure TSDLDisplay.SetCursor(const Value: BOOLEAN);
begin
   FCursor := Value;
   IF (Value) THEN
      SDL_ShowCursor( 1 )
    ELSE
      SDL_ShowCursor( 0 );
end;

end.
