unit Math3D;

interface
uses Math;

type
  P3DVector = ^T3DVector;
  T3DVector = record
    X, Y, Z : Single;
  end;

  function SumVectors( var v1,v2 : T3DVector) :T3DVector;
  function TVector( x, y, z: Single): T3DVector;

  procedure VectorRotateX(ang : Single; var dest : T3DVector);
  procedure VectorRotateY(ang : Single; var dest : T3DVector);
  procedure VectorRotateZ(ang : Single; var dest : T3DVector);
  
implementation
{ -- Vectors Math ------------------------------------------------------------- }
function SumVectors( var v1,v2 : T3DVector) :T3DVector;
begin
  result.X:= v1.X + v2.X;
  result.Y:= v1.Y + v2.Y;
  result.Z:= v1.Z + v2.Z;
end;

function TVector( x, y, z: Single): T3DVector;
begin
  result.X:= x;
  result.Y:= y;
  result.Z:= z;
end;

procedure VectorRotateX(ang : Single; var dest : T3DVector);
var
  y0, z0 : Single;
  radAng : Single;
begin
  y0 := dest.Y;
  z0 := dest.Z;
  radAng := DegToRad(ang);

  dest.Y := (y0 * cos(radAng)) - (z0 * sin(radAng));
  dest.Z := (y0 * sin(radAng)) + (z0 * cos(radAng));
end;

procedure VectorRotateY(ang : Single; var dest : T3DVector);
var
  x0, z0 : Single;
  radAng : Single;
begin
  x0 := dest.X;
  z0 := dest.Z;
  radAng := DegToRad(ang);

  dest.X := (x0 * cos(radAng)) + (z0 * sin(radAng));
  dest.Z := (z0 * cos(radAng)) - (x0 * sin(radAng));
end;

procedure VectorRotateZ(ang : Single; var dest : T3DVector);
var
  x0, y0 : Single;
  radAng : Single;
begin
  x0 := dest.X;
  y0 := dest.Y;
  radAng := DegToRad(ang);

  dest.X := (x0 * cos(radAng)) - (y0 * sin(radAng));
  dest.Y := (y0 * cos(radAng)) + (x0 * sin(radAng));
end;

end.
