program SWAR;

{$DEFINE GAME_CHEAT_ON}

{
                   ��������������������������������������������
                   *                                          *
                   *  SSSSSS  W          W    A      RRRRRR   *
                   *  S       W         W    A A     R     R  *
                   *  SSSSSS   W   W   W    A   A    RRRRRR   *
                   *       S    W W W W    A AAA A   R    R   *
                   *  SSSSSS     W   W    A       A  R     R  *
                   *                                          *
                   ��������������������������������������������
{******************************************************************************}
{                                                                              }
{                        SWAR (Smace's WAR)                                    }
{                                                                              }
{ The initial developer of this Pascal code was :                              }
{ Sergio Marcelo <smace@ieg.com.br>                                            }
{                                                                              }
{ Portions created by Sergio Marcelo are                                       }
{ Copyright (C) 2002 Sergio Marcelo.                                           }
{                                                                              }
{                                                                              }
{ Contributor(s)                                                               }
{ --------------                                                               }
{                                                                              }
{ Obtained through:                                                            }
{ Joint Endeavour of Delphi Innovators ( Project JEDI )                        }
{                                                                              }
{ You may retrieve the latest version of this file at the Project              }
{ JEDI home page, located at http://delphi-jedi.org                            }
{                                                                              }
{ The contents of this file are used with permission, subject to               }
{ the Mozilla Public License Version 1.1 (the "License"); you may              }
{ not use this file except in compliance with the License. You may             }
{ obtain a copy of the License at                                              }
{ http://www.mozilla.org/MPL/MPL-1.1.html                                      }
{                                                                              }
{ Software distributed under the License is distributed on an                  }
{ "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or               }
{ implied. See the License for the specific language governing                 }
{ rights and limitations under the License.                                    }
{                                                                              }
{ Description                                                                  }
{ -----------                                                                  }
{   SWAR (Smace's WAR)                                                         }
{                                                                              }
{                                                                              }
{ Requires                                                                     }
{ --------                                                                     }
{   SDL.dll on Windows platforms                                               }
{                                                                              }
{                                                                              }
{ Programming Notes                                                            }
{ -----------------                                                            }
{ TO-DO List:

  � The map should accept bricks, mountains, ice etc
  � Hierachy (Cabo, Soldier, Major etc)
  � Border
  � Better Graphics - of course
  � O inimigo tamb�m deveria poder pegar os itens
  � Itens: Invencibilidade, Time (faz para todos os outros tanks), Granada, Mina,
  Invisibilidade (os tanks param de atirar por 10s)

  � Add two players game, and networking game as two player mode (friends)
  � I must fix:
    �  CollisionDetect in almost all classes and bricks.... shot_power > 2 = problem
                       When the the speed is 2 or high the tank sometimes can't
                       pass in some places (32x32).
    �  The tank shouldn't run walk in the water
    �  DrawMenu, should have a way, to redraw only what u want. It's faster.
  � When u destroy a brick, u may get a ITEN
  � Adicionar n�vel de dificuldade ao SWAR, sendo que no hard n�o dever�o
    aparecer itens adoidados como aparece no jogo como est�, os itens devers�o
    ser conseguidos destroindo os tijolos, e ser� dificil de conseguir itens.
    No hard os pontos p/ o hig score aumentar�o igual loko e os tanks ser�o mais
    inteligentes (se poss�vel :)
  � Adicionar dois jogadores, e multi-player game.
  � The game (tank, shot) should be a lil. bit faster, because it's boring when
    the game gets slow.
  � DrawMenu procedure: I should change this procedure, to make the game faster,
    only should be redraw what is need.
  � Draw a better base (img)
  � Make the finish
  � Change the screen when u go to the next level, try to create any one like:
     penguin-command
  � When u get an iten or u receive something it should be shown in the screen,
    like in other games, "Bonus 10!" and it would go up, and then disappear.
  � The game must be fast for be cool! :)
  � I must change the screen for next level.
  � Lacks a sounds for next_level... like "tananam!" :)
  � The itens should not appear above the bricks.
  � Aparecer um tanka cada 5 secs ateh ter 8 tanks em jogo.
  � Devido ao fato de o tank andar 2 pixels por vez, as vezes, nao � possivel
    passar por locais 32x32, corrigir isso
  � The DetectColision of the base should be done at the Map array and not by
    the sprite engine. Once the base doesn't move.
  � Add more enemies tanks with life (shield), a stronger one.
  � The base detection colision isn't working well when the shot hits by the left.

}
{ Revision History                                                             }
{ ----------------                                                             }
{ XX/XX/2002: Was created the game                                             }
{                                                                              }
{                                                                              }
{******************************************************************************}

{$R images/images.res}

uses
  SysUtils,
  SDLUtils,
  SDL,
  Logger,
  SDLSprites,
  SDL_Image,
  SDL_Mixer,
  SFont;

const
  App_title     = 'Smace''s WAR by Smace Design - Unfinished Version';
  bpp           = 16;
  Tick_Interval = 33;

  //Ids
  idMyTank      = 1;
  idTiro        = 2;
  idEnemyTank   = 3;
  idTileMap     = 4;
  idIten        = 5;
  idExplosion   = 6;
  idBase        = 7;
  idButton      = 8;
  idPainel      = 9;

  //Z
  zMyTank    = 1;
  zEnemyTank = 1;
  ZTiro      = 2;
  zExplosion = 3;
  zIten      = 2;
  zPanel     = 10;
  zBase      = 5;
  zButton    = 1;
  MapID : array[0..6] of byte = ( 0, 1, 2, 3, 4, 5, 6 );

type

  //TMap = array[0..79, 0..51] of byte; // 640x416
  //TMap = array[0..79, 0..51] of byte; // 800x600
  TMap = array[0..127, 0..87] of byte; // 1024x768


{
  TLetter = class( TSprite )
    XCounter, YCounter, Posicao : integer;
    constructor Create( _Image : PSDL_Surface; ch : char; Width, Height, _pos : integer);
    procedure SetChar( ch : char );
    procedure Move; override;
    procedure Draw; override;
    procedure Free; override;
  end;
}

  TPlayer = class(TSprite)
  public
    xi, yi, ttime, direction, direction_last, fx, fx_time, anim_pos, temp_anim_pos : integer;
    DestRect: TSDL_Rect;
    flip : boolean;
    constructor create;
    procedure Move; override;
    procedure Free; override;
    procedure Draw; override;
    procedure GetCollisionRect(Rect: PSDL_Rect); override;
    procedure WasHit    ;
    procedure DetectCollision;
    procedure Left;
    procedure Right;
    procedure Up;
    procedure Down;
  end;

  TEnemyTank = class(TSprite)
  public
    xi, yi, tiros, speed, direction, ttime, anim_pos,  temp_anim_pos, last_move, count_move,
    count_move2, Shotspeed, Shotdelay, Shield, Power : integer;
    DestRect: TSDL_Rect;
    constructor create;
    procedure Move; override;
    procedure Draw; override;
    procedure DetectCollision;
    procedure WasHit;
    procedure GetCollisionRect(Rect: PSDL_Rect); override;
    procedure Left;
    procedure Right;
    procedure Up;
    procedure Down;
  end;

  TMapa = class(TSprite)
  public
    xi, yi, tipo1, tipo2 : integer; //, anim_pos, anim_pos_temp
    DestRect: TSDL_Rect;
    constructor create(var x_, y_, z_, tipo1_, tipo2_ : integer);
    procedure Draw; override;
  end;

  TIten = class(TSprite)
    PRIVATE
      anim_pos,
      anim_pos_temp,
      tipo,     //What kind of Iten
      time : integer;
      DestRect: TSDL_Rect;
    PUBLIC
      constructor create(var x_,y_,tipo_ : integer);
      procedure Draw; override;
  END;

  TBase = class(TSprite)
    PRIVATE
      tipo : integer;    //What kind of Base
      DestRect: TSDL_Rect;
    PUBLIC
      constructor create;//(var x_,y_,tipo_ : integer);
      procedure Draw; override;
      procedure DetectCollision;
      procedure WasHit;
      procedure GetCollisionRect(Rect: PSDL_Rect); override;
  END;

  TTiro = class(TSprite)
    PRIVATE
      direction,
      anim_pos,
      tipo,     //Is the shot enemy or friend?!
      speed,
      power,
      temp_anim_pos : integer;
      DestRect: TSDL_Rect;
      procedure DestroyBrick( mx, my : integer );
      procedure DetectCollision;
      procedure Left;
      procedure Right;
      procedure Up;
      procedure Down;

    PUBLIC
      constructor create(var x_,y_,direction_,tipo_, speed_, power_ : integer);
      procedure GetCollisionRect(Rect: PSDL_Rect);
      procedure Draw; override;
      procedure Move; override;
  END;

  TExplosion = class(TSprite)
  public
    DestRect: TSDL_Rect;
    Rects: array[0..7] of TSDL_Rect;
    //Speed, SpeedCounter: integer;
    Tipo, Power : integer;
    constructor Create(x_, y_, tipo_, power_: integer);
    procedure Move; override;
    procedure Draw; override;
  end;

  TPainel = class( TSprite )
  public
    Img_panel : PSDL_Surface;
  //  Time4die : Integer;
    constructor Create(_w, _h, _time4die : integer);
    procedure Draw; override;
  end;

  TScene = ( scIntro, scTitle, scGame, scOptions, scDone, scQuitApp, scHighScore, scMenu, scGameMenu, scCredits );
  TDrawMenu = ( drItens, drLives, drLevel, drScore, drAmmo, drAll );



var
  {$IFDEF GAME_CHEAT_ON}
  game_cheat_level :integer;
  {$ENDIF}



  //Images
  Background : PSDL_Surface = nil;
  Screen     : PSDL_Surface = nil;
  Img_Background_,
  Img_Tiles,
  Img_Font_digital19x24,
  Img_Font_digital10x13,
  Img_Arsenal,
  Img_Painel,
  Img_HighScore,
  Img_Tanks,
  Img_FIcons,
  Img_Terrenos,
  Img_Menu,
  Img_Font_,
  Img_Fundo_Camu_Menu,
  Img_Itens,
  Img_Base,
  Img_Explosion : PSDL_Surface;
  //Img_FSmallD

  //Playing
  Pl_level,
  Pl_score,
  //Pl_lives,
  //Pl_score,
  Pl_max_ntank,
  Pl_tiros,
  Pl_nenemy,
  Pl_nenemy_temp,
  Pl_lives : integer;
  Pl_StartPosX : integer = 0;
  Pl_StartPosY : integer = 0;
  Pl_BasePosX : integer = 0;
  Pl_BasePosY : integer = 0;

  //MyTank
  //Player1, Player2 : TPlayer; //for two players

  MyTank_Shot,
  MyTank_Shield,
  MyTank_Speed,
  MyTank_Ammo,
  MyTank_Power : integer;
//const
  MyTank_ShotDelay : array[1..3] of integer = ( 20, 16, 12 );
  MyTank_ShotSpeed : array[1..3] of integer = ( 4, 5, 6 );

//var
  //Sounds
  Sounds : array[0..5] of PMix_Chunk;
  Musics : array[0..1] of PChar;
  SoundFlag   : Cardinal = SDL_INIT_AUDIO;
  NoSound     : Boolean = True;
  PlaySoundFX : Boolean = True;
//  PlayMusic   : Boolean;
  Music       : PMix_Music = nil;
  MusicList   : array of String = nil;
  LastSong    : byte = 255;
  MusicVolume : byte = 128;// 109 = 85%
  HighScore_Name  : array[0..2] of String;
  HighScore_Level : array[0..2] of String;
  HighScore_Score : array[0..2] of String;

  //Rects of the game
  Rects_Map:       array[0..2,0..6] of TSDL_Rect;
  Rects_Tanks:     array[0..1,0..1,0..3] of TSDL_Rect;
  Rects_Arsenal:   array[0..4,0..1,0..3] of TSDL_Rect;
  Rects_Itens:     array[0..6,0..1] of TSDL_Rect;
  Rects_FIcons:    array[0..4] of TSDL_Rect;
  Rects_Base :     array[0..1] of TSDL_Rect;
  Rects_Explosion: array[0..1,0..7] of TSDL_Rect;

  //Other variables
  Event : TSDL_Event;
  RunScene : TScene;
  GameMap : TMap;
  SpriteEngine : TSpriteEngine;
  Next_Time : cardinal = 0;
  Keys: PKeyStateArr;

  max_X, max_Y, MouseX, MouseY,
  Screen_Width, Screen_Height, BG_WIDTH, BG_HEIGHT : integer;


  {  Screen_Width  : integer = 640;
  Screen_Height : integer = 480;
  BG_WIDTH : integer = 640;
  BG_HEIGHT : integer = 416;}
  //BG_WIDTH  = 640;
  //BG_HEIGHT = 416;
  Res_Screen : Shortint = 1;

  Fullscreen  : Boolean = False;
  Mousedown   : Boolean = False;
  Quit        : Boolean = False;
  PlayerAlive : Boolean = False;
  MyBase      : TBase;

{ - TLetter -------------------------------------------------------------- }

{constructor TLetter.Create( _Image : PSDL_Surface; ch : char; Width, Height, _Pos : integer );
begin
  inherited Create( '', Width, Height );
  Image := _Image;
  Posicao := _pos;
  z := LetterZ;
  SetChar( ch );
  XCounter := random( 64 );
  YCounter := random( 64 );
end;

procedure TLetter.SetChar( ch : char );
const
  //Lacks those letters: "=, x"
  Letters='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@.-":()/<>,*'';[]{}
  //+�������$';
{var
  i : integer;
begin
  i := pos( ch, Letters ) - 1;
  SrcRect.y := i * w;
end;

procedure TLetter.Move;
begin
  inc( XCounter, 1 + random( 2 ) );
  if XCounter > 63 then XCounter := XCounter - 64;

  inc( YCounter, 1 + random( 2 ) );
  if YCounter > 63 then YCounter := YCounter - 64;
end;

procedure TLetter.Draw;
const
  SinTable : array[0..15] of integer = ( 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, -1, -1,
    -1, -1, -1, -1 );
var
  DestRect : TSDL_Rect;
begin
  DestRect.x := x + SinTable[XCounter shr 2]- (8*Posicao);
  DestRect.y := y + SinTable[YCounter shr 2];
  SDL_UpperBlit( Image, @SrcRect, Surface, @DestRect );
  PrevRect := DestRect;
end;

procedure TLetter.Free;
begin
  Image := nil;
  inherited Free;
end;

//--- End of TLetter }

procedure Finalize; Forward;

Function img_LoadImage( var Image: PSDL_Surface; Const ImagePath: PChar;
                    colorkey : boolean = false; R : byte = 255; G : byte = 0; B : byte = 255;
                    exit: boolean = True):Boolean;
var
  TempImage : PSDL_Surface;
begin
  Result:= False;
  TempImage:= IMG_Load( ImagePath );
  if TempImage = nil then
  begin
    log.LogError( Format('Loading & creating '+ ImagePath +' surface failed. %s', [SDL_GetError]), 'Initialize');

    if exit then
    begin
      Finalize;
      Halt( 1 );
    end;
  end
else
  begin
    Result:= True;
    If colorkey then
    SDL_SetColorKey( TempImage, SDL_SRCCOLORKEY or SDL_RLEACCEL or SDL_HWACCEL, SDL_MapRGB(
                     TempImage.format, R, G, B ) );
    Image := SDL_DisplayFormat( TempImage );
    SDL_FreeSurface( tempimage );
  end;
end;  //img_LoadImage

procedure img_ColonizeSurface( Surface: PSDL_Surface; R,G,B: byte; border: byte = 0;
                           Up: boolean = True);
var
  TargetRect : TSDL_Rect;
  Clear, Dark : UInt32;
  I     : byte;
begin
  Clear := SDL_MapRGB( Surface.format, R,G,B );

  With TargetRect do
  begin
    x := 0;
    y := 0;
    w := Surface.w;
    h := Surface.h;
  end;

  SDL_FillRect( Surface , @TargetRect , Clear);
  if border > 0 then
  begin
    SDL_LockSurface( Surface );
    if Up then begin
      Clear:= 0;
      Dark := SDL_MapRGB( Surface.format, 255,255,255 );
    end else begin
      Clear:= SDL_MapRGB( Surface.format, 255,255,255 );
      Dark := 0;
    end;
    for I:=0 to border do
    begin
      SDL_DrawLine( Surface, 0 + I, 0, 0 + I, Surface.h - 1 - I, Dark ); // left line
      SDL_DrawLine( Surface, Surface.w - 1 -I, 0 + I, Surface.w - 1 - I, Surface.h - 1, Clear ); // right line
      SDL_DrawLine( Surface, 0, 0 + I, Surface.w - 1 - I, 0 + I, Dark ); // top line
      SDL_DrawLine( Surface, 0 + I, Surface.h - 1 - I, Surface.w - 1, Surface.h - 1 - I, Clear ); // bottom line
    end;
    SDL_UnlockSurface( Surface );
  end;
end;

{procedure img_RS_Blit(bitmap: PSDL_SURFACE; x, y: LongInt; center, enabledCOLORKEY : boolean; R, G, B : integer);
var
  dest: SDL_RECT;
begin
  dest.w := bitmap.w;
  dest.h := bitmap.h;
  if center then
  begin
    dest.x := (trunc(SCREEN_WIDTH/2 - bitmap.w/2));
    dest.y := (trunc(SCREEN_HEIGHT/2 - bitmap.h/2));
  end
  else
  begin
    dest.x := x;
    dest.y := y;
  end;
  If EnabledColorKEY then SDL_SetColorKey(bitmap, SDL_SRCCOLORKEY or SDL_RLEACCEL, SDL_MapRGB(bitmap.format, R, G, B));
  SDL_UpperBlit(bitmap, nil, screen, @dest);
end;} //RS_Blit

{function img_Prep_Image(filename: string): PSDL_SURFACE;
var
  image: PSDL_SURFACE;
begin
  image := SDL_LoadBMP(PChar(filename));
  if image <> nil then
    Result := image
  else
  begin
    Result := nil;
    log.LogError( Format( '%s : ' + filename, [SDL_GetError] ), 'Loading IMG' );
    SDL_Quit;
    halt;
  end;

end;}


{ - TPainel ---------------------------------------------------------- }

constructor TPainel.Create(_w, _h, _time4die : integer);
var
  i, al : integer;
  temp_camu : PSDL_Surface;
  DestRect2 : TSDL_Rect;
begin
  inherited Create( '', _w, _h );
  id := idPainel;
  //Time4die := _time4die;
  img_Panel := SDL_CreateRGBSurface( SDL_SWSURFACE, _w, _h,
    Screen.format.BitsPerPixel, Screen.format.RMask, Screen.format.GMask,
    Screen.format.BMask, Screen.format.AMask );

  img_ColonizeSurface( Img_Panel, 0, 0, 0, 1);
  img_LoadImage( temp_camu, 'images/fundo_camu_menu.png', false, 0, 0, 0, true);

  al := 128;
  for i := 0 to 3 do
   if al > 0 then
   begin
     DestRect2.x := i * 204;
     DestRect2.y := 0;
     SDL_SetAlpha( temp_camu, SDL_SRCALPHA, al );
     SDL_UpperBlit( temp_camu, nil, Img_Panel, @DestRect2 );
     dec(al,32);
   end;
  SDL_FreeSurface(temp_camu);
  SDL_SetAlpha( img_Panel, 0, 0 );
  z := zPanel;
end;

procedure TPainel.Draw;
var
  DestRect : TSDL_Rect;
begin
  DestRect.x := trunc((BG_WIDTH  - w)/2);
  DestRect.y := trunc((BG_HEIGHT - h)/2);
{
  x := trunc((BG_WIDTH  - _w)/2);
  y := trunc((BG_HEIGHT - _h)/2);
}
  SDL_UpperBlit( Img_panel, @SrcRect, Surface, @DestRect );
  PrevRect := DestRect;
 // dec(Time4die);
 // If Time4die < 1 then Kill;
end;


function snd_PlaySound( SoundID, Volume : integer): Boolean;
//var FX : PMix_Chunk;
begin
 // Result:= False;
 // If NOT PlaySoundFX or NoSound then Exit;
  Mix_GroupAvailable(0);
  Mix_VolumeChunk(sounds[SoundID], Volume );
  Result:= Mix_PlayChannel(-1, sounds[SoundID], 0) <> -1;
end;

{function snd_LoadMusic( num: byte ): String;
begin
  result:= '';
 // if NOT PlayMusic or NoSound then exit; // Sound enabled?

  music := Mix_LoadMUS( PChar( 'sound/music/' + MusicList[num] ) );
  if Music = nil then
  begin
    Log.LogError( 'SDL_MIXER: loading ' + MusicList[num] + ' failed', 'LoadMusic' );
    result:= 'Error loading :'+MusicList[num];
    LastSong:= 0;
  end else
  begin
    Log.LogStatus( 'SDL_MIXER: loading ' + MusicList[num] + ' was successful',
      'Initialize' );
    LastSong:= num;
    result:= 'Now playing - '+Copy( MusicList[num], 1, pos('.', MusicList[num])-1 );
  end;
end;}

{function snd_SelectSong: byte;
var
  newSong, I: byte;
begin
  I:= 0;
  newSong:= Random( High(MusicList)+1 );
  while (I < 15) and (newSong = LastSong) do
  begin
    newSong:= Random( High(MusicList)+1 );
    Inc(I);
  end;
  result:= newSong;
end;}

procedure Game_DrawMenu(Drawmenu : TDrawMenu);  Forward;
procedure Game_StartGame; Forward;
procedure Game_DiePlayer; Forward;
procedure Game_NextLevel; Forward;

procedure snd_StopMusic;
begin
  if NoSound then exit; // Sound enabled?

  if music <> nil then
    Mix_FreeMusic( music );
  Music := nil; // 0, It's not neccessary but who knows...?
end;

{procedure snd_LoadMusicList;
var
  FileAttrs: Integer;
  sr : TSearchRec;
  I  : byte;
begin
  SetLength(MusicList, 0);
  I:= 1;
  FileAttrs := faAnyFile;
  if FindFirst('sound/music/'+'*.*', FileAttrs, sr) = 0 then
  begin
    repeat
      if ((sr.Attr and FileAttrs) = sr.Attr)and
         ( (Pos('it', sr.Name) <> 0)or(Pos('mod', sr.Name) <> 0) ) then
      begin
        SetLength(MusicList, I);
        MusicList[I-1] := sr.Name;
        Inc(I);
      end;
    until FindNext(sr) <> 0;
    FindClose(sr);
  end else PlayMusic:= False;
  if I = 1 then PlayMusic:= False;
end;}

procedure snd_StartMusic(num: byte = 255);
begin
//  if NOT PlayMusic or NoSound then exit; // Sound enabled?
  //if num = 255 then num:= snd_SelectSong;
  music := Mix_LoadMUS( Musics[0] );
  Mix_PlayMusic( music , 0 ); //music
  Mix_VolumeMusic( MusicVolume ); //Music Volume
end;

procedure snd_RestartMusic;
begin
  // This is a hook for restarting music automatically when it ends
  if Mix_PlayingMusic = 0 then
  begin
    log.LogStatus( 'RestartMusic HOOK executed', 'RestartMusic' );
    snd_StopMusic;
    snd_StartMusic;
//    StartMusic; (random)
  end;
end;

//-------- BEGIN OF EXPLOSION ---------

constructor TExplosion.Create(x_, y_, tipo_, power_: integer);
begin
  inherited Create('', 32, 32);
  ID := idExplosion;
  x := x_;
  y := y_;
  z := zExplosion;
  tipo := tipo_;
  power := power_;
end;

procedure TExplosion.Move;
begin
  inc(AnimPhase);
  If tipo = 0 then if AnimPhase = 8 shl 2 then Kill;
  If tipo = 1 then if AnimPhase = 2 shl 2 then Kill;
end;

procedure TExplosion.Draw;
begin
dec(x,1);
dec(y,1);
  DestRect.x := x;
  DestRect.y := y;
  SDL_UpperBlit(Img_Explosion, @Rects_Explosion[tipo,AnimPhase shr 2], Surface, @DestRect);
  PrevRect := DestRect;
end;

//-------- BEGIN OF ITEN ---------

constructor TIten.Create(var x_,y_,tipo_ : integer); //falta a dire��o
begin
  Inherited Create('',16,16);
  id := idIten;
  tipo := tipo_;
  anim_pos := 0;
  anim_pos_temp := 0;
  time := 0;
  x := x_;
  y := y_;
  z := zIten;
end;

procedure TIten.Draw;
begin
  inc(time); inc(anim_pos_temp);

  if anim_pos_temp > 5 then
  begin
    Anim_pos := 1 - Anim_pos;
    anim_pos_temp := 0;
  end;

  DestRect.x := x;
  DestRect.y := y;
  SDL_UpperBlit(Img_Itens, @Rects_itens[tipo,anim_pos], Surface, @DestRect);
  PrevRect := DestRect;
  If time > 200 then Kill;
end;

//-------- END OF ITEN ------------

procedure TEnemyTank.Draw;
begin
  DestRect.x := x;
  DestRect.y := y;
  SDL_UpperBlit(Img_Tanks, @Rects_tanks[1,anim_pos,last_move], Surface, @DestRect);
  PrevRect := DestRect;
end;

procedure TEnemyTank.GetCollisionRect(Rect: PSDL_Rect);
begin
  Rect.x := x;
  Rect.y := y;
  Rect.w := 32;
  Rect.h := 32;
end;

procedure TEnemyTank.WasHit;
var
  i, i2, tipo : integer;
  tmp_enemytank : TEnemyTank;
begin
  dec(Shield);
  if Shield < 1 then
  begin
    snd_PlaySound(1,35);
    Kill;
    if Pl_nenemy_temp > 0 then dec(pl_nenemy_temp);
    SpriteEngine.AddSprite(TExplosion.Create(x, y, 0, 0));
    If random(10) = 0 then
    begin
      i  := x shl 3;   //random(600);
      i2 := y shl 3;   //random(400);
      tipo:=random(7);
      SpriteEngine.AddSprite(TIten.Create(i,i2,tipo));
    end;
    if pl_nenemy_temp > (pl_max_ntank-1) then
    begin

///habilidades espe3ciacis
  tmp_enemytank := TEnemyTank.create;
      if random(5-Pl_level) = 0 then
        with tmp_enemytank do
          Case random(3) of
            0 : inc(speed,(1+random(2)));
            1 : inc(Shield,(1+random(2)));
            2 : inc(Power,(1+random(2)));
          end;
//habilidades especiais

     SpriteEngine.AddSprite(tmp_enemytank);
    end;
    pl_score:=pl_score+5;
  end;
  inc(pl_score,5);
  Game_DrawMenu(drAll);
end;

//End of EnemyTank

procedure Game_MakeRects;
var i, i2 : integer;
begin
//  FIcon Rects
  Rects_FIcons[0].x := 0;
  Rects_Ficons[0].y := 0;
  Rects_Ficons[0].w := 11;
  Rects_Ficons[0].h := 14;

  Rects_Ficons[1].x := 11;
  Rects_Ficons[1].y := 0;
  Rects_Ficons[1].w := 11;
  Rects_Ficons[1].h := 14;

  Rects_Ficons[2].x := 22;
  Rects_Ficons[2].y := 0;
  Rects_Ficons[2].w := 11;
  Rects_Ficons[2].h := 14;

  Rects_Ficons[3].x := 33;
  Rects_Ficons[3].y := 0;
  Rects_Ficons[3].w := 6;
  Rects_Ficons[3].h := 14;

  Rects_Ficons[4].x := 39;
  Rects_Ficons[4].y := 0;
  Rects_Ficons[4].w := 17;
  Rects_Ficons[4].h := 14;
  //Makes the Rects of Arsenal
  //-down
  Rects_arsenal[0,0,0].x := 1;
  Rects_arsenal[0,0,0].y := 1;
  Rects_arsenal[0,0,0].w := 7;
  Rects_arsenal[0,0,0].h := 15;

  Rects_arsenal[0,1,0].x := 1;
  Rects_arsenal[0,1,0].y := 18;
  Rects_arsenal[0,1,0].w := 7;
  Rects_arsenal[0,1,0].h := 15;

  //-up
  Rects_arsenal[0,0,1].x := 1;
  Rects_arsenal[0,0,1].y := 35;
  Rects_arsenal[0,0,1].w := 7;
  Rects_arsenal[0,0,1].h := 15;

  Rects_arsenal[0,1,1].x := 1;
  Rects_arsenal[0,1,1].y := 52;
  Rects_arsenal[0,1,1].w := 7;
  Rects_arsenal[0,1,1].h := 15;

  //-right
  Rects_arsenal[0,0,2].x := 1;
  Rects_arsenal[0,0,2].y := 69;
  Rects_arsenal[0,0,2].w := 15;
  Rects_arsenal[0,0,2].h := 7;

  Rects_arsenal[0,1,2].x := 1;
  Rects_arsenal[0,1,2].y := 86;
  Rects_arsenal[0,1,2].w := 15;
  Rects_arsenal[0,1,2].h := 7;

  //-left
  Rects_arsenal[0,0,3].x := 1;
  Rects_arsenal[0,0,3].y := 103;
  Rects_arsenal[0,0,3].w := 15;
  Rects_arsenal[0,0,3].h := 7;

  Rects_arsenal[0,1,3].x := 1;
  Rects_arsenal[0,1,3].y := 120;
  Rects_arsenal[0,1,3].w := 15;
  Rects_arsenal[0,1,3].h := 7;
  //End Tipo=0

  //Makes the Rects of Arsenal
  //-down
  Rects_arsenal[1,0,0].x := 18;
  Rects_arsenal[1,0,0].y := 1;
  Rects_arsenal[1,0,0].w := 7;
  Rects_arsenal[1,0,0].h := 15;

  Rects_arsenal[1,1,0].x := 18;
  Rects_arsenal[1,1,0].y := 18;
  Rects_arsenal[1,1,0].w := 7;
  Rects_arsenal[1,1,0].h := 15;

  //-up
  Rects_arsenal[1,0,1].x := 18;
  Rects_arsenal[1,0,1].y := 35;
  Rects_arsenal[1,0,1].w := 7;
  Rects_arsenal[1,0,1].h := 15;

  Rects_arsenal[1,1,1].x := 18;
  Rects_arsenal[1,1,1].y := 52;
  Rects_arsenal[1,1,1].w := 7;
  Rects_arsenal[1,1,1].h := 15;

  //-right
  Rects_arsenal[1,0,2].x := 18;
  Rects_arsenal[1,0,2].y := 69;
  Rects_arsenal[1,0,2].w := 15;
  Rects_arsenal[1,0,2].h := 7;

  Rects_arsenal[1,1,2].x := 18;
  Rects_arsenal[1,1,2].y := 86;
  Rects_arsenal[1,1,2].w := 15;
  Rects_arsenal[1,1,2].h := 7;
  //-left
  Rects_arsenal[1,0,3].x := 18;
  Rects_arsenal[1,0,3].y := 103;
  Rects_arsenal[1,0,3].w := 15;
  Rects_arsenal[1,0,3].h := 7;

  Rects_arsenal[1,1,3].x := 18;
  Rects_arsenal[1,1,3].y := 120;
  Rects_arsenal[1,1,3].w := 15;
  Rects_arsenal[1,1,3].h := 7;
  //End Tipo=0

{ Baixo � 0 � Cima � 1 � Direita � 2 � Esquerda � 3 }

  for i := 0 to 1 do
    for i2 := 0 to 3 do
    begin
      Rects_tanks[0,i,i2].x := i2*32;
      Rects_tanks[0,i,i2].y := i*32;
      Rects_tanks[0,i,i2].w := 32;
      Rects_tanks[0,i,i2].h := 32;
    end;

  for i := 0 to 1 do
    for i2 := 0 to 3 do
    begin
      Rects_tanks[1,i,i2].x := i2*32;
      Rects_tanks[1,i,i2].y := 64+i*32;
      Rects_tanks[1,i,i2].w := 32;
      Rects_tanks[1,i,i2].h := 32;
    end;

{ Baixo � 0 � Cima � 1 � Direita � 2 � Esquerda � 3 }

  //~It helps to make rects_map!!!
  for i2 := 0 to 2 do
    for i := 0 to 3 do
    begin
      Rects_map[i2,i].x := 32 * i;
      Rects_map[i2,i].y := 32 * i2;
      Rects_map[i2,i].w := 32;
      Rects_map[i2,i].h := 32;
    end;

//Base
  Rects_Base[0].x := 0;
  Rects_Base[0].y := 0;
  Rects_Base[0].w := 32;
  Rects_Base[0].h := 32;

  Rects_Base[1].x := 32;
  Rects_Base[1].y := 0;
  Rects_Base[1].w := 32;
  Rects_Base[1].h := 32;

  //Itens rects
   For i2:= 0 to 1 do
   begin
   for i := 0 to 6 do
   begin
    Rects_itens[i,i2].x := i*16;
    Rects_itens[i,i2].y := i2*16;
    Rects_itens[i,i2].w := 16;
    Rects_itens[i,i2].h := 16;
   end;
  end;

  for i := 0 to 7 do
  begin
    Rects_Explosion[0,i].x := i * 32;
    Rects_Explosion[0,i].y := 0;
    Rects_Explosion[0,i].w := 32;
    Rects_Explosion[0,i].h := 32;
  end;
  
  Rects_Explosion[1,0].x := 256;
  Rects_Explosion[1,0].y := 0;
  Rects_Explosion[1,0].w := 5;
  Rects_Explosion[1,0].h := 5;

  Rects_Explosion[1,1].x := 262;
  Rects_Explosion[1,1].y := 0;
  Rects_Explosion[1,1].w := 14;
  Rects_Explosion[1,1].h := 14;

  Rects_Explosion[1,2].x := 301;
  Rects_Explosion[1,2].y := 0;
  Rects_Explosion[1,2].w := 30;
  Rects_Explosion[1,2].h := 30;

  Rects_Explosion[1,3].x := 332;
  Rects_Explosion[1,3].y := 0;
  Rects_Explosion[1,3].w := 33;
  Rects_Explosion[1,3].h := 32;

end; //End of Game_MakeRects



procedure Game_DrawMenu(DrawMenu : TDrawMenu);

procedure DigitalText19x24_Draw(Surface_: PSDL_Surface; x: Integer; y: Integer; text:
  pchar);
var
  i : integer;
  str_temp : string;
begin
//   img_LoadImage( Img_font_digital19x24, 'images/fonts/digital19x24.png', false, 0, 0, 0, true);
  for i := 0 to Length(text) do str_temp := copy(text,(i+1),1) + str_temp;
  for i := 0 to Length( str_temp ) - 1 do
  begin
    SDL_UpperBlit(Img_font_digital19x24, PSDLRect(strtoint(Copy(str_temp,(i+1),1))*19,0,19,24), Surface_, PSDLRect(x-(i*19),y,19,24));
  end;
 // SDL_UpperBlit(Img_font_digital19x24,nil,nil,nil);
end;

procedure DigitalText10x13_Draw(Surface_: PSDL_Surface; x: Integer; y: Integer; text: pchar);
var
  i : integer;
  str_temp : string;
begin
 // img_LoadImage( Img_font_digital10x13, 'images/fonts/digital10x13.png', false, 0, 0, 0, true);
  for i := 0 to Length(text) do str_temp := copy(text,(i+1),1) + str_temp;
  for i := 0 to Length(str_temp)-1 do
  begin
   SDL_UpperBlit(Img_font_digital10x13, PSDLRect(strtoint(Copy(str_temp,(i+1),1))*10,0,10,13), Surface_, PSDLRect(x-(i*10),y,10,13));
  end;
 // SDL_FreeSurface(Img_font_digital10x13);
end;

procedure DrawIcons(Surface_: PSDL_Surface; X, Y, Icon, Qtd_Icon, Space : Integer);
var
  i : integer;
begin
  for i := 0 to Qtd_Icon-1 do
  begin
    SDL_UpperBlit(Img_FIcons, PSDLRect(Rects_FIcons[Icon].x, Rects_FIcons[Icon].y,
                  Rects_FIcons[Icon].w, Rects_FIcons[Icon].h), Surface_,
                  PSDLRect(x-((i*Rects_FIcons[Icon].w) + i*Space), y, Rects_FIcons[Icon].w, Rects_FIcons[Icon].h));
  end;  //End of For
end;  //End of DrawIcons

begin //DrawMenu procedure

// I should change this procedure, to make the game faster, only should be redraw what is need.

  Case DrawMenu of
    drLives  :
    Begin
      SDL_UpperBlit(Img_Painel, PSDLRect(80,9,36,24), Img_Menu, PSDLRect(80,9,36,24));
      DigitalText19x24_Draw(Img_menu, 99, 9, pchar(inttostr(pl_lives)));
      SDL_UpperBlit(Img_menu, PSDLRect(80,9,36,24), Screen, PSDLRect(80,BG_HEIGHT+9,36, 24));
      SDL_UpdateRect(Screen, 80,BG_HEIGHT+9,36, 24);
    End;
    drItens  :
    Begin
      SDL_UpperBlit(Img_Painel, PSDLRect(16,34,180,14), Img_Menu, PSDLRect(16,34,180,14));
      DrawIcons(Img_menu, 106, 35, 0, MyTank_Shield, 3);  //0
      DrawIcons(Img_menu, 150, 34, 1, (MyTank_Speed-1),  2);  //1
      DrawIcons(Img_menu, 188, 35, 2, (MyTank_Shot-1),   1);  //2
      DrawIcons(Img_menu,  54, 34, 4, MyTank_Power,  2);  //4
      SDL_UpperBlit(Img_menu, PSDLRect(16,34,180,14), Screen, PSDLRect(16,BG_HEIGHT+34,180, 14));
      SDL_UpdateRect(Screen, 16,BG_HEIGHT+34,180, 14);
    End;
    drLevel  :
    Begin
      SDL_UpperBlit(Img_Painel, PSDLRect(193,10,36,24), Img_Menu, PSDLRect(193,10,36,24));
      DigitalText19x24_Draw(Img_menu, 212, 9, pchar(inttostr(Pl_level)));
      SDL_UpperBlit(Img_menu, PSDLRect(193,10,36,24), Screen, PSDLRect(193,BG_HEIGHT+10,36, 24));
      SDL_UpdateRect(Screen, 193,BG_HEIGHT+10,36, 24);
    End;
    drScore  :
    begin
      SDL_UpperBlit(Img_Painel, PSDLRect(421,13,140,13), Img_Menu, PSDLRect(421,13,140,13));
      DigitalText10x13_Draw(Img_menu, 552, 13, pchar(inttostr(Pl_score)));
      SDL_UpperBlit(Img_menu, PSDLRect(421,13,140,13), Screen, PSDLRect(421,BG_HEIGHT+13,140, 13));
      SDL_UpdateRect(Screen, 421,BG_HEIGHT+13,140, 13);
    end;
    drAmmo   :
    begin
      SDL_UpperBlit(Img_Painel, PSDLRect(282,31,64,13), Img_Menu, PSDLRect(282,31,64,13));
      DigitalText10x13_Draw(Img_menu, 335, 31, pchar(inttostr(MyTank_Ammo)));
      SDL_UpperBlit(Img_menu, PSDLRect(282,31,64,13), Screen, PSDLRect(282,BG_HEIGHT+31,64, 13));
      SDL_UpdateRect(Screen, 282,BG_HEIGHT+31,64,13);
    end;
    drAll:
    begin
      SDL_FillRect( Img_Menu, nil, 0 );
      SDL_UpperBlit(Img_Painel, nil, Img_Menu, nil);

      DrawIcons(Img_menu, 106, 35, 0, MyTank_Shield, 3);  //0
      DrawIcons(Img_menu, 150, 34, 1, MyTank_Speed-1,  2);  //1
      DrawIcons(Img_menu, 188, 35, 2, MyTank_Shot-1,   1);  //2
      DrawIcons(Img_menu,  54, 34, 4, MyTank_Power,  2);  //4

      DigitalText19x24_Draw(Img_menu,  99, 9, pchar(inttostr(pl_lives)));        //163-182
      DigitalText19x24_Draw(Img_menu, 212, 9, pchar(inttostr(Pl_level)));        //284-303
      DigitalText10x13_Draw(Img_menu, 308, 12, pchar(inttostr(Pl_nenemy_temp ))); //401-412
      DigitalText10x13_Draw(Img_menu, 336, 12, pchar(inttostr(Pl_nenemy)));      //429-440
      DigitalText10x13_Draw(Img_menu, 552, 13, pchar(inttostr(Pl_score)));       //537-548
      DigitalText10x13_Draw(Img_menu, 335, 31, pchar(inttostr(MyTank_Ammo)));       //-31

      SDL_UpperBlit(Img_menu, nil, Screen, PSDLRect(0,BG_HEIGHT,BG_WIDTH, 64));
      SDL_UpdateRect(Screen, 0, BG_HEIGHT, BG_WIDTH, 64);
    end;
  end;

end;

function img_DrawShieldProtect(image_: PSDL_SURFACE): PSDL_SURFACE;
var
  image: PSDL_SURFACE;
  i, x, y: integer;
begin
  image := image_;
  if image <> nil then Result := image else Result := nil;

  SDL_SetColorKey(image, SDL_SRCCOLORKEY or SDL_RLEACCEL, SDL_MapRGB(image.format, 0, 0, 0));
  for i := 0 to 800 do
  begin
   x:= random(image.w); y:= random(image.h);
   SDL_LockSurface(Image);
   If SDL_GetPixel(image_,x,y) <> 0 then SDL_PutPixel(image,x,y,1212-i);
   SDL_UnLockSurface(Image);
  end;

end;

procedure TPlayer.Draw;
var
  temp_img : PSDL_Surface;
begin
 { Baixo � 0 � Cima � 1 � Direita � 2 � Esquerda � 3 }

//  if flip = true then
//  begin
  //  if (direction_last = 0) then y := y + 10;  {and (direction = 1) }
  //  if (direction_last = 1) then y := y - 10;  {and (direction = 0) }
//  End;
If FX <> 0 Then
Begin
  temp_img := SDL_CreateRGBSurface( SDL_SWSURFACE or SDL_HWPALETTE, w, h, bpp, 0, 0, 0, 0 );

  SDL_UpperBlit(Img_Tanks, @Rects_tanks[0,anim_pos,direction], temp_img, nil);
  If FX = 1 then img_DrawShieldProtect(temp_img);

  DestRect.x := x; //maybe, the solution for the tank movement is here
  DestRect.y := y;

  SDL_UpperBlit(temp_img, nil, Surface, @DestRect);
  dec(fx_time); If fx_time < 0 then FX:=0;
end
else
begin
  DestRect.x := x; //maybe, the solution for the tank movement is here
  DestRect.y := y;
  SDL_UpperBlit(Img_Tanks, @Rects_tanks[0,anim_pos,direction], Surface, @DestRect);
end;

  PrevRect := DestRect;
end;

//--------- TBase -----------------------
procedure TBase.Draw;
begin
  DetectCollision;
  DestRect.x := x;
  DestRect.y := y;
  SDL_UpperBlit(Img_Base, @Rects_Base[tipo], Surface, @DestRect);
//  PrevRect := DestRect;
end;

constructor TBase.Create;  //(var x_,y_, tipo_ : integer)
begin
  inherited Create('',32,32);
  id := idBase;
  Z := zBase;
  SDL_UpperBlit(Img_Base, @Rects_Base[tipo], Surface, @DestRect);
end;

procedure TBase.GetCollisionRect(Rect: PSDL_Rect);
begin
  Rect.x := x;
  Rect.y := y;
  Rect.w := 32;
  Rect.h := 32;
end;

procedure TBase.DetectCollision;
var
  i : integer;
  MyRect, NMERect: TSDL_Rect;
begin
  GetCollisionRect(@MyRect);
  //i get a error here, when i kill the last enemytank
  for i := 0 to ParentList.Count -1 do
    if ParentList[i] <> Self then
    begin
      ParentList[i].GetCollisionRect(@NMERect);
      if isCollideRects(@MyRect, @NMERect) then
      begin
        if (ParentList[i].ID = idTiro) or (ParentList[i].ID = idEnemyTank) then
        begin
          ParentList[i].Kill; //It kills the TTiro
          WasHit;             //It kills the TBase
          Game_DrawMenu(drAll);
        end;
      end;
    end;
end;

procedure TBase.WasHit;
begin
    Kill;
    snd_PlaySound(1,100);
    SpriteEngine.AddSprite(TExplosion.Create(x, y, 0, 0));
    pl_Lives := 0;
    PlayerAlive := False;
end;

//--------- TMapa -----------------------
procedure TMapa.Draw;
begin
{  If anim_pos_temp > 5 then
  begin
    If random(10) = 0 then
    Begin
     Anim_pos := 1 - Anim_pos;
     anim_pos_temp := 0;
    end;
    anim_pos_temp := 0;
  end;
  inc(anim_pos_temp);
}
  DestRect.x := x;
  DestRect.y := y;
  SDL_UpperBlit(Img_Terrenos, @Rects_map[tipo1,(tipo2)], Surface, @DestRect);//+Anim_pos
//  PrevRect := DestRect;
end;


constructor TMapa.Create(var x_,y_,z_, tipo1_, tipo2_ : integer);
begin
  inherited Create('',32,32);
  id := idTileMap;
  X := x_;
  Y := y_;
  Z := z_;
  tipo1:=tipo1_;
  tipo2:=tipo2_;
//  anim_pos := 0;
//  anim_pos_temp := 0;
  SDL_UpperBlit(Img_Terrenos, @Rects_map[tipo1,tipo2], Surface, @DestRect);
end;

// TTiro ------------------------------------------------
constructor TTiro.Create(var x_,y_,direction_,tipo_, speed_, power_ : integer); //falta a dire��o
var h_, w_ : integer;
begin

  If (direction_ = 0) or (direction_ = 1) then
  begin
   w_ := 8; h_ := 16;
  end
  else
  begin
   w_ := 16; h_ := 8;
  end;

  Inherited Create('',w_,h_);
  id := idTiro;
  Z := zTiro;
  power := power_;

  direction := direction_;
  speed := speed_;

  { direction � Down � 0 � Up � 1 � Right � 2 � left � 3 }
  case direction of
    0 :
    begin
      X := x_ + 12; //12 = 32/2 - 4
      Y := y_ + 32;
    end;
    1 :
    begin
      X := x_ + 12;
      Y := y_ - 10;
    end;
    2 :
    begin
      X := x_ + 30; //+20
      Y := y_ + 12 ;
    end;
    3 :
    begin
      X := x_ - 4;
      Y := y_ + 12;
    end;
  end; //End of case Event

  anim_pos  := 0;
  Temp_Anim_pos := 0;
  tipo := tipo_;
end;

procedure TTiro.Draw;
begin
  DestRect.x := x;
  DestRect.y := y;
  SDL_UpperBlit(Img_Arsenal, @Rects_arsenal[tipo,anim_pos,direction], Surface, @DestRect);
  PrevRect := DestRect;
end;

//- MY TANK ----------------
constructor TPlayer.Create;
var i  : integer;
begin

  If PlayerAlive = False then
  Begin

with SpriteEngine.Sprites do
 for i := 0 to Count -1 do
    if Items[i] <> Self Then
      if Items[i].ID = idMyTank then
        Items[i].Kill;

    PlayerAlive := true;
    inherited Create('', 32, 32);
    id       := idMyTank;
    anim_pos := 0;   Fx := 1; fx_time :=40;
    Pl_tiros := 0;
    X := Pl_StartPosX shl 3;
    Y := Pl_StartPosY shl 3;
    Z := zMyTank;
    ttime:=MyTank_shotdelay[MyTank_Shot];
    { Valores para: direction � Baixo � 0 � Cima � 1 � Direita � 2 � Esquerda � 3 }
    direction:=1;
    direction_last:=1;


  End; //Should I use this?!
end;


procedure TPlayer.Free;
begin
  PlayerAlive := false;
  inherited Free;
end;

procedure TPlayer.DetectCollision;
var
  i, i2: integer;
  MyRect, NMERect: TSDL_Rect;
begin
  GetCollisionRect(@MyRect);

  for i := 0 to ParentList.Count -1 do
    if ParentList[i] <> Self then
    begin
      ParentList[i].GetCollisionRect(@NMERect);
      if isCollideRects(@MyRect, @NMERect) then
      begin
        if ParentList[i].ID = idTiro then
        begin
          if (TTiro(ParentList[i]).Tipo) = 1 then
          begin
            ParentList[i].Kill; //It kills the TTiro
            WasHit;
            //PlayerAlive := False;             //It kills the TMyTank
          end;
        end
       else
        if ParentList[i].ID = idIten then
        begin
        snd_PlaySound(2,100);
        Case TIten(ParentList[i]).Tipo of
          0 : if MyTank_speed < 4 then inc(MyTank_speed) else inc(Pl_score,10);
          1 : if Pl_lives < 98 then inc(Pl_lives) else inc(Pl_score,10);
          2 : if MyTank_shield < 3 then inc(MyTank_Shield) else inc(Pl_score,10);
          3 : if MyTank_Shot < 3 then inc(MyTank_Shot) else inc(Pl_score,10);
          4 : If MyTank_Power < 3 then inc(MyTank_Power) else inc(Pl_score,10);
          5 : with SpriteEngine do
                for i2 := 0 to Sprites.Count-1 do
                  if Sprites.Items[i2].ID = idEnemyTank then
                    with TEnemyTank(ParentList[i2]) do WasHit;
          6 : MyTank_Ammo := MyTank_Ammo + 64;
        end;
          Game_DrawMenu(drAll);
          ParentList[i].Kill;
        end;
      end;
    end;
end;

procedure TPlayer.GetCollisionRect(Rect: PSDL_Rect);
begin
  Rect.x := x;
  Rect.y := y;
  Rect.w := 32;
  Rect.h := 32;
end;

procedure TPlayer.WasHit;
begin
  dec(MyTank_Shield);
  if (Mytank_Shield < 1) and (FX <> 1) then
  begin
    PlayerAlive := False;
    Kill;
    SpriteEngine.AddSprite(TExplosion.Create(x, y, 0, 0));
    MyTank_Shield := 1;
  end
  else
  Begin
    Fx := 1;
    fx_time :=10;
  End;
  Game_DrawMenu(drItens);
end;

//END OF MYTANK

procedure TEnemyTank.DetectCollision;
var
  i: integer;
  MyRect, NMERect: TSDL_Rect;
begin
  GetCollisionRect(@MyRect);

  for i := 0 to ParentList.Count -1 do
    if ParentList[i] <> Self then
    begin
      ParentList[i].GetCollisionRect(@NMERect);
      if isCollideRects(@MyRect, @NMERect) then
      begin
        if ParentList[i].ID = idTiro then
        begin
          if (TTiro(ParentList[i]).Tipo) = 0 then
          begin
            ParentList[i].Kill; //It kills the TTiro
            WasHit; {Try to Kill EnemyTank}
          end;
        end
       else
        if ParentList[i].ID = idMyTank then
        begin
          WasHit;
          //PlayerAlive := False;

          with TPlayer(ParentList[i]) do WasHit; {Try to Kill MyTank}
        end;
      end;
    end;
end;

constructor TEnemyTank.Create;
begin
  inherited Create('', 32, 32);  //This must be before the ID
  id := idEnemyTank;
  anim_pos :=0;
  tiros := 0;
  speed := 2; 
  shotdelay := 20;
  ttime := shotdelay;
  X := random(max_X)*33;
  Y := -h; //BG_HEIGHT-33;
  Z := zEnemyTank;
  temp_anim_pos := 0;
  count_move  := 0;
  count_move2 := 5;
  shotspeed := 5;
  Power := 1;
  last_move := random(4);
  if (last_move = 0) or (last_move = 1) then count_move2 := max_X;
  if (last_move = 2) or (last_move = 3) then count_move2 := max_Y;
end;


//Start of DetectCollision
procedure TPlayer.Left;
var
  mx, my1, my2, my3, my4, my5 : integer;
  Coll : byte;
  NewX : integer;
begin
  NewX := x + xi;
  if NewX < 0 then
  begin
    // Player reaches the left side of screen (Just for SAFETY). It never
    // could be happen!
    x := 0;
    xi := 0;
    exit;
  end;
//  if ( ( NewX + 4 ) and 15 ) >= 12 then
//  begin
    mx := ( NewX + 0 ) div 8;
    my1 := ( y  ) div 8;
    Coll := MapID[GameMap[mx, my1]];
    my2 := ( y + 7 ) div 8;
    Coll := Coll or MapID[GameMap[mx, my2]];
    my3 := ( y + 15 ) div 8;
    Coll := Coll or MapID[GameMap[mx, my3]];
    my4 := ( y + 23 ) div 8;
    Coll := Coll or MapID[GameMap[mx, my4]];
    my5 := ( y + 31 ) div 8;
    Coll := Coll or MapID[GameMap[mx, my5]];

    if ( Coll  ) > 0 then
    begin
      // The player swims into grassy area, clear his way
    //  if MapID[GameMap[mx, my1]] > 0 then
    //  Begin
        xi:=0;
        NewX := x;
    //  End;
   {   if MapID[GameMap[mx, my2]] > 0 then
      Begin
        xi:=0;
        NewX := x;
      End;
      if MapID[GameMap[mx, my3]] > 0 then
      Begin
        xi:=0;
        NewX := x;
      End;
      if MapID[GameMap[mx, my4]] > 0 then
      Begin
        xi:=0;
        NewX := x;
      End;
      if MapID[GameMap[mx, my5]] > 0 then
      Begin
        xi:=0;
        NewX := x;
      End;  }

    end;
//  end;
  x := NewX;
end;

procedure TPlayer.Right;
var
  mx, my1, my2, my3, my4, my5 : integer;
  Coll : byte;
  NewX : integer;
begin
  NewX := x + xi;
  if NewX > Screen_Width - w then
  begin
    // Player reaches the right side of screen (Just for SAFETY). It never
    // could be happen!
    x := Screen_Width - w;
    xi := 0;
    exit;
  end;
//  if ( ( NewX + 27 ) and 15 ) > 4 then
//  begin
    mx := ( NewX + w ) div 8;
    my1 := ( y ) div 8;
    Coll := MapID[GameMap[mx, my1]];
    my2 := ( y + 7 ) div 8;
    Coll := Coll or MapID[GameMap[mx, my2]];
    my3 := ( y + 15 ) div 8;
    Coll := Coll or MapID[GameMap[mx, my3]];
    my4 := ( y + 23 ) div 8;
    Coll := Coll or MapID[GameMap[mx, my4]];
    my5 := ( y + 31 ) div 8;
    Coll := Coll or MapID[GameMap[mx, my5]];

    if ( Coll ) > 0 then
    begin
{      if MapID[GameMap[mx, my1]] = 1 then
      Begin}
        xi:=0;
        NewX := x;
     // End;
    end;
//  end;
  x := NewX;
end;


procedure TPlayer.Up;
var
  mx1, mx2, mx3, mx4, mx5, my : integer;
  Coll : byte;
  NewY : integer;
begin        //_w=29
  NewY := y + yi;
  if NewY < 0 then
  begin
    // Player reaches the top of screen (Just for SAFETY). It never
    // could be happen!
    y := 0;
    yi := 0;
    exit;
  end;
//  if (  NewY + 6 ) and 15 ) > 11 then
//  begin
    my := ( NewY + 0 ) div 8;
    mx1 := ( x   ) div 8;
    Coll := MapID[GameMap[mx1, my]];
    mx2 := ( x + 7 ) div 8;
    Coll := Coll or MapID[GameMap[mx2, my]];
    mx3 := ( x + 15 ) div 8;
    Coll := Coll or MapID[GameMap[mx3, my]];
    mx4 := ( x + 23 ) div 8;
    Coll := Coll or MapID[GameMap[mx4, my]];
    mx5 := ( x + 31 ) div 8;
    Coll := Coll or MapID[GameMap[mx5, my]];

    if ( Coll  ) > 0 then
    begin
     // if MapID[GameMap[mx1, my]] > 0 then
     // Begin
        yi:=0;
        NewY := y;
     // End;
     { if MapID[GameMap[mx2, my]] > 0 then
      Begin
        yi:=0;
        NewY := y;
      End;
      if MapID[GameMap[mx3, my]] > 0 then
      Begin
        yi:=0;
        NewY := y;
      End;
      if MapID[GameMap[mx4, my]] > 0 then
      Begin
        yi:=0;
        NewY := y;
      End;
      if MapID[GameMap[mx5, my]] > 0 then
      Begin
        yi:=0;
        NewY := y;
      End; }
    end;
//  end;
  y := NewY;
end;


procedure TPlayer.Down;
var
  mx1, mx2, mx3, mx4, mx5, my : integer;
  Coll : byte;
  NewY : integer;
begin      //28x, 25h;

  NewY := y + yi;
  if NewY > BG_HEIGHT - h then
  begin
// Player reaches the bottom of screen (Just for SAFETY). It never could be happen!
    y := BG_HEIGHT - h;
    yi := 0;
    exit;
  end;

 // if ( ( NewY + h )  ) < 3 then
  //begin
    my  := ( NewY + h) div 8;
    mx1 := ( x ) div 8;
    Coll := MapID[GameMap[mx1, my]];
    mx2 := ( x + 7 ) div 8;
    Coll := Coll or MapID[GameMap[mx2, my]];
    mx3 := ( x + 15 ) div 8;
    Coll := Coll or MapID[GameMap[mx3, my]];
    mx4 := ( x + 23 ) div 8;
    Coll := Coll or MapID[GameMap[mx4, my]];
    mx5 := ( x + 31 ) div 8;
    Coll := Coll or MapID[GameMap[mx5, my]];
    if ( Coll ) > 0 then
    begin
      //if MapID[GameMap[mx1, my]] > 0 then
      //Begin
        yi:=0;
        NewY := y;
      //End;
      {if MapID[GameMap[mx2, my]] > 0 then
      Begin
        yi:=0;
        NewY := y;
      End;
      if MapID[GameMap[mx3, my]] > 0 then
      Begin
        yi:=0;
        NewY := y;
      End;
      if MapID[GameMap[mx4, my]] > 0 then
      Begin
        yi:=0;
        NewY := y;
      End;
      if MapID[GameMap[mx5, my]] > 0 then
      Begin
        yi:=0;
        NewY := y;
      End; }
    end;
  //end;

  y := NewY;
end;


procedure TPlayer.Move;
var moveu : boolean;
    tipo : integer;
begin
 moveu:=false; flip :=false;

//The error in the mov out of the screen is about diferent H and W
        //Setas Baixo-Cima

        If (Keys[SDLK_DOWN] = SDL_PRESSED) and (Y < BG_HEIGHT-H) then
        Begin
            yi := MyTank_speed;
            direction := 0;
            if direction_last <> direction then flip:=true;
            direction_last:=0;
            moveu:=true;
        end
        else
        if yi > -1 then yi := 0;

        If (Keys[SDLK_UP] = SDL_PRESSED) and (Y > 0)  then
        Begin
          yi := -MyTank_speed;
          direction := 1;
          if direction_last <> direction then flip:=true;
          direction_last:=1;
          moveu:=true;
        end
        else
        if yi < 1 then yi := 0;

        //Setas Direita-Esquerda
        If (Keys[SDLK_RIGHT] = SDL_PRESSED) and (X < BG_WIDTH-W) then
        Begin
          xi := MyTank_speed;
          direction := 2;
          if direction_last <> direction then flip:=true;
          direction_last:=2;
          moveu:=true;
        end
        else
        if xi > -1 then xi := 0;

        If (Keys[SDLK_LEFT] = SDL_PRESSED) and (X > 0) then
        Begin
          xi := -MyTank_speed;
          direction := 3;
          if direction_last <> direction then flip:=true;
          direction_last:=3;
          moveu:=true;
        end
        else
        if xi < 1 then xi := 0;

        //Shot - Canon
        If (Keys[SDLK_LCTRL] = SDL_PRESSED) and (ttime>MyTank_shotdelay[MyTank_Shot]) then
        Begin
          If MyTank_Ammo > 0 then
          Begin
            tipo:=0;
            ttime:=0;
            inc(Pl_tiros);
            dec(MyTank_Ammo);
            SpriteEngine.AddSprite(TTiro.Create(x, y, direction, tipo, MyTank_shotspeed[MyTank_Shot], MyTank_power));
            snd_PlaySound(3,50);
            Game_DrawMenu(drAmmo);
          end;
        end;
        inc(ttime);

  if moveu=true then inc(temp_anim_pos);

  if temp_anim_pos > 5 then //it should change according to the speed
  begin
    Anim_pos := 1 - Anim_pos;
    Temp_Anim_pos := 0;
  end;

{  If (direction = 0) or (direction = 1) then
  begin
   w := 32; h := 45;
  end
  else
  begin
   w := 45; h := 32;
  end;
}
  if yi < 0 then Up
   else if yi > 0 then Down;
  if xi < 0 then Left
   else if xi > 0 then Right;

  //x := x + xi;
  //y := y + yi;

  DetectCollision;
end;
//Start of DetectCollision
procedure TEnemyTank.Left;
var
  mx, my1, my2, my3, my4, my5 : integer;
  Coll : byte;
  NewX : integer;
begin
  NewX := x + xi;
  if NewX < 0 then
  begin
    // Player reaches the left side of screen (Just for SAFETY). It never
    // could be happen!
    x := 0;
    xi := 0;
    count_move := 0;
    last_move := 2;
    exit;
  end;
//  if ( ( NewX + 4 ) and 15 ) >= 12 then
//  begin
    mx := ( NewX + 0 ) div 8;
    my1 := ( y  ) div 8;
    Coll := MapID[GameMap[mx, my1]];
    my2 := ( y + 7 ) div 8;
    Coll := Coll or MapID[GameMap[mx, my2]];
    my3 := ( y + 15 ) div 8;
    Coll := Coll or MapID[GameMap[mx, my3]];
    my4 := ( y + 23 ) div 8;
    Coll := Coll or MapID[GameMap[mx, my4]];
    my5 := ( y + 31 ) div 8;
    Coll := Coll or MapID[GameMap[mx, my5]];

    if ( Coll  ) > 0 then
    begin
      xi:=0;
      NewX := x;
        count_move := 0;
        last_move := random(4);
    end;
//  end;
  x := NewX;
end;

procedure TEnemyTank.Right;
var
  mx, my1, my2, my3, my4, my5 : integer;
  Coll : byte;
  NewX : integer;
begin
  NewX := x + xi;
  if NewX > Screen_Width - w then
  begin
    // Player reaches the right side of screen (Just for SAFETY). It never
    // could be happen!
    x := Screen_Width - w;
    xi := 0;
    count_move := 0;
    last_move := 3;
    exit;
  end;
    mx := ( NewX + w ) div 8;
    my1 := ( y ) div 8;
    Coll := MapID[GameMap[mx, my1]];
    my2 := ( y + 7 ) div 8;
    Coll := Coll or MapID[GameMap[mx, my2]];
    my3 := ( y + 15 ) div 8;
    Coll := Coll or MapID[GameMap[mx, my3]];
    my4 := ( y + 23 ) div 8;
    Coll := Coll or MapID[GameMap[mx, my4]];
    my5 := ( y + 31 ) div 8;
    Coll := Coll or MapID[GameMap[mx, my5]];

    if ( Coll ) > 0 then
    begin
        xi:=0;
        NewX := x;
        count_move := 0;
        last_move := random(4);
    end;
  x := NewX;
end;


procedure TEnemyTank.Up;
var
  mx1, mx2, mx3, mx4, mx5, my : integer;
  Coll : byte;
  NewY : integer;
begin        //_w=29
  NewY := y + yi;
  if NewY < 0 then
  begin
    // Player reaches the top of screen (Just for SAFETY). It never
    // could be happen!
    y := 0;
    yi := 0;
    count_move := 0;
    last_move := 0;
    exit;
  end;
//  if (  NewY + 6 ) and 15 ) > 11 then
//  begin
    my := ( NewY + 0 ) div 8;
    mx1 := ( x   ) div 8;
    Coll := MapID[GameMap[mx1, my]];
    mx2 := ( x + 7 ) div 8;
    Coll := Coll or MapID[GameMap[mx2, my]];
    mx3 := ( x + 15 ) div 8;
    Coll := Coll or MapID[GameMap[mx3, my]];
    mx4 := ( x + 23 ) div 8;
    Coll := Coll or MapID[GameMap[mx4, my]];
    mx5 := ( x + 31 ) div 8;
    Coll := Coll or MapID[GameMap[mx5, my]];

    if ( Coll  ) > 0 then
    begin
        yi:=0;
        NewY := y;
        count_move := 0;
        last_move := random(4);
    end;
//  end;
  y := NewY;
end;


procedure TEnemyTank.Down;
var
  mx1, mx2, mx3, mx4, mx5, my : integer;
  Coll : byte;
  NewY : integer;
begin      //28x, 25h;
  NewY := y + yi;
  if NewY > BG_HEIGHT - h then
  begin
// Player reaches the bottom of screen (Just for SAFETY). It never could be happen!
    y := BG_HEIGHT - h;
    yi := 0;
    count_move := 0;
    last_move := 1;
    exit;
  end;

 // if ( ( NewY + h )  ) < 3 then
  //begin
    my  := ( NewY + h) div 8;
    mx1 := ( x ) div 8;
    Coll := MapID[GameMap[mx1, my]];
    mx2 := ( x + 7 ) div 8;
    Coll := Coll or MapID[GameMap[mx2, my]];
    mx3 := ( x + 15 ) div 8;
    Coll := Coll or MapID[GameMap[mx3, my]];
    mx4 := ( x + 23 ) div 8;
    Coll := Coll or MapID[GameMap[mx4, my]];
    mx5 := ( x + 31 ) div 8;
    Coll := Coll or MapID[GameMap[mx5, my]];
    if ( Coll ) > 0 then
    begin
      //if MapID[GameMap[mx1, my]] > 0 then
      //Begin
        yi:=0;
        NewY := y;
        count_move := 0;
        last_move := random(4);
      //End;
      {if MapID[GameMap[mx2, my]] > 0 then
      Begin
        yi:=0;
        NewY := y;
      End;
      if MapID[GameMap[mx3, my]] > 0 then
      Begin
        yi:=0;
        NewY := y;
      End;
      if MapID[GameMap[mx4, my]] > 0 then
      Begin
        yi:=0;
        NewY := y;
      End;
      if MapID[GameMap[mx5, my]] > 0 then
      Begin
        yi:=0;
        NewY := y;
      End; }
    end;
  //end;

  y := NewY;
end;


procedure TEnemyTank.Move;
var i : integer;
  tipo: integer;
begin
 xi:=0; yi:=0;
  { Value for direction � down � 0 � up � 1 � right � 2 � left � 3 }

  If (count_move2 > 0) then
  Begin
    if count_move < 100 then
    begin
      if (Y < 0) then last_move  := 0; if (Y > BG_HEIGHT-47) then last_move  := 1;
      if (X < 0) then last_move  := 2; if (X > BG_WIDTH-34)  then last_move  := 3;
      if (last_move = 0) then inc(yi,speed); if (last_move = 1) then dec(yi,speed); //down-up
      if (last_move = 2) then inc(xi,speed); if (last_move = 3) then dec(xi,speed); //right-left
      inc(temp_anim_pos);
      inc(count_move);
      dec(count_move2);
    end
  else
    begin
{ Baixo � 0 � Cima � 1 � Direita � 2 � Esquerda � 3 }
    //AI comes here...... :)

//  Pl_BasePosX : integer = 0;
//  Pl_BasetPosY : integer = 0;

      count_move := 0;
      if random(2) = 0 then
      begin
        if random(2) = 0 then
          if y > Pl_BasePosY then last_move := 1 else last_move := 0
        else
          if x > Pl_BasePosX then last_move := 3 else last_move := 2;
      end
      else
      begin
        i := last_move;
        repeat
          last_move := random(4);
        until i <> last_move;
      end;
  end;

//  w := 32;
//  h := 32;

  if yi < 0 then Up   else if yi > 0 then Down;
  if xi < 0 then Left else if xi > 0 then Right;
    count_move2 := 10;
  end;
  //Shot!
  If (random(20) = 0) and (ttime > shotdelay) then
  Begin
    tipo:=1; ttime:=0;    //Tipo 1 is Enemy Shot
    SpriteEngine.AddSprite(TTiro.Create(x,y,last_move,tipo,shotspeed,Power));
  end;

  //Is time to animate?
  if temp_anim_pos > 5 then
  begin
    Anim_pos := 1 - Anim_pos;
    Temp_Anim_pos := 0;
  end;

  inc(ttime);

  DetectCollision;
end;

//Start of TShot

//Start of DetectCollision

procedure TTiro.GetCollisionRect(Rect: PSDL_Rect);
begin
  Rect.x := x;
  Rect.y := y;
  Rect.w := 8;
  Rect.h := 8;
end;

procedure TTiro.DetectCollision;
var
  i : integer;
  MyRect, NMERect: TSDL_Rect;
begin
  GetCollisionRect(@MyRect);
  //i get a error here, when i kill the last enemytank
  for i := 0 to ParentList.Count -1 do
    if ParentList[i] <> Self then
    begin
      ParentList[i].GetCollisionRect(@NMERect);
      if isCollideRects(@MyRect, @NMERect) then
      begin
        if (ParentList[i].ID = idTiro) then
        begin
          ParentList[i].Kill; //It kills the TTiro
          Kill;
//          SpriteEngine.AddSprite(TExplosion.Create(x, y, 0, 0));
            SpriteEngine.AddSprite(TExplosion.Create(x, y, 1, 0));
          snd_PlaySound(1,20);
          inc(Pl_score,2);
            Game_DrawMenu(drScore);
        end;
      end;
    end;
end;

procedure TTiro.DestroyBrick( mx, my : integer );
var
   SrcRect, DestRect : TSDL_Rect;
   i, i2, i3 : integer;
begin

  SrcRect.w := 8;
  SrcRect.h := 8;

  If random(20) = 0 then
  Begin
   i := random(7);
   i2 := (mx shl 3);
   i3 := (my shl 3);
   SpriteEngine.AddSprite(TIten.Create(i2,i3,i));
  End;


Case Power of
  1 :
  begin
    If (GameMap[mx, my]-1 = 1) or (GameMap[mx, my]-1 = 2) or (GameMap[mx, my]-1 = 3) then
    begin
    GameMap[mx, my] := 0;
    SrcRect.x := mx shl 3 - 8;
    SrcRect.y := my shl 3;
    DestRect.x := mx shl 3;
    DestRect.y := my shl 3;
    SDL_UpperBlit( Img_background_, @SrcRect, Background, @DestRect );
    SDL_UpperBlit( Img_background_, @SrcRect, Screen, @DestRect );
    SpriteEngine.AddSprite(TExplosion.Create((mx shl 3), (my shl 3), 1, power));
   end;
  end;
  2 :
  begin
    Case Direction of
      0,1 :
      begin

      If Power = 2 then If (GameMap[mx, my]-1 = 1) or (GameMap[mx, my]-1 = 2) or (GameMap[mx, my]-1 = 3) then
      begin
        GameMap[mx, my] := 0;
        SrcRect.x := mx shl 3 - 8;
        SrcRect.y := my shl 3;
        DestRect.x := mx shl 3;
        DestRect.y := my shl 3;
        SDL_UpperBlit( Img_background_, @SrcRect, Background, @DestRect );
        SDL_UpperBlit( Img_background_, @SrcRect, Screen, @DestRect );
        If Direction = 0 then
        SpriteEngine.AddSprite(TExplosion.Create((mx shl 3), (((my+1) shl 3)-2), 1, power))
        else SpriteEngine.AddSprite(TExplosion.Create((mx shl 3), (((my-1) shl 3)+2), 1, power));
      end;

      If Power = 2 then If (GameMap[mx-1, my]-1 = 1) or (GameMap[mx-1, my]-1 = 2) or (GameMap[mx-1, my]-1 = 3) then
      begin
        GameMap[mx-1, my] := 0;
        SrcRect.x := (mx-1) shl 3 - 8;
        SrcRect.y := my shl 3;
        DestRect.x := (mx-1) shl 3;
        DestRect.y := my shl 3;
        SDL_UpperBlit( Img_background_, @SrcRect, Background, @DestRect );
        SDL_UpperBlit( Img_background_, @SrcRect, Screen, @DestRect );
        SpriteEngine.AddSprite(TExplosion.Create(((mx-1) shl 3), (my shl 3), 1, power));
      end;

      If Power = 2 then If (GameMap[mx+1, my]-1 = 1) or (GameMap[mx+1, my]-1 = 2) or (GameMap[mx+1, my]-1 = 3) then
      begin
        GameMap[mx+1, my] := 0;
        SrcRect.x := (mx+1) shl 3 - 8;
        SrcRect.y := my shl 3;
        DestRect.x := (mx+1) shl 3;
        DestRect.y := my shl 3;
        SDL_UpperBlit( Img_background_, @SrcRect, Background, @DestRect );
        SDL_UpperBlit( Img_background_, @SrcRect, Screen, @DestRect );
        SpriteEngine.AddSprite(TExplosion.Create(((mx+1) shl 3), (my shl 3), 1, power));
      end;
    end;
    2,3 :
    Begin
      If Power = 2 then If (GameMap[mx, my]-1 = 1) or (GameMap[mx, my]-1 = 2) or (GameMap[mx, my]-1 = 3) then
      begin
        GameMap[mx, my] := 0;
        SrcRect.x := mx shl 3 - 8;
        SrcRect.y := my shl 3;
        DestRect.x := mx shl 3;
        DestRect.y := my shl 3;
        SDL_UpperBlit( Img_background_, @SrcRect, Background, @DestRect );
        SDL_UpperBlit( Img_background_, @SrcRect, Screen, @DestRect );
        SpriteEngine.AddSprite(TExplosion.Create((mx shl 3), (my shl 3), 1, power));

      end;


      If Power = 2 then If (GameMap[mx, my-1]-1 = 1) or (GameMap[mx, my-1]-1 = 2) or (GameMap[mx, my-1]-1 = 3) then
      begin
        GameMap[mx, my-1] := 0;
        SrcRect.x := (mx-1) shl 3;
        SrcRect.y := (my-1) shl 3;
        DestRect.x := mx shl 3;
        DestRect.y := (my-1) shl 3;
        SDL_UpperBlit( Img_background_, @SrcRect, Background, @DestRect );
        SDL_UpperBlit( Img_background_, @SrcRect, Screen, @DestRect );
        SpriteEngine.AddSprite(TExplosion.Create((mx shl 3), ((my-1) shl 3), 1, power));

      {  If random(10) = 0 then
        Begin
          i := random(7);
          i2 := (mx shl 3);
          i3 := (my shl 3);
          SpriteEngine.AddSprite(TIten.Create(i2,i3,i));
        End;    }

      end;

      If Power = 2 then If (GameMap[mx, my+1]-1 = 1) or (GameMap[mx, my+1]-1 = 2) or (GameMap[mx, my+1]-1 = 3) then
      begin
        GameMap[mx, my+1] := 0;
        SrcRect.x := mx shl 3 - 8;
        SrcRect.y := (my+1) shl 3;
        DestRect.x := mx shl 3;
        DestRect.y := (my+1) shl 3;
        SDL_UpperBlit( Img_background_, @SrcRect, Background, @DestRect );
        SDL_UpperBlit( Img_background_, @SrcRect, Screen, @DestRect );
        SpriteEngine.AddSprite(TExplosion.Create((mx shl 3), ((my+1) shl 3), 1, power));

       { If random(10) = 0 then
        Begin
          i := random(7);
          i2 := (mx shl 3);
          i3 := (my shl 3);
          SpriteEngine.AddSprite(TIten.Create(i2,i3,i));
        End;     }

      end;

    end;

    End; // End of Direction 2-3
   End; //End of Case Direction 1..3

   3 :
   begin

    Case Direction of
      0,1 :
      begin

      If (GameMap[mx, my]-1 <> 0 ) then
      begin
        GameMap[mx, my] := 0;
        SrcRect.x := mx shl 3 - 8;
        SrcRect.y := my shl 3;
        DestRect.x := mx shl 3;
        DestRect.y := my shl 3;
        SDL_UpperBlit( Img_background_, @SrcRect, Background, @DestRect );
        SDL_UpperBlit( Img_background_, @SrcRect, Screen, @DestRect );
        If Direction = 0 then
        SpriteEngine.AddSprite(TExplosion.Create((mx shl 3), (((my+1) shl 3)-2), 1, power))
        else SpriteEngine.AddSprite(TExplosion.Create((mx shl 3), (((my-1) shl 3)+2), 1, power));
      end;

      If (GameMap[mx-1, my] <> 0 ) then
      begin
        GameMap[mx-1, my] := 0;
        SrcRect.x := (mx-1) shl 3 - 8;
        SrcRect.y := my shl 3;
        DestRect.x := (mx-1) shl 3;
        DestRect.y := my shl 3;
        SDL_UpperBlit( Img_background_, @SrcRect, Background, @DestRect );
        SDL_UpperBlit( Img_background_, @SrcRect, Screen, @DestRect );
        SpriteEngine.AddSprite(TExplosion.Create(((mx-1) shl 3), (my shl 3), 1, power));
      end;

      If (GameMap[mx+1, my]-1 <> 0) then
      begin
        GameMap[mx+1, my] := 0;
        SrcRect.x := (mx+1) shl 3 - 8;
        SrcRect.y := my shl 3;
        DestRect.x := (mx+1) shl 3;
        DestRect.y := my shl 3;
        SDL_UpperBlit( Img_background_, @SrcRect, Background, @DestRect );
        SDL_UpperBlit( Img_background_, @SrcRect, Screen, @DestRect );
        SpriteEngine.AddSprite(TExplosion.Create(((mx+1) shl 3), (my shl 3), 1, power));
      end;
    end;
    2,3 :
    Begin
      If (GameMap[mx, my]-1 <> 0) then
      begin
        GameMap[mx, my] := 0;
        SrcRect.x := mx shl 3 - 8;
        SrcRect.y := my shl 3;
        DestRect.x := mx shl 3;
        DestRect.y := my shl 3;
        SDL_UpperBlit( Img_background_, @SrcRect, Background, @DestRect );
        SDL_UpperBlit( Img_background_, @SrcRect, Screen, @DestRect );
        SpriteEngine.AddSprite(TExplosion.Create((mx shl 3), (my shl 3), 1, power));
      end;

      If (GameMap[mx, my-1]-1 <> 0) then
      begin
        GameMap[mx, my-1] := 0;
        SrcRect.x := (mx-1) shl 3;
        SrcRect.y := (my-1) shl 3;
        DestRect.x := mx shl 3;
        DestRect.y := (my-1) shl 3;
        SDL_UpperBlit( Img_background_, @SrcRect, Background, @DestRect );
        SDL_UpperBlit( Img_background_, @SrcRect, Screen, @DestRect );
        SpriteEngine.AddSprite(TExplosion.Create((mx shl 3), ((my-1) shl 3), 1, power));
      end;

      If (GameMap[mx, my+1]-1 <> 0) then
      begin
        GameMap[mx, my+1] := 0;
        SrcRect.x := mx shl 3 - 8;
        SrcRect.y := (my+1) shl 3;
        DestRect.x := mx shl 3;
        DestRect.y := (my+1) shl 3;
        SDL_UpperBlit( Img_background_, @SrcRect, Background, @DestRect );
        SDL_UpperBlit( Img_background_, @SrcRect, Screen, @DestRect );
        SpriteEngine.AddSprite(TExplosion.Create((mx shl 3), ((my+1) shl 3), 1, power));
      end;

    end;

    End; // End of Direction 2-3
   End; //End of Case Direction 1..3


   end;

{      If Power = 2 then If (GameMap[mx-1, my]-1 = 1) or (GameMap[mx-1, my]-1 = 2) or (GameMap[mx-1, my]-1 = 3) then
      begin
        GameMap[mx-1, my] := 0;
        SrcRect.x := (mx-1) shl 3 - 8;
        SrcRect.y := my shl 3;
        DestRect.x := (mx-1) shl 3;
        DestRect.y := my shl 3;
        SDL_UpperBlit( Img_background_, @SrcRect, Background, @DestRect );
        SDL_UpperBlit( Img_background_, @SrcRect, Screen, @DestRect );
        SpriteEngine.AddSprite(TExplosion.Create(((mx-1) shl 3), (my shl 3), 1));
      end;

      If Power = 2 then If (GameMap[mx+1, my]-1 = 1) or (GameMap[mx+1, my]-1 = 2) or (GameMap[mx+1, my]-1 = 3) then
      begin
        GameMap[mx+1, my] := 0;
        SrcRect.x := (mx+1) shl 3 - 8;
        SrcRect.y := my shl 3;
        DestRect.x := (mx+1) shl 3;
        DestRect.y := my shl 3;
        SDL_UpperBlit( Img_background_, @SrcRect, Background, @DestRect );
        SDL_UpperBlit( Img_background_, @SrcRect, Screen, @DestRect );
        SpriteEngine.AddSprite(TExplosion.Create(((mx+1) shl 3), (my shl 3), 1));
      end;   }

{
  3 : //There are a lot of problems here...... normal
  begin
    Case Direction of
      0,1 :
    begin
      GameMap[mx, my] := 0;
      SrcRect.x := mx shl 3 - 8;
      SrcRect.y := my shl 3;
      DestRect.x := mx shl 3;
      DestRect.y := my shl 3;
      SDL_UpperBlit( Img_background_, @SrcRect, Background, @DestRect );
      SDL_UpperBlit( Img_background_, @SrcRect, Screen, @DestRect );
      SpriteEngine.AddSprite(TExplosion.Create((mx shl 3), (my shl 3), 1));

      GameMap[mx, my] := 0;
      SrcRect.x := mx shl 3;
      DestRect.x := mx shl 3 + 8;
      SDL_UpperBlit( Img_background_, @SrcRect, Background, @DestRect );
      SDL_UpperBlit( Img_background_, @SrcRect, Screen, @DestRect );

      GameMap[mx-1, my] := 0;
      SrcRect.x := mx shl 3 - 16;
      DestRect.x := mx shl 3 - 8;
      SDL_UpperBlit( Img_background_, @SrcRect, Background, @DestRect );
      SDL_UpperBlit( Img_background_, @SrcRect, Screen, @DestRect );
   end;
   2,3 :
   begin
   //There are erros here!!!!
    GameMap[mx, my] := 0;
    SrcRect.x := mx shl 3;
    SrcRect.y := my shl 3 - 8;

    DestRect.x := mx shl 3;
    DestRect.y := my shl 3;
    SDL_UpperBlit( Img_background_, @SrcRect, Background, @DestRect );
    SDL_UpperBlit( Img_background_, @SrcRect, Screen, @DestRect );
    SpriteEngine.AddSprite(TExplosion.Create((mx shl 3), (my shl 3), 1));

    GameMap[mx, my+1] := 0;
    SrcRect.y := mx shl 3 + 8;
    DestRect.y := mx shl 3;
    SDL_UpperBlit( Img_background_, @SrcRect, Background, @DestRect );
    SDL_UpperBlit( Img_background_, @SrcRect, Screen, @DestRect );

    GameMap[mx, my-1] := 0;
    SrcRect.y := mx shl 3 - 8;
    DestRect.y := mx shl 3 - 16;
    SDL_UpperBlit( Img_background_, @SrcRect, Background, @DestRect );
    SDL_UpperBlit( Img_background_, @SrcRect, Screen, @DestRect );
   end;

    GameMap[mx, my] := 0;
    GameMap[mx+1, my] := 0;
    GameMap[mx-1, my] := 0;
    GameMap[mx, my+1] := 0;
    GameMap[mx, my-1] := 0;

    SrcRect.x := mx shl 3 - 8;
    SrcRect.y := my shl 3;
    SrcRect.w := 8;
    SrcRect.h := 8;
    DestRect.x := mx shl 3;
    DestRect.y := my shl 3;
    SDL_UpperBlit( Img_background_, @SrcRect, Background, @DestRect );
    SDL_UpperBlit( Img_background_, @SrcRect, Screen, @DestRect );

    SrcRect.y := my shl 3;
    DestRect.y := my shl 3;
    SrcRect.w := 8;
    SrcRect.h := 8;

    GameMap[mx, my+1] := 0;
    SrcRect.x := mx shl 3;
    DestRect.x := mx shl 3 + 8;
    SDL_UpperBlit( Img_background_, @SrcRect, Background, @DestRect );
    SDL_UpperBlit( Img_background_, @SrcRect, Screen, @DestRect );

    GameMap[mx, my-1] := 0;
    SrcRect.x := mx shl 3 - 16;
    DestRect.x := mx shl 3 - 8;
    SDL_UpperBlit( Img_background_, @SrcRect, Background, @DestRect );
    SDL_UpperBlit( Img_background_, @SrcRect, Screen, @DestRect );

    GameMap[mx, my] := 0;
    SrcRect.x := mx shl 3 - 8;
    DestRect.x := mx shl 3;
    SDL_UpperBlit( Img_background_, @SrcRect, Background, @DestRect );
    SDL_UpperBlit( Img_background_, @SrcRect, Screen, @DestRect );
    SpriteEngine.AddSprite(TExplosion.Create((mx shl 3), (my shl 3), 1));

    GameMap[mx, my+1] := 0;
    SrcRect.y := my shl 3+8;
    DestRect.y := my shl 3+8;
    SDL_UpperBlit( Img_background_, @SrcRect, Background, @DestRect );
    SDL_UpperBlit( Img_background_, @SrcRect, Screen, @DestRect );

    GameMap[mx, my-1] := 0;
    SrcRect.y := my shl 3-8;
    DestRect.y := my shl 3-8;
    SDL_UpperBlit( Img_background_, @SrcRect, Background, @DestRect );
    SDL_UpperBlit( Img_background_, @SrcRect, Screen, @DestRect );
  end;
end;
}
 end;

procedure TTiro.Left;
var
  mx, my1 : integer;
  Coll : byte;
  NewX : integer;
begin
  NewX := x - speed;
  if NewX < 0 then
  begin
    // Player reaches the left side of screen (Just for SAFETY). It never
    // could be happen!
    x := 0;
    kill;
    exit;
  end;
//  if ( ( NewX + 4 ) and 15 ) >= 12 then
//  begin
    mx := ( NewX + 0 ) div 8;
    my1 := ( y  ) div 8;
    Coll := MapID[GameMap[mx, my1]];


    if ( Coll  ) > 0 then
    begin
      DestroyBrick( mx, my1 );
 //     NewX := x;
      kill;
    end;
//  end;
  x := NewX;
end;

procedure TTiro.Right;
var
  mx, my1 : integer;
  Coll : byte;
  NewX : integer;
begin
  NewX := x + speed;
  if NewX > Screen_Width - w then
  begin
    // Player reaches the right side of screen (Just for SAFETY). It never
    // could be happen!
    x := Screen_Width - w;
    kill;
    exit;
  end;
    mx := ( NewX - 16 + w ) div 8;
    my1 := ( y ) div 8;
    Coll := MapID[GameMap[mx, my1]];

    if ( Coll ) > 0 then
    begin
      DestroyBrick( mx, my1 );
//      NewX := x;
      kill;
    end;
  x := NewX;
end;


procedure TTiro.Up;
var
  mx1, my : integer;
  Coll : byte;
  NewY : integer;
begin        //_w=29
  NewY := y - speed;
  if NewY < 0 then
  begin
    // Player reaches the top of screen (Just for SAFETY). It never
    // could be happen!
    y := 0;
    kill;
  end;
//  if (  NewY + 6 ) and 15 ) > 11 then
//  begin
    my := ( NewY + 8) div 8;    //    my := ( NewY + 8 ) div 8;
    mx1 := ( x ) div 8;
    Coll := MapID[GameMap[mx1, my]];

    if ( Coll  ) > 0 then
    begin
      DestroyBrick( mx1, my );
      NewY := y;
      kill;
    end;
//  end;
  y := NewY;
end;


procedure TTiro.Down;
var
  mx1, my : integer; //mx2, mx3,
  Coll : byte;
  NewY : integer;
begin      //28x, 25h;
  NewY := y + speed;
  if NewY > BG_HEIGHT - h then
  begin
// Player reaches the bottom of screen (Just for SAFETY). It never could be happen!
    y := BG_HEIGHT - h;
    kill;
    exit;
  end;

  // There are a lof of errors here!!!!
  my  := ( NewY + h - 16) div 8;
  mx1 := ( x ) div 8;
  Coll := MapID[GameMap[mx1, my]];
{  mx2 := ( x + 7) div 8;
  Coll := Coll or MapID[GameMap[mx2, my]];
  mx3 := ( x - 7) div 8;
  Coll := Coll or MapID[GameMap[mx3, my]];
}
  if ( Coll ) > 0 then
  begin

    DestroyBrick( mx1, my );
 //   if MapID[GameMap[mx1, my]] > 0 then DestroyBrick( mx1, my );
 //   if MapID[GameMap[mx1, my]] > 0 then DestroyBrick( mx2, my );
 //   if MapID[GameMap[mx1, my]] > 0 then DestroyBrick( mx3, my );
 // NewY := y;
    Kill;
end;
  y := NewY;
end;


procedure TTiro.Move;
begin
  inc(Temp_Anim_pos);

  if temp_anim_pos > 5 then
  begin
    Anim_pos := 1 - Anim_pos;
    Temp_Anim_pos := 0;
  end;

  case direction of
    0 : down;  //Y := y+speed;
    1 : up;    //Y := y-speed;
    2 : right; //X := x+speed;
    3 : left;  //X := x-speed;
  end;

//X  if not ParentList[idTiro].isDead = true then
//X  if (y<0) or (y>SCREEN_HEIGHT-80) or (x>BG_Width) or (x<0) then Self.Kill;

  DetectCollision;  
end;

function TimeLeft: UInt32;
var
  now : UInt32;
begin
  now := SDL_GetTicks;
  if next_time <= now then
  begin
    next_time := now + TICK_INTERVAL;
    result := 0;
    exit;
  end;
  result := next_time - now;
end;

procedure Initialize;
begin
  log.LogStatus( 'Starting Initialization Video', 'Initialize Video' );

  Case Res_Screen of
    1 :
    begin
      Screen_Width  := 640;
      Screen_Height := 480;
      BG_WIDTH := 640;
      BG_HEIGHT := 416;
    end;
    2 :
    begin
      Screen_Width  := 800;
      Screen_Height := 600;
      BG_WIDTH := 800;
      BG_HEIGHT := 536;
    end;
    3 :
    begin
      Screen_Width  := 1024;
      Screen_Height := 768;
      BG_WIDTH := 1024;
      BG_HEIGHT := 704;
    end;
    else
    begin
      Res_Screen := 1;
      Initialize;
      Exit;
    end;
  end;

  randomize; //BG_WIDTH  := SCREEN_WIDTH;  BG_HEIGHT := SCREEN_HEIGHT-64;
  max_X := (trunc(BG_WIDTH/33)-1); max_Y := (trunc(BG_HEIGHT/46)-1);

  If SDL_Init( SDL_INIT_VIDEO ) < 0 then
  begin
    Log.LogError(Format('initialization failed: %s\n',[SDL_GetError]), 'Start');
    Halt(1);
  end;

  SDL_WM_SetCaption(App_title, nil);
  SDL_WM_SetIcon( IMG_Load('images\icon.png'), 0 );

  // Try to load Audio
  if SoundFlag <> 0 then
  begin
    if Mix_OpenAudio(
    //22050{44100}, AUDIO_S16SYS, 2, 1024 //2048//4096 {AUDIO_S16LSB}
      44100, MIX_DEFAULT_FORMAT, 2, 4096
      ) < 0 then
    begin
      log.LogWarning( 'SDL_MIXER: Initialization failed', 'Initialize' );
      log.LogError( string(SDL_GetError), 'Initialize' );
      // If Audio initialization failed then turn off audio
      NoSound := True;
    end else
    begin
      log.LogStatus( 'SDL_MIXER Initialization was successful', 'Initialize' );

      //Mix_HookMusicFinished( @RestartMusic );
      log.LogStatus( 'SDL_MIXER: hooking RestartMusic was successful',
        'Initialize' );
    end;
  end;

  //I should add here a function to check if the sound file really exists.
  Musics[0] := PChar( 'sound\music\gasgiant.s3m' );
 // Musics[1] := PChar( 'sound\music\smace.s3m' );
  //I should add here a function to check if the sound file really exists.
  Sounds[0] := Mix_LoadWAV( 'sound/fx/BIGGUN.wav' );
  Sounds[1] := Mix_LoadWAV( 'sound/fx/LARGEXPL.wav' );
  Sounds[2] := Mix_LoadWAV( 'sound/fx/PLY_Save.wav' );
  Sounds[3] := Mix_LoadWAV( 'sound/fx/GUN_SHOT.wav' );
  Sounds[4] := Mix_LoadWAV( 'sound/fx/tick.wav' );
  Sounds[5] := Mix_LoadWAV( 'sound/fx/click.wav' );

  MusicVolume := 50;

  If fullscreen then   Screen := SDL_SetVideoMode(SCREEN_WIDTH, SCREEN_HEIGHT, bpp, SDL_FULLSCREEN)
  else  Screen := SDL_SetVideoMode(SCREEN_WIDTH, SCREEN_HEIGHT, bpp, SDL_SWSURFACE);// SDL_SWSURFACE

  // Try to create the background surface
  Background := SDL_CreateRGBSurface( SDL_SWSURFACE or SDL_HWPALETTE, SCREEN_WIDTH,SCREEN_HEIGHT, bpp, 0, 0, 0, 0 );
  if Background = nil then
  begin
{$IFDEF LOG}
    log.LogError( Format( 'Creating background surface failed : %s', [SDL_GetError] ), 'Initialize' );
{$ENDIF}
    Finalize;
    Halt( 1 );
  end
  else
  begin
    Background := SDL_DisplayFormat( Background );
  end;

  SpriteEngine := TSpriteEngine.Create(Screen);
  SpriteEngine.BackgroundSurface := BackGround;

  //Try to load fonts
  Img_font_ := IMG_Load('images/fonts/24P_Arial_NeonYellow.png'); SFont_InitFont(Img_Font_);

  SDL_EnableKeyRepeat( SDL_DEFAULT_REPEAT_DELAY, SDL_DEFAULT_REPEAT_INTERVAL );

  log.LogStatus( 'Initialization was successful', 'Initialize' );
end;

procedure Game_LoadMap( Map : integer );
const
  codes = ' 0123456';
var
  f : text;
  Line : string;
  x, y : integer;
  filename : string;
  x_,y_,z_,tipo1_,tipo2_ : integer;
begin

  fillchar( GameMap, sizeof( GameMap ), 0 );  //Fill the GameMap with 0
  filename := 'Maps/' + inttostr( pl_Level ) + '.lev';

  if not fileexists( filename ) then
  begin
    Log.LogError( 'File not found: ' + filename, 'LoadMap' );
    exit;
  end;
                            //0..127, 0..87]
  assignfile( f, filename );
  reset( f );
  for y := 0 to 87 do
  begin
    readln( f, Line );
    for x := 0 to length( Line ) - 1 do
      If x < 127 then
      case Line[x + 1] of
        'B' :
          begin
            GameMap[x, y] := 0;
            MyBase := TBase.Create;
            MyBase.x := x shl 3;
            MyBase.y := y shl 3;
            MyBase.tipo := 0;
            SpriteEngine.AddSprite( MyBase );
          end;
        'S' :
          begin
            Pl_StartPosX := x;
            Pl_StartPosY := y;
          end;
        'F' :
          begin
            GameMap[x, y] := 0;
            x_ := x shl 3;
            y_ := y shl 3;
            z_ := 2;
            tipo1_ := 2;
            tipo2_ := random(2); //0;
            SpriteEngine.AddSprite(TMapa.Create(x_,y_,z_,tipo1_,tipo2_));
          end;
        'M' :
          begin
            GameMap[x,   y] := 7;
            GameMap[x+1, y] := 7;
            GameMap[x+2, y] := 7;
            GameMap[x+3, y] := 7;
            GameMap[x,   y+1] := 7;
            GameMap[x+1, y+1] := 7;
            GameMap[x+2, y+1] := 7;
            GameMap[x+3, y+1] := 7;
            GameMap[x,   y+2] := 7;
            GameMap[x+1, y+2] := 7;
            GameMap[x+2, y+2] := 7;
            GameMap[x+3, y+2] := 7;
            GameMap[x,   y+3] := 7;
            GameMap[x+1, y+3] := 7;
            GameMap[x+2, y+3] := 7;
            GameMap[x+3, y+3] := 7;

            x_ := x shl 3;
            y_ := y shl 3;
            z_ := 0;
            tipo1_ := 2;
            tipo2_ := 2+random(2); //2;
            SpriteEngine.AddSprite(TMapa.Create(x_,y_,z_,tipo1_,tipo2_));
          end;

        '1'..'7' :
          begin
            GameMap[x, y] := pos( Line[x + 1], Codes ) - 1;
            if y < 51 then GameMap[x, y + 1] := (pos( Line[x + 1], Codes ) - 1 + 4);
          end;
        {
        'a'..'d', 'i'..'l', 'q'..'t', 'y', 'z', '0', '1' :
          begin
            GameMap[x, y] := pos( Line[x + 1], Codes ) - 1;
            if y < 51 then GameMap[x, y + 1] := pos( Line[x + 1], Codes ) - 1 + 4;
          end;
        }
      else
        if Line[x + 1] <> ' ' then GameMap[x, y] := 0;
      end;
  end;

  If Pl_StartPosX = 0 then Pl_StartPosX := 1;
  If Pl_StartPosY = 0 then Pl_StartPosY := 1;

{
  // read initial coords of player
  if not eof( f ) then
    readln( f, PlayerStartX, PlayerStartY )
  else
  begin // This never should be happened
    PlayerStartX := 1;
    PlayerStartY := 1;
  end;
  // read amount of available oxygene level
  if not eof( f ) then
    readln( f, MaxO2 )
  else
    MaxO2 := 100; // This never should be happened
}
  closefile( f );

  Log.LogStatus( 'Loading was successful: ' + filename, 'Initialize' );

end;

procedure Game_DrawMap;
var
    i,i2,x,y,tipo1,tipo2 : integer;
    SrcRect, DestRect : TSDL_Rect;
begin
  log.LogStatus( 'Draw Map', 'Draw Map' );
{
 //Isn't necessary this line, see Game_NextLevel;
  with SpriteEngine do
    for i := 0 to Sprites.Count-1 do
     if Sprites.Items[i].ID = idTileMap then Sprites.Items[i].Kill;
}

  //[0..79, 0..51] // 640x416
  //[0..100, 0..67] // 800x600
  //[0..127, 0..87] // 1024x768

  DestRect.h := 32;
  DestRect.w := 32;
  tipo1 :=0;
  for i2 := 0 to trunc(BG_HEIGHT/32) do
  begin
    for i := 0 to trunc(BG_WIDTH/32) do
    begin
      tipo2:=random(2);
      if random(50) = 0 then tipo2:=2;
      if random(60) = 0 then tipo2:=3;
      DestRect.x := 32 * i; DestRect.y := 32 * i2;
      SDL_UpperBlit(Img_Terrenos, @Rects_map[tipo1,tipo2], BackGround, @DestRect);
    end; //End of FOR
  end; //End of FOR

  SDL_UpperBlit(BackGround, nil,Img_background_,nil);

  Game_LoadMap(pl_Level);

  SrcRect.w := 8;
  SrcRect.h := 8;
  SrcRect.y := 0;

  //[0..79, 0..51] // 640x416
  //[0..100, 0..67] // 800x600
  //[0..127, 0..87] // 1024x768

  for y := 0 to trunc(BG_HEIGHT/8) do    //79, 0..51
   for x := 0 to trunc(BG_WIDTH/8) do      //51*79=2499
   begin
    if GameMap[x, y] > 0 then
    begin
      SrcRect.x := GameMap[x, y] shl 3 - 16;// - 8; //shl 3
      DestRect.x := x shl 3;
      DestRect.y := y shl 3;
      SDL_UpperBlit( Img_Tiles, @SrcRect, Background, @DestRect );
    end;
   end;
   SDL_UpperBlit( Background, @SrcRect, screen, @DestRect );
end;

procedure Game_NextLevel;
var i, time  : integer;
    Quit : boolean;
begin
   quit := false;
   inc(Pl_level);
   time := 100;
   SpriteEngine.AddSprite(TPainel.Create(300,80,time)); //hey - i've got a problem!! :)
   SpriteEngine.Move;
   SDL_UpperBlit( Background, nil, Screen, nil );
   SpriteEngine.Draw;
 SFont_Write(Screen, 320 - 150+20, 240 - 110+20+30, PChar('Level: ' + IntToStr(Pl_level)));
   SDL_UpdateRect(Screen, 0, 0, BG_WIDTH, SCREEN_HEIGHT-64);

  Game_Drawmenu(drLevel);

  repeat
    dec(MyTank_Ammo);
    inc(Pl_score, 2);

//    dec(time);

      while SDL_PollEvent( @Event ) > 0 do
      begin
      //tem error aqui!!!!!!!!
        if Event.key.type_ = SDL_KeyDown then
        if Event.key.keysym.sym = SDLK_SPACE  then Quit := true;
      end;
      keys := PKeyStateArr(SDL_GetKeyState(nil));

    SpriteEngine.Move;
    SDL_UpperBlit( Background, nil, Screen, nil );
    SpriteEngine.Draw;
    SFont_Write(Screen, 320 - 150+20, 240 - 110+20, 'Get Ready!');
    SFont_Write(Screen, 320 - 150+20, 240 - 110+20+30, PChar('Level: ' + IntToStr(Pl_level)));
    SDL_UpdateRect(Screen, 0,0,BG_Width,BG_HEIGHT);
    Game_Drawmenu(drAmmo);
    Game_Drawmenu(drScore);
    SDL_Delay( TimeLeft );
  until (Quit = True) or (MyTank_Ammo = 0) ;  //or (time<1)

  SpriteEngine.Clear;

  {
    with SpriteEngine do
    for i := 0 to Sprites.Count-1 do
      case Sprites.Items[i].ID  of
         idIten : Sprites.Items[i].Kill;
         idTiro : Sprites.Items[i].Kill;
         idEnemyTank : Sprites.Items[i].Kill;
         idTileMap : Sprites.Items[i].Kill;
         idBase : Sprites.Items[i].Kill;
      end;
  }

  Game_DrawMap;
  SpriteEngine.AddSprite(TPlayer.Create);

  inc(Pl_nenemy,2);
  Pl_nenemy_temp := Pl_nenemy;
  MyTank_Ammo := MyTank_Ammo + Pl_nenemy shl 2;

  Game_DrawMenu(drAll);

  for i := 0 to (Pl_max_ntank-1) do SpriteEngine.AddSprite(TEnemyTank.Create);

///habilidades espe3ciacis
  with SpriteEngine.Sprites do
    for i := 0 to  Count-1 do
      if random(5-Pl_level) = 0 then
      if Items[i].ID = idEnemyTank then
        with TEnemyTank(Items[i]) do
          Case random(3) of
            0 : inc(speed,(1+random(pl_level)));
            1 : inc(Shield,(1+random(pl_level)));
            2 : inc(Power,(1+random(2)));
          end;
//habilidades especiais

end;

procedure img_DrawBg;
var
    x, y : integer; //, al
    temp_camu : PSDL_Surface;
    DestRect : TSDL_Rect;

begin
  img_LoadImage( temp_camu, 'images/fundo_camu_menu.png', false, 0, 0, 0, true);

  SDL_SetAlpha( temp_camu, SDL_SRCALPHA, 96 );

    for y := 0 to trunc(SCREEN_HEIGHT/204) do
      for x := 0 to trunc(SCREEN_WIDTH/204) do
        begin
          DestRect.x := x * 204;
          DestRect.y := y * 204;
          SDL_UpperBlit( temp_camu, nil, screen, @DestRect );
        end;

{  for y := 0 to 4 do
  al := 128;
    for y := 0 to 4 do
    begin
      for x := 0 to 3 do
        if al > 0 then
        begin
          DestRect.x := x * 204;
          DestRect.y := y * 204;
          SDL_SetAlpha( temp_camu, SDL_SRCALPHA, al );
          SDL_UpperBlit( temp_camu, nil, screen, @DestRect );
        end;
      al := al - 32;
    end;
  }
  SDL_FreeSurface(temp_camu);
end;


procedure Game_LoadIMGs;
begin
  log.LogStatus( 'Start Loading Images at Game', 'Game' );

  Img_Background_ := SDL_CreateRGBSurface( SDL_SWSURFACE or SDL_HWPALETTE, BG_Width, BG_HEIGHT, bpp, 0, 0, 0, 0 );
  if Img_Background_ = nil then
  begin
    Finalize;
    Halt( 1 );
  end;

  Img_menu := SDL_CreateRGBSurface( SDL_SWSURFACE, BG_WIDTH, 64,
    Screen.format.BitsPerPixel, Screen.format.RMask, Screen.format.GMask,
    Screen.format.BMask, Screen.format.AMask );
  if Img_menu = nil then
  begin
    Finalize;
    Halt( 1 );
  end;

  img_LoadImage( Img_FIcons, 'images/fonts/FIcons.png', false, 0, 0, 0, true);
  img_LoadImage( Img_Base,   'images/Base.png', false, 0, 0, 0, true);
  img_LoadImage( Img_Tiles,  'images/Tiles.png', false, 255, 0, 255, true);
  img_LoadImage( Img_Painel, 'images/Painel.png', false, 255, 0, 255, true);
  img_LoadImage( Img_Tanks,  'images/Tanks.png', true, 255, 0, 255, true);
  img_LoadImage( Img_Itens,  'images/Itens.png', true, 255, 0, 255, true);
  img_LoadImage( Img_Terrenos, 'images/Terrenos.png', true, 255, 0, 255, true);
  img_LoadImage( Img_Arsenal,  'images/Arsenal.png', true, 255, 0, 255, true);
  img_LoadImage( Img_Explosion,'images/Explosion.png', true, 0, 0, 0, true);
  img_LoadImage( Img_font_digital19x24, 'images/fonts/digital19x24.png', false, 0, 0, 0, true);
  img_LoadImage( Img_font_digital10x13, 'images/fonts/digital10x13.png', false, 0, 0, 0, true);

  log.LogStatus( 'End Loading Images at Game', 'Game' );
end;

procedure Game_StartGame;
var i : integer;
begin
  log.LogStatus( 'Starting Initialization', 'Game_StartGame' );

  Game_MakeRects;
  Game_LoadIMGs;

  Pl_max_ntank := 5;
  Pl_nenemy := 10;
  Pl_nenemy_temp := 10;

  {$IFDEF GAME_CHEAT_ON}
  // Com pilantragens
  Pl_level := game_cheat_level;
  {$ELSE}
  // fair play
    Pl_level :=1;
  {$ENDIF}



  
  
  Pl_lives := 3;
  Pl_score := 0;
  MyTank_Ammo  := Pl_nenemy shl 2;
  MyTank_Shot  := 2;
  MyTank_Speed := 2;
  MyTank_Shield:= 1;
  MyTank_Power := 1;
  
  SpriteEngine.Clear;
  //Starting to add sprites...
  Game_DrawMap;

  SpriteEngine.AddSprite(TPlayer.Create);

  SDL_FillRect(Screen,nil,0);
  for i := 0 to (Pl_max_ntank-1) do SpriteEngine.AddSprite(TEnemyTank.Create);
  SpriteEngine.Draw;
  Game_DrawMenu(drAll);

  SDL_SHOWCURSOR(0);
  log.LogStatus( 'Starting Finalizing', 'Game_StartGame' );
end;

procedure Game_Stop;
var
  Buffer : PSDL_Surface;
begin
  Quit := false;
  SDL_SHOWCURSOR(1);
  // Create a buffer with the current display format, so we don't need to use SDL_DisplayFormat()
  Buffer := SDL_CreateRGBSurface( SDL_SWSURFACE, Screen_Width, 480,
    Screen.format.BitsPerPixel, Screen.format.RMask, Screen.format.GMask,
    Screen.format.BMask, Screen.format.AMask );
  // Build the screen
  SDL_SetAlpha( Screen, SDL_SRCALPHA, 128 );
  SDL_UpperBlit( Screen, nil, Buffer, nil );
  SDL_SetAlpha( Screen, 0, 0 );
  SDL_UpperBlit( Buffer, nil, Screen, nil );

  SFont_Write(screen, 20, 20, 'the game is now paused.');
  SFont_Write(screen, 20, 50, 'drink a coffee then hit space');
  SFont_Write(screen, 20, 80, 'to continue playing.');
  SDL_UpdateRect(Screen, 0, 0, 0, 0);
  repeat
    while SDL_PollEvent( @Event ) > 0 do
    begin
      case Event.key.type_ of
      
       SDL_KeyDown :
       Begin
        if Event.key.keysym.sym = SDLK_SPACE  then Quit := true;
        if Event.key.keysym.sym = SDLK_ESCAPE then
         Begin
          RunScene := scGameMenu;
          Quit := true;
         End;
       End;
        {
        if Event.key.type_ = SDL_MOUSEBUTTONDOWN then
        begin
          Mousedown := true;
        end;
        }
         SDL_QUITEV :
         begin
           RunScene := scQuitApp;
           Quit := true;
         end;
      end;
    end;
    SDL_Delay( TimeLeft );
  until Quit = true;

  // Blank screen
  SDL_SetAlpha( Buffer, 0, 0 );
  SDL_UpperBlit( Buffer, nil, Screen, nil );
  SDL_FreeSurface( Buffer );
  SDL_UpperBlit( Background, nil, Screen, nil );
  Game_DrawMenu(drAll);
end;

procedure Game_LoadHighScore;
  Var
    destFile : TextFile;
    S        : String;
    Posicao, posicao2 : Integer;

  procedure SetVar;
  begin
  case posicao2 of
        0     : HighScore_Name[posicao]  := S;
        1     : HighScore_Level[posicao] := S;
        2     : HighScore_Score[posicao] := S;
      end;
    end;

  begin
    posicao := 0; posicao2 := 0;
    if Not FileExists( 'highscore.lst' ) then Exit;
    AssignFile(destFile, 'highscore.lst');
    Reset(destFile);
    try
      while Not EOF(destFile) do
      begin
        Readln(destFile, S);
        if (pos(';', S) <> 1) and (pos('{', S) <> 1) then
        begin
          SetVar;
          Inc(posicao2);
          if posicao2 > 2 then
          begin
           inc(posicao);
           posicao2:=0;
          end;
        end;
      end;
    finally
      CloseFile(destFile);
    end;
end;


procedure sc_HighScore( Level, Score : integer);
var
//        Logo_ : PSDL_Surface;
        temp_Name : string;
        temp_screen : PSDL_Surface;

begin
  SDL_FillRect( Screen, nil, 0 );
  SDL_SHOWCURSOR(1);
  RunScene := scHighScore;
  img_DrawBg;

  log.LogStatus( 'Initializing HighScore Screen', 'sc_HighScore' );
  Mousedown := False;
  Game_LoadHighScore;

  SFont_Write(screen, 20, 20,  'Smace''s WAR - High Score');
  SFont_Write(screen, 20, 70,  PChar('1) ' + HighScore_Name[0]));
  SFont_Write(screen, 20, 100, PChar('    Level: ' + HighScore_Level[0])); //IntTostr(HighScore_Level[0])));
  SFont_Write(screen, 20, 130, PChar('    Score: ' + HighScore_Score[0])); //IntTostr(HighScore_Score[0])));

  SFont_Write(screen, 20, 170, PChar('2) ' + HighScore_Name[1]));
  SFont_Write(screen, 20, 200, PChar('    Level: ' + HighScore_Level[1])); //IntTostr(HighScore_Level[1])));
  SFont_Write(screen, 20, 230, PChar('    Score: ' + HighScore_Score[1])); //IntTostr(HighScore_Score[1])));

  SFont_Write(screen, 20, 270, PChar('3) ' + HighScore_Name[2]));
  SFont_Write(screen, 20, 300, PChar('    Level: ' + HighScore_Level[2])); //IntTostr(HighScore_Level[2])));
  SFont_Write(screen, 20, 330, PChar('    Score: ' + HighScore_Score[2])); //IntTostr(HighScore_Score[2])));
  SFont_Write(screen, 20, 370, 'Menu');
  SDL_UpdateRect(screen, 0, 0, 0, 0);
  temp_Name := '';
  temp_screen := SDL_CreateRGBSurface( SDL_SWSURFACE or SDL_HWPALETTE, Screen_Width, 480, bpp, 0, 0, 0, 0 );
  if temp_screen = nil then
  begin
    Finalize;
    Halt( 1 );
  end;
  SDL_UpperBlit(screen,nil,temp_screen,nil);
 repeat
    while SDL_PollEvent(@Event) > 0 do
    begin

      case Event.key.type_ of
       SDL_QUITEV :
       begin
         RunScene := scQuitApp;
       end;

     {  SDL_KeyDown :
       begin
         if Event.key.keysym.sym = SDLK_ESCAPE then RunScene := scQuitApp
         else RunScene := scGame;
       end; }

       SDL_MOUSEBUTTONDOWN:
        begin
          Mousedown := true;
        end;


          SDL_KeyDown : begin
            case Event.key.keysym.sym of

              SDLK_ESCAPE : RunScene := scMenu;

              SDLK_RETURN, SDLK_KP_ENTER: RunScene := scMenu;

              SDLK_a..SDLK_z, SDLK_SPACE :
                begin
                  if Length(temp_Name) < 20 then
                  temp_Name := temp_Name + char(Event.key.keysym.sym);
                end;
              SDLK_BACKSPACE:
                begin
                  temp_Name := copy(temp_Name,0,(length(temp_Name)-1));
                end;
         {
              SDLK_PERIOD, SDLK_KP_PERIOD :
                begin
                  AddString( Window, '.' );
               end;
         }
          end;
      end;
   end;
   end;
   keys := PKeyStateArr(SDL_GetKeyState(nil));

    SDL_GetMouseState(MouseX, MouseY);
    if MouseDown then
    begin
      if (Mousex > 20) and (Mousex < 250) and (Mousey > 370) and (Mousey < 400) then RunScene := scMenu;
      Mousedown := False;
    end;

  SDL_UpperBlit(temp_screen,nil,screen,nil);
  SFont_Write(screen, 0, 450, PCHAR(temp_Name));
  SDL_UpdateRect(screen,0,0,0,0);
  SDL_Delay(TimeLeft);
 Until RunScene <> scHighScore;

  log.LogStatus( 'Finalizing HighScore', 'sc_HighScore' );
  SDL_FillRect( Screen, nil, 0 );
  SDL_UpdateRect( Screen, 0, 0, 0, 0 );
  log.LogStatus( 'Finalization successful', 'sc_HighScore' );
end;


procedure Game_DiePlayer;
var
  time : integer;
  Quit : boolean;
  i : integer;
begin

  If pl_lives > 0 then dec(Pl_lives);
  snd_PlaySound(1,100);

  Game_DrawMenu(drAll);

  if Pl_lives < 1 then
  begin
//   Pl_lives:=3; Pl_score:=0; Pl_nenemy:=30; Pl_nenemy_temp:=30;
   RunScene := scHighScore;
   quit := false;
   time:=100;
   SpriteEngine.AddSprite(TPainel.Create(300,80,time));
   With SpriteEngine.Sprites Do
     For i := 0 to Count-1 Do
       If Items[i].ID = idPainel Then
       begin
          SFont_Write(TPainel(Items[i]).Img_panel, 20, 135, PChar('Game Over!'));
          log.LogWarning('---------------------', 'HEY AQUI -----------');
       end;

//   SFont_Write(Img_Panel, 20, trunc(h/2)-15, '');
   SFont_Write(Screen, 20, 0, PChar('Game Over!'));
   SpriteEngine.Move;
   SDL_UpperBlit( Background, nil, Screen, nil );

   SpriteEngine.Draw;
   SDL_UpdateRect(Screen, 0, 0, BG_WIDTH, SCREEN_HEIGHT-64);
   Game_DrawMenu(drLives);
    repeat
     dec(time);
     while SDL_PollEvent( @Event ) > 0 do
     begin
      //tem error aqui!!!!!!!!
       if Event.key.type_ = SDL_KeyDown then
       if Event.key.keysym.sym = SDLK_SPACE  then Quit := true;
     end;
     keys := PKeyStateArr(SDL_GetKeyState(nil));
     SpriteEngine.Move;
     SDL_UpperBlit( Background, nil, Screen, nil );
     SpriteEngine.Draw;
     SDL_UpdateRect(Screen, 0, 0, BG_WIDTH, BG_HEIGHT);
     SDL_Delay( TimeLeft );
    until (Quit = True) or (Time = 0);  //or (time<1)
    sc_HighScore(Pl_level,Pl_score);
  end
  else
  Begin
    If PlayerAlive = False then
     SpriteEngine.AddSprite(TPlayer.Create);

    MyTank_Shot  := 2;
    MyTank_Speed := 2;
    MyTank_Shield:= 1;
    MyTank_Power := 1;
    Game_DrawMenu(drItens);
  end;

end;

procedure sc_GameMenu; forward;

procedure sc_Game;
 var i, i2, tipo : integer;   //, x, y, z, tipo1, tipo2
begin
  log.LogStatus( 'Initializing sc_Game', 'sc_Game' );
  Game_StartGame;
  log.LogStatus( 'Starting to Play the Game', 'sc_Game' );
  SDL_WM_SetCaption(PChar('Smace''s WAR was created by S�rgio Marcelo - Unfinished Version'), nil);
  repeat
    repeat
      while SDL_PollEvent(@Event) > 0 do
      begin
        if ( Event.active.State = SDL_APPACTIVE ) or
           ( Event.active.State = SDL_APPINPUTFOCUS ) then
        if Event.active.gain = 0 then Game_Stop;

        case Event.key.type_ of
         SDL_QUITEV : RunScene := scQuitApp;
         SDL_KeyDown : if Event.key.keysym.sym = SDLK_ESCAPE then RunScene := scGameMenu;
        end; //End of case Event
      end; //End of While

     keys := PKeyStateArr(SDL_GetKeyState(nil));

     If random(300) = 0 then
      begin
        i:=random(600); i2:=random(400); tipo:=random(7);
        SpriteEngine.AddSprite(TIten.Create(i,i2,tipo));
      end;

     SpriteEngine.Move;
     SDL_UpperBlit( Background, nil, Screen, nil );
     SpriteEngine.Draw;
     SDL_UpdateRect(Screen, 0, 0, BG_WIDTH, SCREEN_HEIGHT-64);
     SDL_Delay(TimeLeft);
   Until ( not PlayerAlive ) or (Pl_nenemy_temp < 1) or (RunScene <> scGame);

   If RunScene = scGameMenu then sc_GameMenu
   else If RunScene <> scQuitApp then
   If PlayerAlive then Game_NextLevel else Game_DiePlayer;

 Until RunScene <> scGame;

  log.LogStatus( 'Finalizing Show_Game', 'sc_Game' );
  SDL_FreeSurface( Img_Terrenos );
  SDL_FreeSurface( Img_Tanks );
  SDL_FreeSurface( Img_menu );
  SDL_FreeSurface( Img_fundo_camu_menu );
  SDL_FreeSurface( Img_FIcons);

  SDL_FillRect( Screen, nil, 0 );
  SpriteEngine.Clear;
  log.LogStatus( 'Finalization successful', 'sc_Game' );
end;

procedure Finalize;
begin
   log.LogStatus( 'Starting finalization', 'Finalize' );
   SDL_FreeSurface( Background );
   SDL_FreeSurface( Img_Font_ );
   SpriteEngine.Free;
   SDL_Quit;
   Halt(0);
   log.LogStatus( 'Finalization DONE', 'Finalize' );
end;

procedure sc_Title;
var
        Img_Smace : PSDL_Surface;
        i : integer;

{procedure Draw_FSmallD(Surface_: PSDL_Surface; x: Integer; y: Integer; text: pchar);
const
  Letters='ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
var
  i, i2 : integer;
  str_temp : string;
  Img_FSmallD : PSDL_Surface;
begin
  img_LoadImage( Img_FSmallD,'images/fonts/FSmallD.png',false,0,0,0,true);
  str_temp := text;
  UpperCase(str_temp);
  for i := 0 to Length(str_temp)-1 do
  begin
      i2 := pos( str_temp[i + 1], Letters ) - 1;
      SDL_UpperBlit(Img_FSmallD, PSDLRect((i2*11),0,11,14), Surface_, PSDLRect(x+(i*11),y,0,0));
  end;
  SDL_FreeSurface(Img_FSmallD);
end;  }

begin
  log.LogStatus( 'Initializing Screen_Title', 'sc_Title' );

  img_LoadImage( Img_Smace, 'images/Smace.png', true, 0, 0, 0, true);

  i := 0;
  snd_StartMusic;

  //SpriteEngine.Draw;

  img_DrawBg;
  SFont_Write(screen, 270, 270, 'presents');
  SDL_UpperBlit(Img_Smace, nil,screen, PSDLRect( (320-(Img_Smace.w div 2)), (240-(Img_Smace.h div 2)-50), Img_Smace.w, Img_Smace.h));

  SDL_UpdateRect(Screen, 0, 0, 0, 0);
 repeat

  inc(i);
  if i > 300 then RunScene := scMenu;

   // SpriteEngine.Move;
    while SDL_PollEvent(@Event) > 0 do
    begin

      case Event.key.type_ of
       SDL_QUITEV :
       begin
         RunScene := scQuitApp;
       end;

       SDL_KeyDown :
       begin
         if Event.key.keysym.sym = SDLK_ESCAPE then RunScene := scQuitApp
         else RunScene := scMenu;
       end;

       SDL_MOUSEBUTTONDOWN:
        begin
          RunScene := scMenu;
        end;
      end;
   end;
   keys := PKeyStateArr(SDL_GetKeyState(nil));
  //SpriteEngine.Draw;
  //SDL_UpdateRect(Screen, 0, 0, 0, 0);
  SDL_Delay(TimeLeft);
 Until (RunScene <> scTitle);

  log.LogStatus( 'Finalizing Title', 'sc_Title' );
  SDL_FreeSurface( Img_Smace );
 // SpriteEngine.Clear;
  SDL_FillRect( Screen, nil, 0 );
  SDL_UpdateRect( Screen, 0, 0, 0, 0 );
  log.LogStatus( 'Finalization successful', 'sc_Title' );
end;

procedure sc_Credits;
{const
  Scroll : array[0..9] of PCHAR = (
    'Smace Design� presents...',
    'SWAR�',
    'Press any key to continue...',
    'A game from Sumidouro-RJ Brazil!',
    'Project leader: Sergio Marcelo',
    'Any bug, or sugestion?! Mail-us!',
    'Our email: smace@ieg.com.br',
    'Web site: www.smace.hpg.com.br',
    ' ',
    ' '
  );
}
begin
  log.LogStatus( 'Initializing scCredits', 'scCredits' );

  img_DrawBg;
  SFont_Write(screen, 20, 10, 'Smace Design presents:');
  SFont_Write(screen, 100, 50, 'SMACE''s WAR');
  SFont_Write(screen, 20, 100, 'This game was created by Smace!');
  SFont_Write(screen, 20, 190, 'Main programmer: Sergio Marcelo');
  SFont_Write(screen, 20, 230, 'Design: Sergio Marcelo');
  SFont_Write(screen, 20, 260, 'Music/Sounds: Unknown ');
  SFont_Write(screen, 20, 290, 'Huge help: Ariel Jacob, KiCHY,');
  SFont_Write(screen, 50, 320, 'Dominique Louis.');
  SFont_Write(screen, 20, 350, 'Thanks: JEDI-SDL Group, Pimpim,');
  SFont_Write(screen, 50, 380, 'my mom for have done dinner.');
  SFont_Write(screen, 20, 440, 'Click for go back to MENU');

{
  SFont_Write(screen,  65,  30, 'This game was written by...');
  SFont_Write(screen,  40, 340, 'Smace Design by Sergio Marcelo');
  SFont_Write(screen, 120, 370, 'Sumidouro-RJ Brazil!');
  SFont_Write(screen, 100, 410, 'E-mail: smace@ieg.com.br');
}

 {  i := 0;
   inc(line);
   if line > 9 then line := 0; }
  {  SDL_FillRect( Screen, nil, 0 );
  img_DrawBg;
  SFont_Write(screen, 50, 320, Scroll[Line]);
  SDL_UpperBlit(Img_Smace, nil,screen, PSDLRect( (320-(Img_Smace.w div 2)), (240-(Img_Smace.h div 2)), Img_Smace.w, Img_Smace.h));
}

  SpriteEngine.Draw;
  //Draw_FSmallD(Screen,50,50,'Maximus');
 repeat
    SpriteEngine.Move;
    while SDL_PollEvent(@Event) > 0 do
    begin

      case Event.key.type_ of
       SDL_QUITEV :
       begin
         RunScene := scQuitApp;
       end;

       SDL_KeyDown :
       begin
         if Event.key.keysym.sym = SDLK_ESCAPE then RunScene := scQuitApp
         else RunScene := scMenu;
       end;

       SDL_MOUSEBUTTONDOWN:
        begin
          RunScene := scMenu;
          snd_PlaySound(5,80);
        end;
      end;
   end;
   keys := PKeyStateArr(SDL_GetKeyState(nil));
  SpriteEngine.Draw;
  SDL_UpdateRect(Screen, 0, 0, 0, 0);
  SDL_Delay(TimeLeft);
 Until (RunScene <> scCredits);

  log.LogStatus( 'Finalizing Credits', 'Sc_Credits' );
//  SpriteEngine.Clear;
  SDL_FillRect( Screen, nil, 0 );
  SDL_UpdateRect( Screen, 0, 0, 0, 0 );
  log.LogStatus( 'Finalization successful', 'Sc_Credits' );
end;



procedure sc_Menu;

procedure RS_Blit(bitmap: PSDL_SURFACE; rects_bitmap: TSDL_Rect; x, y: LongInt);
var
  dest: SDL_RECT;
begin
  dest.x := x;
  dest.y := y;
  dest.w := bitmap.w;
  dest.h := bitmap.h;
  SDL_UpperBlit(bitmap, @rects_bitmap, screen, @dest);
end; //RS_Blit

procedure get_bg(var surface: PSDL_SURFACE; x, y: LongInt);
var
  src, dst: SDL_RECT;
begin
  src.x := x;
  src.y := y;
  src.w := surface.w;
  src.h := surface.h;

  dst.x := 0;
  dst.y := 0;
  dst.w := surface.w;
  dst.h := surface.h;
  SDL_UpperBlit(screen, @src, surface, @dst);
end; // get_bg

procedure Draw_FSmallD(Surface_: PSDL_Surface; x: Integer; y: Integer; text: string; invert : boolean);
const
  Letters='ABCDEFGHIJLMNOPQRSTUVXZKWYX0123456789';   //total: 37
var
  i, i2 : integer;
  str_temp : string;
  Img_FSmallD : PSDL_Surface;
begin
  img_LoadImage( Img_FSmallD,'images/fonts/FSmallD.png',false,0,0,0,true);

  if invert then
  for i := 0 to Length(text) do str_temp := copy(text,(i+1),1) + str_temp
  else   str_temp := text;

  str_temp := UpperCase(str_temp);

  for i := 0 to Length(str_temp)-1 do
  begin
      i2 := pos( str_temp[i + 1], Letters ) - 1; //11x13
      if invert then
      SDL_UpperBlit(Img_FSmallD, PSDLRect((i2*10)-1,0,10,14), Surface_, PSDLRect(x-(i*11),y,0,0))
      else SDL_UpperBlit(Img_FSmallD, PSDLRect((i2*10)-1,0,10,14), Surface_, PSDLRect(x+(i*11),y,0,0));
  end;
  SDL_FreeSurface(Img_FSmallD);
end; // Draw_FSmallD

var
     Img_MainMenu, Img_SmaceDesign, Img_Logo_ : PSDL_Surface; //, Button_
     i, i2, bt_last, bt_current, cursor_time, cursor_current : Integer;
     MouseX_, MouseY_ : LongInt;
     Rects_cursor       : array[0..4] of TSDL_Rect;
     Rects_MainMenuImg  : array[0..1,0..5] of TSDL_Rect;
     Pos_MainMenu     : array[0..4] of TSDL_Rect;
     Img_Cursor, Img_Cursor2, Img_temp_screen : PSDL_Surface;       //temp_camu

begin

  log.LogStatus( 'Initializing Sc_Menu', 'Sc_Menu' );

  Rects_MainMenuImg[0,5].x := 0;
  Rects_MainMenuImg[0,5].y := 0;
  Rects_MainMenuImg[0,5].w := 181;  //(362/2)
  Rects_MainMenuImg[0,5].h := 176;  //(176/5)  35

  For i2 := 0 to 1 do
    For i := 0 to 4 do
    begin
      Rects_MainMenuImg[i2,i].x := i2*181;
      Rects_MainMenuImg[i2,i].y := i*35;
      Rects_MainMenuImg[i2,i].w := 181;
      Rects_MainMenuImg[i2,i].h := 35;
    end;

  For i := 0 to 4 do
  begin
    Pos_MainMenu[i].x := 30;
    Pos_MainMenu[i].y := 170+i*50;
    Pos_MainMenu[i].h := 35;
    Pos_MainMenu[i].w := 181;
  end;

  For i := 0 to 4 do
  begin
     Rects_cursor[i].x := 15*i;  Rects_cursor[i].y := 0;
     Rects_cursor[i].w := 15;    Rects_cursor[i].h := 15;
  end;

  img_LoadImage( Img_Cursor, 'images/cursor.png', true, 255, 0, 255, true); //cursor: png
  img_LoadImage( Img_Logo_, 'images/logo.png', true, 0, 0, 0, true);
  img_LoadImage( Img_Cursor2, 'images/cursor.png', false, 0, 0, 0, true);
  img_LoadImage( Img_HighScore, 'images/highscore.png', true, 255, 0, 255, true);
  img_LoadImage( Img_SmaceDesign, 'images/smacedesign.png', true, 0, 0, 0, true);
  img_LoadImage( Img_MainMenu, 'images/mainmenu.png', true, 0, 0, 0, true);

  img_DrawBg;

  SDL_GetMouseState(MouseX, MouseY);
  If ((MouseX-8) < 0) then MouseX := 8;
  If ((MouseY-8) < 0) then MouseY := 8;
  get_bg(Img_Cursor2, MouseX-8, MouseY-8);
  SDL_SHOWCURSOR(0);
  Mousedown := False;

  SDL_UpperBlit(Img_SmaceDesign, nil, screen, PSDLRect(380, 440, Img_SmaceDesign.w, Img_SmaceDesign.h));
  SDL_UpperBlit(Img_Logo_, nil, screen, PSDLRect(30, 30, Img_Logo_.w, Img_Logo_.h));
  SDL_UpperBlit(Img_HighScore, nil, screen, PSDLRect(390, 300, Img_HighScore.w, Img_HighScore.h));

  Game_LoadHighScore;
  log.LogWarning(HighScore_Name[0],HighScore_Level[0]);

  For i := 0 to 2 do
  Begin
    Draw_FSmallD( Screen, 405, 331+i*19, copy( HighScore_Name[i],  0, 9), false );
    Draw_FSmallD( Screen, 522, 331+i*19, copy( HighScore_Level[i], 0, 2), true );
    Draw_FSmallD( Screen, 606, 331+i*19, copy( HighScore_Score[i], 0, 7), true );
  End;

  For i := 0 to 4 do
    SDL_UpperBlit(Img_MainMenu, @Rects_MainMenuImg[0,i], screen, @Pos_MainMenu[i]);

 // SpriteEngine.Clear;
  SDL_UpdateRect(screen, 0, 0, 0, 0);
  Img_temp_screen := screen;
  If Music = nil then snd_StartMusic;
  bt_current := 0;
  cursor_current := 0;
  cursor_time := 0;

  repeat
   MouseX_ := MouseX;
   MouseY_ := MouseY;
   Mousedown := False;
   while SDL_PollEvent(@Event) > 0 do
   begin
     case Event.key.type_ of
       SDL_QUITEV :   RunScene := scQuitApp;

       SDL_KeyDown :
         if Event.key.keysym.sym = SDLK_ESCAPE then
           RunScene := scQuitApp;

       SDL_MOUSEBUTTONDOWN:  Mousedown := True;
      // SDL_MOUSEBUTTONUP:    Mousedown := False;
     end;
   end;
   keys := PKeyStateArr(SDL_GetKeyState(nil));

   SDL_GetMouseState(MouseX, MouseY);
   If ((MouseX-8) < 0) then MouseX := 8;
   If ((MouseY-8) < 0) then MouseY := 8;

   bt_last := bt_current;
   bt_current := 0;

   For i := 0 to 4 do
     if (MouseX > Pos_MainMenu[i].x) and (Mousex < (Pos_MainMenu[i].x + Pos_MainMenu[i].w)) and
      (MouseY > Pos_MainMenu[i].y) and (Mousey < (Pos_MainMenu[i].y + Pos_MainMenu[i].h)) then
     begin
       inc(cursor_time);
       if cursor_time > 10 then
       begin
         inc(cursor_current);
         cursor_time :=0;
         if cursor_current > 4 then cursor_current := 1;
       end;
       bt_current := 1+i;
     end;

   If bt_last <> bt_current then
      If bt_current > 0 then
       begin
        SDL_UpperBlit(Img_MainMenu, @Rects_MainMenuImg[1,(bt_current-1)], screen, @Pos_MainMenu[bt_current-1] );
        snd_PlaySound(4,40);
       end;

   If bt_last <> bt_current then
     If bt_last > 0 then
       begin
         SDL_UpperBlit(Img_MainMenu, @Rects_MainMenuImg[0,(bt_last-1)], screen, @Pos_MainMenu[bt_last-1] );
         cursor_current := 0;
       end;

   if MouseDown then
   begin
     For i := 0 to 4 do
     if (MouseX > Pos_MainMenu[i].x) and (Mousex < (Pos_MainMenu[i].x + Pos_MainMenu[i].w)) and (MouseY > Pos_MainMenu[i].y) and (Mousey < (Pos_MainMenu[i].y + Pos_MainMenu[i].h)) then
     begin
       Case i of  // yack ! ..it started nicely.. !@#$ - Ariel :)
           0  :
           begin
            RunScene := scGame;
           end;
           1  :
           begin
            snd_PlaySound(5,80);
            sc_HighScore(1,1000);
           end;
           2  :
           begin
            snd_PlaySound(5,80);
            RunScene := scOptions;
           end;
           3  :
           begin
            snd_PlaySound(5,80);
            RunScene := scCredits;
           end;
           4  :
           begin
            snd_PlaySound(5,80);
            RunScene := scQuitApp;
            SDL_Delay(100);
           end;
       end;
     end;
   end;

  // SpriteEngine.Move;
  // SpriteEngine.Draw;

  // If (MouseX <> MouseX_) or (MouseY <> MouseY_) then
  // begin

     SDL_UpperBlit(Img_temp_screen, PSDLRect(Mousex_-8, Mousey_-8, 15, 15), screen, PSDLRect(Mousex_-8,  Mousey_-8, 15, 15));
     SDL_UpdateRect(screen, Mousex_-8,  Mousey_-8, 15, 15);
     get_bg(Img_Cursor2, Mousex-8, Mousey-8);
     RS_Blit(Img_Cursor, Rects_cursor[Cursor_Current], Mousex-8, Mousey-8);
     SDL_UpdateRect(screen, Mousex-8, Mousey-8, 15, 15);

     If bt_current > 0 then
      SDL_UpdateRect(screen, Pos_MainMenu[bt_current-1].x, Pos_MainMenu[bt_current-1].y,Pos_MainMenu[bt_current-1].w, Pos_MainMenu[bt_current-1].h );
     If bt_last <> bt_current then
       If bt_last > 0 then
         SDL_UpdateRect(screen, Pos_MainMenu[bt_last-1].x, Pos_MainMenu[bt_last-1].y,Pos_MainMenu[bt_last-1].w, Pos_MainMenu[bt_last-1].h );

     RS_Blit(Img_Cursor2, Rects_cursor[0], Mousex-8, Mousey-8);
  // end;

 SDL_Delay(TimeLeft);

 Until RunScene <> scMenu;

  log.LogStatus( 'Finalizing Sc_Menu', 'Sc_Menu' );
  SDL_SHOWCURSOR(1);
  SDL_FreeSurface( Img_Logo_ );
  SDL_FreeSurface( Img_Cursor );
  SDL_FreeSurface( Img_Cursor2 );
  SDL_FreeSurface( Img_temp_screen );
  SDL_FillRect( Screen, nil, 0 );
  SDL_UpdateRect( Screen, 0, 0, 0, 0 );
  log.LogStatus( 'Finalization Sc_Menu', 'Sc_Menu' );
end;




procedure sc_GameMenu;
var
        Buffer : PSDL_Surface;
begin
//  log.LogStatus( 'BG_WIDTH=' + inttostr(BG_WIDTH) + ' BG_HEIGHT='+ inttostr(BG_HEIGHT), '=P' );
  //BG_WIDTH  := SCREEN_WIDTH;  BG_HEIGHT := SCREEN_HEIGHT-64;

  log.LogStatus( 'Initializing Show_GameMenu', 'Show_GameMenu' );
  Quit := false;   Mousedown := False;
    SDL_SHOWCURSOR(1);

  // Create a buffer with the current display format, so we don't need to use SDL_DisplayFormat()
  Buffer := SDL_CreateRGBSurface( SDL_SWSURFACE, Screen_Width, Screen_Height,
    Screen.format.BitsPerPixel, Screen.format.RMask, Screen.format.GMask,
    Screen.format.BMask, Screen.format.AMask );
  // Build the screen
  SDL_SetAlpha( Screen, SDL_SRCALPHA, 128 );
  SDL_UpperBlit( Screen, nil, Buffer, nil );
  SDL_SetAlpha( Screen, 0, 0 );
  SDL_UpperBlit( Buffer, nil, Screen, nil );

//  SDL_SetAlpha( Screen, SDL_SRCALPHA, 64 );
//  SDL_UpdateRect(screen, 0, 0, 0, 0);

  SFont_Write(screen, 20, 20,  'Smace''s WAR - Paused');
  SFont_Write(screen, 20, 70,  '- Back to the game');
  SFont_Write(screen, 20, 100, '- I give up!');
  SFont_Write(screen, 20, 130, '- Exit to Windows');
  SFont_Write(screen, 20, 180, '');
  SDL_UpdateRect(Screen, 0, 0, BG_WIDTH, SCREEN_HEIGHT-64);
 repeat
    while SDL_PollEvent(@Event) > 0 do
    begin

      case Event.key.type_ of
       SDL_QUITEV :
       begin
         RunScene := scQuitApp;
       end;

     {  SDL_KeyDown :
       begin
         if Event.key.keysym.sym = SDLK_ESCAPE then RunScene := scQuitApp
         else RunScene := scGame;
       end; }

        SDL_MOUSEBUTTONDOWN:
        begin
          Mousedown := true;
        end;

      end;
   end;
   keys := PKeyStateArr(SDL_GetKeyState(nil));

    SDL_GetMouseState(MouseX, MouseY);
    if MouseDown then
    begin

    //   'Back to the game'
      if (Mousex > 20) and (Mousex < 250)
      and (Mousey > 70) and (Mousey < 100) then
        RunScene := scGame;

    //   'I give up!'
      if (Mousex > 20) and (Mousex < 250)
      and (Mousey > 100) and (Mousey < 130) then
        RunScene := scMenu;

    //   'Exit to Windows'
      if (Mousex > 20) and (Mousex < 250)
      and (Mousey > 130) and (Mousey < 160) then
        RunScene := scQuitApp;

    //20, 180, 'Exit'
    //  if (x > 20) and (x < 250)
    //  and (y > 180) and (y < 210) then
    //    RunScene := scQuitApp;

      //SDL_UpdateRect(screen_, x, y, 1, 1);
      Mousedown := False;
    end;


  SDL_Delay(TimeLeft);
 Until RunScene <> scGameMenu;

  log.LogStatus( 'Finalizing Show_GameMenu', 'Show_GameMenu' );
  SDL_FillRect( Screen, nil, 0 );
  //SDL_UpdateRect( Screen, 0, 0, 0, 0 );
  If  RunScene = scGame then Game_DrawMenu(drAll);
  log.LogStatus( 'Finalization Show_GameMenu', 'Show_GameMenu' );
end;


procedure sc_Options;
var
 // Logo_ : PSDL_Surface;
 // Fundo
     opt_changed : boolean;


procedure Change_Screen;
begin
  SpriteEngine.Clear;

  Case Res_Screen of
    1 :
    begin
      Screen_Width  := 640;
      Screen_Height := 480;
      BG_WIDTH := 640;
      BG_HEIGHT := 416;
    end;
    2 :
    begin
      Screen_Width  := 800;
      Screen_Height := 600;
      BG_WIDTH := 800;
      BG_HEIGHT := 536;
    end;
    3 :
    begin
      Screen_Width  := 1024;
      Screen_Height := 768;
      BG_WIDTH := 1024;
      BG_HEIGHT := 704;
    end;
  end;

  log.LogStatus( 'Flipping screen mode', 'Options - ToggleFullScreen' );
  SDL_FreeSurface( Screen );
  if SoundFlag <> 0 then
  begin
    log.LogStatus( 'Freeing music', 'Options' );
    snd_StopMusic; //PauseMusic;
    log.LogStatus( 'Finalization free music  - OK', 'Options - ToggleFullScreen' );
    Mix_CloseAudio;
    log.LogStatus( 'Finalization CloseAudio  - OK', 'Options - ToggleFullScreen' );
  end;
  log.LogStatus( 'Quiting SDL', 'Options' );
  SDL_Quit;

  SoundFlag:= SDL_INIT_AUDIO;
  if SDL_Init( SDL_INIT_VIDEO or SoundFlag ) = -1 then
  begin
    log.LogError( 'SDL init failed', 'ToggleFullScreen' );
    log.LogError( string(SDL_GetError), 'ToggleFullScreen' );
    Finalize;
    Halt( 1 );
  end;

  if SDL_Init( SDL_INIT_VIDEO or SoundFlag ) = -1 then
  begin
   log.LogError( 'SDL init failed', 'ToggleFullScreen' );
   log.LogError( string(SDL_GetError), 'ToggleFullScreen' );
   Finalize;
   Halt( 1 );
  end;

        if SoundFlag <> 0 then
        begin
          if Mix_OpenAudio( 44100, MIX_DEFAULT_FORMAT, 2, 4096 ) < 0 then
          begin
            log.LogWarning( 'SDL_MIXER: Initialization failed', 'Options - ToggleFullScreen' );
            log.LogError( string(SDL_GetError), 'ToggleFullScreen' );
            // If Audio initialization failed then turn off audio
            NoSound := True;
          end else
          begin
            log.LogStatus( 'SDL_MIXER Initialization was successful', 'Options - ToggleFullScreen' );
            Mix_VolumeMusic( MusicVolume );
            Mix_HookMusicFinished( @snd_RestartMusic );
            log.LogStatus( 'SDL_MIXER: hooking RestartMusic was successful', 'Options - ToggleFullScreen' );
          end;
        end;


  If Fullscreen = True then
    Screen := SDL_SetVideoMode( Screen_Width, Screen_Height, bpp, SDL_SWSURFACE or SDL_HWPALETTE or SDL_FULLSCREEN)
  else Screen := SDL_SetVideoMode( Screen_Width, Screen_Height, bpp, SDL_SWSURFACE or SDL_HWPALETTE);

  if Screen = nil then
  begin
  // It was UNsuccessful
    log.LogError( 'Setting video mode 640x480 failed', 'Options - ToggleFullScreen' );
    log.LogError( string(SDL_GetError), 'Options - ToggleFullScreen' );
    Finalize;
    Halt( 1 );
  end
  else
  begin
    log.LogStatus( 'Setting video mode 640x480 was successful', 'Options - ToggleFullScreen' );
  end;

  SDL_WM_SetCaption( App_title, nil );
  snd_StartMusic;


  // Try to create the background surface
  Background := SDL_CreateRGBSurface( SDL_SWSURFACE or SDL_HWPALETTE, SCREEN_WIDTH,SCREEN_HEIGHT, bpp, 0, 0, 0, 0 );
  if Background = nil then
  begin
{$IFDEF LOG}
    log.LogError( Format( 'Creating background surface failed : %s', [SDL_GetError] ), 'Initialize' );
{$ENDIF}
    Finalize;
    Halt( 1 );
  end
  else
  begin
    Background := SDL_DisplayFormat( Background );
  end;

  SpriteEngine := TSpriteEngine.Create(Screen);
  SpriteEngine.BackgroundSurface := BackGround;



end; //Change_Screen

//var Pos_OptionsMenu     : array[0..4] of TSDL_Rect;

begin
  log.LogStatus( 'Initializing Show_Options', 'Show_Options' );

 { For i := 0 to 4 do
  begin
    Pos_MainMenu[i].x := 30;
    Pos_MainMenu[i].y := 170+i*50;
    Pos_MainMenu[i].h := 35;
    Pos_MainMenu[i].w := 181;
  end;}


  Mousedown := False;
  Opt_changed := False;

  img_DrawBg;

  SFont_Write(screen, 40, 20,  'Smace''s WAR - Options');
  if fullscreen then SFont_Write(screen, 20, 70, 'Full Screen - On')
  else SFont_Write(screen, 20, 70, 'Full Screen - Off');
  SFont_Write(screen, 20, 100,  'Resolution:');

  SFont_Write(screen, 40, 130, '  640x480');
  SFont_Write(screen, 40, 160, '  600x800');
  SFont_Write(screen, 40, 190, '  1024x768');
{
  Case Res_Screen of
    1 : SFont_Write(screen, 40, 130, '*');
    2 : SFont_Write(screen, 40, 160, '*');
    3 : SFont_Write(screen, 40, 190, '*');
  end;
}
  SFont_Write(screen, 150, 240, 'Apply!');
  SFont_Write(screen, 20, 240, 'Menu');

  SFont_Write(screen, 20, 270, 'Dev_EnableMap');

  SDL_UpdateRect(screen, 0, 0, 0, 0);

 repeat
    while SDL_PollEvent(@Event) > 0 do
    begin

      case Event.key.type_ of
       SDL_QUITEV :
       begin
         RunScene := scQuitApp;
       end;

     {  SDL_KeyDown :
       begin
         if Event.key.keysym.sym = SDLK_ESCAPE then RunScene := scQuitApp
         else RunScene := scGame;
       end; }

       SDL_MOUSEBUTTONDOWN:
        begin
          Mousedown := true;
        end;

      end;
   end;
   keys := PKeyStateArr(SDL_GetKeyState(nil));

    SDL_GetMouseState(MouseX, MouseY);
    if MouseDown then
    begin
      snd_PlaySound(5,80);
  {   For i := 0 to 4 do
     if (MouseX > Pos_MainMenu[i].x) and (Mousex < (Pos_MainMenu[i].x + Pos_MainMenu[i].w)) and (MouseY > Pos_MainMenu[i].y) and (Mousey < (Pos_MainMenu[i].y + Pos_MainMenu[i].h)) then
     begin
       Case i of
           0  : RunScene := scGame;
           1  : sc_HighScore(1,1000);
           2  : RunScene := scOptions;
           3  : RunScene := scCredits;
           4  : RunScene := scQuitApp;
       end;
     end; }

    // 40, 100, 'Full Screen'
      if (Mousex > 40) and (Mousex < 300) and (Mousey > 70) and (Mousey < 100) then
        Begin
        if fullscreen then
        begin
          fullscreen := false;
          opt_changed := true;
        end
        else
        begin
          fullscreen := true;
          opt_changed := true;
        end;
        end;

    // 80, 240, 'Aplicar Altera��es'
      if (Mousex > 150) and (Mousex < 500)
       and (Mousey > 240) and (Mousey < 270) then
        begin
           Change_Screen;
           Opt_changed := true;
       //  SDL_Quit;
       //  Initialize;
       //  RunScene := scTitle;
        end;

      if (Mousex > 40) and (Mousex < 300)
       and (Mousey > 130) and (Mousey < 160) then
        begin
          Res_Screen := 1;
          opt_changed := true;
        end;

       if (Mousex > 40) and (Mousex < 300)
       and (Mousey > 160) and (Mousey < 190) then
        begin
          Res_Screen := 2;
          opt_changed := true;
        end;

        if (Mousex > 40) and (Mousex < 300)
       and (Mousey > 190) and (Mousey < 210) then
        begin
          Res_Screen := 3;
          opt_changed := true;
        end;

    //20, 240, 'Menu'
      if (Mousex > 20) and (Mousex < 120)
       and (Mousey > 240) and (Mousey < 270) then
        RunScene := scMenu;


      //SDL_UpdateRect(screen_, x, y, 1, 1);
      Mousedown := False;
    end;

  SDL_Delay(TimeLeft);
  Until (RunScene <> scOptions) or (Opt_changed = true);

  log.LogStatus( 'Finalizing Options', 'Show_Options' );
 // SDL_FreeSurface( Logo_ );
  SDL_FillRect( Screen, nil, 0 );
  SDL_UpdateRect( Screen, 0, 0, 0, 0 );
  log.LogStatus( 'Finalization successful', 'Show_Options' );
end;

procedure Game_Cheat_On;
begin
  //  TScene = ( scIntro, scTitle, scGame, scOptions, scDone, scQuitApp, scHighScore, scMenu, scGameMenu, scCredits );
  // Level = // Invencibilidade, Velocidade, Poder, Municao

  if Copy(ParamStr(1),0,6) = 'level=' then
  begin
    try
      game_cheat_level := StrToInt( Copy(ParamStr(1), 7, length(ParamStr(1))-6) );
    except
      game_cheat_level := 1;
    end;
  end;

end;



///////////////////////////////////////////////////////////////////////////////
/////////////////// HERE THE GAME STARTS! :) //////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
begin
  Initialize;

  {$IFDEF GAME_CHEAT_ON}
  Game_Cheat_On;
  {$ENDIF}

  RunScene := scTitle;
  repeat
    case RunScene of
      scTitle       : sc_Title;
      scMenu        : sc_Menu;
      scGame        : sc_Game;
//      scHighScore   : sc_HighScore;
      scOptions     : sc_Options;
      scCredits     : sc_Credits;
    end;
  until RunScene = scQuitApp;

  Finalize;

end.

