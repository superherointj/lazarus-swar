unit smpegplayer;

interface

uses
  sdl,
  smpeg;

type
  TSMPEGPlayer = class( TObject )
  private
    { Private declarations }
    FSurface : PSDL_Surface;
    FSMPEGHandle : PSMPEG;
    FMPEGFileName : string;
    FMPEGInfo : TSMPEG_Info;
    FPlaying : Boolean;
    FSound : Integer;
    FVolume : Integer;
    FLoop : Boolean;
    function GetSound : Boolean;
    procedure SetSound( const Value : Boolean );
    function GetMPEGFileName: string;
    procedure SetMPEGFileName(const Value: string);
    function GetVolume: Integer;
    procedure SetVolume(const Value: Integer);
    function GetLoop: Boolean;
    procedure SetLoop(const Value: Boolean);
    procedure ResetSMPEGHandle;
  protected
    { Protected declarations }
  public
    { Public declarations }
    constructor Create( aSurface : PSDL_Surface; aFileName : string = '' );
    destructor Destroy; override;
    procedure Play;
    procedure Stop;
    procedure Pause;
    function Status : TSMpegStatus;
    property Handle : PSMPEG read FSMPEGHandle;
    property Loop : Boolean read GetLoop write SetLoop;
    property MPEGFileName : string read GetMPEGFileName write SetMPEGFileName;
    property Sound : Boolean read GetSound write SetSound;
    property Volume : Integer read GetVolume write SetVolume;
  end;

implementation

uses
  SysUtils,
  sdl_mixer;

{ TSMPEGPlayer }
constructor TSMPEGPlayer.Create( aSurface: PSDL_Surface; aFileName : string = '' );
begin
  inherited Create;
  if aSurface = nil then
    raise Exception.Create( 'SDL_SetVideoMode failed' )
  else
  begin
    FSound := 1; // Turn Sound On my default
    FLoop := false; // Turn Looping Off my default
    FSurface := aSurface;
    if aFileName <> '' then
      MPEGFileName := aFileName;
  end;

  if SDL_getenv( 'SDL_VIDEO_YUV_HWACCEL' ) = '' then
    SDL_putenv( 'SDL_VIDEO_YUV_HWACCEL=0' );
end;

destructor TSMPEGPlayer.Destroy;
begin
  ResetSMPEGHandle;
  inherited;
end;

function TSMPEGPlayer.GetLoop: Boolean;
begin
  result := FLoop;
end;

function TSMPEGPlayer.GetMPEGFileName: string;
begin
  result := FMPEGFileName;
end;

function TSMPEGPlayer.GetSound: Boolean;
begin
  Result := Boolean( FSound );
end;

function TSMPEGPlayer.GetVolume: Integer;
begin
  result := FVolume;
end;

procedure TSMPEGPlayer.Pause;
begin
  if ( FPlaying )
  and ( FSMPEGHandle <> nil ) then
    SMPEG_pause( FSMPEGHandle );
end;

procedure TSMPEGPlayer.Play;
begin
  if FPlaying then
    Exit;
  FPlaying := True;

  if FSMPEGHandle <> nil then
    SMPEG_play( FSMPEGHandle );
end;

procedure TSMPEGPlayer.ResetSMPEGHandle;
begin
  if FSMPEGHandle <> nil then
  begin
    Stop;
    SMPEG_delete( FSMPEGHandle );
  end;
end;

procedure TSMPEGPlayer.SetLoop(const Value: Boolean);
begin
  if FLoop <> Value then
  begin
    FLoop := Value;
    if FSMPEGHandle <> nil then
      SMPEG_loop( FSMPEGHandle, Integer( FLoop ) );
  end;
end;

procedure TSMPEGPlayer.SetMPEGFileName(const Value: string);
var
  // Audio Specs
  aspec : TSDL_AudioSpec;
  format : Uint16;
  freq, chan : integer;
begin
  if FMPEGFileName <> Value then
  begin
    FMPEGFileName := Value;

    ResetSMPEGHandle;
    FSMPEGHandle := SMPEG_new( PChar( FMPEGFileName ), @FMPEGInfo, FSound );

    if FSMPEGHandle = nil then
      raise Exception.Create( 'Cannot create MPEG stream' );

    // Disable Audio
    SMPEG_enableaudio( FSMPEGHandle, 0 );

    // Query Mixer
    Mix_QuerySpec( freq, format, chan );
    aspec.freq := freq;
    aspec.format := format;
    aspec.channels := chan;

    // Tell Smpeg what we want
    Smpeg_actualSpec( FSMPEGHandle, @aspec );

    // Hook the mixer audio playing function
    Mix_HookMusic( @SMPeg_PlayAudioSDL, FSMPEGHandle );

    // Set Audio as per FSound variable
    SMPEG_enableaudio( FSMPEGHandle, FSound );

    //SMPEG_scaleXY( FSMPEGHandle, FSurface.w, FSurface.h );

    SMPEG_setdisplay( FSMPEGHandle, FSurface, nil, nil );

    // Reenable Video
    SMPEG_enablevideo( FSMPEGHandle, 1 );
  end;
end;

procedure TSMPEGPlayer.SetSound(const Value: Boolean);
begin
  FSound := Integer( Value );
  if FSMPEGHandle <> nil then
    SMPEG_enableaudio( FSMPEGHandle, FSound );
end;

procedure TSMPEGPlayer.SetVolume(const Value: Integer);
begin
  if FVolume <> Value then
  begin
    FVolume := Value;
    if FVolume < 0 then
      FVolume := 0
    else if FVolume > 100 then
      FVolume := 100;

    if FSMPEGHandle <> nil then
      SMPEG_setvolume( FSMPEGHandle, FVolume );
  end;
end;

function TSMPEGPlayer.Status: TSMpegStatus;
begin
  if FSMPEGHandle <> nil then
    result := SMPEG_status( FSMPEGHandle )
  else
    result := STATUS_SMPEG_ERROR;
end;

procedure TSMPEGPlayer.Stop;
begin
  if not FPlaying then
    Exit;
  FPlaying := False;
  if FSMPEGHandle <> nil then
    SMPEG_stop( FSMPEGHandle );
end;

end.
