unit SDLGUI;

interface

uses sdl, sdlutils, SDLDraw, SDLFont, Classes;

type
  TSDLBarKind = (sbHorizontal, sbVertical);

  TSDLCursor = record
    Image : TSDLImage;
    HotSpotX : Byte;
    HotSpotY : Byte;
  end;
  TStateImage = (siFocused,siDown,siDisabled,siNone);
  TStateImages = set of TStateImage;
  TDrawOption = (doNormal,doCenter,doTile,doStretch);
  TSDLDragMode = (sdmManual,sdmAutomatic);

  // Forward declarations
  TSDLGui = class;
  TSDLImageObject = class;
  TSDLComponent = class;
  TSDLControl = class;

  // Events

  TSDLNotifyEvent = procedure(Sender: TSDLComponent) of object;
  TSDLMouseEvent = procedure(Sender: TSDLComponent;Button: Integer;
                            Modifier: TSDLMod;AX,AY: Word) of object;
  TSDLMouseClickEvent = procedure(Sender: TSDLComponent;AX,AY: Integer) of object;
  TSDLMouseMoveEvent = procedure(Sender: TSDLComponent;Modifier: TSDLMod;
                                AX,AY : Integer) of object;
  TSDLMouseWheelEvent = procedure(Sender: TSDLComponent;Dir: Integer;
                       Modifier: TSDLMod;AX,AY : Word) of object;
  TSDLKeyEvent = procedure(Sender: TSDLControl; var Key: Word;
                          Modifier: TSDLMod) of object;
  TSDLKeyPressEvent = procedure (Sender: TSDLControl; var Key: Char) of object;
  TSDLDrawItemEvent = procedure(Sender: TSDLControl;const ItemNo: Integer;
                      Rect: TSDL_Rect; State: Integer) of object;

  TSDLObject = class(TPersistent)
  private
    FGUI : TSDLGui;
    FParent: TSDLObject;
    FObjects: TList;
    FControlList : TList;
    FActiveControl : TSDLControl;
    FControlParent: TSDLControl;
    FZ : Integer;
    FWidth : Integer;
    FHeight : Integer;
    procedure Add(AObject: TSDLObject);
    procedure AddControl(Control: TSDLControl);
    procedure Remove(AObject: TSDLObject);
    procedure Draw;
    procedure Animate(MoveCount: Integer);
    function GetObject(Index: Integer): TSDLImageObject;
    function GetCount: Integer;
    function GetControlCount: Integer;
    function GetControl(Index: Integer): TSDLControl;
  protected
    procedure SetHeight(const Value: Integer); virtual;
    procedure SetWidth(const Value: Integer); virtual;
    procedure SetZ(const Value: Integer); virtual;
  public
    constructor Create(AParent: TSDLObject); virtual;
    destructor Destroy; override;
    procedure Clear;
    function IndexOfObject(AObject: TSDLImageObject): Integer;
    function IndexOfControl(Control: TSDLControl): Integer;
    property Count: Integer read GetCount;
    property ControlCount: Integer read GetControlCount;
    property GUI: TSDLGui read FGui;
    property Objects[Index: Integer]: TSDLImageObject read GetObject;
    property Controls[Index: Integer]: TSDLControl read GetControl;
    property ActiveControl: TSDLControl read FActiveControl write FActiveControl;
    property Parent: TSDLObject read FParent;
    property ControlParent: TSDLControl read FControlParent;
  published
    property Z: Integer read FZ write SetZ;
    property Height: Integer read FHeight write SetHeight;
    property Width: Integer read FWidth write SetWidth;
  end;

  TSDLImageObject = class(TSDLObject)
  private
    FX : Integer;
    FY : Integer;
    FVisible : Boolean;
    FImage: TSDLImage;
    FDead : Boolean;
    FDrawOption: TDrawOption;
    FAnimLooped: Boolean;
    FAnimSpeed: Double;
    FAnimPos: Double;
    FAnimCount: Integer;
    FAnimStart: Integer;
    procedure Draw;
    procedure Animate(MoveCount: Integer);
    function GetWorldX: Integer;
    function GetWorldY: Integer;
  protected
    procedure SetX(const Value: Integer); virtual;
    procedure SetY(const Value: Integer); virtual;
    procedure SetVisible(const Value: Boolean); virtual;
    procedure SetImage(Value: TSDLImage); virtual;
    procedure DoDraw; virtual;
    procedure DoAnimate(MoveCount: Integer); virtual;
    function TestCollision: Boolean; virtual;
    function GetBoundsRect: TSDL_Rect; virtual;
  public
    constructor Create(AParent: TSDLObject); override;
    destructor Destroy; override;
    procedure Kill;
    property WorldX: Integer read GetWorldX;
    property WorldY: Integer read GetWorldY;
    property BoundsRect: TSDL_Rect read GetBoundsRect;
    property Image: TSDLImage read FImage write SetImage;
    property Dead: Boolean read FDead;
    //Maybe I could move these to published section
    property AnimCount: Integer read FAnimCount write FAnimCount;
    property AnimLooped: Boolean read FAnimLooped write FAnimLooped;
    property AnimPos: Double read FAnimPos write FAnimPos;
    property AnimSpeed: Double read FAnimSpeed write FAnimSpeed;
    property AnimStart: Integer read FAnimStart write FAnimStart;
  published
    property X: Integer read FX write SetX;
    property Y: Integer read FY write SetY;
    property Visible: Boolean read FVisible write SetVisible;
    property DrawOption: TDrawOption read FDrawOption write FDrawOption;
  end;

  TSDLComponent = class(TSDLImageObject)
  private
    FBorderWidth: Byte;
    FEnabled: Boolean;
    FFont: TSDLFont;
    FPopupMenu: TSDLComponent;
    FPushed : Boolean;
    FTag: Integer;
    FDescription: string;
    FHint: string;
    FCursor: TSDLCursor;
    FOnDblClick: TSDLMouseClickEvent;
    FOnClick: TSDLMouseClickEvent;
    FOnMouseDown: TSDLMouseEvent;
    FOnMouseUp: TSDLMouseEvent;
    FOnMouseMove: TSDLMouseMoveEvent;
    FOnMouseWheel: TSDLMouseWheelEvent;
    FOnDestroy: TSDLNotifyEvent;
    FDragMode: TSDLDragMode;
    FDesigning: Boolean;
    FDraging : Boolean;
    FOldPos : TPoint;
  protected
    procedure SetBorderWidth(const Value: Byte); virtual;
    procedure SetEnabled(const Value: Boolean); virtual;
    procedure SetFont(const Value: TSDLFont); virtual;
    procedure MouseMove(Modifier: TSDLMod;AX,AY : Word); virtual;
    procedure MouseUp(Button: Integer;Modifier: TSDLMod;AX,AY : Integer); virtual;
    procedure MouseDown(Button: Integer;Modifier: TSDLMod;AX,AY : Integer); virtual;
    procedure MouseWheel(Dir: Integer;Modifier: TSDLMod;AX,AY : Integer); virtual;
    procedure Click(AX,AY: Integer); virtual;
    procedure DblClick(AX,AY: Integer); virtual;
  public
    constructor Create(AParent: TSDLObject); override;
    procedure BeforeDestruction; override;
    procedure HidePopupMenu; virtual;
    procedure Popup(AX,AY: Integer); virtual; //only used by TPopupMenu
    property Pushed: Boolean read FPushed write FPushed;
    property Font: TSDLFont read FFont write SetFont;
    property Cursor : TSDLCursor read FCursor write FCursor;
    property Designing: Boolean read FDesigning write FDesigning;
    //Use TPopupMenu.Create(SomeComponent) to assign this value
    property PopupMenu: TSDLComponent read FPopupMenu write FPopupMenu;
  published
    property BorderWidth: Byte read FBorderWidth write SetBorderWidth;
    property Enabled: Boolean read FEnabled write SetEnabled;
    property Description: string read FDescription write FDescription;
    property Hint: string read FHint write FHint;
    property Tag : Integer read FTag write FTag;
    property OnDestroy: TSDLNotifyEvent read FOnDestroy write FOnDestroy;
    property OnMouseMove: TSDLMouseMoveEvent read FOnMouseMove write FOnMouseMove;
    property OnMouseUp: TSDLMouseEvent read FOnMouseUp write FOnMouseUp;
    property OnMouseDown: TSDLMouseEvent read FOnMouseDown write FOnMouseDown;
    property OnClick: TSDLMouseClickEvent read FOnClick write FOnClick;
    property OnDblClick: TSDLMouseClickEvent read FOnDblClick write FOnDblClick;
    property OnMouseWheel: TSDLMouseWheelEvent read FOnMouseWheel write FOnMouseWheel;
    property DragMode: TSDLDragMode read FDragMode write FDragMode;
  end;

  TSDLObjectClass = class of TSDLComponent;

  TSDLControl = class(TSDLComponent)
  private
    FTabOrder: Integer;
    FInFocus: Boolean;
    FOnKeyDown: TSDLKeyEvent;
    FOnKeyUp: TSDLKeyEvent;
    FOnKeyPress: TSDLKeyPressEvent;
    FOnExit: TSDLNotifyEvent;
    FOnEnter: TSDLNotifyEvent;
    procedure SetTabOrder(const Value: Integer);
  protected
    procedure SetEnabled(const Value: Boolean); override;
    procedure SetVisible(const Value: Boolean); override;
    procedure MouseDown(Button: Integer;Modifier: TSDLMod;AX,AY: Integer); override;
    procedure KeyUp(var Key: Word; Modifier: TSDLMod); virtual;
    procedure KeyDown(var Key: Word; Modifier: TSDLMod); virtual;
    procedure KeyPress(var Key: Char); virtual;
    function LoseFocus: Boolean; virtual;
  public
    constructor Create(AParent: TSDLObject); override;
    destructor Destroy; override;
    function SetFocus: Boolean; virtual;
    property HasFocus: Boolean read FInFocus;
  published
    property TabOrder: Integer read FTabOrder write SetTabOrder;
    property OnKeyUp: TSDLKeyEvent read FOnKeyUp write FOnKeyUp;
    property OnKeyDown: TSDLKeyEvent read FOnKeyDown write FOnKeyDown;
    property OnKeyPress: TSDLKeyPressEvent read FOnKeyPress write FOnKeyPress;
    property OnEnter: TSDLNotifyEvent read FOnEnter write FOnEnter;
    property OnExit: TSDLNotifyEvent read FOnExit write FOnExit;
  end;

  TSDLStdControl = class(TSDLControl)
  private
    FFocusColor: Cardinal;
    FColor: Cardinal;
    FDisabledColor: Cardinal;
    FAlwaysUp: Boolean;
    FAlwaysDown: Boolean;
    FStateImages: TStateImages;
    procedure SetAlwaysDown(const Value: Boolean);
    procedure SetAlwaysUp(const Value: Boolean);
  protected
    procedure DrawNormal;
    procedure DrawFocus;
    procedure DrawDown;
    procedure DrawDisabled;
    procedure DoDraw; override;
    procedure SetImage(Value: TSDLImage); override;
    procedure SetEnabled(const Value: Boolean); override;
    procedure MouseUp(Button: Integer;Modifier: TSDLMod;AX,AY : Integer); override;
    procedure MouseDown(Button: Integer;Modifier: TSDLMod;AX,AY : Integer); override;
    function LoseFocus: Boolean; override;
  public
    constructor Create(AParent: TSDLObject); override;
    function SetFocus: Boolean; override;
  published
    property Color: Cardinal read FColor write FColor;
    property FocusColor: Cardinal read FFocusColor write FFocusColor;
    property DisabledColor: Cardinal read FDisabledColor write FDisabledColor;
    property AlwaysDown: Boolean read FAlwaysDown write SetAlwaysDown;
    property AlwaysUp: Boolean read FAlwaysUp write SetAlwaysUp;
    property StateImages : TStateImages read FStateImages write FStateImages;
  end;

  TSDLMouse = class(TSDLImageObject)
  private
    FOldClickTime: Cardinal;
    FDownControl : TSDLComponent;
    FOverControl : TSDLComponent;
    FConstCursor : Boolean;
    FOldTime: Cardinal;
    FHotSpotY: Byte;
    FHotSpotX: Byte;
    FDefaultCursor: TSDLCursor;
    procedure MouseMove(Modifier: TSDLMod;AX,AY: Integer);
    procedure MouseDown(Button: Integer;Modifier: TSDLMod;AX,AY: Integer);
    procedure MouseUp(Button: Integer;Modifier: TSDLMod;AX,AY: Integer);
    procedure Click(AX,AY: Integer);
    procedure MouseWheel(Dir: Integer;Modifier: TSDLMod;AX,AY: Integer);
    function  GetControl: TSDLComponent;
    procedure SetConstCursor(const Value: Boolean);
  protected
    procedure SetImage(Value: TSDLImage); override;
    procedure DoAnimate(MoveCount: Integer); override;
  public
    procedure SetCursor(const Value: TSDLCursor);
    constructor Create(AParent: TSDLObject); override;
    destructor Destroy; override;
    property ConstCursor: Boolean read FConstCursor write SetConstCursor;
    property HotSpotX: Byte read FHotSpotX write FHotSpotX;
    property HotSpotY: Byte read FHotSpotY write FHotSpotY;
    property DefaultCursor: TSDLCursor read FDefaultCursor write FDefaultCursor;
    property DownControl: TSDLComponent read FDownControl;
    property OverControl: TSDLComponent read FOverControl;
  end;

  TSDLGui = class(TSDLObject)
  private
    FOldTime: Cardinal;
    FAllCount: Integer;
    FDeadList: TList;
    FSDLMouse : TSDLMouse;
    FPopupMenu: TSDLComponent;
    FHint: string;
    FHintCoord: TPoint;
    procedure KeyDown(var Key: Word; Modifier: TSDLMod);
  public
    HintColor: Cardinal;
    ShowHintAfter: Cardinal;
    HintFont: TSDLFont;
    constructor Create(AParent: TSDLObject); override;
    destructor Destroy; override;
    procedure Kill;
    procedure ProcessEvent(const Event: TSDL_Event);
    procedure Update;
    property AllCount: Integer read FAllCount;
    property SDLMouse: TSDLMouse read FSDLMouse;
    property PopupMenu: TSDLComponent read FPopupMenu write FPopupMenu;
  end;

const
  //Consts used to get mouse button out of Modifier in MouseMove
  KMOD_MBLEFT = 4;
  KMOD_MBMIDDLE = 8;
  KMOD_MBRIGHT = 16;

  //Consts used for State parameter of TSDLDrawItemEvent method
  //If you need anything else add it and inform me
  stSelected = 1;
  stFocused = 2;
  stChecked = 4;
  stDefault = 8;

implementation

uses SysUtils;

{ TSDLObject }

procedure TSDLObject.Add(AObject: TSDLObject);
var L, H, I, C: Integer;
begin
  if FObjects=nil then
    FObjects := TList.Create;

  L := 0;
  H := FObjects.Count - 1;
  while L <= H do
  begin
    I := (L + H) div 2;
    C := TSDLObject(FObjects[I]).Z-AObject.Z;
    if C < 0 then L := I + 1 else
      H := I - 1;
  end;
  FObjects.Insert(L, AObject);

  if AObject is TSDLControl then
    AddControl(TSDLControl(AObject));
end;

procedure TSDLObject.AddControl(Control: TSDLControl);
var L, H, I, C: Integer;
begin
  if FControlList=nil then
    FControlList := TList.Create;

  L := 0;
  H := FControlList.Count - 1;
  while L <= H do
  begin
    I := (L + H) div 2;
    C := TSDLControl(FControlList[I]).FTabOrder-Control.FTabOrder;
    if C < 0 then L := I + 1 else
      H := I - 1;
  end;
  FControlList.Insert(L, Control);
end;

procedure TSDLObject.Animate(MoveCount: Integer);
var i: Integer;
begin
  if FObjects<>nil then
    for i:=0 to FObjects.Count-1 do
      TSDLImageObject(FObjects[i]).Animate(MoveCount);
end;

procedure TSDLObject.Clear;
begin
  while Count>0 do
    Objects[0].Free;
end;

constructor TSDLObject.Create(AParent: TSDLObject);
begin
  FParent := AParent;
  if FParent<>nil then
  begin
    if FParent is TSDLImageObject then
      FZ := FParent.Count
    else FZ := MaxInt-10;

    FParent.Add(Self);

    if FParent is TSDLGui then
      FGui := TSDLGui(FParent)
    else
    begin
      FGui := FParent.FGui;
      if FParent is TSDLControl then
        FControlParent := TSDLControl(FParent)
      else
        FControlParent := FParent.FControlParent;
    end;
    Inc(FGui.FAllCount);
  end;
end;

destructor TSDLObject.Destroy;
begin
  Clear;
  if FParent<>nil then
  begin
    Dec(FGui.FAllCount);
    FParent.Remove(Self);
    FGui.FDeadList.Remove(Self);
  end;
  FObjects.Free;
  FControlList.Free;
  inherited;
end;

procedure TSDLObject.Draw;
var i: Integer;
begin
  if Assigned(FObjects) then
    for i:=0 to FObjects.Count-1 do
      TSDLImageObject(FObjects[i]).Draw;
end;

function TSDLObject.GetControl(Index: Integer): TSDLControl;
begin
  if FControlList<>nil then
    Result := FControlList[Index]
  else
    Result := nil;
end;

function TSDLObject.GetControlCount: Integer;
begin
   if FControlList<>nil then
    Result := FControlList.Count
  else
    Result := 0;
end;

function TSDLObject.GetCount: Integer;
begin
  if FObjects<>nil then
    Result := FObjects.Count
  else
    Result := 0;
end;

function TSDLObject.GetObject(Index: Integer): TSDLImageObject;
begin
  if FObjects<>nil then
    Result := FObjects[Index]
  else
    Result := nil;
end;

function TSDLObject.IndexOfControl(Control: TSDLControl): Integer;
begin
  Result := FControlList.IndexOf(Control);
end;

function TSDLObject.IndexOfObject(AObject: TSDLImageObject): Integer;
begin
  Result := FObjects.IndexOf(AObject);
end;

procedure TSDLObject.Remove(AObject: TSDLObject);
begin
  FObjects.Remove(AObject);
  if AObject is TSDLControl then
    FControlList.Remove(AObject);
  if FObjects.Count=0 then
  begin
    FObjects.Free;
    FObjects := nil;
    FControlList.Free;
    FControlList := nil;
  end;
end;

procedure TSDLObject.SetHeight(const Value: Integer);
begin
  FHeight := Value;
end;

procedure TSDLObject.SetWidth(const Value: Integer);
begin
  FWidth := Value;
end;

procedure TSDLObject.SetZ(const Value: Integer);
begin
  if FZ<>Value then
  begin
    FZ := Value;
    if FParent<>nil then
    begin
      FParent.Remove(Self);
      FParent.Add(Self);
    end;
  end;
end;

{ TSDLImageObject }

procedure TSDLImageObject.Animate(MoveCount: Integer);
begin
  DoAnimate(MoveCount);
  inherited;
end;

constructor TSDLImageObject.Create(AParent: TSDLObject);
begin
  inherited;
  FVisible := True;
  FDrawOption := doStretch;
end;

procedure TSDLImageObject.Kill;
begin
  if Assigned(FGui)and(not FDead) then
  begin
    FDead := True;
    FGui.FDeadList.Add(Self);
  end;
end;

destructor TSDLImageObject.Destroy;
begin
  with FGui.FSDLMouse do
  begin
    if FDownControl=Self then
      FDownControl := nil;
    if FOverControl=Self then
      FOverControl := nil;
  end;
  inherited;
end;

procedure TSDLImageObject.DoAnimate(MoveCount: Integer);
  function Mod2(i: Double; i2: Integer): Double;
  begin
    if i2=0 then
      Result := i
    else
    begin
      Result := i-Trunc(i/i2)*i2;
      if Result<0 then
        Result := i2+Result;
    end;
  end;
begin
  AnimPos := AnimPos + AnimSpeed*MoveCount;

  if AnimLooped then
  begin
    if AnimCount>0 then
      AnimPos := Mod2(AnimPos, AnimCount)
    else
      AnimPos := 0;
  end else
  begin
    if AnimPos>=AnimCount then
    begin
      AnimPos := AnimCount-1;
      AnimSpeed := 0;
    end;
    if AnimPos<0 then
    begin
      AnimPos := 0;
      AnimSpeed := 0;
    end;
  end;
end;

procedure TSDLImageObject.DoDraw;
var ImageIndex,px,py: Integer;
    r,s : TSDL_Rect;
begin
  if FImage=nil then Exit;

  r := BoundsRect;
  ImageIndex := AnimStart+Trunc(AnimPos);
  if FDrawOption<>doStretch then
  begin
    s := FImage.PatternRects[ImageIndex];
    if r.w<s.w then InflateRect(s,(r.w-s.w)div 2,0);
    if r.h<s.h then InflateRect(s,0,(r.h-s.h)div 2);
  end;

  case FDrawOption of
  doNormal: FImage.DrawRect(SDLScreen.Surface,r.x,r.y,s);
  doCenter:
    begin
      OffsetRect(r,(r.w-s.w)div 2,(r.h-s.h)div 2);
      FImage.DrawRect(SDLScreen.Surface,r.x,r.y,s);
    end;
  doTile:
    begin
      SDLScreen.SetClipRect(nil,@r);
      py := r.y;
      repeat
        px := r.x;
        repeat
          FImage.DrawRect(SDLScreen.Surface,px,py,s);
          Inc(px,s.w);
        until px>r.x+r.w;
        Inc(py,s.h);
      until py>r.y+r.h;
      SDLScreen.SetClipRect(nil,nil);
    end;
  else
    FImage.StretchDraw(SDLScreen.Surface,r,ImageIndex);
  end;
end;

procedure TSDLImageObject.Draw;
begin
  if FVisible then
  begin
    if (FGui<>nil)and OverlapRect(SDLScreen.SurfaceRect, BoundsRect) then
      DoDraw;
    inherited;
  end;
end;

function TSDLImageObject.GetBoundsRect: TSDL_Rect;
begin
  Result := SDLRect(WorldX, WorldY, Width, Height);
end;

function TSDLImageObject.GetWorldX: Integer;
begin
  if Assigned(FParent)and(FParent is TSDLImageObject) then
    Result := TSDLImageObject(FParent).WorldX+FX
  else
    Result := FX;
end;

function TSDLImageObject.GetWorldY: Integer;
begin
  if Assigned(FParent)and(FParent is TSDLImageObject) then
    Result := TSDLImageObject(FParent).WorldY+FY
  else
    Result := FY;
end;

procedure TSDLImageObject.SetImage(Value: TSDLImage);
begin
  if FImage=Value then Exit;

  FImage := Value;
  if FImage=nil then Exit;
  if Height<4 then Height := FImage.PatternHeight;
  if Width<4 then Width := FImage.PatternWidth;

  AnimCount := FImage.PatternCount;
  if AnimCount>1 then
    AnimSpeed := 3/1000;
  AnimPos := 0;
  AnimLooped := True;
end;

procedure TSDLImageObject.SetVisible(const Value: Boolean);
begin
  FVisible := Value;
end;

procedure TSDLImageObject.SetX(const Value: Integer);
begin
  FX := Value;
end;

procedure TSDLImageObject.SetY(const Value: Integer);
begin
  FY := Value;
end;

function TSDLImageObject.TestCollision: Boolean;
begin
  Result := True;
end;

{ TSDLComponent }

procedure TSDLComponent.BeforeDestruction;
begin
  inherited;
  if Gui.FHint=Hint then Gui.FHint := '';
  if Assigned(OnDestroy) then OnDestroy(Self);
end;

procedure TSDLComponent.Click(AX, AY: Integer);
begin
  if Assigned(FControlParent) and not FControlParent.FInFocus then Exit;

  if PointInRect(SDLPoint(AX,AY),BoundsRect) then
    if Assigned(OnClick) then OnClick(Self,AX,AY);
end;

constructor TSDLComponent.Create(AParent: TSDLObject);
begin
  inherited;
  FEnabled := True;
  FBorderWidth := 2;
  if Parent is TSDLComponent then
    Designing := TSDLComponent(Parent).Designing;
end;

procedure TSDLComponent.DblClick(AX, AY: Integer);
begin
  if Assigned(OnDblClick) then OnDblClick(Self,AX,AY);
end;

procedure TSDLComponent.HidePopupMenu;
var r: Boolean;
  procedure Check(It: TSDLImageObject);
  var i: Integer;
  begin
    for i := 0 to It.Count-1 do
    begin
      r := (FGui.FSDLMouse.FOverControl=It.Objects[i]);
      if r then Exit;
      if It.Objects[i].Count>0 then
        Check(It.Objects[i]);
    end;
  end;
begin
  if FGui.PopupMenu=nil then Exit;
  r := (FGui.FSDLMouse.FOverControl=FGui.PopupMenu);
  if not r then
    Check(FGui.PopupMenu);
  if not r then FGui.FPopupMenu := nil;
end;

procedure TSDLComponent.MouseDown(Button: Integer; Modifier: TSDLMod; AX,
  AY: Integer);
begin
  if Assigned(FControlParent) then
    with FControlParent do
      if not FInFocus then
        if not SetFocus then Exit;

  FPushed := Button=1;
  FOldPos := SDLPoint(AX,AY);
  FDraging := FPushed and((FDragMode=sdmAutomatic)or FDesigning);

  if Assigned(OnMouseDown) then OnMouseDown(Self,Button,Modifier,AX,AY);
end;

procedure TSDLComponent.MouseMove(Modifier: TSDLMod; AX, AY: Word);
begin
  if FDraging then
  begin
    X := X+(AX-FOldPos.X);
    Y := Y+(AY-FOldPos.Y);
    FOldPos.X := AX;
    FOldPos.Y := AY;
  end;
  if Assigned(OnMouseMove) then OnMouseMove(Self,Modifier,AX,AY);
end;

procedure TSDLComponent.MouseUp(Button: Integer; Modifier: TSDLMod; AX,
  AY: Integer);
begin
  FDraging := False;
  if Assigned(FControlParent) and not FControlParent.FInFocus then Exit;

  FPushed := False;

  if PointInRect(SDLPoint(AX,AY),BoundsRect) then
  begin
    if Assigned(OnMouseUp) then OnMouseUp(Self,Button,Modifier,AX,AY);
    if (Button=3)and Assigned(FPopupMenu) then FPopupMenu.Popup(AX,AY);
  end;
end;

procedure TSDLComponent.MouseWheel(Dir: Integer; Modifier: TSDLMod; AX,
  AY: Integer);
begin
  if Assigned(OnMouseWheel) then OnMouseWheel(Self,Dir,Modifier,AX,AY);
end;

procedure TSDLComponent.Popup(AX, AY: Integer);
begin
  // Only PopupMenu uses this but this controls needs to call it on
  // right click
end;

procedure TSDLComponent.SetBorderWidth(const Value: Byte);
begin
  FBorderWidth := Value;
end;

procedure TSDLComponent.SetEnabled(const Value: Boolean);
begin
  FEnabled := Value;
end;

procedure TSDLComponent.SetFont(const Value: TSDLFont);
begin
  FFont := Value;
end;

{ TSDLControl }

constructor TSDLControl.Create(AParent: TSDLObject);
begin
  FTabOrder := AParent.ControlCount;
  inherited;
end;

destructor TSDLControl.Destroy;
begin
  if FParent.FActiveControl=Self then
    FParent.FActiveControl:=nil;
  inherited;
end;

procedure TSDLControl.KeyDown(var Key: Word; Modifier: TSDLMod);
var Tab : Integer;
begin
  Tab := -1;
  if Assigned(FActiveControl) then
  begin
    FActiveControl.KeyDown(Key,Modifier);
    if Key=0 then Exit;
    if Assigned(FActiveControl) then
      Tab := FActiveControl.FTabOrder;
  end;

  if Assigned(OnKeyDown) then OnKeyDown(Self,Key,Modifier);

  if (ControlCount<2)or(KMOD_CTRL and Modifier>0) then Exit;

  repeat
  case Key of
  SDLK_TAB :
    if KMOD_Shift and Modifier>0 then
    begin
      Dec(Tab);
      if Tab<0 then Tab := ControlCount-1;
    end
    else
      Tab := (Tab+1) mod ControlCount;
  SDLK_RIGHT,SDLK_DOWN: Tab := (Tab+1)mod ControlCount;
  SDLK_LEFT,SDLK_UP   :
    begin
      Dec(Tab);
      if Tab<0 then Tab := ControlCount-1;
    end
  else
    Tab := -1;
  end;

  if Tab<0 then Break;
  until Controls[Tab].SetFocus;
  if Tab>=0 then
    Key := 0;
end;

procedure TSDLControl.KeyPress(var Key: Char);
begin
  if Assigned(FActiveControl) then
  begin
    FActiveControl.KeyPress(Key);
    if Key=#0 then Exit;
  end;
  if Assigned(OnKeyPress) then OnKeyPress(Self,Key);
end;

procedure TSDLControl.KeyUp(var Key: Word; Modifier: TSDLMod);
begin
  if Assigned(FActiveControl) then
  begin
    FActiveControl.KeyUp(Key,Modifier);
    if Key=0 then Exit;
  end;
  if Assigned(OnKeyUp) then OnKeyUp(Self,Key,Modifier);
end;

function TSDLControl.LoseFocus: Boolean;
begin
  Result := True;
  if not FInFocus then Exit;
  FInFocus := False;
  if Assigned(OnExit) then OnExit(Self);
  if Parent.ActiveControl=Self then Parent.ActiveControl := nil;
  if Assigned(FActiveControl) then
    FActiveControl.LoseFocus;
end;

procedure TSDLControl.MouseDown(Button: Integer; Modifier: TSDLMod; AX,
  AY: Integer);
begin
  if Assigned(FControlParent) and not FControlParent.FInFocus then
    if not FControlParent.SetFocus then Exit;

  if not FInFocus then SetFocus;
  inherited;
end;

procedure TSDLControl.SetEnabled(const Value: Boolean);
begin
  inherited;
  if not FEnabled then
  begin
    LoseFocus;
    with FGui.FSDLMouse do
    begin
      if FDownControl=Self then
        FDownControl := nil;
      if FOverControl=Self then
        FOverControl := nil;
    end;
  end;
end;

function TSDLControl.SetFocus: Boolean;
begin
  Result := False;
  if not(Visible and Enabled) then Exit;
  if Assigned(FControlParent) then
    with FControlParent do
    begin
      if not FInFocus then
      begin
        if not SetFocus then Exit;
      end
      else
        if Assigned(FActiveControl) then
          if not FActiveControl.LoseFocus then Exit;
      FActiveControl := Self;
    end
  else
    with FGUI do
    begin
      if Assigned(FActiveControl) then
        if not FActiveControl.LoseFocus then Exit;
      FActiveControl := Self;
    end;
  FInFocus := True;
  Result := True;
  if Assigned(OnEnter) then OnEnter(Self);
  if Assigned(FActiveControl) then
    FActiveControl.SetFocus
  else if ControlCount>0 then
    Controls[0].SetFocus;
end;

procedure TSDLControl.SetTabOrder(const Value: Integer);
begin
  FTabOrder := Value;
  FParent.Remove(Self);
  FParent.Add(Self);
end;

procedure TSDLControl.SetVisible(const Value: Boolean);
begin
  inherited;
  if not FVisible then
  begin
    LoseFocus;
    with FGui.FSDLMouse do
    begin
      if FDownControl=Self then
        FDownControl := nil;
      if FOverControl=Self then
        FOverControl := nil;
    end;
  end;
end;

{ TSDLStdControl }

constructor TSDLStdControl.Create(AParent: TSDLObject);
begin
  inherited;
  FFocusColor := $B4C3DC;
  FColor := LightColor(FFocusColor,-30);
  FDisabledColor := $B4B4B4;
end;

procedure TSDLStdControl.DoDraw;
var c: Cardinal;
begin
  inherited;

  if Image=nil then
  begin
    if HasFocus then c := FFocusColor
    else if not Enabled then c := FDisabledColor
    else c := FColor;
    SDLScreen.Draw3DControl(SDLScreen.Surface,BoundsRect,c,BorderWidth,
                          (Pushed or FAlwaysDown) and not FAlwaysUp);
  end;
end;

procedure TSDLStdControl.DrawDisabled;
begin
  if siDisabled in StateImages then
  begin
    FAnimStart := FAnimCount*3;
    if not(siFocused in StateImages) then
      Dec(FAnimStart,FAnimCount);
    if not(siDown in StateImages) then
      FAnimStart := FAnimCount;
  end
  else FAnimStart := 0;
end;

procedure TSDLStdControl.DrawDown;
begin
  if siDown in StateImages then
    if siFocused in StateImages then
      AnimStart := AnimCount shl 1
    else
      AnimStart := AnimCount
  else
    AnimStart := 0;
end;

procedure TSDLStdControl.DrawFocus;
begin
  if siFocused in StateImages then
    AnimStart := AnimCount
  else
    AnimStart := 0;
end;

procedure TSDLStdControl.DrawNormal;
begin
  AnimStart := 0;
end;

function TSDLStdControl.LoseFocus: Boolean;
begin
  Result := inherited LoseFocus;
  DrawNormal;
  FPushed := False;
end;

procedure TSDLStdControl.MouseDown(Button: Integer; Modifier: TSDLMod; AX,
  AY: Integer);
begin
  inherited;

  if FInFocus and (Button = 1) then
  begin
    DrawDown;
    FPushed := True;
  end;
end;

procedure TSDLStdControl.MouseUp(Button: Integer; Modifier: TSDLMod; AX,
  AY: Integer);
begin
  inherited;
  if FInFocus then
  begin
    DrawFocus;
    FPushed := False;
  end;
end;

procedure TSDLStdControl.SetAlwaysDown(const Value: Boolean);
begin
  if FAlwaysDown=Value then Exit;
  FAlwaysDown := Value;
  if Value then
    FAlwaysUp := False;
end;

procedure TSDLStdControl.SetAlwaysUp(const Value: Boolean);
begin
  if FAlwaysUp=Value then Exit;
  FAlwaysUp := Value;
  if Value then
    FAlwaysDown := False;
end;

procedure TSDLStdControl.SetEnabled(const Value: Boolean);
begin
  if FEnabled=Value then Exit;
  inherited;
  if FEnabled then DrawNormal
  else DrawDisabled;
  FPushed := False;
end;

function TSDLStdControl.SetFocus: Boolean;
begin
  Result := inherited SetFocus;
  if Result then DrawFocus;
end;

procedure TSDLStdControl.SetImage(Value: TSDLImage);
var RowCount: Byte;
begin
  inherited;

  if Height>0 then
  begin
    RowCount := Image.Height div Height;
    AnimCount := AnimCount div RowCount;
    case RowCount of
    1: StateImages := [siNone];
    2: StateImages := [siFocused];
    3: StateImages := [siFocused,siDown];
    else
      StateImages := [siFocused,siDown,siDisabled];
    end;
  end;
end;

{ TSDLMouse }

procedure TSDLMouse.Click(AX, AY: Integer);
var t: Cardinal;
    tc: TSDLComponent;
begin
  if Assigned(FDownControl) then
  begin
    tc := FDownControl;
    FDownControl := nil;
    tc.Click(AX,AY);
    t := SDL_GetTicks;
    if t-FOldClickTime<500 then
      tc.DblClick(AX,AY)
    else FOldClickTime := t;
  end;
end;

constructor TSDLMouse.Create(AParent: TSDLObject);
begin
  inherited;
  Z := MaxInt;
end;

destructor TSDLMouse.Destroy;
begin
  inherited;
  SDL_ShowCursor(SDL_ENABLE);
end;

procedure TSDLMouse.DoAnimate(MoveCount: Integer);
begin
  inherited;
  if Assigned(FOverControl) and
     (SDL_GetTicks-FOldTime>Gui.ShowHintAfter) then
  begin
    Gui.FHint := FOverControl.Hint;
    Gui.FHintCoord.x := FOverControl.WorldX;
    Gui.FHintCoord.y := FOverControl.WorldY+FOverControl.Height;
  end;
end;

function TSDLMouse.GetControl: TSDLComponent;
var HotSpot: TPoint;
    TheEnd : Boolean;
  procedure ControlCollision(Control: TSDLImageObject);
  var i : Integer;
  begin
    if not (Control is TSDLComponent) then Exit;
    with Control as TSDLComponent do
      if not(FEnabled and FVisible) then Exit;

    if PointInRect(HotSpot,Control.BoundsRect)then
    begin
      if Control.TestCollision then
      begin
        Result := TSDLComponent(Control);
        TheEnd := False;

        i := Result.Count;
        if i>0 then
          repeat
            Dec(i);
            ControlCollision(Result.Objects[i]);
          until TheEnd or(i<1);
      end;
      TheEnd := True;
    end;
  end;

var i: Integer;
begin
  Result := nil;
  HotSpot := SDLPoint(X+HotSpotX,Y+HotSpotY);
  TheEnd := False;

  i := Gui.Count;
  repeat
    Dec(i);
    ControlCollision(Gui.Objects[i]);
  until TheEnd or(i<1);
end;

procedure TSDLMouse.MouseDown(Button: Integer; Modifier: TSDLMod; AX,
  AY: Integer);
begin
  if Assigned(FDownControl) then Exit;
  if Assigned(FOverControl) then
  begin
    if Button=1 then FDownControl := FOverControl;
    FOverControl.MouseDown(Button,Modifier,AX,AY);
  end;
  if Assigned(Gui.FPopupMenu)then
    Gui.FPopupMenu.HidePopupMenu;
end;

procedure TSDLMouse.MouseMove(Modifier: TSDLMod; AX, AY: Integer);
var tc: TSDLComponent;
    t: Cardinal;
begin
  t := SDL_GetTicks;
  X := AX;
  Y := AY;

  if Assigned(FDownControl) then
  begin
    FDownControl.MouseMove(Modifier,AX,AY);
    Exit;
  end;

  if Visible then
    tc := GetControl
  else
    tc := nil;

  if FOverControl<>tc then
  begin
    if Assigned(FOverControl) then
      FOverControl.MouseMove(Modifier,AX,AY);

    Gui.FHint := '';
    if Assigned(Tc) then
    begin
      FOverControl := tc;
      FOldTime := t;             //Used for hints
      if not FConstCursor then
        SetCursor(tc.Cursor)
      else
        SetCursor(DefaultCursor);
    end
    else
    begin
      FOverControl := nil;
      SetCursor(DefaultCursor);
    end;
  end;

  if Assigned(FOverControl) then
    FOverControl.MouseMove(Modifier,AX,AY);
end;

procedure TSDLMouse.MouseUp(Button: Integer; Modifier: TSDLMod; AX,
  AY: Integer);
begin
  if Assigned(FDownControl) then
  begin
    if Button=1 then   //if right pressed while left is also pressed
      FDownControl.MouseUp(Button,Modifier,AX,AY);
  end
  else if Assigned(FOverControl) then
    FOverControl.MouseUp(Button,Modifier,AX,AY);
end;

procedure TSDLMouse.MouseWheel(Dir: Integer; Modifier: TSDLMod; AX,
  AY: Integer);
begin
  if Assigned(Gui.FPopupMenu) then
    Gui.FPopupMenu.MouseWheel(Dir,Modifier,AX,AY)
  else if Assigned(FOverControl) then
    FOverControl.MouseWheel(Dir,Modifier,AX,AY);
end;

procedure TSDLMouse.SetConstCursor(const Value: Boolean);
begin
  if FConstCursor=Value then Exit;

  FConstCursor := Value;
  if FConstCursor then
    SetCursor(DefaultCursor)
  else
    if Assigned(FOverControl)then
      SetCursor(FOverControl.Cursor);
end;

procedure TSDLMouse.SetCursor(const Value: TSDLCursor);
begin
  if not Assigned(Value.Image) then Exit;

  SetImage(Value.Image);
  HotSpotX := Value.HotSpotX;
  HotSpotY := Value.HotSpotY;
end;

procedure TSDLMouse.SetImage(Value: TSDLImage);
begin
  inherited;
  if not Assigned(DefaultCursor.Image)then
  begin
    FDefaultCursor.Image := Value;
    SDL_ShowCursor(SDL_DISABLE);
  end;
end;

{ TSDLGui }

constructor TSDLGui.Create(AParent: TSDLObject);
begin
  inherited;
  FDeadList := TList.Create;
  FSDLMouse := TSDLMouse.Create(Self);
  FOldTime := SDL_GetTicks;
  SDL_EnableUnicode(1);
  SDL_EnableKeyRepeat(SDL_DEFAULT_REPEAT_DELAY,SDL_DEFAULT_REPEAT_INTERVAL);
  HintColor := $FFFF80;
  ShowHintAfter := 500;
  Width := SDLScreen.SurfaceRect.w;
  Height := SDLScreen.SurfaceRect.h;
end;

procedure TSDLGui.Kill;
var i : Integer;
begin
  for i := 0 to FDeadList.Count-1 do
    TSDLObject(FDeadList.Items[0]).Free;
end;

destructor TSDLGui.Destroy;
begin
  inherited;
  FDeadList.Free;
  SDL_EnableUnicode(0);
  SDL_EnableKeyRepeat(0,0);
end;

procedure TSDLGui.KeyDown(var Key: Word; Modifier: TSDLMod);
var Tab : Integer;
begin
  Tab := -1;
  if Assigned(FActiveControl) then
  begin
    FActiveControl.KeyDown(Key,Modifier);
    if Key=0 then Exit;
    if Assigned(FActiveControl) then
      Tab := FActiveControl.FTabOrder;
  end;

  if ControlCount=0 then Exit;

  case Key of
  SDLK_TAB :
    if KMOD_Shift and Modifier>0 then
    begin
      Dec(Tab);
      if Tab<0 then Tab := ControlCount-1;
    end
    else
      Tab := (Tab+1) mod ControlCount;
  SDLK_RIGHT,SDLK_DOWN: Tab := (Tab+1)mod ControlCount;
  SDLK_LEFT,SDLK_UP   :
    begin
      Dec(Tab);
      if Tab<0 then Tab := ControlCount-1;
    end
  else
    Tab := -1;
  end;

  if Tab>=0 then
    Controls[Tab].SetFocus;
end;

procedure TSDLGui.ProcessEvent(const Event: TSDL_Event);
var Modifier: TSDLMod;
    Button: Cardinal;
    s : Word;
    c : Char;
begin
  if Event.type_ in [4,5,6] then //Mouse events
    Modifier := SDL_GetModState
  else
  begin
    Modifier := 0;
    if Event.key.keysym.sym>$FFFF then Exit;
    s := Event.key.keysym.sym;
  end;
  case Event.type_ of
  SDL_MOUSEMOTION:
    with Event.motion do
    begin
      if state>0 then
      begin
        s := state;
        if s=4 then s:=3;
        Button := 1 shl (s+1);
        Modifier := Modifier or Button;
      end;
      FSDLMouse.MouseMove(Modifier,x,y);
    end;
  SDL_MOUSEBUTTONDOWN:
    with Event.button, FSDLMouse do
      if button<4 then
        MouseDown(button,Modifier,x,y)
      else
        if button=5 then
          MouseWheel(1,Modifier,x,y)
        else MouseWheel(-1,Modifier,x,y);
  SDL_MOUSEBUTTONUP:
    with Event.button do
    begin
      FSDLMouse.MouseUp(button,Modifier,x,y);
      if button=1 then FSDLMouse.Click(x,y);
    end;
  SDL_KEYDOWN:
    with Event.key.keysym do
    begin
      KeyDown(s,modifier);
      c := Char(unicode and $FF);
      if Assigned(FActiveControl)and(unicode<$80)and
                         not(unicode in [0,9,27]) then  //Esc and Backspace
        FActiveControl.KeyPress(c);
    end;
  SDL_KEYUP:
    with Event.key.keysym do
      if Assigned(FActiveControl) then
        FActiveControl.KeyUp(s,Modifier);
  end;
end;

procedure TSDLGui.Update;
var nt: Cardinal;
    size: TSDL_Rect;
begin
  Draw;
  if Assigned(HintFont)and(FHint<>'') then
  begin
    size := HintFont.TextExtent(FHint);
    InflateRect(size,4,0);
    OffsetRect(size,FHintCoord.x,FHintCoord.y);
    if size.x+size.w>=SDLScreen.SurfaceRect.w then
      size.x := SDLScreen.SurfaceRect.w-size.w-1;
    if size.y+size.h>=SDLScreen.SurfaceRect.h then
      size.y := SDLScreen.SurfaceRect.h-size.h-1;
    SDLScreen.FillRect(SDLScreen.Surface,size,HintColor);
    SDLScreen.DrawFrame(SDLScreen.Surface,size,0);
    HintFont.TextOut(SDLScreen.Surface,size.x+2,size.y,FHint);
  end;
  nt := SDL_GetTicks;
  Animate(nt-FOldTime);
  FOldTime := nt;
  Kill;
end;

end.
