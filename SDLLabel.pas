unit SDLLabel;

{*********************************************************************
             SDLLabel -  30.04.2003.

Author:      Stojkovic Igor
Contact:     jimyiigor@ptt.yu  (please report any bugs)
Description: TSDLLabel.

You may freely use this source code for your own projects.
If you have any suggestions or you made some modifications
please inform me.
**********************************************************************}

interface

uses sdl, SDLGUI, SDLDraw, SDLFont;

type
  TSDLLabel = class(TSDLComponent)
  private
    FWrapManager: TWrapManager;
    FCaption: String;
    FWordWrap: Boolean;
    FAlignment: TSDLAlignment;
    FLayout: TSDLTextLayout;
    FAutoSize: Boolean;
    procedure SetCaption(const Value: String);
    procedure SetWordWrap(const Value: Boolean);
    procedure SetAutoSize(const Value: Boolean);
  protected
    procedure SetWidth(const Value: Integer); override;
    procedure SetBorderWidth(const Value: Byte); override;
    procedure DoDraw; override;
  public
    constructor Create(AParent: TSDLObject); override;
    destructor Destroy; override;
  published
    property WordWrap:Boolean read FWordWrap write SetWordWrap;
    property Caption: String read FCaption write SetCaption;
    property Alignment: TSDLAlignment read FAlignment write FAlignment;
    property Layout: TSDLTextLayout read FLayout write FLayout;
    property AutoSize: Boolean read FAutoSize write SetAutoSize;
  end;

implementation

{ TSDLLabel }

constructor TSDLLabel.Create(AParent: TSDLObject);
begin
  inherited;
  BorderWidth := 0;
  FCaption := 'DXLabel';
  Alignment := taCenter;
  Layout := tlCenter;
end;

destructor TSDLLabel.Destroy;
begin
  inherited;
  FWrapManager.Free;
end;

procedure TSDLLabel.DoDraw;
var txtRect: TSDL_Rect;
    ta: TSDLAlignment;
    tl: TSDLTextLayout;
begin
  inherited;

  if FCaption='' then Exit;

  txtRect := BoundsRect;
  InflateRect(txtRect,-BorderWidth,-BorderWidth);

  ta := Font.Alignment;
  tl := Font.Layout;
  Font.Alignment := FAlignment;
  Font.Layout := FLayout;
  if FWordWrap then
    FWrapManager.TextRect(SDLScreen.Surface,txtRect)
  else
    Font.TextRect(SDLScreen.Surface,txtRect,FCaption);
  Font.Alignment := ta;
  Font.Layout := tl;
end;

procedure TSDLLabel.SetAutoSize(const Value: Boolean);
var tr: TSDL_Rect;
begin
  if FAutoSize=Value then Exit;
  FAutoSize := Value;
  if not FAutoSize then Exit;
  tr := Font.TextExtent(FCaption);
  Height := tr.h;
  Width := tr.w;
end;

procedure TSDLLabel.SetBorderWidth(const Value: Byte);
begin
  inherited;
  if FWordWrap then
    FWrapManager.WrapWidth := Width-BorderWidth;
end;

procedure TSDLLabel.SetCaption(const Value: String);
var tr: TSDL_Rect;
begin
  if Value=FCaption then Exit;
  FCaption := Value;
  if FWordWrap then
    FWrapManager.WrapingText := FCaption
  else if FAutoSize then
  begin
    tr := Font.TextExtent(FCaption);
    Height := tr.h;
    Width := tr.w;
  end;
end;

procedure TSDLLabel.SetWidth(const Value: Integer);
begin
  inherited;
  if FWordWrap then
    FWrapManager.WrapWidth := Width-BorderWidth;
end;

procedure TSDLLabel.SetWordWrap(const Value: Boolean);
begin
  if FWordWrap=Value then Exit;

  FWordWrap := Value;
  if FWordWrap then
  begin
    FWrapManager := TWrapManager.Create(Font);
    FWrapManager.WrapWidth := Width-BorderWidth;
    FWrapManager.WrapingText := FCaption;
  end
  else
  begin
    FWrapManager.Free;
    FWrapManager := nil;
  end;
end;

end.
