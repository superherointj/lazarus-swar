unit sdlgameinterface;

interface

uses
  sdl,
  sdlwindow;

type
  TGameInterfaceClass = class of TGameInterface;

  TGameInterface = class( TObject )
  private
    FNextGameInterface : TGameInterfaceClass;
  protected
    Dragging : Boolean;
    Loaded : Boolean;
    procedure FreeSurfaces; virtual;
    procedure Render; virtual; abstract;
    procedure Close; virtual; 
    procedure MouseDown( Button : Integer; Shift: TSDLMod; MousePos : TPoint ); virtual;
    procedure MouseMove( Shift: TSDLMod; CurrentPos : TPoint; RelativePos : TPoint ); virtual;
    procedure MouseUp( Button : Integer; Shift: TSDLMod; MousePos : TPoint ); virtual;
    procedure MouseWheelScroll( WheelDelta : Integer; Shift: TSDLMod; MousePos : TPoint ); virtual;
    procedure KeyDown( var Key: TSDLKey; Shift: TSDLMod; unicode : UInt16 ); virtual;
  public
    MainWindow : TSDL2DWindow;
    procedure LoadSurfaces; virtual;
    function PointIsInRect( Point : TPoint; x, y, w, h : integer ) : Boolean;
    constructor Create( const aMainWindow : TSDL2DWindow );
    destructor Destroy; override;
    property NextGameInterface : TGameInterfaceClass read FNextGameInterface write FNextGameInterface;
  end;

implementation

{ TGameInterface }
procedure TGameInterface.Close;
begin
  FNextGameInterface := nil;
end;

constructor TGameInterface.Create( const aMainWindow : TSDL2DWindow );
begin
  inherited Create;
  MainWindow := aMainWindow;
  FNextGameInterface := TGameInterface;
  MainWindow.InputManager.Mouse.OnMouseDown := MouseDown;
  MainWindow.InputManager.Mouse.OnMouseMove := MouseMove;
  MainWindow.InputManager.Mouse.OnMouseUp := MouseUp;
  MainWindow.InputManager.Mouse.OnMouseWheel := MouseWheelScroll;
  MainWindow.InputManager.KeyBoard.OnKeyDown := KeyDown;
  MainWindow.OnRender := Render;
  MainWindow.OnClose := Close;
end;

destructor TGameInterface.Destroy;
begin
  if Loaded then
    FreeSurfaces;
  inherited;
end;

procedure TGameInterface.FreeSurfaces;
begin
  Loaded := False;
end;

procedure TGameInterface.KeyDown(var Key: TSDLKey; Shift: TSDLMod; unicode: UInt16);
begin

end;

procedure TGameInterface.LoadSurfaces;
begin
  Loaded := True;
end;

procedure TGameInterface.MouseDown(Button: Integer; Shift: TSDLMod; MousePos: TPoint);
begin
  Dragging := True;
end;

procedure TGameInterface.MouseMove(Shift: TSDLMod; CurrentPos, RelativePos: TPoint);
begin

end;

procedure TGameInterface.MouseUp(Button: Integer; Shift: TSDLMod; MousePos: TPoint);
begin
  Dragging := True;
end;

procedure TGameInterface.MouseWheelScroll(WheelDelta: Integer; Shift: TSDLMod; MousePos: TPoint);
begin

end;

function TGameInterface.PointIsInRect( Point : TPoint; x, y, w, h: integer ): Boolean;
begin
  if ( Point.x >= x )
  and ( Point.y >= y )
  and ( Point.x <= w )
  and ( Point.y <= h ) then
    result := true
  else
    result := false;
end;

end.
